import unittest

import os, random
random.seed(os.environ["PYTHONHASHSEED"])

from occam.jobs.manager import JobManager
from occam.jobs.records.job import JobRecord

from occam.workflows.manager import WorkflowManager
from occam.workflows.database import WorkflowDatabase
from occam.workflows.records.run import RunRecord

from occam.workflows.datatypes.node import Node
from occam.workflows.datatypes.workflow import Workflow

from occam.objects.write_manager import ObjectWriteManager

from tests.unit.objects.mock import ObjectManagerMock
from tests.unit.accounts.mock import AccountManagerMock

from tests.helper import multihash, uuid, random_email, initialize, record_matcher

from unittest.mock import patch, Mock, MagicMock, create_autospec, call, ANY

from callee import Matching


class TestWorkflowManager:
  class TestGetInitialNodes(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      # Set up the manager we are testing
      self.workflows = WorkflowManager()

      # Maintain the object ID for the workflow
      self.workflowId = multihash()

      # Maintain ids for other objects
      self.simulatorId = multihash()
      self.simulatorRevision = multihash("sha1")
      self.dataId = multihash()
      self.dataRevision = multihash("sha1")

      # Create some objects in our mock Object Store
      self.workflows.objects = ObjectManagerMock([
        (
          {
            "id": self.workflowId,
            "type": "workflow",
            "name": "Test Workflow",
            "file": "data.json",
          },
          {
            "data.json": """{
              "connections": [
                {
                  "name": "MySim",
                  "type": "simulator",
                  "id": "%s",
                  "revision": "%s",

                  "inputs": [],
                  "outputs": []
                },
                {
                  "name": "MyData",
                  "type": "data",
                  "id": "%s",
                  "revision": "%s",

                  "inputs": [],
                  "outputs": []
                }
              ]
            }""" % (self.simulatorId, self.simulatorRevision,
                    self.dataId,      self.dataRevision)
          },
        ),

        {
          "id": self.simulatorId,
          "revision": self.simulatorRevision,
          "type": "simulator",
          "name": "MySim",

          "run": {
            "command": "usr/bin/mysim"
          }
        },

        {
          "id": self.dataId,
          "revision": self.dataRevision,
          "type": "data",
          "name": "MyData"
        },
      ])

      # Establish the workflow object
      self.workflow = self.workflows.objects.retrieve(id = self.workflowId)

      # Mock out the datastore interfact to pull out those jobs
      self.workflows.datastore = Mock(WorkflowDatabase)

    @patch('occam.person.Person')
    def test_should_return_a_list(self, Person):
      # Set up the person that is running the workflow
      person = Person()

      workflow = self.workflows.createWorkflow(self.workflow)
      ret = self.workflows.getInitialNodes(workflow, person)

      # Determine that it did what it is supposed to do
      self.assertTrue(isinstance(ret, list))

    @patch('occam.person.Person')
    def test_should_return_a_tuple_with_a_Node_for_each_node(self, Person):
      # Set up the person that is running the workflow
      person = Person()

      workflow = self.workflows.createWorkflow(self.workflow)
      ret = self.workflows.getInitialNodes(workflow, person)

      # Only should report the running nodes (MySim)
      self.assertTrue(len(ret) == 1)

    @patch('occam.person.Person')
    def test_should_return_an_item_for_each_runnable_object(self, Person):
      # Set up the person that is running the workflow
      person = Person()

      workflow = self.workflows.createWorkflow(self.workflow)
      ret = self.workflows.getInitialNodes(workflow, person)

      # Determine that it did what it is supposed to do
      numberOfNodes = list(filter(lambda x:
                                    isinstance(x, tuple) and isinstance(x[0], Node) and isinstance(x[1], int),
                                  ret))

      # Should not have filtered anything out
      self.assertTrue(len(numberOfNodes) == len(ret))

  class TestGenerateNextTasks(unittest.TestCase):
    @classmethod
    def setUpClass(self):
      # Set up the manager we are testing
      self.workflows = WorkflowManager()

      # Maintain the object ID for the workflow
      self.objectId = multihash()

      # Create some objects
      self.workflows.objects = ObjectManagerMock([
        {
          "id": self.objectId,
          "file": "data.json",
          "_files": {
            "data.json": "{}"
          }
        }
      ])

      # Establish some pre-existing runs/jobs
      self.jobs = JobManager()

      # Mock out the datastore interfact to pull out those jobs
      self.workflows.datastore = Mock(WorkflowDatabase)

    @patch('occam.person.Person')
    def test_should_return_a_list(self, Person):
      # Set up the person that is running the workflow
      person = Person()

      # Determine that it did what it is supposed to do
      self.assertTrue(True)

  
  class TestCreateRun(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.workflows = WorkflowManager()
      self.ownerId = multihash()

      self.workflows.objects = ObjectManagerMock([
          {
            "id": self.ownerId,
            "type": "person",
            "name": "Test Owner",
          }
        ])

      self.workflows.database = create_autospec(self.workflows.database)      
      self.owner = self.workflows.objects.retrieve(id = self.ownerId)

      from occam.jobs.records.job import JobRecord
      self.job = Mock(JobRecord)

    @patch('occam.person.Person')
    def test_should_return_a_runrecord(self, Person):
      person = Person()
      workflow = create_autospec(Workflow)
      workflow.tag = "myTag"
      workflow.name = "testName"
      workflow.id = multihash()

      self.job.id = 100
      expected_object_tag = self.workflows.objects.objectTagFor(self.owner)

      ret = self.workflows.createRun(self.owner, workflow, person)

      self.assertTrue(isinstance(ret, RunRecord))
      self.assertEqual(ret.workflow_name, workflow.name)
      self.assertEqual(ret.workflow_tag, workflow.tag)
      self.assertEqual(ret.workflow_uid, workflow.id)
      self.assertEqual(ret.object_tag, expected_object_tag)
      self.assertEqual(ret.object_name, "Test Owner")

    @patch('occam.person.Person')
    def test_database_should_commit_runrecord_with_expected_values(self, Person):
      person = Person()
      workflow = create_autospec(Workflow)
      workflow.tag = "myTag"
      workflow.name = "testName"
      workflow.id = multihash()

      self.job.id = 100
      expected_object_tag = self.workflows.objects.objectTagFor(self.owner)

      ret = self.workflows.createRun(self.owner, workflow, person)

      self.workflows.database.commit.assert_called()
      self.workflows.database.update.assert_called()

      args, _ = self.workflows.database.update.call_args_list[0]
      updated_run_record = args[1]
      self.assertEqual(updated_run_record.workflow_name, workflow.name)
      self.assertEqual(updated_run_record.workflow_tag, workflow.tag)
      self.assertEqual(updated_run_record.workflow_uid, workflow.id)
      self.assertEqual(updated_run_record.object_tag, expected_object_tag)
      self.assertEqual(updated_run_record.object_name, "Test Owner")

  #TODO: Use autospec instead of Mocking return value
  class TestDataFor(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.workflows = WorkflowManager()
      self.workflowId = multihash()

      self.workflows.objects = ObjectManagerMock([
          {
            "id": self.workflowId,
            "type": "workflow",
            "name": "testName",
            "file": "myFile.json"
          }
        ])

      self.wfObject = self.workflows.objects.retrieve(id = self.workflowId)
      self.workflows.objects.retrieveJSONFrom = Mock(return_value = ["foo", "bar"])

    @patch('occam.person.Person')
    def test_should_return_workflow_data(self, Person):
      person = Person()
      ret = self.workflows.dataFor(self.wfObject, person)
      self.assertEqual(ret, ["foo", "bar"])

  class TestIsNodeWorkFlow(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.workflows = WorkflowManager()

    @patch('occam.person.Person')
    def test_should_return_true_for_workflow_node(self, Person):
      person = Person()
      node = create_autospec(Node)("nodestr", "indexstr", "workflowstr")
      node.isType.return_value = True
      ret = self.workflows.isNodeWorkflow(node, person)
      self.assertTrue(ret is True)

    @patch('occam.person.Person')
    def test_should_return_false_for_nonworkflow_node(self, Person):
      person = Person()
      node = create_autospec(Node)("nodestr", "indexstr", "workflowstr")
      node.isType.return_value = False
      ret = self.workflows.isNodeWorkflow(node, person)      
      self.assertTrue(ret is False)


  class TestIsNodeRunnable(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.workflows = WorkflowManager()
      self.noRunObjectId = multihash()
      self.runObjectId = multihash()
      self.runScriptObjectId = multihash()
      self.workflows.objects = ObjectManagerMock([
        (
          {
            "id": self.noRunObjectId,
            "type": "simulator",
          }    
        ),
        (
          {
            "id": self.runObjectId,
            "type": "script",
            "run": {
              "command": "usr/bin/myTest"
            }
          }    
        ),
        (
          {
            "id": self.runScriptObjectId,
            "type": "script",
            "run": {
              "script": "dosomething"
            }
          }    
        )
      ])  

    @patch('occam.person.Person')
    def test_should_return_false_if_no_connections_and_not_runnable(self, Person):
      person = Person()
      node = create_autospec(Node)
      node.id = self.noRunObjectId
      obj = self.workflows.objects.retrieve(id = node.id)
      node.info = self.workflows.objects.infoFor(obj)
      node.self.connections = []

      ret = self.workflows.isNodeRunnable(node, person)
      self.assertTrue(ret is False)

    @patch('occam.person.Person')
    def test_should_return_true_if_no_connections_and_runnable(self, Person):
      person = Person()
      node = create_autospec(Node)
      node.id = self.runObjectId
      obj = self.workflows.objects.retrieve(id = node.id)
      node.info = self.workflows.objects.infoFor(obj)
      node.self.connections = []

      ret = self.workflows.isNodeRunnable(node, person)
      self.assertTrue(ret is True)

    @patch('occam.person.Person')
    def test_should_return_false_if_has_connections_and_not_runnable(self, Person):
      person = Person()
      node = create_autospec(Node)
      node.id = self.noRunObjectId
      obj = self.workflows.objects.retrieve(id = node.id)
      node.info = self.workflows.objects.infoFor(obj)
      node.self.connections = [{
        "type": "simulator",
        "name": "firstConnection"
        }]
      
      ret = self.workflows.isNodeRunnable(node, person)
      self.assertTrue(ret is False)
    
    @patch('occam.person.Person')
    def test_should_return_true_if_has_connections_and_is_runnable_with_runscript(self, Person):
      person = Person()
      node = create_autospec(Node)
      node.id = self.runScriptObjectId
      obj = self.workflows.objects.retrieve(id = node.id)
      node.info = self.workflows.objects.infoFor(obj)
      node.self.connections = [{
        "type": "simulator",
        "name": "firstConnection"
        }]
      
      ret = self.workflows.isNodeRunnable(node, person)
      self.assertTrue(ret is True)


  #TODO: How can this be better tested?
  # Check what default values for non-existing records are
  # Since if there isn't a value (None or otherwise) for job_id, there will be
  # a key error
  class TestJobsForNode(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.workflows = WorkflowManager()
      self.workflows.database = create_autospec(self.workflows.database)
      self.runId = multihash()
    
    def test_should_return_none_list_for_nonexistent_runid(self):
      self.workflows.database.many.return_value = [{"job_id": None}]
      nodeIndex = 0
      ret = self.workflows.jobsForNode(self.runId, nodeIndex)
      self.assertEqual(ret, [None])

    def test_should_return_list_when_database_entries_are_found(self):
      self.workflows.database.many.return_value = [{"job_id": 100}]
      nodeIndex = 0
      ret = self.workflows.jobsForNode(self.runId, nodeIndex)
      self.assertEqual(ret, [100])


  class TestGenerateRunForNode(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.workflows = WorkflowManager()
    
    def placeholder_test(self):
      self.assertTrue(True)
  

  class TestPermuteRun(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.workflows = WorkflowManager()
    
    def placeholder_test(self):
      self.assertTrue(True)


  class TestTailNodes(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.workflows = WorkflowManager()
      self.oneNodeNoDanglingOutputId = multihash()
      self.oneNodeYesDanglingOutputId = multihash()
      self.noNodeId = multihash()
      self.workflows.objects = ObjectManagerMock([
        (
          {
            "id": self.oneNodeNoDanglingOutputId,
            "type": "workflow",
            "file": "data.json",
          },
          {
            "data.json": """{
              "connections": [
                {
                  "name": "MySim",
                  "type": "simulator",
                  "inputs": [],
                  "outputs": [
                    {
                      "name": "FirstOutput"
                      "type": "data"
                      "connections": [
                        {
                          "name": "FirstOutputFirstConnection"
                          "type": "script"
                        }
                      ]
                    }
                  ]
                }
              ]
            }"""
          },
        ),
        (
          {
            "id": self.noNodeId,
            "type": "workflow"
          }
        ),
        (
          {
            "id": self.oneNodeYesDanglingOutputId,
            "type": "workflow",
            "file": "dataTwo.json",
          },
          {
            "dataTwo.json": """{
              "connections": [
                {
                  "name": "MySimTwo",
                  "type": "simulator",
                  "inputs": [],
                  "outputs": [
                    {
                      "name": "FirstOutput",
                      "type": "script"
                    }
                  ]
                }
              ]
            }"""
          }
        )          
      ])

      self.oneNodeNoDanglingOutput = self.workflows.objects.retrieve(id = self.oneNodeNoDanglingOutputId)
      self.noNode = self.workflows.objects.retrieve(id = self.noNodeId)
      self.oneNodeYesDanglingOutput = self.workflows.objects.retrieve(id = self.oneNodeYesDanglingOutputId)

    @patch('occam.person.Person')
    def test_should_return_empty_list_for_no_dangling_output(self, Person):
      person = Person()
      ret = self.workflows.tailNodes(self.oneNodeNoDanglingOutput, person)
      self.assertEqual(ret, [])
    
    @patch('occam.person.Person')
    def test_should_return_empty_list_for_no_nodes(self, Person):
      person = Person()
      ret = self.workflows.tailNodes(self.noNode, person)
      self.assertEqual(ret, [])
    
    @patch('occam.person.Person')
    def test_should_return_list_of_nodes_when_workflow_has_dangling_nodes(self, Person):
      person = Person()
      ret = self.workflows.tailNodes(self.oneNodeYesDanglingOutput, person)
      self.assertTrue(isinstance(ret, list))
      self.assertTrue(isinstance(ret[0], Node))


  class TestQueue(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.workflows = WorkflowManager()
      self.objectId = multihash()
      self.workflows.objects, self.workflows.accounts = initialize([
          {
            "id": self.objectId,
            "type": "workflow",
          }
        ],
        [
          {
            "username": uuid(),
            "email": random_email()
          }
        ]
      )
      self.testObject = self.workflows.objects.retrieve(id = self.objectId)
      print(self.testObject.id)
      self.workflows.createJob = create_autospec(self.workflows.createJob)
      self.workflows.createRun = create_autospec(self.workflows.createRun)
      self.workflows.mapJobToRun = create_autospec(self.workflows.mapJobToRun)

    @patch('occam.person.Person')
    def test_should_return_a_runrecord(self, Person):
      person = Person()
      self.workflows.createRun.return_value = RunRecord()
      ret = self.workflows.queue(self.testObject, person)
      self.assertIsInstance(ret, RunRecord)
      self.workflows.mapJobToRun.assert_called()
 

  # Wraps WorkflowManager.datastore.retrieveRunJobFor
  class TestRunJobFor(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.workflows = WorkflowManager()
      self.workflows.datastore.retrieveRunJobFor = create_autospec(self.workflows.datastore.retrieveRunJobFor)
    
    def test_should_return_runjobrecord(self):
      from occam.workflows.records.run_job import RunJobRecord
      self.workflows.datastore.retrieveRunJobFor.return_value = RunJobRecord()
      ret = self.workflows.runJobFor(100)
      self.assertIsInstance(ret, RunJobRecord)
      

  class TestNodesIn(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.workflows = WorkflowManager()
      self.connWorkflowId = multihash()
      self.noConnWorkflowId = multihash()      
      self.simulatorId = multihash()
      self.workflows.objects = ObjectManagerMock([
        (
          {
            "id": self.connWorkflowId,
            "type": "workflow",
            "file": "data.json",
          },
          {
            "data.json": """{
              "connections": [
                {
                  "name": "MySim",
                  "type": "simulator",
                  "id": "%s"
                }
              ]
            }""" % (self.simulatorId)
          },
        ),
        (
          {
            "id": self.noConnWorkflowId,
            "type": "workflow"
          }
        )
      ])

      # Retrieving objects and creating Workflows from them
      # One with nodes
      connObject = self.workflows.objects.retrieve(id = self.connWorkflowId)
      self.connWf = self.workflows.createWorkflow(connObject)
      # One without nodes
      noConnObject = self.workflows.objects.retrieve(id = self.noConnWorkflowId)
      self.noConnWf = self.workflows.createWorkflow(noConnObject)

    @patch('occam.person.Person')
    def test_should_return_empty_list_when_workflow_has_no_nodes(self, Person):
      person = Person()
      ret = self.workflows.nodesIn(self.noConnWf, person)
      self.assertEqual(ret, [])
    
    @patch('occam.person.Person')
    def test_should_return_list_of_nodes_when_workflow_has_nodes(self, Person):
      person = Person()
      ret = self.workflows.nodesIn(self.connWf, person)
      self.assertTrue(len(ret) > 0)
      self.assertIsInstance(ret[0], Node)


  #TODO: Just autospec the Workflow that gets passed in instead of creating it
  # in setUp
  class TestNodesAt(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.workflows = WorkflowManager()
      self.workflowId = multihash()
      self.simulatorId = multihash()
      self.workflows.objects = ObjectManagerMock([
        (
          {
            "id": self.workflowId,
            "type": "workflow",
            "file": "data.json",
            "name": "Test Workflow"
          },
          {
            "data.json": """{
              "connections": [
                {
                  "name": "MySim",
                  "type": "simulator",
                  "id": "%s"
                }
              ]
            }""" % (self.simulatorId)
          },
        )
      ])
    
      workflowObject = self.workflows.objects.retrieve(id = self.workflowId)
      self.workflow = self.workflows.createWorkflow(workflowObject)

    @patch('occam.person.Person')
    def test_should_retrieve_existing_node(self, Person):
      person = Person()
      nodeIndex = 0
      ret = self.workflows.nodeAt(self.workflow, nodeIndex, person)
      self.assertTrue(isinstance(ret, Node))
      self.assertEqual(ret.name, "MySim")
    
    #TODO: I'm considering the IndexError desired behavior. Follow up
    @patch('occam.person.Person')
    def test_nodeindex_out_of_range(self, Person):
      person = Person()
      nodeIndex = 5
      with self.assertRaises(IndexError):
        self.workflows.nodeAt(self.workflow, nodeIndex, person)


  #TODO: Test with passed inputs
  class TestLaunch(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.workflows = WorkflowManager()
      self.workflowId = multihash()
      self.workflowRevision = multihash()
      self.simulatorId = multihash()
      self.simulatorRevision = multihash()

      self.workflows.objects = ObjectManagerMock([
        (
          {
            "id": self.workflowId,
            "type": "workflow",
            "file": "data.json",
            "name": "Test Workflow",
            "revision": self.workflowRevision
          },
          {
            "data.json": """{
              "connections": [
                {
                  "name": "MySim",
                  "type": "simulator",
                  "id": "%s",
                  "revision": "%s",
                  "inputs": [],
                  "outputs": []
                }
              ]
            }""" % (self.simulatorId, self.simulatorRevision)
          },
        ),
        (
          {
            "name": "MySim",
            "type": "simulator",
            "id": self.simulatorId,
            "revision": self.simulatorRevision,
            "run": {
              "command": "usr/bin/mysim"
            }
          }
        )
      ])

      self.workflows.generateNextTasks = create_autospec(self.workflows.generateNextTasks)

    def test_generatenexttasks_called_with_correct_kwargs(self):
      testObject = self.workflows.objects.retrieve(id = self.workflowId)
      runRecord = create_autospec(RunRecord)
      
      self.workflows.launch(testObject, runRecord)
      self.workflows.generateNextTasks.assert_called()
      _, callKwargs = self.workflows.generateNextTasks.call_args_list[0]
      self.assertEqual(callKwargs['run'], runRecord)
      # Id of first elem in tuple, in first elem in list of nodes
      self.assertEqual(callKwargs['nodes'][0][0].id, self.simulatorId)
  

  class TestCompleteJob(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.workflows = WorkflowManager()
      self.workflowId = multihash()
      self.workflowRevision = multihash()
      self.simulatorId = multihash()
      self.simulatorRevision = multihash()

      self.workflows.objects = ObjectManagerMock([
        (
          {
            "id": self.workflowId,
            "type": "workflow",
            "file": "data.json",
            "name": "Test Workflow",
            "revision": self.workflowRevision
          },
          {
            "data.json": """{
              "connections": [
                {
                  "name": "MySim",
                  "type": "simulator",
                  "id": "%s",
                  "revision": "%s",
                  "inputs": [],
                  "outputs": []
                }
              ]
            }""" % (self.simulatorId, self.simulatorRevision)
          },
        ),
        (
          {
            "name": "MySim",
            "type": "simulator",
            "id": self.simulatorId,
            "revision": self.simulatorRevision,
            "run": {
              "command": "usr/bin/mysim"
            }
          }
        )
      ])

      self.workflows.runFor = create_autospec(self.workflows.runFor)
      self.workflows.nextNodes = create_autospec(self.workflows.nextNodes)
      self.workflows.generateNextTasks = create_autospec(self.workflows.generateNextTasks)
    
    def test_generatenexttasks_called_with_correct_kwargs(self):
      from occam.jobs.records.job import JobRecord
      jobRecord = create_autospec(JobRecord)
      jobRecord.id = 100
      node = create_autospec(Node)
      runRecord = create_autospec(RunRecord)
      runRecord.id = 100
      runRecord.workflow_tag = self.workflowId

      self.workflows.nextNodes.return_value = [node]
      self.workflows.runFor.return_value = runRecord

      self.workflows.completeJob(jobRecord)
      self.workflows.generateNextTasks.assert_called()
      _, callKwargs = self.workflows.generateNextTasks.call_args_list[0]
      self.assertEqual(callKwargs['nodes'][0], node)
      self.assertEqual(callKwargs['run'], runRecord)
  

  class TestFail(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.workflows = WorkflowManager()
      self.workflows.database = create_autospec(self.workflows.database)
      self.workflows.removeRunFolder = create_autospec(self.workflows.removeRunFolder)
      self.workflows.accounts = create_autospec(self.workflows.accounts)
    
    def test_should_make_calls_to_database(self):
      run = create_autospec(RunRecord)
      run.id = multihash()
      self.workflows.fail(run, None)
      self.workflows.database.update.assert_called()
      self.workflows.database.commit.assert_called()


  class TestWorkflowForRun(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.workflows = WorkflowManager()
      self.workflowId = multihash()
      self.revision = multihash()
      self.link = "myLink"
      self.workflows.objects = ObjectManagerMock([
        (
          {
            "id": self.workflowId,
            "revision": self.revision,
            "link": self.link,
            "name": "myObject"
          }
        )
      ])

      self.workflows.retrieveRun = create_autospec(self.workflows.retrieveRun)

    @patch('occam.person.Person')
    def test_should_return_object_with_given_identifiers(self, Person):
      person = Person()
      runId = multihash()

      run = create_autospec(RunRecord)
      run.workflow_uid = self.workflowId
      run.workflow_revision = self.revision
      run.workflow_link = self.link
      self.workflows.retrieveRun.return_value = run
      testObject = self.workflows.objects.retrieve(id = self.workflowId)

      ret = self.workflows.workflowForRun(runId, person)
      self.assertEqual(ret, testObject)      

    # Assumes retrieveRun returns None if runId not present. I believe this is
    # true. (See database manager and sqlite3 plugin. fetchone() should return
    #  None). Is this error desired?
    @patch('occam.person.Person')
    def test_should_raise_attributeerror_when_no_record_found(self, Person):
      person = Person()
      runId = multihash()

      run = None
      self.workflows.retrieveRun.return_value = run
      with self.assertRaises(AttributeError):
        self.workflows.workflowForRun(runId, person)


  class TestObjectForRun(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.workflows = WorkflowManager()
      self.objectId = multihash()
      self.workflows.objects = ObjectManagerMock([
          {
            "id": self.objectId,
            "name": "myObject",
            "type": "script"
          }
        ])
      
      self.workflows.retrieveRun = create_autospec(self.workflows.retrieveRun)
      self.testObject = self.workflows.objects.retrieve(id = self.objectId)

    #TODO: Fix here. objects.resolve is giving attribute error
    @patch('occam.person.Person')
    def test_should_return_object_with_given_identifiers(self, Person):
      person = Person()
      runId = multihash()
      expected_object_tag = self.workflows.objects.objectTagFor(self.testObject)

      run = create_autospec(RunRecord)
      run.object_tag = expected_object_tag

      ret = self.workflows.objectForRun(run, person)
      self.assertEqual(ret, self.testObject) 
  
    # Make sure retrieveRun can actually return None
    @patch('occam.person.Person')
    def test_should_raise_attributeerror_when_no_record_found(self, Person):
      person = Person()
      runId = multihash()

      run = None
      self.workflows.retrieveRun.return_value = run
      with self.assertRaises(AttributeError):
        self.workflows.objectForRun(runId, person)


  class TestRunsForObject(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.workflows = WorkflowManager()
      self.workflows.database = create_autospec(self.workflows.database)
      self.job_id, self.status, self.kind = 100, "finished", "build"
      self.objectId = multihash()

      self.runRowInformation = [{
        "node_index": 0,
        "job_id": self.job_id,
        "status": self.status,
        "kind": self.kind,
      }]
      self.workflows.objects = ObjectManagerMock([
        (
          {
            "id": self.objectId,
            "name": "myObject"
          }
        )
      ])

      self.testObject = self.workflows.objects.retrieve(id = self.objectId)

    def test_should_return_list_of_runrecords(self):
      self.workflows.database.many.return_value = self.runRowInformation
      ret = self.workflows.runsForObject(self.testObject)
      self.assertTrue(isinstance(ret, list))
      self.assertTrue(isinstance(ret[0], RunRecord))
      self.assertEqual(ret[0].job_id, self.job_id)


  class TestMapJobToRun(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.workflows = WorkflowManager()
      self.runId = random.randint(0, 1000)
      self.jobId = random.randint(0, 1000)
      self.nodeIndex = random.randint(0, 1000)
      self.workflows.database = create_autospec(self.workflows.database)
      self.workflows.datastore.retrieveRunFor = MagicMock(spec=self.workflows.datastore.retrieveRunFor,
                                                          side_effect=lambda x:
          RunRecord({'job_id': self.jobId}) if x == self.jobId else None)
    
    def test_should_commit_with_expected_values(self):
      from occam.workflows.records.run_job import RunJobRecord

      ret = self.workflows.mapJobToRun(self.runId, self.jobId, self.nodeIndex)

      self.workflows.database.update.assert_any_call(ANY, Matching(record_matcher(RunJobRecord, 'run_id', self.runId)))
      self.workflows.database.update.assert_any_call(ANY, Matching(record_matcher(RunJobRecord, 'job_id', self.jobId)))
      self.workflows.database.update.assert_any_call(ANY, Matching(record_matcher(RunJobRecord, 'node_index', self.nodeIndex)))

      self.workflows.database.commit.assert_called()

    def test_should_return_a_RunJobRecord(self):
      from occam.workflows.records.run_job import RunJobRecord

      ret = self.workflows.mapJobToRun(self.runId, self.jobId, self.nodeIndex)
      self.assertIsInstance(ret, RunJobRecord)


  #TODO: RunIds are ints, change runId types above
  # (though in cases above it doesn't affect the tests)
  # Find way to add test for what query is/what execute args are?
  class TestRetrieveRun(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.workflows = WorkflowManager()
      self.workflows.database = create_autospec(self.workflows.database)

    def test_should_return_runrecord_when_record_exists(self):
      runId = 100
      self.workflows.database.fetch.return_value = {"object_name": "testObject"}
      ret = self.workflows.retrieveRun(runId)
      self.assertTrue(isinstance(ret, RunRecord))
      self.assertEqual(ret.object_name, "testObject")
    
    # See note above. What will fetch actually return?
    # If it always returns something, then there will be an attribute error
    # when trying to create the RunRecord object. (I think None see RetrieveRunFor)
    def test_should_return_none_when_no_record_found(self):
      runId = 100
      self.workflows.database.fetch.return_value = None

      ret = self.workflows.retrieveRun(runId)
      self.assertTrue(ret is None)

  
  # Wrapper for WorkflowManager.database.retrieveRunFor
  class TestRunFor(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.workflows = WorkflowManager()
      self.jobId = random.randint(0, 1000)
      self.workflows.database = create_autospec(self.workflows.database)
      self.workflows.datastore.retrieveRunFor = MagicMock(spec=self.workflows.datastore.retrieveRunFor,
                                                          side_effect=lambda x:
          RunRecord({'job_id': self.jobId}) if x == self.jobId else None)

    def test_should_return_the_RunRecord_associated_with_the_given_job(self):
      ret = self.workflows.runFor(self.jobId)
      self.assertIsInstance(ret, RunRecord)
      self.assertEqual(ret.job_id, self.jobId)

    def test_should_return_None_when_the_given_job_is_not_known(self):
      ret = self.workflows.runFor(self.jobId + 1)
      self.assertIsNone(ret)

  
  class TestJobsFor(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.runId = random.randint(0, 1000)
      self.jobId = random.randint(0, 1000)
      self.jobId0 = random.randint(0, 1000)
      self.jobId1 = random.randint(0, 1000)

      self.workflows = WorkflowManager()

      # Stub out a set of jobs
      self.workflows.jobs = create_autospec(self.workflows.jobs)
      self.workflows.jobs.jobFromId = MagicMock(spec=self.workflows.jobs.jobFromId,
                                                side_effect=lambda x:
          JobRecord({'job_id': x}) if x in [self.jobId, self.jobId0, self.jobId1] else None)

      # Place a single node structure in at the given run id
      self.workflows.nodesFor = MagicMock(spec=self.workflows.nodesFor,
                                          side_effect=lambda x: {
                                            "nodes": {
                                              "nodeIndex0": {
                                                "jobs": [
                                                  {
                                                    "id": self.jobId0
                                                  }
                                                ]
                                              },
                                              "nodeIndex1": {
                                                "jobs": [
                                                  {
                                                    "id": self.jobId1
                                                  }
                                                ]
                                              }
                                            }} if x == self.runId else {"nodes": {}})
    
    def test_should_return_list_of_JobRecords(self):
      ret = self.workflows.jobsFor(self.runId)
      self.assertIsInstance(ret, list)
      self.assertIsInstance(ret[0], JobRecord)
      self.assertIsInstance(ret[1], JobRecord)

    def test_should_return_empty_list_if_no_jobs_found(self):
      ret = self.workflows.jobsFor(self.runId + 1)
      self.assertEqual(ret, [])


  # Wrapper for WorkflowManager.datastore.retrieveJobsFor
  #TODO: Follow up on retrieveJobsFor
  class TestJobsForRun(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.workflows = WorkflowManager()
      self.workflows.datastore.retrieveJobsFor = create_autospec(self.workflows.datastore.retrieveJobsFor)

    def test_should_call_retrieverunfor_and_return_runrecord(self):
      from occam.workflows.records.run_job import RunJobRecord
      self.workflows.datastore.retrieveJobsFor.return_value = RunJobRecord()
      run_id = 100
      ret = self.workflows.jobsForRun(run_id)
      self.workflows.datastore.retrieveJobsFor.assert_called()
      self.assertTrue(isinstance(ret, RunJobRecord))


  class TestNodesFor(unittest.TestCase):
    @classmethod
    def setUp(self):
      import datetime
      self.workflows = WorkflowManager()
      self.workflows.database = create_autospec(self.workflows.database)
      self.job_id_0, self.status, self.kind = 100, "finished", "build"
      self.start_time = datetime.datetime(2020, 9, 1, 9, 30)
      self.queue_time = datetime.datetime(2020, 9, 1, 9)
      self.job_id_1 = 200

      self.runRowInformation = [{
        "node_index": 0,
        "job_id": self.job_id_0,
        "status": self.status,
        "kind": self.kind,
        "start_time": self.start_time,
        "queue_time": self.queue_time,
        "finish_time": None
      },
      {
        "node_index": 1,
        "job_id": self.job_id_1,
        "status": self.status,
        "kind": self.kind,
        "start_time": self.start_time,
        "queue_time": self.queue_time,
        "finish_time": None
      }]

    def test_should_return_dict_of_dict_of_run_information(self):
      self.workflows.database.many.return_value = self.runRowInformation
      runId = 100
      ret = self.workflows.nodesFor(runId)
      self.assertTrue(isinstance(ret, dict))
      self.assertEqual(ret["nodes"][0]['jobs'][0]['id'], self.job_id_0)
      self.assertEqual(ret["nodes"][1]['jobs'][0]['id'], self.job_id_1)

    #TODO: How to test query properly picks up kwargs
#    def test_should_apply_kwargs_to_query(self):
#      self.workflows.database.many.return_value = self.runRowInformation
#      runId = 100
#      ret = self.workflows.nodesFor(runId)
#      queryArgs = self.workflows.database.execute.call_args[1]
#      self.assertTrue(True)


  class TestIsInputSourceSelf(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.workflows = WorkflowManager()
    
    def test_should_return_false_when_pin_elem_non_negative(self):
      node = create_autospec(Node)
      node.followInputWire.return_value = [(0, 0), (0, -1)]
      index = 0
      ret = self.workflows.isInputSourceSelf("workflow", node, "wireIndex", index)
      self.assertTrue(ret is False)
    
    def test_should_return_true_when_pin_elem_negative(self):
      node = create_autospec(Node)
      node.followInputWire.return_value = [(0, -1), (0, 0)]
      index = 0
      ret = self.workflows.isInputSourceSelf("workflow", node, "wireIndex", index)
      self.assertTrue(ret is True)

    def test_pin_index_out_of_range(self):
      node = create_autospec(Node)
      node.followInputWire.return_value = [(0, 0), (0, 0)]
      index = 5
      with self.assertRaises(IndexError):
        self.workflows.isInputSourceSelf("workflow", node, "wireIndex", index)


  class TestResolveInputSource(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.workflows = WorkflowManager()
    
    def test_should_return_node_at_zeroth_index(self):
      node = create_autospec(Node)
      node.followInputWire.return_value = [(0, 1)]
      workflow = create_autospec(Workflow)
      # Placeholder string should be fine for test purposes
      workflow.nodes = ["nodeOne", "nodeTwo"]
      wireIndex = 0
      index = 0

      ret = self.workflows.resolveInputSource(workflow, node, wireIndex, index)
      self.assertEqual(ret, "nodeOne")
    
    def test_pin_index_out_of_range(self):
      node = create_autospec(Node)
      node.followInputWire.return_value = [(0, 1)]
      workflow = create_autospec(Workflow)
      # Placeholder string should be fine for test purposes
      workflow.nodes = ["nodeOne", "nodeTwo"]
      wireIndex = 0
      index = 5
      with self.assertRaises(IndexError):
        self.workflows.resolveInputSource(workflow, node, wireIndex, index)

  class TestGenerateDefaultConfigurations(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.workflows = WorkflowManager()
      self.simulatorId = multihash()
      self.simulatorRevision = multihash()
      self.workflows.objects = ObjectManagerMock([
          {
              "id": self.simulatorId,
              "type": "simulator",
              "name": "mySim",
              "revision": self.simulatorRevision,
              "inputs": [
                {
                  "type": "text",
                  "name": "inputText"
                },
                {
                  "type": "script",
                  "name": "inputScript"
                }
              ]
          }
      ])

      #TODO: Fix here.
      self.workflows.objects.write = create_autospec(ObjectWriteManager)

    def placeholder(self):
      node = create_autospec(Node)
      node.id = self.simulatorId
      node.revision = self.simulatorRevision
      wireIndex = 0

      #ret = self.workflows.generateDefaultConfigurations(node, wireIndex)

      #self.assertIsInstance(ret, list)


  class TestGenerateConfigurations(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.workflows = WorkflowManager()
    
    def test_placeholder(self):
      self.assertTrue(True)


#TODO: More tests here
  class TestNextNodes(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.workflows = WorkflowManager()
      self.workflows.runFor = create_autospec(self.workflows.runFor)
      self.workflows.objects.resolve = create_autospec(self.workflows.objects.resolve)
      self.workflows.objects.parseObjectTag = create_autospec(self.workflows.objects.parseObjectTag)
      self.workflowId = multihash()
      self.simulatorId = multihash()
      self.workflows.objects = ObjectManagerMock([
        (
          {
              "id": self.workflowId,
              "type": "workflow",
              "name": "myWorkflow",
              "file": "data.json"
          },
          {
            "data.json": """{
              "connections": [
                {
                  "name": "mySim",
                  "type": "simulator",
                  "id": "%s",
                  outputs: [
                    {
                      "name": myText,
                      "type": text
                    }
                  ]
                }
              ]
            }""" % (self.simulatorId)
          },
        ),
        (
          {
            "id": self.simulatorId,
            "type": "simulator",
            "name": "mySim"
          }
        )
      ])

    @patch.object(WorkflowManager, "Log", Mock())
    def test_unresolved_object_throws_valueerror(self):
      from occam.jobs.records.job import JobRecord
      jobRecord = create_autospec(JobRecord)
      jobRecord.id = 100
      runRecord = create_autospec(RunRecord)
      runRecord.workflow_uid = multihash()
      runRecord.workflow_revision = multihash()
      runRecord.workflow_tag = multihash()
      self.workflows.runFor.return_value = runRecord
      self.workflows.objects.resolve.return_value = None

      with self.assertRaises(ValueError):
        self.workflows.nextNodes(jobRecord)
      WorkflowManager.Log.assert_has_calls([call.error(
        "Could not find object %s@%s." % (runRecord.workflow_uid,
          runRecord.workflow_revision))])
  
    def test_should_return_list_of_nodes_to_run(self):
      self.assertTrue(True)


  # Wraps WorkflowManager.jobs.create
  class TestCreateJob(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.workflows = WorkflowManager()
      self.workflows.jobs = create_autospec(self.workflows.jobs)
      self.workflowId = multihash()
      self.workflows.objects = ObjectManagerMock([
        (
          {
            "id": self.workflowId,
            "name": "myWorkflow",
            "type": "workflow"
          }
        )
      ])
      self.testObject = self.workflows.objects.retrieve(id = self.workflowId)

    def test_should_call_create_in_JobManager_and_return_resultant_JobRecord(self):
      from occam.jobs.records.job import JobRecord

      ret = self.workflows.createJob(self.testObject, existingPath = "some/path",
                                                      initialize = "something")

      args, kwargs = self.workflows.jobs.create.call_args_list[0]
      self.assertEqual(kwargs['existingPath'], "some/path")
      self.assertEqual(kwargs['initialize'], "something")


  class TestRunExecutedByJob(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.workflows = WorkflowManager()
      self.workflows.runJobFor = create_autospec(self.workflows.runJobFor)
      self.workflows.retrieveRun = create_autospec(self.workflows.retrieveRun)

    @patch.object(WorkflowManager, "Log", Mock())
    def test_should_return_none_if_runjob_not_found(self):
      job_id = 100
      self.workflows.runJobFor.return_value = None

      ret = self.workflows._runExecutedByJob(job_id)
      self.assertTrue(ret is None)
      WorkflowManager.Log.assert_has_calls([call.warning(
        "Job %s is not running a workflow." % (job_id))])

    @patch.object(WorkflowManager, "Log", Mock())
    def test_should_return_none_if_run_not_found(self):
      from occam.workflows.records.run_job import RunJobRecord
      job_id = 100
      runJobRecord = create_autospec(RunJobRecord)
      runJobRecord.run_id = 100
      self.workflows.runJobFor.return_value = runJobRecord
      self.workflows.retrieveRun.return_value = None

      ret = self.workflows._runExecutedByJob(job_id)
      self.assertTrue(ret is None)
      WorkflowManager.Log.assert_has_calls([call.error(
        "Could not find run %s." % (runJobRecord.run_id))])

    def test_should_return_runrecord(self):
      runRecord = create_autospec(RunRecord)
      self.workflows.retrieveRun.return_value = runRecord
      self.assertTrue(isinstance(self.workflows._runExecutedByJob(100), RunRecord))



  #TODO: Fix Mock below
  class TestTerminate(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.workflows = WorkflowManager()
      self.workflows.jobs = create_autospec(self.workflows.jobs)
      self.workflows.runFor = create_autospec(self.workflows.runFor)
      self.workflows.nodesFor = create_autospec(self.workflows.nodesFor)

    @patch.object(WorkflowManager, "Log", Mock())
    def test_should_return_minus_one_when_job_not_found_within_run(self):
      from occam.jobs.records.job import JobRecord
      # TODO: move mocks to setup and generalize the nodesFor return value
      superJobRecord = create_autospec(JobRecord)
      superJobRecord.status = "finished"
      job_id = 100
      self.workflows.nodesFor.return_value = {
        "nodes": {
          "0": {
            "jobs": [
              {
                "id": job_id
              }
            ]
          }
        }
      }

      runRecord = create_autospec(RunRecord)
      runRecord.job_id = 200
      runRecord.id = 300
      self.workflows.jobs.jobFromId.side_effect = (lambda x:
        superJobRecord if x == runRecord.job_id else None)

      ret = self.workflows.terminate(runRecord, None)
      self.assertEqual(ret, -1)
      WorkflowManager.Log.assert_has_calls([call.error(
        "Failed to find job %d within run %d" % (job_id, runRecord.id))])
