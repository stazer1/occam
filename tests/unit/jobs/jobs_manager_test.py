import pytest
import unittest
import random

from occam.object import Object
from occam.jobs.manager import JobManager, scheduler
from occam.jobs.records.job import JobRecord

from occam.databases.manager     import DataNotUniqueError
from occam.objects.write_manager import ObjectWriteManager

from unittest.mock import (
  DEFAULT,
  MagicMock,
  Mock,
  call,
  create_autospec,
  mock_open,
  patch,
  ANY,
)

from tests.helper import uuid, multihash
from tests.unit.objects.mock import ObjectManagerMock
from tests.unit.builds.mock import BuildManagerMock

class WriteModeMatch:
  def __eq__(self, mode):
    return 'w' in mode or 'a' in mode

class ReadModeMatch:
  def __eq__(self, mode):
    return 'r' in mode

class Writer:
  def __init__(self, path, write_dict):
    self.path = path
    self.file_writes = write_dict
  def __enter__(self):
    return self
  def __exit__(self, ex_type, ex_value, traceback):
    pass
  def write(self, string):
    self.file_writes[self.path] = self.file_writes.get(self.path, [])
    self.file_writes[self.path].append(string)
  def close(self):
    pass


DEFAULT_PROVIDES = {
  "architecture": "x86",
  "environment": ["linux", "singularity"],
}

class TaskFactory:
  def __init__(self, task_structure={}):
    self.next_connection_index = 1
    self.task_structure = task_structure

  def get_path_section(self, obj_id, obj_revision, index, provides=DEFAULT_PROVIDES):
    volume_path = f"/occam/{obj_id}-{obj_revision}"
    obj_index_path = f"/home/occam/task/objects/{index}"
    arch = provides.get("architecture")
    env = provides.get("environment")
    vol_dict = {}
    local_dict = {}

    if arch:
      vol_dict.setdefault(arch, {})
      local_dict.setdefault(arch, {})
      if isinstance(env, list):
        for e in env:
          vol_dict[arch][e] = volume_path
          local_dict[arch][e] = obj_index_path

      elif isinstance(env, str):
        vol_dict[arch][env] = volume_path
        local_dict[arch][env] = obj_index_path

    path_section = {
      "volume": vol_dict,
      "mount": self.task_structure.get("paths", {}).get("mount", volume_path),
      "separator": "/",
      "local": local_dict,
      "localMount": obj_index_path,
      "taskLocal": "/home/occam/task",
      "cwd": f"{obj_index_path}/local",
      "mountLocal": [
        "/home/occam/local"
      ],
    }

    return path_section

  def get_dependency_section(self, index):
    obj_id = multihash()
    obj_uid = multihash()
    obj_revision = multihash(hashType="sha1")

    link = [
      {
        "to": "/some/absolute/path",
        "source": "some_relative_path"
      }
    ]
    env = {
      "VAR": "VARIABLE"
    }
    version = "8.8"
    build_id = multihash()
    build_revision = multihash(hashType="sha1")

    return [
      {
        "id": obj_id,
        "uid": obj_uid,
        "name": "dependency_name",
        "type": "runtime",
        "revision": obj_revision,
        "init": {
          "link": link,
          "env": env
        },
        "version": version,
        "build": {
          "id": build_id,
          "revision": build_revision,
        },
        "lock": "2.x,>=2.25-bootstrap",
        "requested": "2.x,>=2.25-bootstrap",
        "index": index,
        "paths": self.get_path_section(obj_id, obj_revision, index),
        "environment": "linux",
        "architecture": "x86-64",
        "dependencies": [],
      }
    ]

  def get_install_section(self):
    return [
      {
        "type": "resource",
        "subtype": "application/made-up",
        "source": "https://example.com/made-up.tar.xz",
        "to": "made-up.tar.xz",
        "name": "MadeUp 88 Source Code",
        "actions": {
          "unpack": "."
        },
        "id": multihash(),
        "uid": multihash(),
        "revision": multihash()
      }
    ]

  def get_next_task_index(self):
    ret = self.next_connection_index
    self.next_connection_index += 1
    return ret

  def get_connections(self, connections_structure):
    connections = []

    if not isinstance(connections_structure, dict):
      raise ValueError("Unexpected connections_structure type.")

    for task_index, so_structure in connections_structure.items():

      input_index = self.get_next_task_index()
      connections.append(
        TaskFactory(so_structure).create_task(
          ["inputs", "run", "index", "paths"], index=task_index
        )
      )

    return connections

  def get_input_section(self):
    inputs_structure = self.task_structure.get("inputs", [])
    inputs = []

    if not isinstance(inputs_structure, list):
      raise ValueError("Unexpected inputs_structure type.")

    for inp in inputs_structure:
      inputs.append(
        {
          "type": "input_type",
          "subtype": "input_subtype",
          "name": "Input Name",
          "connections": self.get_connections(
            inp.get("connections", {})
          )
        }
      )

    return inputs

  def get_output_section(self):
    outputs_structure = self.task_structure.get("outputs", {})
    outputs_section = []
    for output in outputs_structure:
      outputs_section.append(
        {
          "type": "configuration",
          "subtype": "application/json",
          "file": "configuration.json",
          "name": "Low-level"
        }
      )

    return outputs_section

  def get_objects_section(self):
    objects_structure = self.task_structure.get("objects", {})
    # The objects section also contains subtasks!!!
    objects_section = []

    if not isinstance(objects_structure, dict):
      raise ValueError("Unexpected objects_structure type.")

    for task_index, so_structure in objects_structure.items():
      objects_section.append(
        TaskFactory(so_structure).create_task(
          ["index", "run", "runs", "paths", "running"],
          index=task_index
        )
      )
    return objects_section

  def get_running_section(self):
    running_structure = self.task_structure.get("running", {})
    # The running section contains subtasks.
    running_section = []

    for task_index, so_structure in running_structure.items():
      running_section.append(
        TaskFactory(so_structure).create_task(
          index=task_index
        )
      )

    return running_section

  def get_build_section(self, index, build_id):
    return {
      "install": self.get_install_section(),
      "dependencies": self.get_dependency_section(index),
      "command": [
        "/bin/bash",
        "build.sh"
      ],
      "id": build_id,
      "revision": None,
    }

  def get_run_section(self, obj_id, obj_revision, obj_name, interactive):
    return {
      "command": [
        f"/occam/{obj_id}-{obj_revision}/usr/bin/{obj_name}"
      ],
      "interactive": interactive,
      "env": {
        "VAR": "VALUE"
      }
    }

  def get_provides_section(self):
    return {
      "architecture": "x86",
      "environment": "dos",
    }

  def get_init_section(self):
    init_structure = self.task_structure.get("init", {})
    init_section = {
      "env": {
        "VAR": "VALUE",
      },
      "link": [
      ],
      "copy": [
      ]
    }

    for link in init_structure.get("link", []):
      init_section["link"].append(
        {
          "source": link.get("source", "some_relative_path"),
          "to": link.get("to", "/some/absolute/path")
        }
      )
    for copy in init_structure.get("copy", []):
      init_section["copy"].append(
        {
          "source": copy.get("source", "some_relative_path"),
          "to": copy.get("to", "/some/absolute/path")
        }
      )

    return init_section

  def get_builds_section(self, paths):
    builds_structure = self.task_structure.get("builds", {})
    builds_section =  {
      "id": multihash(),
      "uid": multihash(),
      "revision": multihash(hashType="sha1"),
      'index': builds_structure.get("index", 1)
    }
    builds_section_extras = {
      "stage": {
        "id": multihash()
      },
      "paths": paths
    }

    for extra in builds_section_extras:
      if extra in builds_structure:
        builds_section[extra] = builds_section_extras[extra]

    return builds_section

  def get_runs_section(self, paths, index):
    runs_structure = self.task_structure.get("runs", {}) 
    runs_section = {
      "id": multihash(),
      "uid": multihash(),
      "revision": multihash(hashType="sha1"),
      'index': runs_structure.get("index", index)
    }

    runs_section_extras = {
      "stage": {
        "id": multihash()
      },
      "paths": paths
    }

    for extra in runs_section_extras:
      if extra in runs_structure:
        runs_section[extra] = runs_section_extras[extra]

    return runs_section
    
  def get_network_section(self):
    network_structure = self.task_structure.get("network", {})

    network_section = {
      "ports": []
    }
    for port in network_structure.get("ports", []):
      a_port = {}
      if "bind" in port:
        a_port["bind"] = port["bind"]
      network_section["ports"].append(a_port)

    return network_section

  def create_task(self, extra_sections=[], index=1):
    obj_id = multihash()
    obj_uid = multihash()
    obj_revision = multihash(hashType="sha1")
    obj_name = "erasmus"
    obj_env = "linux"
    obj_arch = "x86-64"
    build_id = multihash()
    interactive = False

    provides_section = self.get_provides_section()

    sub_paths = self.get_path_section(
      obj_id, obj_revision, 1, provides=provides_section
    )

    # FIXME: What is the occam-wide supported minimal task??
    task = {
      "id": obj_id,
      "uid": obj_id,
      "name": obj_name,
      "type": "task",
      "revision": obj_revision,
      "environment": "linux",
      "architecture": "x86-64",
    }

    extras = {
      "dependencies": self.get_dependency_section(index),
      "run": self.get_run_section(obj_id, obj_revision, obj_name, interactive),
      "runs": self.get_runs_section(sub_paths, index),
      "running": self.get_running_section(),
      "build": self.get_build_section(index, build_id),
      "init": self.get_init_section(),
      "outputs": self.get_output_section(),
      "inputs": self.get_input_section(),
      "summary": "Object description for task.",
      "interactive": interactive,
      "index": index,
      "paths": self.get_path_section(obj_id, obj_revision, index, provides=provides_section),
      "network": self.get_network_section(),
      "builds": self.get_builds_section(sub_paths),
      "provides": provides_section,
      "objects": self.get_objects_section(),
    }

    for extra in extra_sections:
      task[extra] = extras[extra]

    # Include anything required for the task structure of the factory.
    for key in self.task_structure.keys():
      task[key] = extras[key]

    return task

class TestJobManager:
  class TestRegister(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()

    def test_register(self):
      class DummyDriver:
        pass

      self.jobs.register('someAdapter', DummyDriver)
      self.assertEqual(self.jobs.drivers['someAdapter'], DummyDriver)

  class TestConfigurationFor(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()

    def test_should_return_empty_dict_when_config_is_empty(self):
      self.jobs.configuration = {}
      self.assertEqual(self.jobs.configurationFor('a'), {})

    def test_should_return_empty_dict_when_no_schedulers_found(self):
      self.jobs.configuration = {'schedulers': {}}
      self.assertEqual(self.jobs.configurationFor('a'), {})

    def test_should_return_scheduler_configuration_when_schedulers_found(self):
      a_config = {'ayy': 'bee'}
      self.jobs.configuration = {'schedulers': {'a': a_config}}
      self.assertEqual(self.jobs.configurationFor('a'), a_config)

  class TestDriverFor(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()

    @patch('importlib.import_module', autospec=True)
    def test_should_return_none_when_scheduler_not_found(self, import_module):
      self.assertEqual(self.jobs.driverFor('totallyFake'), None)

    @patch.object(JobManager, "Log", Mock())
    @patch('importlib.import_module', autospec=True)
    def test_should_return_scheduler_object_when_scheduler_found(self, import_module):
      # Perform the same side effect expected of importlib.
      def set_drivers(s):
        def take_config(config):
          return 'occam-182'
        self.jobs.drivers['occam'] = take_config

      import_module.side_effect = set_drivers

      self.assertEqual(self.jobs.driverFor('occam'), 'occam-182')

    @patch.object(JobManager, "Log", Mock())
    @patch('importlib.import_module')
    def test_should_warn_and_return_none_on_importlib_import_error(self, import_module, autospec=True):
      def raiseme(s):
        raise ImportError('Bad')

      import_module.side_effect = raiseme

      self.assertEqual(self.jobs.driverFor('totallyFake'), None)
      JobManager.Log.assert_has_calls(
        [call.warning('Failed to import scheduler plugin for totallyFake: Bad')]
      )

  class TestUpdateRunSection(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()

    @pytest.mark.xfail
    def test_should_throw_value_error_when_task_is_none(self):
      with self.assertRaises(ValueError):
        ret = self.jobs.updateRunSection(None, None, root = None)

    def test_should_return_an_empty_dict_when_task_and_runSection_are_empty(self):
      ret = self.jobs.updateRunSection({}, {}, root = None)
      self.assertEqual(ret, {})

    def test_should_return_root_when_no_index_in_task(self):
      ret = self.jobs.updateRunSection(
        TaskFactory().create_task(), {}, root = {'a': '1'}
      )
      self.assertEqual(ret, {'a': '1'})

    def test_should_update_task_with_desired_run_section(self):
      from copy import deepcopy
      task = TaskFactory().create_task(["index", "run", "runs"])

      # The task's run section should be updated to include keys
      # from the old run section, and the keys in the provided run
      # section.
      run_section = {"a": 1}

      expected_task = deepcopy(task)
      expected_task["run"]["a"] = 1

      self.jobs.updateRunSection(task, run_section)
      self.assertEqual(task, expected_task)

    def test_should_update_run_section_cwd_with_paths_cwd(self):
      from copy import deepcopy
      task = TaskFactory().create_task(
        [
          "index", "run", "runs", "paths"
        ]
      )
      expectedTask = deepcopy(task)
      expectedTask["run"]["a"] = '1'
      expectedTask["run"]["cwd"] = "/b"
      expectedTask["paths"]["cwd"] = "/b"

      run_section = {'a': '1', 'cwd': '/b'}
      ret = self.jobs.updateRunSection(task, run_section)
      self.assertEqual(task, expectedTask)

    def test_shouldnt_modify_subobjects_when_run_section_is_modified(self):
      from copy import deepcopy
      task_structure = {"running": {1: {}}}
      task = TaskFactory(task_structure).create_task(
        [
          "index", "run", "runs", "paths", "inputs", "running"
        ]
      )
      expectedTask = deepcopy(task)
      expectedTask["run"]["a"] = '1'
      expectedTask["run"]["cwd"] = "/b"
      expectedTask["paths"]["cwd"] = "/b"

      run_section = {'a': '1', 'cwd': '/b'}
      ret = self.jobs.updateRunSection(task, run_section)
      self.assertEqual(task, expectedTask)

    def test_should_modify_input_subobjects_when_run_section_not_updated(self):
      from copy import deepcopy
      task = TaskFactory().create_task(
        [
          "run", "runs", "inputs"
        ]
      )
      expectedTask = deepcopy(task)
      run_section = {'a': '1', 'cwd': '/b'}
      for i in expectedTask["inputs"]:
        for c in i["connections"]:
          c["run"]["a"] = "1"
          c["run"]["cwd"] = "/b"
          c["paths"]["cwd"] = "/b"

      ret = self.jobs.updateRunSection(task, run_section)
      self.assertEqual(task, expectedTask)

    def test_should_modify_running_subobjects_when_run_section_not_updated(self):
      from copy import deepcopy
      task_structure = {"running": {1: {"objects": {}}}}
      task = TaskFactory(task_structure).create_task(
        [
          "run", "runs", "running"
        ]
      )
      expectedTask = deepcopy(task)
      run_section = {'a': '1', 'cwd': '/b'}
      for i in expectedTask["running"]:
        for c in i["objects"]:
          c["run"]["a"] = "1"
          c["run"]["cwd"] = "/b"
          c["paths"]["cwd"] = "/b"

      ret = self.jobs.updateRunSection(task, run_section)
      self.assertEqual(task, expectedTask)

  class TestUpdateJob(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()
      self.jobs.datastore = create_autospec(self.jobs.datastore)

    def test_should_return_result_of_updateJob(self):
      jr = JobRecord()
      self.jobs.datastore.updateJob.return_value = jr

      ret = self.jobs.updateJob(id=1)
      self.assertTrue(ret is jr)

  class TestWriteTask(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()
      self.task_path = "/some/path"
      task_structure = {"running": {1: {}}}
      self.task = TaskFactory(task_structure).create_task(
        [
          "dependencies",
          "run",
          "runs",
          "running",
          "build",
          "init",
          "outputs",
          "inputs",
          "summary",
          "interactive",
          "index",
          "paths",
        ]
      )
      self.task_object_path = "/some/path/task/object.json"
      self.task_stdin_path = "/some/path/stdin"

    @patch('os.path.exists', return_value=False)
    def test_should_create_stdin_file_if_it_doesnt_exist(self, exists):
      with patch("builtins.open", mock_open()) as mock_handle:
        self.jobs.writeTask(self.task, self.task_path)

        mock_handle.assert_any_call(self.task_stdin_path, WriteModeMatch())

    @patch('os.path.exists', return_value=True)
    def test_shouldnt_create_stdin_file_if_it_exists(self, exists):
      with patch("builtins.open", mock_open()) as mock_handle:
        self.jobs.writeTask(self.task, self.task_path)

        try:
          mock_handle.assert_any_call(self.task_stdin_path, WriteModeMatch())
        except AssertionError as e:
          return
        raise AssertionError("Unexpected open of stdin file.")

    @patch('os.path.exists', return_value=False)
    def test_should_create_object_json_if_it_doesnt_exist(self, exists):
      with patch("builtins.open", mock_open()) as mock_handle:
        self.jobs.writeTask(self.task, self.task_path)
        mock_handle.assert_any_call(self.task_object_path, WriteModeMatch())

    @patch('os.path.exists', return_value=False)
    def test_should_write_object_file_so_it_can_be_read_back_as_json(self, exists):
      import json

      # Collect writes to the object file and make sure it is read back in as
      # the same structure.
      file_writes = {}
      def write_collector(path, mode):
        return Writer(path, file_writes)

      with patch("builtins.open", mock_open()) as mock_handle:
        mock_handle.side_effect = write_collector

        self.jobs.writeTask(self.task, self.task_path)

        loaded_task = file_writes[self.task_object_path]
        loaded_task = "".join(loaded_task)
        loaded_task = json.loads(loaded_task)
        self.assertEqual(loaded_task, self.task)

    @patch('os.path.exists', return_value=True)
    def test_shouldnt_create_object_json_if_it_exists(self, exists):
      with patch("builtins.open", mock_open()) as mock_handle:
        self.jobs.writeTask(self.task, self.task_path)

        try:
          mock_handle.assert_any_call(self.task_object_path, WriteModeMatch())
        except AssertionError as e:
          return

        raise AssertionError("Unexpected open of stdin file.")

    @patch('os.path.exists', return_value=False)
    def test_should_raise_OSError_when_open_fails(self, exists):
      with patch("builtins.open", mock_open()) as mock_handle:
        # Open for stdin.
        mock_handle.side_effect = OSError("Couldn't open file.")
        with self.assertRaises(OSError):
          self.jobs.writeTask(self.task, self.task_path)
        self.assertEqual(mock_handle.call_count, 1)

      with patch("builtins.open", mock_open()) as mock_handle:
        # Open for object.json.
        mock_handle.side_effect = [Writer('doesnt_matter', {}), OSError("Couldn't open file.")]
        with self.assertRaises(OSError):
          self.jobs.writeTask(self.task, self.task_path)

        self.assertEqual(mock_handle.call_count, 2)

  class TestWriteNetwork(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()
      self.task_path = "/some/path"
      self.network = {"a": 1}
      self.network_path = "/some/path/network.json"

    @patch('os.path.exists', return_value=False)
    def test_should_create_network_file_if_it_doesnt_exist(self, exists):
      with patch("builtins.open", mock_open()) as mock_handle:
        self.jobs.writeNetwork(self.network, self.task_path)
        mock_handle.assert_any_call(self.network_path, WriteModeMatch())

    @patch('os.path.exists', return_value=False)
    def test_should_write_network_json_so_it_can_be_read_back_as_json(self, exists):
      import json

      # Collect writes to the network file and make sure it is read back in as
      # the same structure.
      file_writes = {}
      def write_collector(path, mode):
        return Writer(path, file_writes)

      with patch("builtins.open", mock_open()) as mock_handle:
        mock_handle.side_effect = write_collector

        self.jobs.writeNetwork(self.network, self.task_path)

        loaded_network = file_writes[self.network_path]
        loaded_network = "".join(loaded_network)
        loaded_network = json.loads(loaded_network)
        self.assertEqual(loaded_network, self.network)

  class TestDeployBuild(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()

      self.jobs._deploy = create_autospec(self.jobs._deploy)
      self.jobs._deploy.return_value = {
        "deployed": True
      }

      self.jobs.createPathFor = create_autospec(self.jobs.createPathFor)
      self.jobs.createPathFor.return_value = (
        "/some/other/path"
      )

      self.jobs.getObjectFromTask = create_autospec(
        self.jobs.getObjectFromTask
      )
      self.jobs.getObjectFromTask.return_value = {}
      self.jobs.prepareTaskPath = create_autospec(
        self.jobs.prepareTaskPath
      )
      self.jobs.updateTaskWithRuntimeRequests = create_autospec(
        self.jobs.updateTaskWithRuntimeRequests
      )

      self.jobs.storage = create_autospec(self.jobs.storage)

      self.obj_id = multihash()
      self.obj_uid = multihash()
      self.revision_id = multihash(hashType="sha1")

      self.jobs.objects = ObjectManagerMock([
        {
          "id": self.obj_id,
          "uid": self.obj_uid,
          "revision": self.revision_id,
          "path": "/some/path",
          "type": "data",
          "name": "MyData"
        }
      ])

    # TODO: Fix get() on None.
    @pytest.mark.xfail
    @patch("os.mkdir")
    @patch("os.makedirs")
    @patch("shlex.split")
    def test_should_raise_exception_when_getObjectFromTask_fails(self, shlex_split, os_mkdir):
      with patch("builtins.open", mock_open()) as mock_handle:
        with self.assertRaises(ValueError):
          self.jobs.deployBuild({})

    @patch('os.path.exists', return_value=False)
    @patch("os.mkdir")
    @patch("os.makedirs")
    @patch("shlex.split")
    def test_should_deploy_task_with_staged_build_using_the_correct_cwd(self, shlex_split, os_mkdirs, os_mkdir, os_path_exists):
      task_structure = {
        "builds": {
          "stage": {}
        }
      }
      task = TaskFactory(task_structure).create_task()
      # TODO: Deduplicate these.
      self.jobs.objects = ObjectManagerMock([
        {
          "id": task["builds"]["id"],
          "uid": task["builds"]["uid"],
          "revision": task["builds"]["revision"],
          "path": "/some/path",
          "type": "data",
          "name": "MyData"
        }
      ])
      with patch("builtins.open", mock_open()) as mock_handle:
        ret = self.jobs.deployBuild(
          task = task,
          command="not a command"
        )
        self.jobs._deploy.assert_called()
        args, kwargs = self.jobs._deploy.call_args_list[0]
        self.assertEqual(kwargs["cwd"], "/some/metadata/builds/object")
        self.assertEqual(ret, {"deployed": True})

    @patch('os.path.exists', return_value=False)
    @patch("os.mkdir")
    @patch("os.makedirs")
    @patch("shlex.split")
    def test_should_deploy_local_task_with_non_staged_build_using_the_correct_cwd(self, shlex_split, os_mkdirs, os_mkdir, os_path_exists):
      task = TaskFactory().create_task(["builds"])

      self.jobs.objects = ObjectManagerMock([
        {
          "id": task["builds"]["id"],
          "uid": task["builds"]["uid"],
          "revision": task["builds"]["revision"],
          "path": "/some/path",
          "type": "data",
          "name": "MyData"
        }
      ])

      with patch("builtins.open", mock_open()) as mock_handle:
        ret = self.jobs.deployBuild(
          task = task,
          command="not a command",
          local=True,
          cwd="/some/path",
        )

        self.jobs._deploy.assert_called()
        args, kwargs = self.jobs._deploy.call_args_list[0]
        self.assertEqual(kwargs["cwd"], "/some/path")
        self.assertEqual(ret, {"deployed": True})

    @patch('os.path.exists', return_value=False)
    @patch("os.mkdir")
    @patch("os.makedirs")
    @patch("shlex.split")
    def test_deploy_stored_object_task_using_the_correct_cwd(self, shlex_split, os_mkdirs, os_mkdir, os_path_exists):
      task = TaskFactory().create_task(["builds"])
      self.jobs.objects = ObjectManagerMock([
        {
          "id": task["builds"]["id"],
          "uid": task["builds"]["uid"],
          "revision": task["builds"]["revision"],
          "path": "/some/path",
          "type": "data",
          "name": "MyData"
        }
      ])
      with patch("builtins.open", mock_open()) as mock_handle:
        ret = self.jobs.deployBuild(
          task = task,
        )

        self.jobs._deploy.assert_called()
        args, kwargs = self.jobs._deploy.call_args_list[0]
        self.assertFalse("cwd" in kwargs)
        self.assertEqual(ret, {"deployed": True})

    @patch('os.path.exists', return_value=False)
    @patch("os.mkdir")
    @patch("os.makedirs")
    @patch("shlex.split")
    def test_should_call_makedirs_for_dirs_of_local_object_task(self, shlex_split, os_mkdirs, os_mkdir, os_path_exists):
      task = TaskFactory().create_task(["builds"])
      self.jobs.objects = ObjectManagerMock([
        {
          "id": task["builds"]["id"],
          "uid": task["builds"]["uid"],
          "revision": task["builds"]["revision"],
          "path": "/some/path",
          "type": "data",
          "name": "MyData"
        }
      ])
      with patch("builtins.open", mock_open()) as mock_handle:
        self.jobs.deployBuild(
          task = task,
          local=True,
          cwd="/some/path"
        )
        os_mkdirs.assert_any_call("/some/other/path/task", exist_ok=True)
        os_mkdirs.assert_any_call("/some/path/.occam/local", exist_ok=True)
        os_mkdirs.assert_any_call("/some/path/.occam/local/build", exist_ok=True)

    @patch('os.path.exists', return_value=False)
    @patch("os.mkdir")
    @patch("os.makedirs")
    @patch("shlex.split")
    def test_should_call_makedirs_for_dirs_of_staged_object_task(self, shlex_split, os_mkdirs, os_mkdir, os_path_exists):
      task = TaskFactory().create_task(["builds"])
      self.jobs.objects = ObjectManagerMock([
        {
          "id": task["builds"]["id"],
          "revision": task["builds"]["revision"],
          "path": "/some/path",
          "type": "data",
          "name": "MyData"
        }
      ])
      with patch("builtins.open", mock_open()) as mock_handle:
        self.jobs.deployBuild(
          task = {
            "builds": {
              "id": self.obj_id,
              "uid": self.obj_uid,
              "revision": self.revision_id,
              "stage": {
                "id": multihash()
              }
            }
          },
          local=True,
          cwd="/some/path"
        )
        os_mkdirs.assert_any_call("/some/other/path/task", exist_ok=True)
        os_mkdirs.assert_any_call("/some/path/../metadata/builds/vm", exist_ok=True)
        os_mkdirs.assert_any_call("/some/metadata/builds/build", exist_ok=True)

    @patch('os.path.exists', return_value=False)
    @patch("os.mkdir")
    @patch("os.makedirs")
    @patch("shlex.split")
    def test_should_write_object_json_so_it_can_be_read_back_as_json(self, shlex_split, os_mkdirs, os_mkdir, os_path_exists):
      import json

      file_writes = {}
      def write_collector(path, mode):
        return Writer(path, file_writes)

      task_structure = {
        "builds": {
          "stage": {}
        }
      }
      task = TaskFactory(task_structure).create_task()
      self.jobs.objects = ObjectManagerMock([
        {
          "id": task["builds"]["id"],
          "uid": task["builds"]["uid"],
          "revision": task["builds"]["revision"],
          "path": "/some/path",
          "type": "data",
          "name": "MyData"
        }
      ])
      with patch("builtins.open", mock_open()) as mock_handle:
        mock_handle.side_effect = write_collector
        ret = self.jobs.deployBuild(
          task = task,
          local=True,
          cwd="/some/path"
        )
        loaded_task = file_writes["/some/path/../metadata/builds/vm/object.json"]
        loaded_task = "".join(loaded_task)
        loaded_task = json.loads(loaded_task)
        self.assertEqual(loaded_task, task)

        file_writes = {}

        task["builds"]["stage"] = {}
        ret = self.jobs.deployBuild(
          task = task,
          local=True,
          cwd="/some/path"
        )
        loaded_task = file_writes["/some/path/.occam/local/object.json"]
        loaded_task = "".join(loaded_task)
        loaded_task = json.loads(loaded_task)
        self.assertEqual(loaded_task, task)

  class TestDeployRun(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()
      self.jobs.updateRunSection = create_autospec(self.jobs.updateRunSection)
      self.jobs.getObjectFromTask = create_autospec(self.jobs.getObjectFromTask)
      self.jobs.prepareTaskPath = create_autospec(self.jobs.prepareTaskPath)
      self.jobs.updateTaskWithRuntimeRequests = create_autospec(
        self.jobs.updateTaskWithRuntimeRequests
      )
      self.jobs.storage = create_autospec(self.jobs.storage)
      self.jobs._deploy = create_autospec(self.jobs._deploy)
      self.jobs.createPathFor = create_autospec(self.jobs.createPathFor)

      self.obj_id = multihash()
      self.obj_uid = multihash()
      self.revision_id = multihash(hashType="sha1")

      self.jobs.objects = ObjectManagerMock([
        {
          "id": self.obj_id,
          "uid": self.obj_uid,
          "revision": self.revision_id,
          "path": "/some/path",
          "type": "data",
          "name": "MyData"
        },
      ])

      self.jobs.getObjectFromTask.return_value = {}

      self.jobs._deploy.return_value = {
        "deployed": True
      }

      self.jobs.createPathFor.return_value = (
        "/some/other/path"
      )

    @patch("os.mkdir")
    @patch("shlex.split")
    def test_should_raise_exception_when_getObjectFromTask_fails(self, shlex_split, os_mkdir):
      self.jobs.getObjectFromTask.return_value = None
      with patch("builtins.open", mock_open()) as mock_handle:
        with self.assertRaises(ValueError):
          self.jobs.deployRun({})

    @patch('os.path.exists', return_value=False)
    @patch("os.mkdir")
    @patch("shlex.split")
    def test_should_deploy_task_with_staged_build_using_the_correct_cwd(self, shlex_split, os_mkdir, os_path_exists):
      task_structure = {
        "runs": {
          "stage": {}
        }
      }
      task = TaskFactory(task_structure).create_task()
      self.jobs.objects = ObjectManagerMock([
        {
          "id": task["runs"]["id"],
          "revision": task["runs"]["revision"],
          "path": "/some/path",
          "type": "data",
          "name": "MyData"
        }
      ])
      with patch("builtins.open", mock_open()) as mock_handle:
        ret = self.jobs.deployRun(
          task = task,
          command="not a command",
          runSection={"fake": "run"}
        )

        self.jobs._deploy.assert_called()
        args, kwargs = self.jobs._deploy.call_args_list[0]
        self.assertEqual(kwargs["cwd"], "/some/path")
        self.assertEqual(ret, {"deployed": True})

    @patch('os.path.exists', return_value=False)
    @patch("os.mkdir")
    @patch("shlex.split")
    def test_should_deploy_local_task_with_non_staged_build_using_the_correct_cwd(self, shlex_split, os_mkdir, os_path_exists):
      with patch("builtins.open", mock_open()) as mock_handle:
        ret = self.jobs.deployRun(
          task = TaskFactory().create_task(["runs"]),
          command="not a command",
          local=True,
          cwd="/some/path",
        )
        self.assertEqual(ret, {"deployed": True})

        self.jobs._deploy.assert_called()
        args, kwargs = self.jobs._deploy.call_args_list[0]
        self.assertEqual(kwargs["cwd"], "/some/path")
        self.assertEqual(ret, {"deployed": True})

    @patch('os.path.exists', return_value=False)
    @patch("os.mkdir")
    @patch("shlex.split")
    def test_should_deploy_stored_object_task_using_the_correct_cwd(self, shlex_split, os_mkdir, os_path_exists):
      with patch("builtins.open", mock_open()) as mock_handle:
        ret = self.jobs.deployRun(
          task = TaskFactory().create_task(["runs"]),
        )
        self.assertEqual(ret, {"deployed": True})

        self.jobs._deploy.assert_called()
        args, kwargs = self.jobs._deploy.call_args_list[0]
        self.assertTrue(kwargs["cwd"] is None)
        self.assertEqual(ret, {"deployed": True})

    @patch('os.path.exists', return_value=False)
    @patch("os.makedirs")
    @patch("shlex.split")
    def test_should_call_makedirs_for_all_dirs(self, shlex_split, os_mkdirs, os_path_exists):
      task_structure = {
        "runs": {
          "stage": {}
        }
      }
      task = TaskFactory(task_structure).create_task()
      self.jobs.objects = ObjectManagerMock([
        {
          "id": task["runs"]["id"],
          "uid": task["runs"]["uid"],
          "revision": task["runs"]["revision"],
          "path": "/some/path",
          "type": "data",
          "name": "MyData"
        }
      ])
      with patch("builtins.open", mock_open()) as mock_handle:
        ret = self.jobs.deployRun(
          task = task,
          local=True,
          cwd="/some/path"
        )
        os_mkdirs.assert_any_call(f"/some/tasks/{task['runs']['id']}", exist_ok=True)
        os_mkdirs.assert_any_call(f"/some/tasks/{task['runs']['id']}/task", exist_ok=True)

  class TestPrivateDeploy(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()
      self.jobs.installTask = create_autospec(self.jobs.installTask)
      self.jobs.installNetwork = create_autospec(self.jobs.installNetwork)
      self.jobs.pathsForTask = create_autospec(self.jobs.pathsForTask)
      self.jobs.writeNetwork = create_autospec(self.jobs.writeNetwork)
      self.jobs.writeTask = create_autospec(self.jobs.writeTask)

      self.obj_id = multihash()
      self.revision_id = multihash(hashType="sha1")

      self.jobs.pathsForTask.return_value = {}

    @patch("os.path.isdir")
    @patch("os.makedirs")
    @patch("os.path.exists")
    def test_should_call_installTask_when_initializeTaskPath_is_true(self, os_exists, os_mkdirs, os_isdir):
      with patch("builtins.open", mock_open()) as mock_handle:
        paths = {}
        self.jobs.pathsForTask.return_value = paths

        task = {}

        self.jobs._deploy(task, "/some/path")
        self.jobs.installTask.assert_any_call("/some/path/task/root", task, task, paths)

    @patch("os.path.isdir")
    @patch("os.makedirs")
    @patch("os.path.exists")
    def test_should_prefer_pathsFor_over_pathsForTask(self, os_exists, os_mkdirs, os_isdir):
      with patch("builtins.open", mock_open()) as mock_handle:
        self.jobs.pathsForTask.return_value = {
          "replacepath": {
            "id": self.obj_id, "revision": self.revision_id, "path": "/shouldnt/see"
          }
        }
        _, paths, _, _ = self.jobs._deploy(
          {}, "/some/path", pathsFor=[
            {"id": self.obj_id, "revision": self.revision_id, "path": "/should/see"}
          ]
        )

        self.assertEqual(paths["replacepath"]["path"], "/should/see")

    @patch("os.path.isdir")
    @patch("os.makedirs")
    @patch("os.path.exists")
    def test_should_set_paths_localMount_to_runs_paths_localMount_when_no_provides_section_present(self, os_exists, os_mkdirs, os_isdir):
      with patch("builtins.open", mock_open()) as mock_handle:
        self.jobs.pathsForTask.return_value = {
          "modpath": {
            "id": self.obj_id, "revision": self.revision_id, "path": "dontcare",
            "index": 1
          }
        }

        task_structure = {
          "runs": {
            "paths": {}
          }
        }
        task = TaskFactory(task_structure).create_task()
        paths_for = [
          {"id": self.obj_id, "revision": self.revision_id, "path": "dontcare"}
        ]

        _, paths, _, _ = self.jobs._deploy(
          task=task, taskPath="/some/path", pathsFor=paths_for, cwd="/cwd"
        )

        self.assertEqual(
          paths.get("modpath", {}).get("localMount"),
          f"{task['runs']['paths']['localMount']}/local"
        )

    @patch("os.path.isdir")
    @patch("os.makedirs")
    @patch("os.path.exists")
    def test_should_set_paths_localMount_to_task_local_when_localMounts_matches_provides_section(self, os_exists, os_mkdirs, os_isdir):
      with patch("builtins.open", mock_open()) as mock_handle:
        self.jobs.pathsForTask.return_value = {
          "modpath": {
            "id": self.obj_id, "revision": self.revision_id, "path": "dontcare",
            "index": 1
          }
        }

        task_structure = {
          "runs": {
            "paths": {}
          }
        }
        task = TaskFactory(task_structure).create_task(["provides"])
        paths_for = [
          {"id": self.obj_id, "revision": self.revision_id, "path": "dontcare"}
        ]

        _, paths, _, _ = self.jobs._deploy(
          task=task, taskPath="/some/path", pathsFor=paths_for, cwd="/cwd"
        )

        self.assertEqual(
          paths.get("modpath", {}).get("localMount"),
          f"{task['runs']['paths']['local']['x86']['dos']}/local"
        )

    @patch("os.path.isdir")
    @patch("os.makedirs")
    @patch("os.path.exists", return_value=False)
    def test_should_call_makedirs_for_stdin_containing_dir(self, os_exists, os_mkdirs, os_isdir):
      with patch("builtins.open", mock_open()) as mock_handle:
        self.jobs.pathsForTask.return_value = {
          "modpath": {
            "id": self.obj_id, "revision": self.revision_id, "path": "dontcare",
            "index": 1
          }
        }

        task_structure = {
          "runs": {
            "paths": {}
          }
        }
        task = TaskFactory(task_structure).create_task(["provides"])
        paths_for = [
          {"id": self.obj_id, "revision": self.revision_id, "path": "dontcare"}
        ]

        coffee_symbol = u'\u2615'
        stdin = f"{multihash()}{coffee_symbol}".encode("utf-8")
        _, paths, _, _ = self.jobs._deploy(
          task=task, taskPath="/some/path", pathsFor=paths_for, cwd="/cwd",
          stdin=stdin
        )

        os_mkdirs.assert_any_call("/some/path/objects/1", exist_ok=True)

    @patch("os.path.isdir")
    @patch("os.makedirs")
    @patch("os.path.exists")
    def test_should_write_stdin_so_it_can_be_read_back_exactly_as_it_was_piped_in(self, os_exists, os_mkdirs, os_isdir):
      file_writes = {}
      def write_collector(path, mode):
        return Writer(path, file_writes)

      with patch("builtins.open", mock_open()) as mock_handle:
        mock_handle.side_effect = write_collector
        self.jobs.pathsForTask.return_value = {
          "modpath": {
            "id": self.obj_id, "revision": self.revision_id, "path": "dontcare",
            "index": 1
          }
        }

        task_structure = {
          "runs": {
            "paths": {}
          }
        }
        task = TaskFactory(task_structure).create_task(["provides"])
        paths_for = [
          {"id": self.obj_id, "revision": self.revision_id, "path": "dontcare"}
        ]

        coffee_symbol = u'\u2615'
        stdin = f"{multihash()}{coffee_symbol}".encode("utf-8")
        _, paths, _, _ = self.jobs._deploy(
          task=task, taskPath="/some/path", pathsFor=paths_for, cwd="/cwd",
          stdin=stdin
        )

        written_stdin = file_writes["/some/path/objects/1/stdin"]
        written_stdin = b"".join(written_stdin)
        self.assertEqual(written_stdin, stdin)

    @patch("os.path.isdir")
    @patch("os.makedirs")
    @patch("os.path.exists")
    def test_should_write_network_and_task_files(self, os_exists, os_mkdirs, os_isdir):
      with patch("builtins.open", mock_open()) as mock_handle:
        self.jobs.pathsForTask.return_value = {
          "modpath": {
            "id": self.obj_id, "revision": self.revision_id, "path": "dontcare",
            "index": 1
          }
        }

        paths_for = [
          {"id": self.obj_id, "revision": self.revision_id, "path": "dontcare"}
        ]

        coffee_symbol = u'\u2615'
        stdin = f"{multihash()}{coffee_symbol}".encode("utf-8")
        self.jobs._deploy(task={}, taskPath="/some/path")

        self.jobs.installNetwork.assert_called()
        self.jobs.writeTask.assert_called()
        self.jobs.writeNetwork.assert_called()

  class TestExecute(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()

      self.jobs.backends = create_autospec(self.jobs.backends)
      self.jobs.uninstallNetwork = create_autospec(self.jobs.uninstallNetwork)
      self.jobs.cleanupTaskOutputFilenames = create_autospec(
        self.jobs.cleanupTaskOutputFilenames
      )
      self.jobs.pullRunReport = create_autospec(self.jobs.pullRunReport)
      self.jobs.updateRunReportOnError = create_autospec(self.jobs.updateRunReportOnError)

      self.obj_id = multihash()
      self.revision_id = multihash(hashType="sha1")

      self.jobs.pullRunReport.return_value = {}

    def test_should_return_dict_with_run_report(self):
      run_report = {"somethingInteresting": 1}
      self.jobs.pullRunReport.return_value = run_report

      ret = self.jobs.execute({}, {"home": "/home/fake"}, {}, Mock())

      self.assertTrue("runReport" in ret)
      self.assertEqual(ret["runReport"], run_report)

    def test_should_update_code_in_return_value_to_reflect_bad_exit_code_from_backend(self):
      exit_code = 1
      self.jobs.backends.run.return_value = exit_code

      ret = self.jobs.execute({}, {"home": "/home/fake"}, {}, Mock())

      self.assertTrue("code" in ret)
      self.assertEqual(ret["code"], exit_code)

    def test_should_update_code_in_return_value_to_reflect_run_report_exit_code(self):
      run_report = {"code": 1}
      self.jobs.backends.run.return_value = 0
      self.jobs.pullRunReport.return_value = run_report

      ret = self.jobs.execute({}, {"home": "/home/fake"}, {}, Mock())

      self.assertTrue("code" in ret)
      self.assertEqual(ret["code"], 1)

    def test_should_call_updateRunReportOnError_when_exit_code_is_nonzero(self):
      run_report = {"code": 1}
      self.jobs.backends.run.return_value = 0
      self.jobs.pullRunReport.return_value = run_report

      ret = self.jobs.execute({}, {"home": "/home/fake"}, {}, Mock())

      self.jobs.updateRunReportOnError.assert_called()

      self.jobs.updateRunReportOnError.reset_mock()

      self.jobs.backends.run.return_value = 1
      self.jobs.pullRunReport.return_value = {}

      ret = self.jobs.execute({}, {"home": "/home/fake"}, {}, Mock())

      self.jobs.updateRunReportOnError.assert_called()

  class TestGetRunReportPath(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()

    def test_should_return_run_report_path_from_runs_section(self):
      task = TaskFactory().create_task(["runs"])
      ret = self.jobs.getRunReportPath(task, "/")
      self.assertEqual(ret, f"/task/objects/{task['runs']['index']}/run.json")

    def test_should_return_run_report_path_from_builds_section(self):
      task = TaskFactory().create_task(["builds"])
      ret = self.jobs.getRunReportPath(task, "/")
      self.assertEqual(ret, f"/task/objects/{task['builds']['index']}/run.json")

  class TestUpdateRunReportOnError(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()
      self.jobs.pullRunReport = create_autospec(self.jobs.pullRunReport)
      self.jobs.getRunReportPath = create_autospec(self.jobs.getRunReportPath)

      self.jobs.getRunReportPath.return_value = "/rrpath.json"

    def test_shouldnt_load_report_if_provided_as_argument(self):
      import json

      with patch("builtins.open", mock_open()) as mock_handle:
        task = TaskFactory().create_task()
        self.jobs.updateRunReportOnError({}, "/some/path", {})

        self.jobs.pullRunReport.assert_not_called()

    def test_should_load_report_if_not_provided_as_argument(self):
      import json

      self.jobs.pullRunReport.return_value = {}
      task = TaskFactory().create_task()

      with patch("builtins.open", mock_open()) as mock_handle:
        self.jobs.updateRunReportOnError({}, "/some/path")

        self.jobs.pullRunReport.assert_called()

    def test_should_write_run_report_so_it_can_be_read_back_as_json(self):
      import json

      file_writes = {}
      def write_collector(path, mode):
        return Writer(path, file_writes)

      with patch("builtins.open", mock_open()) as mock_handle:
        mock_handle.side_effect = write_collector

        task = TaskFactory().create_task()
        self.jobs.updateRunReportOnError({}, "/some/path", {})

        read_report = file_writes["/rrpath.json"]
        read_report = "".join(read_report)
        read_report = json.loads(read_report)
        self.assertTrue("status" in read_report)
        self.assertEqual(read_report["status"], "failed" )

  class TestUpdateTaskWithRuntimeRequests(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()

    def test_should_insert_interactive_key_when_interactive_is_set_to_true(self):
      task = {}
      self.jobs.updateTaskWithRuntimeRequests(task, interactive=True)
      self.assertTrue("interactive" in task)
      self.assertTrue(task["interactive"] is True)

      task = {}
      self.jobs.updateTaskWithRuntimeRequests(task, interactive=False)
      self.assertFalse("interactive" in task)

      task = {}
      self.jobs.updateTaskWithRuntimeRequests(task, interactive=None)
      self.assertFalse("interactive" in task)

    def test_should_insert_interactive_key_for_running_objects_subtasks_when_they_provide_and_run(self):
      task_structure = {
        "running": {
          1: {
            "objects": {
              2: {
                "run": {},
                "provides": {}
              }
            }
          }
        }
      }
      task = TaskFactory(task_structure).create_task()
      self.jobs.updateTaskWithRuntimeRequests(task, interactive=True)
      self.assertTrue("interactive" in task["running"][0]["objects"][0])
      self.assertTrue(task["running"][0]["objects"][0]["interactive"] is True)

  class TestPullRunReport(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()
      self.jobs.getRunReportPath = create_autospec(self.jobs.getRunReportPath)

      self.jobs.getRunReportPath.return_value = "/rrpath.json"

    @patch("os.path.exists", return_value=False)
    def test_should_return_empty_dict_when_report_path_doesnt_exist(self, exists):
      json = '{"read": "fine"}'
      with patch("builtins.open", mock_open(read_data=json)) as mock_handle:
        ret = self.jobs.pullRunReport({}, "/task/path")
        self.assertEqual(ret, {})

    @patch("os.path.exists", return_value=True)
    def test_should_return_empty_dict_when_file_isnt_json(self, exists):
      json = '/////'
      with patch("builtins.open", mock_open(read_data=json)) as mock_handle:
        ret = self.jobs.pullRunReport({}, "/task/path")
        self.assertEqual(ret, {})

    @patch("os.path.exists", return_value=True)
    def test_should_return_report_dict_when_json_can_be_read(self, exists):
      json = '{"read": "fine"}'
      with patch("builtins.open", mock_open(read_data=json)) as mock_handle:
        ret = self.jobs.pullRunReport({}, "/task/path")
        self.assertEqual(ret, {"read": "fine"})

    @patch("os.path.exists", return_value=True)
    def test_should_return_report_under_phases_if_report_json_is_a_list(self, exists):
      json = '[{"read": "fine"}]'
      with patch("builtins.open", mock_open(read_data=json)) as mock_handle:
        ret = self.jobs.pullRunReport({}, "/task/path")
        self.assertTrue("phases" in ret)
        self.assertEqual(ret["phases"][0], {"read": "fine"})

  class TestCreate(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()

      self.jobs.datastore = create_autospec(self.jobs.datastore)
      self.jobs.driverFor = create_autospec(self.jobs.driverFor)
      self.jobs.failed = create_autospec(self.jobs.failed)

      self.jobs.configuration = {
        "targets": {
          "default": {
            "scheduler": "mydefaultscheduler",
          },
          "myadapter": {
            "scheduler": "myscheduler"
          }
        }
      }

    def test_should_create_job_with_scheduler_from_config_when_target_argument_not_set(self):
      self.jobs.create({}, multihash(), multihash("sha1"))
      self.jobs.datastore.createJob.assert_called()

      args, kwargs = self.jobs.datastore.createJob.call_args_list[0]
      self.assertEqual(kwargs["scheduler"], "mydefaultscheduler")

    def test_should_create_job_with_scheduler_from_target_argument_over_config(self):
      self.jobs.create({}, multihash(), multihash("sha1"), target="myadapter")
      self.jobs.datastore.createJob.assert_called()

      args, kwargs = self.jobs.datastore.createJob.call_args_list[0]
      self.assertEqual(kwargs["scheduler"], "myscheduler")

    def test_should_create_job_with_empty_string_as_kind_when_builds_and_type_not_in_task(self):
      self.jobs.create({}, multihash(), multihash("sha1"))
      self.jobs.datastore.createJob.assert_called()

      args, kwargs = self.jobs.datastore.createJob.call_args_list[0]
      self.assertEqual(kwargs["kind"], "")

    def test_should_create_job_with_build_as_kind_when_builds_in_task(self):
      task = TaskFactory().create_task(["builds"])
      self.jobs.create(task, multihash(), multihash("sha1"))
      self.jobs.datastore.createJob.assert_called()

      args, kwargs = self.jobs.datastore.createJob.call_args_list[0]
      self.assertEqual(kwargs["kind"], "build")

    def test_should_create_job_with_run_as_kind_when_task_as_type(self):
      task = TaskFactory().create_task()
      self.jobs.create(task, multihash(), multihash("sha1"))
      self.jobs.datastore.createJob.assert_called()

      args, kwargs = self.jobs.datastore.createJob.call_args_list[0]
      self.assertEqual(kwargs["kind"], "run")

    def test_should_return_a_job_if_the_task_could_be_queued_but_not_run(self):
      """ When the job can't be queued, but can be created.
      """
      self.jobs.driverFor.return_value = None

      job = JobRecord()
      self.jobs.datastore.createJob.return_value = job

      ret = self.jobs.create({}, multihash(), multihash("sha1"))

      self.jobs.driverFor.assert_called()

      self.assertTrue(ret is job)

    def test_should_return_none_if_queuing_a_job_fails(self):
      """ When the job could be queued, but queueing failed.
      """
      # In this particular case, we want to see what the reference passed in
      # was.

      job = JobRecord()
      self.jobs.datastore.createJob.return_value = job
      fake_driver = Mock()
      fake_driver.queue.return_value = False
      self.jobs.driverFor.return_value = fake_driver

      ret = self.jobs.create({}, multihash(), multihash("sha1"))

      self.jobs.driverFor.assert_called()
      self.jobs.failed.assert_called_with(job)

      self.assertTrue(ret is None)

  class TestRetrieve(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()
      self.jobs.datastore = create_autospec(self.jobs.datastore)

      self.job = JobRecord()
      self.jobs.datastore.retrieveJobs.return_value = self.job

    def test_should_return_what_retrieveJobs_returns(self):
      ret = self.jobs.retrieve()
      self.assertEqual(ret, self.job)

  class TestPull(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()
      self.jobs.datastore = create_autospec(self.jobs.datastore)

      self.job = JobRecord()
      self.jobs.datastore.pullJob.return_value = self.job

    def test_should_return_what_pullJob_returns(self):
      ret = self.jobs.pull()
      self.assertEqual(ret, self.job)

  class TestFinished(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()
      self.jobs.datastore = create_autospec(self.jobs.datastore)
      self.jobs.jobFromId = create_autospec(self.jobs.jobFromId)

    def test_should_use_updateJob_to_set_job_status_to_finished(self):
      self.jobs.finished(Mock())
      self.jobs.datastore.updateJob.assert_called()
      args, kwargs = self.jobs.datastore.updateJob.call_args_list[0]
      self.assertEqual(kwargs["status"], "finished")

  class TestFailed(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()
      self.jobs.datastore = create_autospec(self.jobs.datastore)
      self.jobs.jobFromId = create_autospec(self.jobs.jobFromId)

    def test_should_use_updateJob_to_set_job_status_to_failed(self):
      self.jobs.failed(Mock())
      self.jobs.datastore.updateJob.assert_called()
      args, kwargs = self.jobs.datastore.updateJob.call_args_list[0]
      self.assertEqual(kwargs["status"], "failed")

  class TestPing(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()
      self.jobs.driverFor = create_autospec(self.jobs.driverFor)

    def test_should_return_true_when_drivers_ping_successfully_called(self):
      driver = Mock()
      self.jobs.driverFor.return_value = driver
      ret = self.jobs.ping(Mock())
      self.assertTrue(ret)
      driver.ping.assert_called()

    # FIXME: No record the status couldn't be reported.
    @pytest.mark.xfail
    def test_should_return_false_if_the_driver_for_the_job_is_missing(self):
      self.jobs.driverFor.return_value = None
      ret = self.jobs.ping(Mock())
      self.assertFalse(ret)

  class TestPersonFor(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()

      self.jobs.driverFor = create_autospec(self.jobs.driverFor)
      self.jobs.accounts = create_autospec(self.jobs.accounts)
      self.retrievedPerson = Mock(roles=['someRole'])

      self.jobs.accounts.retrievePerson.return_value = self.retrievedPerson

    def test_should_return_none_when_job_has_no_identity(self):
      job = JobRecord({'identity': None})

      ret = self.jobs.personFor(job)

      self.assertTrue(ret is None)
      self.jobs.accounts.retrievePerson.assert_not_called()

    def test_should_return_retrievePerson_return_value_without_roles_when_job_has_identity(self):
      person = Mock()
      job = JobRecord({'identity': person})

      ret = self.jobs.personFor(job)

      self.assertTrue(ret is self.retrievedPerson)
      self.assertTrue(ret.roles == [])
      self.jobs.accounts.retrievePerson.assert_called()

  class TestQueue(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()
      self.jobs.create = create_autospec(self.jobs.create)

      self.obj_id = multihash()
      self.revision_id = multihash(hashType="sha1")
      self.jobs.objects = ObjectManagerMock([
        {
          "id": self.obj_id,
          "revision": self.revision_id,
          "path": "/some/path",
          "type": "data",
          "name": "MyData"
        }
      ])
      self.jobs.configuration = {}

    def test_should_raise_typeerror_when_task_is_none(self):
      with self.assertRaises(TypeError):
        ret = self.jobs.queue(None, None, None)

    def test_should_return_job_id_when_create_is_successful(self):
      self.jobs.create.return_value = Mock(id=88)
      ret = self.jobs.queue(self.jobs.objects.retrieve(self.obj_id), None, None)
      self.assertEqual(ret, 88)

    def test_should_use_scheduler_from_config_when_argument_is_none(self):
      self.jobs.configuration = {
        "schedulers": {
          "defaultAdapter": "myscheduler"
        }
      }

      self.jobs.create.return_value = Mock(id=88)
      ret = self.jobs.queue(self.jobs.objects.retrieve(self.obj_id), None, None)

      args, kwargs = self.jobs.create.call_args_list[0]
      self.assertEqual(kwargs["target"], None)

    def test_should_use_scheduler_from_args_over_scheduler_from_config(self):
      self.jobs.configuration = {
        "targets": {
          "default": "myscheduler"
        }
      }

      self.jobs.create.return_value = Mock(id=88)
      ret = self.jobs.queue(
        self.jobs.objects.retrieve(self.obj_id), None, None, target="else"
      )

      args, kwargs = self.jobs.create.call_args_list[0]
      self.assertEqual(kwargs["target"], "else")

  class TestRun(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()
      self.jobs.accounts = create_autospec(self.jobs.accounts)
      self.jobs.taskFor = create_autospec(self.jobs.taskFor)
      self.jobs.create = create_autospec(self.jobs.create)
      self.jobs.deploy = create_autospec(self.jobs.deploy)
      self.jobs.updatePath = create_autospec(self.jobs.updatePath)
      self.jobs.execute = create_autospec(self.jobs.execute)

      self.obj_id = multihash()
      self.revision_id = multihash(hashType="sha1")

      self.task = TaskFactory().create_task()
      paths = {
        "task": "/some/path"
      }
      network = {}
      stdout = Mock()

      self.data = self.task, paths, network, stdout
      self.jobs.deploy.return_value = self.data

      self.jobs.objects = ObjectManagerMock([
        self.task
      ])

      self.jobs.execute.return_value = {
        "runReport": {}
      }

    @patch("os.path.exists", return_value=False)
    def test_should_return_none_and_not_call_deploy_when_no_task_found(self, exists):
      ret = self.jobs.run(Mock(path="/fake/path"))
      self.assertTrue(ret is None)
      self.jobs.deploy.assert_not_called()

    @patch("os.path.exists", return_value=False)
    def test_should_return_the_result_of_deploy_when_task_found(self, exists):
      self.jobs.taskFor.return_value = self.jobs.objects.retrieve(self.task["id"])
      ret = self.jobs.run(Mock(path="/fake/path"))
      self.jobs.deploy.assert_called()
      self.assertEqual(ret, self.data)

    @patch("os.path.exists", return_value=False)
    def test_should_not_execute_the_task_if_it_has_no_paths(self, exists):
      modified_data =  self.task, None, {}, Mock()
      self.jobs.deploy.return_value = modified_data

      self.jobs.taskFor.return_value = self.jobs.objects.retrieve(self.task["id"])
      ret = self.jobs.run(Mock(path="/fake/path"))

      self.jobs.execute.assert_not_called()
      self.jobs.deploy.assert_called()
      self.assertEqual(ret, modified_data)

  class TestPublicDeploy(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()
      self.jobs.deployBuild = create_autospec(self.jobs.deployBuild)
      self.jobs.deployRun = create_autospec(self.jobs.deployRun)

    def test_should_call_deployBuild_and_not_deployRun_if_builds_section_in_task(self):
      task = TaskFactory().create_task(["builds"])

      self.jobs.deploy(task, task["revision"])

      self.jobs.deployBuild.assert_called()
      self.jobs.deployRun.assert_not_called()

    def test_should_call_deployRun_and_not_deployBuild_if_runs_section_in_task(self):
      task = TaskFactory().create_task(["runs"])

      self.jobs.deploy(task, task["revision"])

      self.jobs.deployRun.assert_called()
      self.jobs.deployBuild.assert_not_called()

    def test_should_do_nothing_and_return_none_array_if_task_not_understood(self):
      task = TaskFactory().create_task()

      ret = self.jobs.deploy(task, task["revision"])

      self.jobs.deployRun.assert_not_called()
      self.jobs.deployBuild.assert_not_called()

      self.assertEqual(ret, [None, None, None, None])

  class TestSignal(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()
      self.jobs.backends = create_autospec(self.jobs.backends)

    def test_should_not_call_signal_if_task_is_none(self):
      self.jobs.signal(None)
      self.jobs.backends.signal.assert_not_called()

    def test_should_call_backend_signal_when_the_task_is_not_none(self):
      task = TaskFactory().create_task(["runs"])
      self.jobs.signal(task)
      self.jobs.backends.signal.assert_called()

  class TestTerminate(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()
      self.jobs.backends = create_autospec(self.jobs.backends)
      self.jobs.taskInfoFor = create_autospec(self.jobs.taskInfoFor)
      self.jobs.pidFor = create_autospec(self.jobs.pidFor)
      self.jobs.driverFor = create_autospec(self.jobs.driverFor)
      self.jobs.failed = create_autospec(self.jobs.failed)

    @patch("os.killpg")
    def test_should_report_failure_when_terminate_is_called(self, killpg):
      self.jobs.terminate(Mock())
      self.jobs.failed.assert_called()

    @patch("os.killpg")
    def test_should_try_to_kill_the_task_via_the_backend(self, killpg):
      self.jobs.backends.terminate.return_value = 0
      self.jobs.terminate(Mock())
      self.jobs.backends.terminate.assert_called()
      killpg.assert_not_called()

    @patch("os.killpg")
    def test_should_kill_scheduled_worker_for_task_unterminated_by_backend(self, killpg):
      self.jobs.backends.terminate.return_value = 1
      self.jobs.terminate(Mock())
      self.jobs.backends.terminate.assert_called()
      killpg.assert_called()

    @patch("os.killpg")
    def test_should_cancel_undeployed_jobs(self, killpg):
      self.jobs.taskInfoFor.return_value = None
      driver = Mock()
      self.jobs.driverFor.return_value = driver

      self.jobs.terminate(Mock())
      driver.cancel.assert_called()

  class TestPathForUUID(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()

      self.jobs.configuration = {
        "paths": {
          "mysubpath": "/my/sub/path"
        }
      }
      self.uuid = "835a76f0-3417-4ce3-866e-e91b6d33dbe6"

    @patch("occam.jobs.manager.Config")
    def test_should_return_none_when_configured_subpath_path_is_none(self, config):
      self.jobs.configuration = {
        "paths": {
          "runs": None
        }
      }
      config.root.return_value = "/root/path/"
      ret = self.jobs.pathForUUID(self.uuid)
      self.assertTrue(ret is None)

    @patch("occam.jobs.manager.Config")
    def test_should_use_config_root_path_as_base_path_when_subpath_not_in_config(self, config):
      self.jobs.configuration = {}
      config.root.return_value = "/root/path/"
      ret = self.jobs.pathForUUID(self.uuid)
      self.assertEqual(ret, f"/root/path/runs/83/5a/{self.uuid}")

    @patch("occam.jobs.manager.Config")
    def test_should_use_configured_path_when_available(self, config):
      self.jobs.configuration = {
        "paths": {
          "runs": "/runs/path"
        }
      }
      config.root.return_value = "/root/path/"
      ret = self.jobs.pathForUUID(self.uuid)
      self.assertEqual(ret, f"/runs/path/83/5a/{self.uuid}")

  class TestCreatePathFor(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()
      self.jobs.pathForUUID = create_autospec(self.jobs.pathForUUID)

      self.uuid = "835a76f0-3417-4ce3-866e-e91b6d33dbe6"

      self.jobs.pathForUUID.return_value = "/path"

    @patch("os.makedirs")
    def test_should_return_the_result_of_pathForUUID(self, makedirs):
      ret = self.jobs.createPathFor(self.uuid)
      self.assertEqual(ret, "/path")

    @patch("os.makedirs")
    def test_should_call_makedirs_for_path(self, makedirs):
      self.jobs.createPathFor(self.uuid)
      makedirs.assert_called()

    @patch("os.makedirs")
    def test_should_return_none_if_uuid_is_none(self, makedirs):
      ret = self.jobs.createPathFor(None)
      makedirs.assert_not_called()
      self.assertTrue(ret is None)

  class TestCleanupTaskOutputFilenames(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()

      self.existing_paths = [
        "/path/task/objects/1/stdout",
        "/path/task/objects/1/stderr",
      ]

      def create_ce(self, extra_existing_paths=[]):
        existing_paths = self.existing_paths
        existing_paths.extend(extra_existing_paths)
        def conditional_exist(path):
          if (path in existing_paths):
            return True
          return False
        return conditional_exist
      self.create_ce = create_ce

    # FIXME: 'NoneType' object is not subscriptable.
    @pytest.mark.xfail
    @patch("os.path.exists")
    @patch("os.rename")
    def test_should_do_nothing_if_no_runs_or_builds_section_in_task(self, rename, exists):

      exists.side_effect = self.create_ce()
      task = TaskFactory().create_task()
      self.jobs.cleanupTaskOutputFilenames(task, "/path")
      rename.assert_not_called()

    @patch("os.path.exists")
    @patch("os.rename")
    def test_should_append_ordinal_value_to_output_stream_filenames_when_runs_section_in_task(self, rename, exists):
      exists.side_effect = self.create_ce()
      task = TaskFactory().create_task(["runs"])
      self.jobs.cleanupTaskOutputFilenames(task, "/path")
      rename.assert_called()
      rename.assert_any_call(
        "/path/task/objects/1/stdout", "/path/task/objects/1/stdout.0"
      )
      rename.assert_any_call(
        "/path/task/objects/1/stderr", "/path/task/objects/1/stderr.0"
      )

    @patch("os.path.exists")
    @patch("os.rename")
    def test_should_append_ordinal_value_to_output_stream_filenames_when_builds_section_in_task(self, rename, exists):
      exists.side_effect = self.create_ce()
      task = TaskFactory().create_task(["builds"])
      self.jobs.cleanupTaskOutputFilenames(task, "/path")
      rename.assert_called()
      rename.assert_any_call(
        "/path/task/objects/1/stdout", "/path/task/objects/1/stdout.0"
      )
      rename.assert_any_call(
        "/path/task/objects/1/stderr", "/path/task/objects/1/stderr.0"
      )

    @patch("os.path.exists")
    @patch("os.rename")
    def test_should_skip_existing_file_names_and_increment_past_them(self, rename, exists):
      exists.side_effect = self.create_ce(
        [
          "/path/task/objects/1/stdout.0",
          "/path/task/objects/1/stderr.0"
        ]
      )
      task = TaskFactory().create_task(["builds"])
      self.jobs.cleanupTaskOutputFilenames(task, "/path")
      rename.assert_called()

      renamed_over_existing = False

      # Ensure the existing paths aren't overwritten.
      try:
        rename.assert_any_call(
          "/path/task/objects/1/stdout", "/path/task/objects/1/stdout.0"
        )
        renamed_over_existing = True
      except AssertionError:
        pass

      try:
        rename.assert_any_call(
          "/path/task/objects/1/stderr", "/path/task/objects/1/stderr.0"
        )
        renamed_over_existing = True
      except AssertionError:
        pass

      self.assertFalse(renamed_over_existing)

      rename.assert_any_call(
        "/path/task/objects/1/stdout", "/path/task/objects/1/stdout.1"
      )
      rename.assert_any_call(
        "/path/task/objects/1/stderr", "/path/task/objects/1/stderr.1"
      )

  class TestPrepareTaskObjectPath(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()
      self.jobs.objects = Mock()
      self.jobs.storage = create_autospec(self.jobs.storage)

    @patch("os.path.lexists", return_value=False)
    @patch("os.symlink")
    @patch("os.makedirs")
    @patch("os.path.exists", return_value=False)
    def test_should_create_the_directories_and_symlinks_needed_to_store_the_input_and_output(self, exists, makedirs, symlink, lexists):
      task = TaskFactory().create_task(["index"], index=2)
      with patch("builtins.open", mock_open()) as mock_handle:
        self.jobs._prepareTaskObjectPath(task, "/path")

        makedirs.assert_any_call("/path/objects/2", exist_ok=True)
        makedirs.assert_any_call("/path/objects/2/inputs", exist_ok=True)
        makedirs.assert_any_call("/path/objects/2/outputs", exist_ok=True)
        symlink.assert_any_call("inputs/0/0", "/path/objects/2/input")
        symlink.assert_any_call("outputs/0/0", "/path/objects/2/output")

    @patch("os.path.lexists", return_value=False)
    @patch("os.symlink")
    @patch("os.makedirs")
    @patch("os.path.exists", return_value=False)
    def test_should_call_objects_install_when_building_a_non_local_object(self, exists, makedirs, symlink, lexists):
      task_structure = {
        "builds": {
          "index": 2
        }
      }
      task = TaskFactory(task_structure).create_task(["builds", "index"], index=2)
      with patch("builtins.open", mock_open()) as mock_handle:
        self.jobs._prepareTaskObjectPath(task, "/path", root=task, build=True)

        self.jobs.objects.install.assert_called()

    @patch("os.path.lexists", return_value=False)
    @patch("os.symlink")
    @patch("os.makedirs")
    @patch("os.path.exists", return_value=False)
    def test_should_create_the_output_directory_structure_when_outputs_in_task(self, exists, makedirs, symlink, lexists):
      task_structure = {
        "outputs": [
          {},
          {}
        ]
      }
      task = TaskFactory(task_structure).create_task(["index"], index=2)
      with patch("builtins.open", mock_open()) as mock_handle:
        self.jobs._prepareTaskObjectPath(task, "/path")

        makedirs.assert_any_call("/path/objects/2/outputs/0", exist_ok=True)
        makedirs.assert_any_call("/path/objects/2/outputs/0/0", exist_ok=True)
        makedirs.assert_any_call("/path/objects/2/outputs/1", exist_ok=True)
        makedirs.assert_any_call("/path/objects/2/outputs/1/0", exist_ok=True)

    @patch("os.path.lexists", return_value=False)
    @patch("os.symlink")
    @patch("os.makedirs")
    @patch("os.path.exists", return_value=False)
    def test_should_create_the_input_directory_structure_when_inputs_in_task(self, exists, makedirs, symlink, lexists):
      task_structure = {
        "inputs": [
          {
            "connections": {
              1: {
                "paths": {
                  "mount": "/some/path/A",
                },
              },
            },
          },
          {
            "connections": {
              2: {
                "paths": {
                  "mount": "/some/path/B",
                },
              },
            },
          },
        ],
      }
      task = TaskFactory(task_structure).create_task(["index"], index=2)
      with patch("builtins.open", mock_open()) as mock_handle:
        self.jobs._prepareTaskObjectPath(task, "/path")

        makedirs.assert_any_call("/path/objects/2/inputs/0", exist_ok=True)
        symlink.assert_any_call("/some/path/A", "/path/objects/2/inputs/0/0")
        makedirs.assert_any_call("/path/objects/2/inputs/1", exist_ok=True)
        symlink.assert_any_call("/some/path/B", "/path/objects/2/inputs/1/0")

    @patch("os.path.lexists", return_value=True)
    @patch("os.symlink")
    @patch("os.makedirs")
    @patch("os.path.exists", return_value=False)
    def test_should_make_directories_for_inputs_not_linked_from_upstream_objects(self, exists, makedirs, symlink, lexists):
      # FIXME: This should come from the TaskFactory. Unfortunately, this test
      # requires that the connections objects are missing either an id,
      # revision, or paths.
      task = {
        "index": 2,
        "inputs": [
          {
            "connections": [
              {
              }
            ]
          },
          {
            "connections": [
              {
              }
            ]
          }
        ]
      }

      with patch("builtins.open", mock_open()) as mock_handle:
        self.jobs._prepareTaskObjectPath(task, "/path")

        makedirs.assert_any_call("/path/objects/2/inputs/0", exist_ok=True)
        makedirs.assert_any_call("/path/objects/2/inputs/0/0", exist_ok=True)
        makedirs.assert_any_call("/path/objects/2/inputs/1", exist_ok=True)
        makedirs.assert_any_call("/path/objects/2/inputs/1/0", exist_ok=True)

    @patch("os.path.lexists", return_value=True)
    @patch("os.symlink")
    @patch("os.makedirs")
    @patch("os.path.exists", return_value=False)
    def test_should_write_task_so_it_can_be_read_back_as_json(self, exists, makedirs, symlink, lexists):
      import json

      task = TaskFactory().create_task(["index"], index=2)

      file_writes = {}
      def write_collector(path, mode):
        return Writer(path, file_writes)

      with patch("builtins.open", mock_open()) as mock_handle:
        mock_handle.side_effect = write_collector
        self.jobs._prepareTaskObjectPath(task, "/path")

        read_task = file_writes["/path/objects/2/task.json"]
        read_task = "".join(read_task)
        read_task = json.loads(read_task)
        self.assertEqual(read_task, task)

  class TestPrepareTaskPath(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()
      self.jobs._prepareTaskObjectPath = create_autospec(
        self.jobs._prepareTaskObjectPath
      )

    @patch("os.makedirs")
    @patch("os.path.exists", return_value=False)
    def test_should_create_root_and_objects_dirs_and_events_file(self, exists, makedirs):
      with patch("builtins.open", mock_open()) as mock_handle:
        task = TaskFactory().create_task()
        self.jobs.prepareTaskPath(task, "/path")

        makedirs.assert_any_call("/path/root", exist_ok=True)
        makedirs.assert_any_call("/path/objects", exist_ok=True)
        mock_handle.assert_any_call("/path/events", WriteModeMatch())

    @patch("os.makedirs")
    @patch("os.path.exists", return_value=False)
    def test_shouldnt_prepare_subobjects_when_the_task_doesnt_have_an_index(self, exists, makedirs):
      with patch("builtins.open", mock_open()) as mock_handle:
        task = TaskFactory().create_task(["builds"])
        self.jobs.prepareTaskPath(task, "/path")

        self.jobs._prepareTaskObjectPath.assert_not_called()

    @patch("os.makedirs")
    @patch("os.path.exists", return_value=False)
    def test_should_prepare_task_object_path_when_index_and_run_in_task(self, exists, makedirs):
      with patch("builtins.open", mock_open()) as mock_handle:
        task = TaskFactory().create_task(["index", "run"])

        self.jobs.prepareTaskPath(task, "/path")

        self.jobs._prepareTaskObjectPath.assert_called()
        args, kwargs = self.jobs._prepareTaskObjectPath.call_args_list[0]
        self.assertEqual(
          args[0],
          task
        )

    @patch("os.makedirs")
    @patch("os.path.exists", return_value=False)
    def test_should_prepare_task_subobject_paths_when_run_and_index_in_subpath(self, exists, makedirs):
      with patch("builtins.open", mock_open()) as mock_handle:
        task_structure = {
          "inputs": [
            {
              "connections": {
                1: {
                  "run": {}
                }
              }
            }
          ]
        }
        task = TaskFactory(task_structure).create_task()

        self.jobs.prepareTaskPath(task, "/path")

        self.jobs._prepareTaskObjectPath.assert_called()
        calls = self.jobs._prepareTaskObjectPath.call_args_list
        called_with_sub_object = False
        for c in calls:
          try:
            self.assertTrue(c[0][0] == task["inputs"][0]["connections"][0])
            called_with_sub_object = True
          except AssertionError as e:
            pass

        self.assertTrue(called_with_sub_object)

  class TestBuildPathForOwner(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()

      self.obj_id = multihash()
      self.obj_uid = multihash()
      self.obj_revision = multihash(hashType="sha1")

      objs = [
        {
          "id": self.obj_id,
          "uid": self.obj_uid,
          "revision": self.obj_revision,
          "path": "/some/path",
          "type": "data",
          "name": "MyData"
        }
      ]

      self.jobs.objects = ObjectManagerMock(objs)
      self.buildId = multihash()

      self.jobs.builds = BuildManagerMock([(objs[0], [
        {
          "id": self.buildId,
          "revision": multihash(hashType="sha1")
        }
      ],)])

    def test_should_return_path_and_buildId(self):
      path, buildId = self.jobs._buildPathForOwner(
        obj=self.jobs.objects.retrieve(self.obj_id),
        revision=self.obj_revision,
        owner_uid=self.obj_uid,
        cache=False
      )
      self.assertEqual(path, f"/build/path/for/{self.obj_uid}/{buildId}")
      self.assertEqual(buildId, self.buildId)

    def test_should_return_none_tuple_when_builds_not_found(self):
      self.jobs.objects = ObjectManagerMock([])
      path, buildId = self.jobs._buildPathForOwner(
        obj=self.jobs.objects.retrieve(self.obj_id),
        revision=self.obj_revision,
        owner_uid=self.obj_uid,
        cache=False
      )
      self.assertTrue(path is None)
      self.assertTrue(buildId is None)

  class TestPathForBuildTask(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()
      self.jobs.storage = create_autospec(self.jobs.storage)

      self.obj_id = multihash()
      self.obj_uid = multihash()
      self.obj_revision = multihash(hashType="sha1")
      objs = [
        {
          "id": self.obj_id,
          "uid": self.obj_uid,
          "revision": self.obj_revision,
          "path": "/some/path",
          "type": "data",
          "name": "MyData"
        }
      ]

      self.buildId = multihash()

      self.jobs.objects = ObjectManagerMock(objs)
      self.jobs.builds = BuildManagerMock([(objs[0], [
        {
          "id": self.buildId,
          "revision": multihash(hashType="sha1")
        }
      ],)])

    # TODO: Fix NoneType has no len().
    #@pytest.mark.xfail
    def test_should_return_dict_with_none_values_when_no_builds_section_exists(self):
      self.jobs.storage.buildPathFor.return_value = None

      ret = self.jobs._pathForBuildTask({}, {}, self.obj_id, self.obj_revision)
      self.assertEqual(ret, {"buildMount": None, "buildPath": None})

    def test_should_fall_back_on_task_localMount_when_no_provides_section_exists(self):
      task_structure = {
        "builds": {
          "paths": {}
        }
      }
      task = TaskFactory(task_structure).create_task()
      ret = self.jobs._pathForBuildTask(task, task, self.obj_uid, self.obj_revision)
      self.assertEqual(
        ret,
        {
          "buildMount": f"{task['builds']['paths']['localMount']}/build",
          "buildPath": f"/build/path/for/{self.obj_uid}/{task.get('id')}"
        }
      )

    def test_should_use_the_task_builds_paths_local_path_when_a_provides_section_exists(self):
      task_structure = {
        "builds": {
          "paths": {}
        }
      }
      task = TaskFactory(task_structure).create_task(["provides"])
      ret = self.jobs._pathForBuildTask(task, task, self.obj_uid, self.obj_revision)
      self.assertEqual(
        ret,
        {
          "buildMount": f"{task['builds']['paths']['local']['x86']['dos']}/build",
          "buildPath": f"/build/path/for/{self.obj_uid}/{task.get('id')}"
        }
      )

    def test_should_get_the_build_path_from_storage_buildPathFor_when_not_given_as_an_argument(self):
      task_structure = {
        "builds": {
          "paths": {}
        }
      }
      task = TaskFactory(task_structure).create_task(["provides"])
      root = task
      ret = self.jobs._pathForBuildTask(task, root, self.obj_uid, self.obj_revision, buildPath="/build/path")
      self.assertEqual(
        ret,
        {
          "buildMount": f"{task['builds']['paths']['localMount']}/build",
          "buildPath": "/build/path"
        }
      )

  class TestPathsForObject(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()
      self.jobs._buildPathForOwner = create_autospec(self.jobs._buildPathForOwner)
      self.jobs._pathForBuildTask = create_autospec(self.jobs._pathForBuildTask)

      self.obj_id = multihash()
      self.obj_uid = multihash()
      self.obj_revision = multihash(hashType="sha1")
      self.jobs.objects = ObjectManagerMock([
        {
          "id": self.obj_id,
          "uid": self.obj_uid,
          "revision": self.obj_revision,
          "path": "/some/path",
          "type": "data",
          "name": "MyData"
        }
      ])
      self.jobs._buildPathForOwner.return_value = None, None
      self.jobs._pathForBuildTask.return_value = {
        "buildMount": "/build/mount/path",
        "buildPath": "/build/path/path",
      }

    def test_should_raise_a_RuntimeError_when_unable_to_retrieve_the_object(self):
      with self.assertRaises(RuntimeError):
        self.jobs.pathsForObject({}, {})

    def test_should_return_path_info_for_simple_input_object(self):
      inputObject = {
        "id": self.obj_id,
        "revision": self.obj_revision,
      }
      task = TaskFactory().create_task()
      ret = self.jobs.pathsForObject(inputObject, task)
      self.assertEqual(
        ret,
        {
          "id": self.obj_id,
          "revision": self.obj_revision,
          "index": None,
          "mount": None,
          "path": f"/instantiated/path/for/{self.obj_uid}"
        }
      )

    def test_should_return_path_info_for_simple_input_object_with_builds(self):
      inputObject = {
        "id": self.obj_id,
        "revision": self.obj_revision,
        "index": 1,
      }
      task = TaskFactory().create_task(["builds"])
      ret = self.jobs.pathsForObject(inputObject, task)
      self.assertEqual(
        ret,
        {
          "id": self.obj_id,
          "revision": self.obj_revision,
          "index": 1,
          "mount": None,
          "path": f"/instantiated/path/for/{self.obj_uid}",
          "buildMount": "/build/mount/path",
          "buildPath": "/build/path/path",
        }
      )

    def test_should_return_path_info_for_simple_input_object_with_runs(self):
      inputObject = {
        "id": self.obj_id,
        "revision": self.obj_revision,
        "index": 1,
      }

      task = TaskFactory().create_task(["runs"])
      ret = self.jobs.pathsForObject(inputObject, task)
      self.assertEqual(
        ret,
        {
          "id": self.obj_id,
          "revision": self.obj_revision,
          "index": 1,
          "mount": None,
          "path": f"/instantiated/path/for/{self.obj_uid}"
        }
      )

      ret = self.jobs.pathsForObject(inputObject, task, buildPath="/should/see")
      self.assertEqual(
        ret,
        {
          "id": self.obj_id,
          "revision": self.obj_revision,
          "index": 1,
          "mount": None,
          "path": "/should/see"
        }
      )

      inputObject["index"] = 2
      ret = self.jobs.pathsForObject(inputObject, task, buildPath="/shouldnt/see")
      self.assertEqual(
        ret,
        {
          "id": self.obj_id,
          "revision": self.obj_revision,
          "index": 2,
          "mount": None,
          "path": f"/instantiated/path/for/{self.obj_uid}"
        }
      )

  class TestPathsForTask(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()
      self.jobs.pathsForObject = create_autospec(self.jobs.pathsForObject)

      self.jobs.pathsForObject.return_value = {}
      self.subobject_count = 6

    def test_should_return_empty_dict_for_task_with_no_inputs_or_running(self):
      task = TaskFactory().create_task()
      ret = self.jobs.pathsForTask(task)
      self.assertEqual(ret, {})

    def test_should_find_all_inputs_subobject_paths(self):
      # Create a task that has 6 input objects
      # The number of input objects is a bit confusing if you just look at the
      # count and the depth (or the dictionary for that matter).
      # 2^1 + 2^2 + ... + 2^depth
      #     t
      #    / \   # for i in task[inputs][0][connections]
      #   i   i
      #  / \ / \ # for i in i[inputs][connections]
      #  i i i i
      task_structure = {
        "inputs": [
          {
            "connections": {
              1: {
                "inputs": [
                  {
                    "connections": {
                      2: {"inputs": []},
                      3: {"inputs": []},
                    },
                  },
                ],
              },
              4: {
                "inputs": [
                  {
                    "connections": {
                      5: {"inputs": []},
                      6: {"inputs": []},
                    },
                  },
                ],
              },
            },
          },
        ],
      }
      task = TaskFactory(task_structure).create_task(
        ["inputs"],
      )

      # Make it possible for pathsForObject() to return distinct results for
      # each inputObject so we can check the outputs.
      pfos = {}
      for i in [i + 1 for i in range(self.subobject_count)]:
        pfos[i] = Mock()

      def get_pfo(inputObject, *args, **kwargs):
        return pfos[inputObject["index"]]

      self.jobs.pathsForObject.side_effect = get_pfo

      ret = self.jobs.pathsForTask(task)

      # Ensure that ret has all of the indexes.
      self.assertEqual(len(ret), self.subobject_count)
      for i in [i + 1 for i in range(self.subobject_count)]:
        self.assertTrue(ret[i] is pfos[i])

    def test_should_find_all_running_subobject_paths(self):
      # FIXME: Not sure if this structure is remotely realistic. Not sure what the
      # indexes should be, but in the tests thus far it hasn't mattered as long
      # as certain indexes are unique.
      task_structure = {
        "running": {
          1: {
            "objects": {
              1: {
                "running": {
                  1: {
                    "objects": {
                      2: {"running": {}},
                    },
                  },
                  3: {
                    "objects": {
                      3: {"running": {}},
                    },
                  },
                },
              },
            },
          },
          2: {
            "objects": {
              4: {
                "running": {
                  5: {
                    "objects": {
                      5: {"running": {}},
                    },
                  },
                  6: {
                    "objects": {
                      6: {"running": {}}
                    },
                  },
                },
              },
            },
          },
        }
      }
      task = TaskFactory(task_structure).create_task(
        ["running"],
      )

      # Make it possible for pathsForObject() to return distinct results for
      # each inputObject so we can check the outputs.
      pfos = {}
      for i in [i + 1 for i in range(self.subobject_count)]:
        pfos[i] = Mock()

      def get_pfo(inputObject, *args, **kwargs):
        return pfos[inputObject["index"]]

      self.jobs.pathsForObject.side_effect = get_pfo

      ret = self.jobs.pathsForTask(task)

      # Ensure that ret has all of the indexes.
      self.assertEqual(len(ret), self.subobject_count)
      for i in [i + 1 for i in range(self.subobject_count)]:
        self.assertTrue(ret[i] is pfos[i])

    def test_should_find_all_mixed_subobject_paths(self):
      # Create a task that has 6 input objects
      # The number of input objects is a bit confusing if you just look at the
      # count and the depth (or the dictionary for that matter).
      # 2^1 + 2^2 + ... + 2^depth
      #     t
      #    / \   # for i in task[inputs][0][connections]
      #   i   i
      #  / \ / \ # for i in i[inputs][connections]
      #  i i i i
      task_structure = {
        "inputs": [
          {
            "connections": {
              1: {
                "running": {
                  1: {
                    "objects": {
                      2: {"inputs": []},
                      3: {"inputs": []},
                    },
                  },
                },
              },
              4: {
                "inputs": [
                  {
                    "connections": {
                      5: {"inputs": []},
                      6: {"inputs": []},
                    },
                  },
                ],
              },
            },
          },
        ]
      }
      task = TaskFactory(task_structure).create_task()

      # Make it possible for pathsForObject() to return distinct results for
      # each inputObject so we can check the outputs.
      pfos = {}
      for i in [i + 1 for i in range(self.subobject_count)]:
        pfos[i] = Mock()

      def get_pfo(inputObject, *args, **kwargs):
        return pfos[inputObject["index"]]

      self.jobs.pathsForObject.side_effect = get_pfo

      ret = self.jobs.pathsForTask(task)

      # Ensure that ret has all of the indexes.
      self.assertEqual(len(ret), self.subobject_count)
      for i in [i + 1 for i in range(self.subobject_count)]:
        self.assertTrue(ret[i] is pfos[i])

  class TestInstallNetwork(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()
      self.jobs.network = create_autospec(self.jobs.network)

      self.jobs.network.allocatePort.return_value = None

    def test_should_return_a_dict_structure_with_an_empty_ports_list_when_no_network_in_task(self):
      task = TaskFactory().create_task()
      ret = self.jobs.installNetwork(task)
      self.assertTrue("ports" in ret)
      self.assertEqual(len(ret["ports"]), 0)

    def test_should_return_structure_with_list_of_allocated_ports_based_on_network_in_task(self):
      self.jobs.network.allocatePort.return_value = 99

      task_structure = {
        "network": {
          "ports": [
            {"bind": 88}
          ]
        }
      }
      task = TaskFactory(task_structure).create_task()
      ret = self.jobs.installNetwork(task)
      self.assertTrue("ports" in ret)
      self.assertEqual(len(ret["ports"]), 1)
      self.assertEqual(ret["ports"][0], {"bind": 88, "port": 99})

    def test_should_modify_the_task_network_structure_to_include_the_allocated_port(self):
      self.jobs.network.allocatePort.return_value = 99

      task_structure = {
        "network": {
          "ports": [
            {"bind": 88}
          ]
        }
      }
      task = TaskFactory(task_structure).create_task()

      self.jobs.installNetwork(task)
      self.assertEqual(len(task["network"]["ports"]), 1)
      self.assertEqual(task["network"]["ports"][0], {"bind": 88, "port": 99})

    def test_should_allocate_and_return_structure_including_subtask_port_information(self):
      self.jobs.network.allocatePort.side_effect = [98, 99]

      task_structure = {
        "inputs": [
          {
            "connections": {
              1: {
                "network": {
                  "ports": [
                    {"bind": 88}
                  ]
                }
              },
            },
          },
        ],
        "running": {
          1: {
            "objects": {
              1: {
                "network": {
                  "ports": [
                    {"bind": 89}
                  ]
                }
              },
            },
          },
        },
      }

      task = TaskFactory(task_structure).create_task()

      ret = self.jobs.installNetwork(task)
      self.assertEqual(len(ret["ports"]), 2)
      self.assertEqual(ret["ports"][0], {"bind": 88, "port": 98})
      self.assertEqual(ret["ports"][1], {"bind": 89, "port": 99})

    def test_should_modify_the_subtask_structure_to_include_allocated_ports(self):
      self.jobs.network.allocatePort.side_effect = [98, 99]

      task_structure = {
        "inputs": [
          {
            "connections": {
              1: {
                "network": {
                  "ports": [
                    {"bind": 88}
                  ]
                }
              },
            },
          },
        ],
        "running": {
          1: {
            "objects": {
              1: {
                "network": {
                  "ports": [
                    {"bind": 89}
                  ]
                }
              },
            },
          },
        },
      }

      task = TaskFactory(task_structure).create_task()

      self.jobs.installNetwork(task)
      self.assertEqual(len(task["inputs"][0]["connections"][0]["network"]["ports"]), 1)
      self.assertEqual(len(task["running"][0]["objects"][0]["network"]["ports"]), 1)
      self.assertEqual(
        task["inputs"][0]["connections"][0]["network"]["ports"][0],
        {"bind": 88, "port": 98}
      )
      self.assertEqual(
        task["running"][0]["objects"][0]["network"]["ports"][0],
        {"bind": 89, "port": 99}
      )

  class TestUninstallNetwork(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()
      self.jobs.network = create_autospec(self.jobs.network)

      self.jobs.network.freePort.return_value = None

    def test_should_do_nothing_if_there_is_no_port_to_free(self):
      self.jobs.uninstallNetwork(
        {"ports": [{"bind": 89}]}
      )
      self.jobs.uninstallNetwork(
        {"ports": []}
      )
      self.jobs.uninstallNetwork(
        {}
      )
      self.jobs.network.freePort.assert_not_called()

    def test_should_call_freePort_on_the_port_in_the_network_dict(self):
      self.jobs.uninstallNetwork(
        {"ports": [{"bind": 89, "port": 99}]}
      )
      self.jobs.network.freePort.assert_called_with(99)

  class TestInstallTask(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()
      self.jobs.linkTaskDirectory = create_autospec(self.jobs.linkTaskDirectory)
      self.jobs.copyTaskDirectory = create_autospec(self.jobs.copyTaskDirectory)

    def test_should_do_nothing_when_no_index_inputs_or_running_in_task(self):
      task = TaskFactory().create_task()
      self.jobs.installTask("/some/path", task)

      self.jobs.linkTaskDirectory.assert_not_called()
      self.jobs.copyTaskDirectory.assert_not_called()

    def test_should_call_linkTaskDirectory_for_link_info_in_task_init_link_section(self):
      task_structure = {
        "init": {
          "link": [{"source": "abc123", "to": "/my/to/path"}],
        },
        "paths": {
          "mount": "/my/path"
        }
      }
      task = TaskFactory(task_structure).create_task(["index"])
      paths = {
        1: {
          "path": "/arg/path"
        }
      }

      self.jobs.installTask("/some/path", task, paths=paths)

      self.jobs.linkTaskDirectory.assert_called_with(
        "/some/path", "/arg/path/abc123", "/my/path/abc123", "/my/to/path"
      )
      self.jobs.copyTaskDirectory.assert_not_called()

    def test_should_call_copyTaskDirectory_for_copy_info_in_task_init_copy_section(self):
      task_structure = {
        "init": {
          "copy": [{"source": "abc123", "to": "/my/to/path"}],
        },
        "paths": {
          "mount": "/my/path"
        }
      }
      task = TaskFactory(task_structure).create_task(["index"])
      paths = {
        1: {
          "path": "/arg/path"
        }
      }

      self.jobs.installTask("/some/path", task, paths=paths)

      self.jobs.linkTaskDirectory.assert_not_called()
      self.jobs.copyTaskDirectory.assert_called_with(
        "/some/path", "/arg/path/abc123", "/my/path/abc123", "/my/to/path"
      )

    def test_should_recursively_install_for_input_subtasks(self):
      task_structure = {
        "inputs": [
          {
            "connections": {
              2: {
                "init": {
                  "copy": [{"source": "abc123", "to": "/my/to/path"}],
                  "link": [{"source": "abc123", "to": "/my/to/path"}],
                },
                "paths": {
                  "mount": "/my/path"
                },
                "provides": {},
                "index": {}
              }
            }
          }
        ]
      }
      task = TaskFactory(task_structure).create_task()

      paths = {
        2: {
          "path": "/arg/path"
        }
      }

      self.jobs.installTask("/some/path", task, paths=paths)

      self.jobs.linkTaskDirectory.assert_called_with(
        "/some/path", "/arg/path/abc123", "/my/path/abc123", "/my/to/path"
      )
      self.jobs.copyTaskDirectory.assert_called_with(
        "/some/path", "/arg/path/abc123", "/my/path/abc123", "/my/to/path"
      )

    def test_should_recursively_install_for_running_subtasks(self):
      task_structure = {
        "running": {
          1: {
            "objects": {
              2: {
                "init": {
                  "copy": [{"source": "abc123", "to": "/my/to/path"}],
                  "link": [{"source": "abc123", "to": "/my/to/path"}],
                },
                "paths": {
                  "mount": "/my/path"
                },
                "provides": {},
                "index": {}
              }
            }
          }
        }
      }
      task = TaskFactory(task_structure).create_task()

      paths = {
        2: {
          "path": "/arg/path"
        }
      }

      self.jobs.installTask("/some/path", task, paths=paths)

      self.jobs.linkTaskDirectory.assert_called_with(
        "/some/path", "/arg/path/abc123", "/my/path/abc123", "/my/to/path"
      )
      self.jobs.copyTaskDirectory.assert_called_with(
        "/some/path", "/arg/path/abc123", "/my/path/abc123", "/my/to/path"
      )

    def test_should_use_paths_volume_dictionary_for_install_path_when_provided(self):
      task_structure = {
        "init": {
          "link": [{"source": "abc123", "to": "/my/to/path"}],
        },
        "paths": {
          "mount": "/my/path"
        }
      }
      task = TaskFactory(task_structure).create_task(["index", "provides"])
      paths = {
        1: {
          "path": "/arg/path"
        }
      }

      self.jobs.installTask("/some/path", task, paths=paths)

      self.jobs.linkTaskDirectory.assert_called_with(
        "/some/path", "/arg/path/abc123",
        f"/occam/{task['id']}-{task['revision']}/abc123", "/my/to/path"
      )
      self.jobs.copyTaskDirectory.assert_not_called()

  class TestCopyTaskDirectory(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()

    # FIXME: Setup pyfakefs for these tests. We shouldn't be so specific about
    # file I/O behavior, just results.
    def test_me(self):
      pass

  class TestLinkTaskDirectory(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()

    # FIXME: Setup pyfakefs for these tests. We shouldn't be so specific about
    # file I/O behavior, just results.
    def test_me(self):
      pass

  class TestGetObjectFromTask(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()

    def test_should_return_none_if_the_uuid_is_not_valid(self):
      task = TaskFactory().create_task()
      ret = self.jobs.getObjectFromTask(task, "not a uuid", "not a revision")
      self.assertTrue(ret is None)

    def test_should_return_none_if_a_matching_object_is_not_found(self):
      task = TaskFactory().create_task()
      ret = self.jobs.getObjectFromTask(
        task=task,
        uuid="QmSCPa5WRNfBNQqVdStqJTnfSMBUk9y2LyF9V15mRvBQV4",
        revision="5dr5n9KoapbxNZ4qibVuUffGnLSgyF"
      )
      self.assertTrue(ret is None)

    def test_should_return_a_matching_running_object(self):
      task_structure = {
        "running": {
          1: {
            "objects": {
              2: {}
            }
          }
        }
      }
      task = TaskFactory(task_structure).create_task()
      running_id = task["running"][0]["objects"][0]["id"]
      running_rev = task["running"][0]["objects"][0]["revision"]
      running_obj = task["running"][0]["objects"][0]
      ret = self.jobs.getObjectFromTask(
        task=task,
        uuid=running_id,
        revision=running_rev
      )

    # FIXME: Unable to find objects under inputs.
    # getObjectFromTask() doesn't look in the connections structure, and I'm
    # not sure if it should. However up until now I have not seen any tasks
    # that have objects or running in the input structure array.
    @pytest.mark.xfail
    def test_should_return_a_matching_inputs_object(self):
      task_structure = {
        "inputs": [
          {
            "connections": {
              2: {}
            }
          }
        ]
      }
      task = TaskFactory(task_structure).create_task()
      running_id = task["inputs"][0]["connections"][0]["id"]
      running_rev = task["inputs"][0]["connections"][0]["revision"]
      running_obj = task["inputs"][0]["connections"][0]
      ret = self.jobs.getObjectFromTask(
        task=task,
        uuid=running_id,
        revision=running_rev
      )
      self.assertEqual(ret, running_obj)

    def test_should_return_a_matching_heavily_nested_object(self):
      task_structure = {
        "running": {
          1: {
            "objects": {
              2: {
                "running": {
                  3: {
                    "objects": {
                      4: {
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      task = TaskFactory(task_structure).create_task()
      ro = task["running"][0]["objects"][0]
      running_id = ro["running"][0]["objects"][0]["id"]
      running_rev = ro["running"][0]["objects"][0]["revision"]
      running_obj = ro["running"][0]["objects"][0]
      ret = self.jobs.getObjectFromTask(
        task=task,
        uuid=running_id,
        revision=running_rev
      )
      self.assertEqual(ret, running_obj)

  class TestJobFromId(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()
      self.jobs.retrieve = create_autospec(self.jobs.retrieve)

    def test_should_lazily_return_the_argument_if_it_is_a_JobRecord(self):
      jr = JobRecord()
      ret = self.jobs.jobFromId(jr)
      self.assertEqual(ret, jr)

    def test_should_return_the_first_job_record_found_from_the_id(self):
      jr1 = JobRecord()
      jr2 = JobRecord()
      self.jobs.retrieve.return_value = [jr1, jr2]

      ret = self.jobs.jobFromId(1)

      self.assertEqual(ret, jr1)

  class TestTaskFor(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()

      self.jobs.jobFromId = create_autospec(self.jobs.jobFromId)

      self.task_id = multihash()
      self.task_uid = multihash()
      self.task_revision = multihash("sha1")
      self.jobs.objects = ObjectManagerMock([
        {
          "id": self.task_id,
          "uid": self.task_uid,
          "revision": self.task_revision,
          "path": "/some/path",
          "type": "data",
          "name": "MyData"
        }
      ])

    def test_should_return_none_when_job_is_none(self):
      self.jobs.jobFromId.return_value = None

      ret = self.jobs.taskFor(1)
      self.assertEqual(ret, None)

    def test_should_return_the_result_of_objects_retrieve(self):
      job = JobRecord({
        'task_uid': self.task_id,
        'task_revision': self.task_revision
      })

      self.jobs.jobFromId.return_value = job
      ret = self.jobs.taskFor(1)
      self.assertEqual(ret, self.jobs.objects.retrieve(self.task_id))

  class TestTaskInfoFor(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()

      self.jobs.jobFromId = create_autospec(self.jobs.jobFromId)
      self.jobs.logFor = create_autospec(self.jobs.logFor)

    def test_should_return_json_dict_decoded_from_the_file_stream_returned_by_logFor(self):
      import json
      import io
      task = TaskFactory().create_task()
      file_contents = json.dumps(task).encode("utf8")
      self.jobs.logFor.return_value = io.BytesIO(file_contents)

      ret = self.jobs.taskInfoFor(1)

      self.jobs.logFor.assert_called_with(ANY, logType='task')
      self.assertEqual(ret, task)

    def test_should_return_empty_dict_when_logFor_returns_none(self):
      self.jobs.logFor.return_value = None

      ret = self.jobs.taskInfoFor(1)

      self.assertEqual(ret, {})

  class TestTaskIdFor(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()

      self.jobs.taskInfoFor = create_autospec(self.jobs.taskInfoFor)

    def test_should_return_none_if_no_task_info_found(self):
      self.jobs.taskInfoFor.return_value = None

      ret = self.jobs.taskIdFor(1)

      self.assertEqual(ret, None)

    def test_should_return_none_if_no_id_key_in_task_info(self):
      self.jobs.taskInfoFor.return_value = {}

      ret = self.jobs.taskIdFor(1)

      self.assertEqual(ret, None)

    def test_should_return_the_id_of_the_task_info_when_present(self):
      some_id = multihash()
      self.jobs.taskInfoFor.return_value = {"id": some_id}

      ret = self.jobs.taskIdFor(1)

      self.assertEqual(ret, some_id)

  class TestPidFor(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()

      self.jobs.metadataFor = create_autospec(self.jobs.metadataFor)

    def test_should_return_none_when_no_metadata_is_found_for_the_job(self):
      self.jobs.metadataFor.return_value = None

      job = JobRecord({'id': random.randint(42, 1000)})

      with patch("builtins.open", mock_open()) as mock_handle:
        ret = self.jobs.pidFor(job)

      self.assertEqual(ret, None)

    def test_should_return_none_if_no_check_in_metadata(self):
      self.jobs.metadataFor.return_value = {"pid": 3000}

      job = JobRecord({'id': random.randint(42, 1000)})

      read = "something\0something\0darkside\0"
      with patch("builtins.open", mock_open(read_data=read)) as mock_handle:
        ret = self.jobs.pidFor(job)

      self.assertEqual(ret, None)

    def test_should_return_none_if_the_metadata_check_and_proc_cmdline_mismatch(self):
      self.jobs.metadataFor.return_value = {"pid": 3000, "check": "something\0"}

      job = JobRecord({'id': random.randint(42, 1000)})

      read = "something\0something\0darkside\0"
      with patch("builtins.open", mock_open(read_data=read)) as mock_handle:
        ret = self.jobs.pidFor(job)

      self.assertEqual(ret, None)

    def test_should_return_the_pid_when_the_metadata_check_matches_proc_cmdline(self):
      self.jobs.metadataFor.return_value = {"pid": 3000, "check": "complete\0"}

      job = JobRecord({'id': random.randint(42, 1000)})

      read = "something\0something\0complete\0"
      with patch("builtins.open", mock_open(read_data=read)) as mock_handle:
        ret = self.jobs.pidFor(job)

      self.assertEqual(ret, 3000)

  class TestIsRunning(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()

      self.jobs.driverFor = create_autospec(self.jobs.driverFor)

    def test_should_return_false_when_no_driver_found(self):
      self.jobs.driverFor.return_value = None

      job = JobRecord({'scheduler': "occam"})

      self.assertTrue(self.jobs.isRunning(job) is False)

    def test_should_return_the_result_of_driver_isRunning_when_a_driver_is_found(self):
      class Driver:
        def isRunning(self, job):
          return True

      self.jobs.driverFor.return_value = Driver()

      job = JobRecord({'scheduler': "occam"})

      self.assertTrue(self.jobs.isRunning(job) is True)

  class TestUpdatePath(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()

      self.jobs.datastore = create_autospec(self.jobs.datastore)

    def test_should_call_datastore_updateJob(self):
      job = JobRecord({'id': random.randint(42, 1000)})
      self.jobs.updatePath(job, "/some/path")

      self.jobs.datastore.updateJob.assert_called()

  class TestMetadataPathFor(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()

    @patch("occam.jobs.manager.Config")
    def test_should_return_the_path_to_the_metadata_file_for_the_given_job_id(self, config):
      config.root.return_value = "/my/path"

      ret = self.jobs.metadataPathFor(1)
      self.assertEqual(ret, "/my/path/job-daemon/1.json")

  class TestMetadataFor(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()

    def test_should_return_none_if_the_json_cant_be_decoded(self):
      read = '/ not / even/ remotely/ json'
      with patch("builtins.open", mock_open(read_data=read)) as mock_handle:
        ret = self.jobs.metadataFor(1)

      self.assertEqual(ret, None)

    def test_should_return_a_dict_decoded_from_the_metadata_json(self):
      metadata = {
        "json": "metadata"
      }

      read = '{"json": "metadata"}'
      with patch("builtins.open", mock_open(read_data=read)) as mock_handle:
        ret = self.jobs.metadataFor(1)

      self.assertEqual(ret, metadata)

  class TestWriteMetadataFor(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()

      self.jobs.metadataPathFor = create_autospec(self.jobs.metadataPathFor)

    @patch("os.makedirs")
    def test_should_return_a_copy_of_the_metadata_written(self, os_makedirs):
      read = 'some neat command\0'
      self.jobs.metadataPathFor.return_value = "/jobs/info.json"

      metadata = {
        "pid": 88,
        "command": "some unrelated command",
        "check": read
      }

      with patch("builtins.open", mock_open(read_data=read)) as mock_handle:
        ret = self.jobs.writeMetadataFor(1, pid=88, args=["some", "unrelated", "command"])

      self.assertEqual(ret, metadata)

    @patch("os.makedirs")
    def test_should_write_metadata_so_it_can_be_read_back_as_json(self, os_makedirs):
      import json

      self.jobs.metadataPathFor.return_value = "/some/file.json"

      read_data = 'some neat command\0'

      metadata = {
        "pid": 88,
        "command": "some unrelated command",
        "check": read_data
      }

      file_writes = {}
      def write_collector(path, mode="doesnt matter"):
        class ReaderWriter(Writer):
          def __init__(self, path, write_dict):
            super().__init__(path, write_dict)
          def read(self):
            return read_data
        return ReaderWriter(path, file_writes)

      with patch("builtins.open", mock_open()) as mock_handle:
        mock_handle.side_effect = write_collector
        ret = self.jobs.writeMetadataFor(1, pid=88, args=["some", "unrelated", "command"])

      read_metadata = file_writes["/some/file.json"]
      read_metadata = "".join(read_metadata)
      read_metadata = json.loads(read_metadata)

      self.assertEqual(read_metadata, metadata)
      self.assertEqual(read_metadata, ret)

  class TestWriteTo(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()

      self.jobs.isRunning = create_autospec(self.jobs.isRunning)
      self.jobs.nodeFor = create_autospec(self.jobs.nodeFor)
      self.jobs.nodes = create_autospec(self.jobs.nodes)
      self.jobs.pidFor = create_autospec(self.jobs.pidFor)
      self.jobs.network = create_autospec(self.jobs.network)

    def test_should_write_to_a_local_jobs_stdin(self):
      self.jobs.pidFor.return_value = 3000

      file_writes = {}
      def write_collector(path, mode="doesnt matter"):
        return Writer(path, file_writes)

      job = JobRecord()

      data = "please write me"
      with patch("builtins.open", mock_open()) as mock_handle:
        mock_handle.side_effect = write_collector
        ret = self.jobs.writeTo(job, data)

      read_data = file_writes["/proc/3000/fd/0"]
      read_data = "".join(read_data)

      self.assertTrue(ret)
      self.assertEqual(read_data, data)

    def test_should_write_to_a_remote_job_via_sendToJob(self):
      self.jobs.pidFor.return_value = None
      self.jobs.isRunning.return_value = True
      self.jobs.network.isURL.return_value = True
      self.jobs.nodes.sendToJob.return_value = True
      self.jobs.nodeFor.return_value = Mock(), 5

      job = JobRecord()

      data = "please write me"
      with patch("builtins.open", mock_open()) as mock_handle:
        ret = self.jobs.writeTo(job, data)
        mock_handle.assert_not_called()

      self.assertTrue(ret)

  class TestLogPathFor(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()

    @patch("occam.jobs.manager.Config")
    def test_should_return_the_path_to_the_given_jobs_log_file(self, config):
      config.root.return_value = "/some/path"
      ret = self.jobs.logPathFor(1)
      self.assertEqual(ret, "/some/path/job-daemon/1.log")

  class TestLogFor(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()
      self.jobs.network = create_autospec(self.jobs.network)
      self.jobs.nodes = create_autospec(self.jobs.nodes)
      self.jobs.nodeFor = create_autospec(self.jobs.nodeFor)
      self.jobs.logPathFor = create_autospec(self.jobs.logPathFor)
      self.jobs.nodeFor.return_value = None, None
      self.jobs.logPathFor.return_value = None
      self.jobs.network.isURL.return_value = False

    def test_should_return_none_if_job_is_none(self):
      with patch("builtins.open", mock_open()) as mock_handle:
        job = None
        ret = self.jobs.logFor(job, logType=None, start=0, tail=False)
        self.assertTrue(ret is None)

    def test_should_return_none_if_the_job_is_local_and_has_no_path(self):
      with patch("builtins.open", mock_open()) as mock_handle:
        job = create_autospec(JobRecord())
        job.id = 1
        job.path = None
        job.scheduler_identifier = None

        ret = self.jobs.logFor(job, logType=None, start=0, tail=False)
        self.assertTrue(ret is None)

    def test_should_return_none_when_trying_to_get_a_special_log_type_when_job_has_no_path(self):
      with patch("builtins.open", mock_open()) as mock_handle:
        job = create_autospec(JobRecord())
        job.id = 1
        job.path = None
        job.scheduler_identifier = None

        for lt in ["network", "task", "events"]:
          ret = self.jobs.logFor(job, logType=lt, start=0, tail=False)
          self.assertTrue(ret is None)

    def test_should_return_a_stream_from_opening_a_local_jobs_log(self):
      with patch("builtins.open", mock_open()) as mock_handle:
        self.jobs.logPathFor.return_value = "/log/path/for"

        job = create_autospec(JobRecord())
        job.id = 1
        job.path = None
        job.scheduler_identifier = None

        handle = mock_handle()
        mock_handle.return_value = handle

        ret = self.jobs.logFor(job, logType=None, start=0, tail=False)
        mock_handle.assert_called_with("/log/path/for", ReadModeMatch())
        self.assertTrue(ret is handle)

    def test_should_seek_for_local_logs(self):
      with patch("builtins.open", mock_open()) as mock_handle:
        self.jobs.logPathFor.return_value = "/log/path/for"

        job = create_autospec(JobRecord())
        job.id = 1
        job.path = None
        job.scheduler_identifier = None

        ret = self.jobs.logFor(job, logType=None, start=2, tail=False)
        handle = mock_handle()
        handle.seek.assert_called_with(2)

    def test_should_seek_for_local_special_log_types(self):
      with patch("builtins.open", mock_open()) as mock_handle:
        job = create_autospec(JobRecord())
        job.id = 1
        job.path = "/job/path"
        job.scheduler_identifier = None

        handle = mock_handle()
        for lt in ["network", "task", "events"]:
          ret = self.jobs.logFor(job, logType=lt, start=2, tail=False)
          handle.seek.assert_called_with(2)
          handle.reset_mock()

    def test_should_return_a_stream_for_special_log_types(self):
      with patch("builtins.open", mock_open()) as mock_handle:
        self.jobs.logPathFor.return_value = "/log/path/for"

        job = create_autospec(JobRecord())
        job.id = 1
        job.path = "/job/path"
        job.scheduler_identifier = None

        handle = mock_handle()
        mock_handle.return_value = handle

        types_and_expected = [
          ("network", "/job/path/../network.json"),
          ("task", "/job/path/object.json"),
          ("events", "/job/path/events"),
        ]

        for ltt in types_and_expected:
          ret = self.jobs.logFor(job, logType=ltt[0], start=0, tail=False)
          self.assertTrue(ret is handle)
          mock_handle.assert_called_with(ltt[1], ReadModeMatch())

    def test_should_return_none_if_the_special_log_type_is_unrecognized(self):
      with patch("builtins.open", mock_open()) as mock_handle:
        self.jobs.logPathFor.return_value = "/log/path/for"

        job = create_autospec(JobRecord())
        job.id = 1
        job.path = "/job/path"
        job.scheduler_identifier = None

        ret = self.jobs.logFor(job, logType="fake", start=0, tail=False)
        self.assertTrue(ret is None)

    def test_should_return_the_stream_for_reading_the_log_over_the_network(self):
      with patch("builtins.open", mock_open()) as mock_handle:
        self.jobs.nodeFor.return_value = Mock(), 5
        self.jobs.network.isURL.return_value = True

        handle = mock_handle()
        self.jobs.nodes.logForJob.return_value = handle, None, None

        self.jobs.logPathFor.return_value = None

        job = create_autospec(JobRecord())
        job.id = 1
        job.path = "/job/path"
        job.scheduler_identifier = "something"

        for lt in [None, "network", "task", "events"]:
          ret = self.jobs.logFor(job, logType=None, start=0, tail=False)
          self.assertTrue(ret is handle)

  class TestNodeFor(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()
      self.jobs.nodes = create_autospec(self.jobs.nodes)
      self.jobs.nodes.search.return_value = None

    def test_should_return_none_tuple_when_remote_job_id_is_invalid(self):
      search_node = Mock()
      self.jobs.nodes.search.return_value = search_node

      job = create_autospec(JobRecord())
      job.id = 1
      job.scheduler_identifier = "not even remotely a job URI"

      node, remote_job_id = self.jobs.nodeFor(job)
      self.assertTrue(remote_job_id is None)
      self.assertTrue(node is None)

    def test_should_return_the_parsed_remote_job_id_and_none_for_no_found_node(self):
      job = create_autospec(JobRecord())
      job.id = 1
      job.scheduler_identifier = "doesn't matter /jobs/889"

      node, remote_job_id = self.jobs.nodeFor(job)
      self.assertEqual(remote_job_id, "889")
      self.assertTrue(node is None)

    def test_should_return_the_parsed_remote_job_id_and_the_found_node(self):
      search_node = Mock()
      self.jobs.nodes.search.return_value = search_node
      job = create_autospec(JobRecord())
      job.id = 1
      job.scheduler_identifier = "doesn't matter /jobs/889"

      node, remote_job_id = self.jobs.nodeFor(job)
      self.assertEqual(remote_job_id, "889")
      self.assertTrue(node is search_node)

  # Need to rewrite these tests to account for new logFor loop.
  class TestConnect(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()
      self.jobs.isRunning = create_autospec(self.jobs.isRunning)
      self.jobs.logFor = create_autospec(self.jobs.logFor)
      self.jobs.nodeFor = create_autospec(self.jobs.nodeFor)
      self.jobs.nodes = create_autospec(self.jobs.nodes)
      self.jobs.network = create_autospec(self.jobs.network)
      self.jobs.writeTo = create_autospec(self.jobs.writeTo)
      self.jobs.jobFromId = create_autospec(self.jobs.jobFromId)

      self.running_job = create_autospec(JobRecord())
      self.running_job.id = 1
      self.running_job.status = "running"
      self.running_job.scheduler_identifier = None

      self.jobs.jobFromId.return_value = self.running_job
      self.jobs.isRunning.side_effect = [True, True, False]
      self.jobs.nodeFor.return_value = None, None
      self.jobs.logFor.return_value = None

    # TODO: Fix issue of calling os.read multiple times in this test case.
    @pytest.mark.xfail
    @patch("time.sleep")
    @patch("os.read")
    @patch("sys.stdin")
    @patch("select.select")
    def test_should_succeed_reading_from_stdin(self, select, sys_stdin, os_read, sleep):
      select.return_value = [88], [], []
      sys_stdin.fileno.return_value = 88
      sys_stdin.buffer.fileno.return_value = 88
      sys_stdin.read.return_value = "Hi"
      os_read.return_value = "Hi"

      ret = self.jobs.connect(self.running_job)

      self.assertTrue(ret)

      os_read.assert_called()
      self.jobs.writeTo.assert_called_once_with(self.running_job, "Hi")

    @patch("time.sleep")
    @patch("os.read")
    @patch("sys.stdin")
    @patch("select.select")
    def test_should_wait_until_job_is_not_queued(self, select, sys_stdin, os_read, sleep):
      select.return_value = [88], [], []
      sys_stdin.fileno.return_value = 88
      sys_stdin.buffer.fileno.return_value = 88
      sys_stdin.read.return_value = "Hi"
      os_read.return_value = "Hi"

      # When a job is queued connect should check for the database to show the
      # job is no longer waiting to execute.
      queued_job = create_autospec(JobRecord())
      queued_job.id = self.running_job.id
      queued_job.status = "queued"
      queued_job.scheduler_identifier = self.running_job.scheduler_identifier
      self.jobs.jobFromId.side_effect = [queued_job, queued_job, self.running_job]

      ret = self.jobs.connect(queued_job)

      self.assertTrue(ret)
      self.assertEqual(self.jobs.jobFromId.call_count, 3)

    @patch.object(JobManager, "Log", Mock())
    @patch("time.sleep")
    @patch("os.read")
    @patch("sys.stdin")
    @patch("select.select")
    def test_should_pipe_from_the_local_log_filehandle(self, select, sys_stdin, os_read, sleep):
      sys_stdin.fileno.return_value = 88
      sys_stdin.buffer.fileno.return_value = 88
      sys_stdin.read.return_value = "Hi"
      os_read.return_value = "Hi"

      with patch("builtins.open", mock_open(read_data="something funny")) as mock_handle:
        mh = mock_handle()
        select.return_value = [mh], [], []
        self.jobs.logFor.return_value = mh

        ret = self.jobs.connect(self.running_job)

        self.assertTrue(ret)

        os_read.assert_not_called()

        JobManager.Log.pipe.assert_has_calls([call(mh), call(mh)])

    @patch("time.sleep")
    @patch("os.read")
    @patch("sys.stdin")
    @patch("select.select")
    def test_should_return_false_when_failing_to_connect_to_a_node_over_the_network(self, select, sys_stdin, os_read, sleep):
      select.return_value = [88], [], []
      sys_stdin.fileno.return_value = 88
      sys_stdin.buffer.fileno.return_value = 88
      sys_stdin.read.return_value = "Hi"
      os_read.return_value = "Hi"

      self.running_job.scheduler_identifier = "some identifier"

      ret = self.jobs.connect(self.running_job)

      self.assertFalse(ret)

    @patch.object(JobManager, "Log", Mock())
    @patch("time.sleep")
    @patch("os.read")
    @patch("sys.stdin")
    @patch("select.select")
    def test_should_read_successfully_from_network_socket(self, select, sys_stdin, os_read, sleep):
      JobManager.Log.output("This shouldn't work")
      self.jobs.nodeFor.return_value = Mock(), 5

      sys_stdin.fileno.return_value = 88
      sys_stdin.buffer.fileno.return_value = 88
      sys_stdin.read.return_value = "Hi"
      os_read.return_value = "Hi"

      self.running_job.scheduler_identifier = "some identifier"

      with patch("builtins.open", mock_open(read_data="wishbone")) as mock_handle:
        mh = mock_handle()
        self.jobs.network.postOccam.return_value = mh, None, None

        select.return_value = [mh], [], []

        ret = self.jobs.connect(self.running_job)

      self.assertTrue(ret)
      JobManager.Log.output.assert_called()

      socket_output_logged = False
      for call in JobManager.Log.output.call_args_list:
        args, kwargs = call
        if len(args) > 0 and args[0] == "wishbone":
          socket_output_logged = True

      self.assertTrue(socket_output_logged)


  class TestOutputsFor(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()
      self.jobs.datastore = create_autospec(self.jobs.datastore)

    def test_should_return_the_result_of_datastore_retrieveJobOutputs(self):
      expected_ret = Mock()
      self.jobs.datastore.retrieveJobOutputs.return_value = expected_ret

      job = create_autospec(JobRecord())
      job.id = 1

      ret = self.jobs.outputsFor(job)

      self.assertTrue(ret is expected_ret)

  class TestOutputsForAll(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()
      self.jobs.datastore = create_autospec(self.jobs.datastore)

    def test_should_return_a_grouped_list_of_job_output_records_when_outputs_are_ordered(self):
      self.jobs.datastore.retrieveJobOutputsForAll.return_value = [
        Mock(job_id=1),
        Mock(job_id=1),
        Mock(job_id=2),
        Mock(job_id=3),
      ]

      ret = self.jobs.outputsForAll([1,2,3])

      # Should be one list per id.
      self.assertEqual(len(ret), 3)
      self.assertEqual(ret[0][0].job_id, 1)
      self.assertEqual(ret[1][0].job_id, 2)
      self.assertEqual(ret[2][0].job_id, 3)

    # FIXME: Function isn't well defined in the case the output's id orders
    # aren't sequential.
    @pytest.mark.xfail
    def test_should_return_a_grouped_list_of_job_output_records_when_outputs_arent_ordered(self):
      self.jobs.datastore.retrieveJobOutputsForAll.return_value = [
        Mock(job_id=1),
        Mock(job_id=2),
        Mock(job_id=3),
        Mock(job_id=1),
      ]

      ret = self.jobs.outputsForAll([1,2,3])

      # Should be one list per id.
      self.assertEqual(len(ret), 3)
      self.assertEqual(ret[0][0].job_id, 1)
      self.assertEqual(ret[1][0].job_id, 2)
      self.assertEqual(ret[2][0].job_id, 3)

  class TestCreateJobOutput(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()
      self.jobs.datastore = create_autospec(self.jobs.datastore)

    def test_should_return_the_result_of_datastore_createJobOutput(self):
      job  = create_autospec(JobManager())
      job.id = 1
      self.jobs.createJobOutput(job=job, object=create_autospec(Object), wireIndex=0, itemIndex=0)
      self.jobs.datastore.createJobOutput.assert_called()

  class TestAddOutput(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()
      self.objId = multihash()
      self.outputId = multihash()
      self.jobs.objects = ObjectManagerMock([
        {
          "id": self.objId,
          "uid": multihash(),
          "revision": multihash("sha1"),
          "path": "/some/path",
          "type": "data",
          "name": "MyData"
        },
        {
          "id": self.outputId,
          "uid": multihash(),
          "revision": multihash("sha1"),
          "path": "/some/path",
          "type": "data",
          "name": "MyData"
        }
      ])
      self.jobs.links = create_autospec(self.jobs.links)

      self.jobs.objects.search.return_value = []
      self.jobs.links.write.create.return_value = None

    @patch.object(JobManager, "Log", Mock())
    def test_should_print_error_when_no_matching_objects_found(self):
      self.jobs.addOutput(Mock(), Mock(), Mock())

      JobManager.Log.error.assert_called()

    @patch.object(JobManager, "Log", Mock())
    def test_should_call_links_write_create_on_the_first_object_found_by_search(self):
      self.jobs.objects.search.return_value = [Mock()]

      self.jobs.addOutput(
        self.jobs.objects.retrieve(self.objId),
        self.jobs.objects.retrieve(self.outputId),
        Mock()
      )

      self.jobs.links.write.create.assert_called()

    @patch.object(JobManager, "Log", Mock())
    def test_should_log_error_when_no_link_created(self):
      self.jobs.objects.search.return_value = [Mock()]
      self.jobs.links.write.create.return_value = None

      self.jobs.addOutput(
        self.jobs.objects.retrieve(self.objId),
        self.jobs.objects.retrieve(self.outputId),
        Mock()
      )

      self.jobs.links.write.create.assert_called()
      JobManager.Log.error.assert_called()

    @patch.object(JobManager, "Log", Mock())
    def test_should_log_noisy_when_link_already_existed(self):
      self.jobs.objects.search.return_value = [Mock()]
      self.jobs.links.write.create.side_effect = DataNotUniqueError()

      self.jobs.addOutput(
        self.jobs.objects.retrieve(self.objId),
        self.jobs.objects.retrieve(self.outputId),
        Mock()
      )

      self.jobs.links.write.create.assert_called()
      JobManager.Log.noisy.assert_called_with(f"Link between {self.objId} and {self.outputId} already exists")

  class TestRakeSubObjectOutput(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()
      self.jobs.datastore = create_autospec(self.jobs.datastore)
      self.jobs.permissions = create_autospec(self.jobs.permissions)
      self.jobs.objects = Mock()
      self.jobs.objects.write = create_autospec(ObjectWriteManager)
      self.jobs.createJobOutput = create_autospec(self.jobs.createJobOutput)
      self.jobs.addOutput = create_autospec(self.jobs.addOutput)
      self.jobs.personFor = create_autospec(self.jobs.personFor)

    @patch("os.listdir", return_value = [])
    @patch("os.path.exists", return_value = False)
    def test_should_return_a_structure_describing_the_given_output_item(self, os_exists, os_listdir):
      obj = Mock(
        id = multihash(),
        uid = multihash(),
        revision = multihash(hashType="sha1")
      )

      self.jobs.objects.write.create.return_value = obj

      task_structure = {
        "outputs": [
          {}
        ]
      }
      task = TaskFactory(task_structure).create_task()

      job = create_autospec(JobRecord())
      job.identity = 'dont care'
      job.id = 2

      wireIndex = 0
      wire = task["outputs"][0]
      with patch("builtins.open", mock_open()) as mock_handle:
        ret = self.jobs._rakeSubObjectOutput(
          task=task, wire=wire, subOutputPath="/sub/output/path", wireIndex=wireIndex, itemIndex=1, generators=[], person=Mock(),
          job=job
        )

      self.assertEqual(ret["output_index"], wireIndex)
      self.assertEqual(ret["object_id"], obj.id)
      self.assertEqual(ret["object_uid"], obj.uid)
      self.assertEqual(ret["object_revision"], obj.revision)

  class TestRakeOutput(unittest.TestCase):
    @classmethod
    def setUp(self):
      self.jobs = JobManager()
      self.jobs.datastore = create_autospec(self.jobs.datastore)
      self.jobs.objects = ObjectManagerMock([])
      self.jobs.createJobOutput = create_autospec(self.jobs.createJobOutput)
      self.jobs._rakeSubObjectOutput = create_autospec(self.jobs._rakeSubObjectOutput)

    @patch("os.listdir", return_value = [])
    @patch("os.path.exists", return_value = False)
    def test_should_return_an_empty_list_when_no_outputs_found(self, os_exists, os_listdir):
      task = TaskFactory().create_task()

      outputs = self.jobs.rakeOutput(task, "/task/path")

      self.assertTrue(isinstance(outputs, list))
      self.assertEqual(len(outputs), 0)

    @patch("os.listdir", return_value = [])
    @patch("os.path.exists", return_value = False)
    def test_should_find_and_return_deeply_nested_outputs(self, os_exists, os_listdir):
      task_structure = {
        "running": {
          1: {
            "objects": {
              2: {
                "inputs": [
                  {
                    "connections": {
                      3: {
                        "outputs": [
                          {}
                        ]
                      }
                    }
                  }
                ]
              }
            }
          }
        }
      }
      task = TaskFactory(task_structure).create_task()

      outputs = self.jobs.rakeOutput(task, "/task/path")

      self.assertEqual(len(outputs), 1)

    @patch("os.listdir", return_value = [])
    @patch("os.path.exists", side_effect = [True, False])
    def test_should_return_with_an_output_count_of_zero_when_output_dir_empty(self, os_exists, os_listdir):
      task_structure = {
        "outputs": [
          {}
        ]
      }
      task = TaskFactory(task_structure).create_task(["index"])

      outputs = self.jobs.rakeOutput(task, "/task/path")

      self.jobs._rakeSubObjectOutput.assert_not_called()

      self.assertEqual(len(outputs), 1)
      self.assertEqual(outputs[0]["output_count"], 0)

    @patch("os.listdir", side_effect = ["file.out"])
    @patch("os.path.exists", side_effect = [True, False])
    def test_should_return_output_count_matching_directory_listing(self, os_exists, os_listdir):
      task_structure = {
        "outputs": [
          {}
        ]
      }
      task = TaskFactory(task_structure).create_task(["index"])

      outputs = self.jobs.rakeOutput(task, "/task/path")

      self.jobs._rakeSubObjectOutput.assert_called()

      self.assertEqual(len(outputs), 1)
      self.assertEqual(outputs[0]["output_count"], 1)
      self.assertEqual(len(outputs[0]["objects"]), 1)

  class TestSchedulerDecorator(unittest.TestCase):
    @patch.object(JobManager, "register", Mock())
    def test_should_call_register_for_the_decorated_class(self):
      @scheduler("target")
      class Target:
        pass
      JobManager.register.assert_called_with("target", Target)
