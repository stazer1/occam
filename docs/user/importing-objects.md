# Importing Existing Objects

By default when you install an Occam instance there will be no objects in the store. You can import objects made for Occam from the following sources:

  * Other Occam Instances
  * Online Git Repositories
  * Ingest Targets

## Pulling from Other Occam Instances

Publicly accessible objects in Occam may be shared between instances through Occam's pulling mechanism. Copy the URL of the object you wish to pull and use the `occam objects pull` command to pull the object and all of its dependencies to your own instance. For example, to pull the Ace editor into the Occam store of a local Occam instance:

```
occam objects pull https://occam.cs.pitt.edu/QmSk23u78xfHA2RVSBVdDeDJGpoHWQUYWWVD8dAgmanY4g
```

## Pulling from Git Repositories

## Using the Ingest Manager

Occam is setup to easily pull from some sources, such as PyPi for Python packages. This is done by using the ingest component.

See the [Ingest component documentation](../../src/occam/ingest/README.md) for how to pull from ingest targets.
