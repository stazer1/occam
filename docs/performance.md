# Code Profiling

To invoke an Occam command and print out a code profile that shows the runtime broken down by function:

```
OCCAM_PROFILE=1 occam objects run QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd
```

Where the occam command can, of course, be any command.

The value of the `OCCAM_PROFILE` environment variable will allow you to sort the results.
Any [`SortKey`](https://docs.python.org/3/library/profile.html#module-pstats) value can be specified.
Some useful values are "CUMULATIVE", "TIME", and "PCALLS" to sort by those columns and "NAME" to sort by function name.

```
OCCAM_PROFILE=CUMULATIVE occam objects run QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd
```

When you enable this for a background daemon, it will by default generate profile files
in the daemon log directory (defaults to `$HOME/.occam/daemons`) where the files are
`profile-` appended with the thread ID. This will match the corresponding log file for
that daemon thread.

You can then use `flameprof` to generate an SVG or print text logs for that profile
dump. The profile dumps themselves are binary files and not text files, unlike the
logged output when profiling a simple, normal command. Here is an example:

```shell
flameprof  ~/.occam/daemons/profile-139732829394688.prof > profile.svg
```
