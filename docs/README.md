# Occam Documentation and Useful Files

This project uses Sphinx to generate html documentation for the modules
within the codebase. This is meant to be used by developers who are
writing code against the occam package.

For documentation for specific components, these are documented at their source
directories within `README.md` files of their own. For instance, the `workflows`
component (and the WorkflowManager) are documented in
`src/occam/workflows/README.md`.

## Directory Layout

```
.
+-- Makefile                     - Script to generate documentation.
+-- README.md                    - This file.
+-- api-docs/                    - API documentation generated from source.
|   +-- conf.py                  - Sphinx configuration.
|   +-- index.rst                - The root page for the documentation.
|   +-- *.rst <generated>        - Generated documentation pages per python module.
|   +-- _static                  - Sphinx complains if this folder is not present.
|   |   +-- .gitignore           - Allows us to commit the empty folder.
|   +-- _templates
|       +-- footer.rst           - The footer template for developer documentation.
+-- daemon-docs/                 - Compiled documentation for users/admins/devs.
|   +-- conf.py                  - Sphinx configuration.
|   +-- index.rst                - The root page for the documentation.
|   +-- developer.rst            - Index for the developer page.
|   +-- _static                  - Sphinx complains if this folder is not present.
|   |   +-- .gitignore           - Allows us to commit the empty folder.
|   +-- _templates
|       +-- footer.rst           - The footer template for developer documentation.
+-- api-html <generated>         - Where the built API HTML documentation site goes.
+-- daemon-html <generated>      - Where the built daemon HTML documentation site goes.
```

## Notes on Style Conventions

The documentation is using the Napoleon markup language which allows for
[Google style Python documentation](http://google.github.io/styleguide/pyguide.html)
with examples [here](http://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html).

## Generate Documentation

When you add new functions or modules to Occam, you should also generate
documentation for them and review this documentation before you push a new
version or pull request. Thankfully, this is done automatically using Sphinx.

```
# within docs subdirectory:
make veryclean
make generate
make html
```

Or as a single command:

```
# within docs subdirectory
make veryclean generate html
```

Here, the `make veryclean` will delete any generated files (if they exist) and then
`make generate` command essentially runs `sphinx-apidoc -e -o . ../src/occam`
which generates the `.rst` files. The `make html` command will generate the HTML from
those `.rst` files using `sphinx-build`.

The `index.rst` file is not generated and contains the main page for the documentation.
A table of contents is generated so that a person can browse the entire documentation
as they need. You may add general documentation items as `.rst` files and then create
links to them in `index.rst` and commit those into the repository.

Afterward, the `html` subdirectory is now available and you can then start a server or
point a server to that directory in order to view the documentation.

**Remember**: when you update files you must run the `veryclean` step.

## Run a webserver

To start a simple webserver to view the documentation locally, simply start a webserver
with Python:

```
python -m http.server
```

Which will spawn a HTTP server on port 8000. To view the documentation, point a browser
to [localhost:8000/html](http://localhost:8000/html)
