# Configuring Occam

Occam is highly configurable, and can be customized in a plug and play fashion.

Firewall requirements are documented separately [here](firewall.md).

## Common Configurations

### Minimalist Install

The absolute minimal installation of Occam is what is installed when following the [install instructions](installing.md). Occam will use SQLite3 for database operations, and no optional features (such as IPFS usage and social component features) will be enabled.

### Production Install

Production servers should, in addition to doing a minimal install:

  * Setup IPFS.
  * Switch to Postgres for database operations.
  * Use Singularity instead of Docker due to security concerns.

### High Availability Cluster Install

Cluster installations should, in addition to doing a production install:

  * Configure Occam to use Slurm for job deployment to spread object execution
    across a Slurm cluster.
  * Run multiple Occam daemons on separate machines behind a reverse proxy to
    handle a higher load of traffic.

## Linux Environment Setup

Occam's functionality depends on certain environment variables being correctly configured. When running from the command line this is typically sourced from your `.profile`, `.bashrc`, `.zshrc` or equivalent shell configuration file. When Occam is running as a service, these variables below are best configured directly in the Systemd service file with the `Environment=` directive.

  - `$PATH` - When trying to run Occam the `PATH` variable needs to be set correctly when Occam is launched, as well as throughout execution. The daemon will occasionally launch programs itself, and your ability to launch the daemon and run Occam commands is predicated on the system being able to find all of the binaries it expects to find. The following items need to be accounted for in your `PATH` variable:
    * The location of the Occam bin folder ([see the installation instructions](installing.md)).
    * The location of the Occam `vendor/` folder. This is optional. The Occam
      binary will find and use this path itself, however you may wish to use
      the tools installed here for your own purposes.
    * The location of the container environment binaries (Docker or
      Singularity). Typically the installation instructions for these tools
      will ensure this is done.
    * The location of `unzip`.
  - `$OCCAM_ROOT` - The Occam daemon relies on `$OCCAM_ROOT` to find where it
    should store all of its files. Unless specified, this will default to
    `$HOME/.occam`
  - `$OCCAM_DEBUG` - This determines if the daemon should print stack traces
    when available. For production environments it is best to leave this unset.

## Using Occam with IPFS

Occam can use IPFS as an aid in federating objects. To enable this feature, download and extract the Go IPFS implementation to the Occam directory and Occam will automatically make use of it:

```
cd ~/occam
wget https://dist.ipfs.io/go-ipfs/v0.5.1/go-ipfs_v0.5.1_linux-amd64.tar.gz
tar -xf go-ipfs_v0.5.1_linux-amd64.tar.gz
rm go-ipfs_v0.5.1_linux-amd64.tar.gz
```

## Using Occam with Postgres

To use Postgres as the database of choice you must install the Postgres extras
for Occam and modify your active config.yml (this is typically found under ~/.occam after running `occam system initialize`.

Installing requirements for Postgres support can be done easily with pip (you must be in the virtualenv in order for this to work):

```
cd ./occam
pip3 install -e .[Postgres]
```

Then modify the database section of your config.yml to be:

```
database:
  adapter: 'postgres'
  timeout: 1000
  host: your-host
  database: your-pg-database-name
  user: your-pg-user
  password: your-pg-user-password
```

Once this is done, run:

```
occam system initialize
```

## Using Occam with Slurm

A description for how to configure Occam to use Slurm can be found in the [Slurm plugin documentation](../../src/occam/jobs/plugins/slurm/README.md).

If you wish for guidance on how to set up a minimal Slurm cluster, see the [Slurm installation guide](slurm-cluster.md).

For an example deployment, see the [AWS deployment instructions](AWS.md).

## Email Notifications

Coming soon.

## Plugins 

See the [plugin documentation](../../plugins/README.md) for how to configure Occam to use extra components.

## config.yml

A number of smaller configuration details are documented in the [example config.yml](../../config.yml.sample).

This is where you can tweak settings such as password policy, trusted certificates, policies, and more.
