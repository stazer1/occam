# Installing Occam

Occam has been successfully installed on Ubuntu, Centos, and Arch Linux using the instructions below. If you are having issues with a different distribution, please contact us at <occam@list.pitt.edu>. 

## Prerequisites
Occam relies on a few other pieces of software and libraries in order to function. Below is a list of what packages Occam relies on:

  * Occam uses Git to track object history.
    * `git`
  * The Occam daemon runs on Python.
    * `python` (3.7 or above)
    * `pip` is also required to easily install Occam's Python dependencies.
    * The version of `python` used must have been built with support for the compression tools listed below.
  * Occam uses one of the following databases to track metadata and account information.
    * `sqlite3`
    * `postgresql`
  * The following compression tools as well as their decompression counterparts are required for resource acquisition. Python must also be built with support for these:
    * `bzip2`
    * `zip`
    * `xz`, the library for which is called `zlib`
    * `gzip`
  * Occam runs all objects in an execution environment. One of the following container tools is required for Occam to fully function:
    * `docker` 
    * `singularity`
    * Be sure that the user running Occam commands is in the appropriate groups to create and run containers using the container tool you choose.


## Automatic Install

Run the installation script and pay attention to the output.

```
sh ./install.sh
```

The `install.sh` script will automatically try to install prerequisites. In some situations, such as lacking sudo rights, this will fail. If running the script on Centos, you will be prompted to install any missing dependencies. You may wish to see Building Prerequisites below.

This installation script will set up a virtual environment for you and also automatically initialize Occam.

## Manual Install

First we will clone the repository. Switch to the directory in which you want to install Occam and run the following:

```
git clone https://gitlab.com/occam-archive/occam occam
cd occam
```

Which will create a directory called `occam` which we can then go into to set up the rest of the system.

### Building Prerequisites

In most cases you can use your system's package manager to install these requirements. However, you may wish or need to build these dependencies yourself. 

In the `scripts/build` directory there are scripts for pulling and building prerequisites. Feel free to contact us at <occam@list.pitt.edu> if you have trouble building for your platform.

Most of our instructions assume all software built for Occam is built into the `vendor` directory. If you choose to build elsewhere, please remember to reflect this in the occam.service file and other places that set up the environment for Occam.

### Installing Occam
First, you may want to set up a virtual environment for Python so you can keep things separate from the rest of your system. This is optional, but you can do so with the following:

```
pip install virtualenv # may require root/sudo access
virtualenv python
source python/bin/activate
```

This will create a copy of Python which will contain Occam's Python libraries separate from installing them system-wide. If you don't do this step, it will install those libraries in your system folder (which may require root access). Either way, continue with the following:

```
pip install -r dev-requirements.txt
./bin/occam system initialize
```

The command "system initialize" will create a database. At this point you can run occam using:

```
./bin/occam
```

This will show the usage information. And you can use the following (while in this directory) to place occam in your path:

```
export PATH=$PATH:$PWD/bin
```

And to add this to your startup (again, while in the occam directory):

```
echo "export PATH=\$PATH:$PWD/bin" >> ~/.profile
```

The first thing you'll want to do is to create an account. The first account created on the system is automatically an administrator account. Replace `<username>` with the username you wish. It will ask for a password which will be used later to authenticate this account.

```
occam accounts new <username>
```

Continue by installing a client, such as the web-client [here](https://gitlab.com/occam-archive/occam-web-client).

These clients generally use Occam as a daemon and interact through this. To start a daemon:

```
occam daemon start
```

It will run in the background and can be closed using:

```
occam daemon stop
```

## Running Occam as a Service

Occam runs well as a typical Systemd service. You may copy `docs/systemd/occam.service` to your service directory and start the occam daemon as a service. Be sure to edit the service file to correctly set the environment expected by Occam (make sure `PATH` and `OCCAM_ROOT` are set correctly).

```
sudo cp occam/docs/systemd/occam.service /etc/systemd/system/
sudo systemctl enable occam
sudo systemctl start occam
```

## Post Install

After installing Occam, you can proceed to [configuring Occam](configuring.md) to suit your needs.

If you are trying to start a cluster on AWS, you will want to look at our [AWS documentation](AWS.md).
