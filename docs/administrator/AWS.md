# How to Set an Occam Cluster Up on AWS

For running the LPS tutorial 2020-10-14 and 2020-11-05 we set up multiple AWS instances. This consisted of 5 instances:

  * Postgres
  * NFS
  * Slurm
  * Backend
  * Frontend

Each of these instances were setup using the procedures below. You may use these procedures to deploy your own Occam instance on AWS, or merge these procedures to run it all on one machine.

All of the instances should be Ubuntu images.

The examples given are all assuming the OS is Ubuntu, however equivalent commands exist for Arch, Fedora, Centos, etc.

## Security Groups / Firewall Rules

You will want to set up security groups such that the following connections can take place.

  * Postgres
    * Allow TCP port 5432 from Backend instances.
  * NFS
    * Allow TCP port 2049 from Slurm and Backend instances.
  * Slurm
    * Allow TCP ports 6817-6819, 7321, 60001-63000 from Backend instances. This allows connections to the database, and allows the random port opening srun and sbatch do.
    * Allow TCP port 32000 for Backend instances.
    * If mirroring the Slurm configuration over NFS, you will also need to allow TCP port 2049 from Backend nodes.
  * Backend
    * Allow TCP port 32000 for Slurm instances.
  * Frontend
    * Allow TCP port 80 from 0.0.0.0/0 if serving Occam to the internet.

## Instances

### Postgres
  * Update package list.
    * `sudo apt update`
  * Install Postgres.
    * `sudo apt install postgresql --no-install-recommends -y`
  * Create the occam database.
    * `sudo -u postgres createdb occam`
  * Create the occam user.
    * `sudo -u postgres psql -c "CREATE USER occam WITH PASSWORD 'YOUR_PASSWORD';"`
  * Tell postgres to listen on all interfaces.
    * `echo "listen_addresses = '*'"|sudo tee -a /etc/postgresql/12/main/postgresql.conf`
    * `echo "host    all             all             0.0.0.0/0            md5"|sudo tee -a /etc/postgresql/12/main/pg_hba.conf`
  * Resart the Postgres service.
    * `sudo service postgresql restart`

### NFS
  * Update the package listing.
    * `sudo apt update`
  * Install the NFS server daemon.
    * `sudo apt install nfs-kernel-server`
  * Create a directory for hosting data.
    * `sudo mkdir /data`
  * Make the folder owned by the user who will need to see the data in this folder. On AWS this is the 'ubuntu' user which is the user of all Occam AWS instances.
    * `sudo chown ubuntu:ubuntu /data`
  * Edit the exports file to export the share to other instances on the network.
    * `sudo vim /etc/exports`
    * Add the following line: `/data   172.31.32.0/20(rw,insecure,no_subtree_check,async)`
    * Replace 172.31.32.0/20 with whatever your subnet is.
  * Restart the NFS daemon.
    * `sudo systemctl restart nfs-kernel-server`

### Slurm

The below instructions are adapted from [Nate George's instructions](https://github.com/nateGeorge/slurm_gpu_ubuntu), which themselves pull from [mknoxnv's repo](https://github.com/mknoxnv/ubuntu-slurm). Thank you both for paving the way forward for a stable Slurm install.

  * Add users for slurm.
    * `sudo adduser munge --disabled-password --gecos ""`
    * `sudo adduser slurm --disabled-password --gecos ""`
  * Update package list.
    * `sudo apt update`
  * Install necessary packages.
    * `sudo apt install -y libmunge-dev libmunge2 munge mysql-server wget git gcc make build-essential ruby ruby-dev rubygems libmysqlclient-dev liblzma-dev nfs-common`
  * Test munge.
    * `munge -n |unmunge|grep STATUS`
  * Do the mysql secure install for Slurm accounting.
    * `sudo mysql_secure_installation`
    * Following the prompts:
      * Do whatever you want for password validation.
      * Set the database root password (and remember it).
      * Remove anonymous users. 
      * Disable remote root login.
      * Remove test database.
      * Reload privileged tables.
    * `sudo mysql`
      * `create database slurm_acct_db;`
      * `create user 'slurm'@'localhost';`
      * `set password for 'slurm'@'localhost' = 'slurmdbpass';`
      * `grant usage on *.* to 'slurm'@'localhost';`
      * `grant all privileges on slurm_acct_db.* to 'slurm'@'localhost';`
      * `flush privileges;`
      * `exit`
  * Install the FPM tool.
    * `sudo gem install --no-document fpm`
  * Download and build Slurm.
    * `sudo mkdir -p /etc/slurm /etc/slurm/prolog.d /etc/slurm/epilog.d /var/spool/slurm/ctld /var/spool/slurm/d /var/log/slurm`
    * `wget https://download.schedmd.com/slurm/slurm-20.02.5.tar.bz2`
    * `tar xvjf slurm-20.02.5.tar.bz2`
    * `cd slurm-20.02.5/`
    * `./configure --prefix=/tmp/slurm-build --sysconfdir=/etc/slurm --enable-pam --with-pam_dir=/lib/x86_64-linux-gnu/security/`
    * `make`
    * `make contrib`
    * `make install`
  * Setup create a deb package for slurm and install from it.
    * `cd ..`
    * `sudo fpm -s dir -t deb -v 1.0 -n slurm-20.02.5 --prefix=/usr -C /tmp/slurm-build .`
    * `sudo dpkg -i slurm-20.02.5_1.0_amd64.deb `
    * `sudo chown slurm /var/spool/slurm/ctld /var/spool/slurm/d /var/log/slurm`
  * Copy the .deb file to `/occam` so we can reuse it when installing Slurm on the back-end as a client.
  * Ensure the following in your /etc/slurm/slurmdbd.conf (be sure to use `sudo`):
```
#
# Example slurmdbd.conf file.
#
# See the slurmdbd.conf man page for more information.
#
# Authentication info
AuthType=auth/munge
#AuthInfo=/var/run/munge/munge.socket.2
#
# slurmDBD info
DbdAddr=localhost
DbdHost=localhost
#DbdPort=7031
SlurmUser=slurm
#MessageTimeout=300
DebugLevel=4
LogFile=/var/log/slurm/slurmdbd.log
PidFile=/var/run/slurmdbd.pid
PluginDir=/usr/lib/slurm
#
# Database info
StorageType=accounting_storage/mysql
#StorageHost=localhost
#StoragePort=1234
StoragePass=slurmdbpass
StorageUser=slurm
StorageLoc=slurm_acct_db
```
  * Ensure the following in your `/etc/slurm/slurm.conf` (be sure to use `sudo`), changing ip-172-31-32-43 to whatever your slurm controller hostname is:
```
# Example slurm.conf file. Please run configurator.html
# (in doc/html) to build a configuration file customized
# for your environment.
#
#
# slurm.conf file generated by configurator.html.
#
# See the slurm.conf man page for more information.
#
ClusterName=compute-cluster
ControlMachine=ip-172-31-32-43
#
SlurmUser=slurm
SlurmctldPort=6817
SlurmdPort=6818
AuthType=auth/munge
StateSaveLocation=/var/spool/slurm/ctld
SlurmdSpoolDir=/var/spool/slurm/d
MpiDefault=none
ProctrackType=proctrack/pgid
ReturnToService=2
SwitchType=switch/none
SlurmctldPidFile=/var/run/slurmctld.pid
SlurmdPidFile=/var/run/slurmd.pid
PluginDir=/usr/lib/slurm
#
# TIMERS
SlurmctldTimeout=300
SlurmdTimeout=300
InactiveLimit=0
MinJobAge=300
KillWait=30
Waittime=0
#
# SCHEDULING
SchedulerType=sched/backfill
SelectType=select/cons_res
SelectTypeParameters=CR_CPU
#
# LOGGING
SlurmctldDebug=3
SlurmctldLogFile=/var/log/slurmctld.log
SlurmdDebug=3
SlurmdLogFile=/var/log/slurmd.log
JobCompType=jobcomp/none
#
# ACCOUNTING
JobAcctGatherType=jobacct_gather/none
#
#AccountingStorageTRES=gres/gpu
DebugFlags=CPU_Bind,gres
AccountingStorageType=accounting_storage/slurmdbd
AccountingStorageHost=ip-172-31-32-43
AccountingStoragePass=/var/run/munge/munge.socket.2
AccountingStorageUser=slurm
#
# COMPUTE NODES
#
# MODIFY THE BELOW NODE LIST TO USE YOUR MACHINE'S HOSTNAME AND CPU COUNT.
#NodeName=ip-172-31-32-43 CPUs=32
#NodeName=ip-172-31-32-42 CPUs=28
PartitionName=debug Nodes=ALL Default=YES MaxTime=00:15:00 DefaultTime=00:15:00 State=UP
```
  * Create /etc/systemd/system/slurmctld.service:
```
Description=Slurm controller daemon
After=network.target munge.service
ConditionPathExists=/etc/slurm/slurm.conf

[Service]
Type=forking
EnvironmentFile=-/etc/sysconfig/slurmctld
Environment="LD_LIBRARY_PATH=/usr/lib/slurm"
ExecStart=/usr/sbin/slurmctld $SLURMCTLD_OPTIONS
ExecReload=/bin/kill -HUP $MAINPID
PIDFile=/var/run/slurmctld.pid

[Install]
WantedBy=multi-user.target
```
  * Create /etc/systemd/system/slurmd.service:
```
[Unit]
Description=Slurm node daemon
After=network.target munge.service
ConditionPathExists=/etc/slurm/slurm.conf

[Service]
Type=forking
EnvironmentFile=-/etc/sysconfig/slurmd
Environment="LD_LIBRARY_PATH=/usr/lib/slurm"
ExecStart=/usr/sbin/slurmd -d /usr/sbin/slurmstepd $SLURMD_OPTIONS
ExecReload=/bin/kill -HUP $MAINPID
PIDFile=/var/run/slurmd.pid
KillMode=process
LimitNOFILE=51200
LimitMEMLOCK=infinity
LimitSTACK=infinity

[Install]
WantedBy=multi-user.target
```
  * Create /etc/systemd/system/slurmdbd.service:
```
[Unit]
Description=Slurm DBD accounting daemon
After=network.target munge.service
ConditionPathExists=/etc/slurm/slurmdbd.conf

[Service]
Type=forking
EnvironmentFile=-/etc/sysconfig/slurmdbd
Environment="LD_LIBRARY_PATH=/usr/lib/slurm"
ExecStart=/usr/sbin/slurmdbd $SLURMDBD_OPTIONS
ExecReload=/bin/kill -HUP $MAINPID
PIDFile=/var/run/slurmdbd.pid

[Install]
WantedBy=multi-user.target
```
  * Restart the slurm daemons.
    * `sudo systemctl restart slurmctld`
    * `sudo systemctl restart slurmdbd`
    * `sudo systemctl restart slurmd`
  * Either map a cross-instance used occam directory or install occam.
    * `sudo mkdir /occam`
    * `sudo chown ubuntu:ubuntu /occam/`
    * `sudo vim /etc/fstab `
      * Add `172.31.36.173:/data     /occam/ nfs     auto    0       0` to the file.
      * Change 172.31.36.173 to the IP of the instance you created your NFS share on.
    * `sudo mount /occam`
  * Export your slurm config for clients to use.
    * `sudo apt install nfs-kernel-server`
    * `sudo vim /etc/exports`
      * Add the following line: `/etc/slurm   172.31.32.0/20(rw,insecure,no_subtree_check,async)`
      * Replace 172.31.32.0/20 with whatever your subnet is.
    * `sudo systemctl restart nfs-kernel-server`
  * Make sure the same `/etc/munge/munge.key` is present on all servers using slurm.
  * Install Singularity dependencies.
    * `sudo apt install -y build-essential libssl-dev uuid-dev libgpgme-dev squashfs-tools libseccomp-dev wget pkg-config git cryptsetup-bin`
  * Install Singularity.
    * `export VERSION=1.14.2 OS=linux ARCH=amd64 && wget https://dl.google.com/go/go$VERSION.$OS-$ARCH.tar.gz && sudo tar -C /usr/local -xzvf go$VERSION.$OS-$ARCH.tar.gz && rm go$VERSION.$OS-$ARCH.tar.gz`
    * `echo 'export GOPATH=/usr/local/go/bin' >> ~/.profile && echo 'export PATH=${PATH}:${GOPATH}' >> ~/.profile && source ~/.profile`
    * `export VERSION=3.5.3 && wget https://github.com/sylabs/singularity/releases/download/v${VERSION}/singularity-${VERSION}.tar.gz && tar -xzf singularity-${VERSION}.tar.gz && cd singularity`
    * `./mconfig && make -C ./builddir && sudo make -C ./builddir install`
  * Setup the local occam instance.
    * Install Occam and the Occam Postgres dependencies.
      * Install the Occam back-end in `/occam/occam` so the backend machine can use the binaries as well.
      * See the [installation instructions](installing.md).
      * This Occam instance should use SQLite3, not Postgres. But install the Postgres dependencies anyway so they can be used on the back-end instance. See "Using Occam with Postgres" in the [configuration guide](configuring.md).
      * Make sure the Occam bin folder (`/occam/occam/bin`) is inserted into PATH by your shell initialization script (.profile or equivalent).
        * `echo "export PATH=\$PATH:/occam/occam/bin" >> ~/.bashrc`
      * By default the OCCAM_ROOT is in the current user's home directory. This is fine for this instance.
  * Create a service file to automatically start Occam.
    * `sudo cp $PATH_TO_OCCAM/docs/systemd/occam.service /etc/systemd/system/`
    * Edit the occam.service file to use the correct user.
  * Start the Occam service.
    * `sudo systemctl start occam`

### Backend
  * Add users for slurm.
    * `sudo adduser munge --disabled-password --gecos ""`
    * `sudo adduser slurm --disabled-password --gecos ""`
  * Update package list.
    * `sudo apt update`
  * Install necessary packages.
    * `sudo apt install -y libmunge-dev libmunge2 munge liblzma-dev nfs-common libpq-dev`
  * Test munge.
    * `munge -n |unmunge|grep STATUS`
  * Map the cross-instance used occam directory.
    * `sudo mkdir /occam`
    * `sudo chown ubuntu:ubuntu /occam/`
    * `sudo vim /etc/fstab `
      * Add `172.31.36.173:/data     /occam/ nfs     auto    0       0` to the file.
      * Change 172.31.36.173 to the IP of the instance you created your NFS share on.
    * `sudo mount /occam`
  * Copy the munge key from the Slurm instance and overwrite `/etc/munge/munge.key` with it.
  * Install from the deb package you copied into `/occam`.
    * `cd /occam`
    * `sudo mkdir -p /etc/slurm /etc/slurm/prolog.d /etc/slurm/epilog.d /var/spool/slurm/ctld /var/spool/slurm/d /var/log/slurm`
    * `sudo dpkg -i slurm-20.02.5_1.0_amd64.deb `
    * `sudo chown slurm /var/spool/slurm/ctld /var/spool/slurm/d /var/log/slurm`
  * Get the mirrored Slurm configuration from your Slurm instance.
    * `sudo vim /etc/fstab `
      * Add `172.31.36.173:/etc/slurm     /etc/slurm/ nfs     auto    0       0` to the file.
      * Change 172.31.36.173 to the IP of the instance you installed as a Slurm controller.
  * Be sure to edit your shell initialization script(.profile or equivalent) as necessary so that OCCAM_ROOT is `/occam/.occam`.
    * `echo "export OCCAM_ROOT=/occam/.occam" >> ~/.profile`
  * Make sure the Occam bin folder (`/occam/occam/bin`) is inserted into PATH by your shell initialization script (.profile or equivalent).
    * `echo "export PATH=\$PATH:/occam/occam/bin" >> ~/.profile`
  * Source the .profile script.
    * `. ~/.profile`
  * Copy the Occam example config to the Occam root.
    * `mkdir -p /occam/.occam`
    * `cp /occam/occam/config.yml.sample /occam/.occam/config.yml`
  * Edit the configuration to use the Postgres instance and Slurm cluster.
    * See the [configuration guide](configuring.md) for what fields need to be added to the database section of the config.
    * Change externalHost to the hostname of your instance. For example, ip-172-31-46-54.
    * Don't forget to change the daemon host to the IP of your instance's AWS subnet interface.
    * Change the targets section to target the Slurm cluster you just set up.
  * Initialize Occam.
    * `occam system initialize`
    * You should see several output lines referring to Postgres tables being created.
    * `occam accounts new occam`
  * Create a service file to automatically start Occam.
    * `sudo cp $PATH_TO_OCCAM/docs/systemd/occam.service /etc/systemd/system/`
    * Edit the occam.service file to use the correct user.
  * Start the Occam service.
    * `sudo systemctl start occam`

### Frontend
  * Update the package listing.
    * `sudo apt update`
  * Install the Occam frontend.
    * See the [README on the occam-web-client repo](https://gitlab.com/occam-archive/occam-web-client/-/blob/develop/README.md).
  * Create a service file to automatically start the occam frontend.
    * `sudo cp $PATH_TO_OCCAM_WEB/docs/systemd/occam-web-client.service /etc/systemd/system/`
    * Edit the occam-web-client.service file to use the `ubuntu` user.
  * Install nginx.
  * Install the example nginx configs to proxy port 80 to the occam frontend port.
    * `sudo cp $PATH_TO_OCCAM_WEB/docs/nginx/nginx.conf /etc/nginx/`
    * Edit the configuration to use the `ubuntu` user.
  * Edit the ~/.occam/web-config.yml.
    * Change the daemon host variable to point to the back-end daemon running on the back-end instance.
    * Set daemonize to true.
  * Start the Occam frontend service.
    * `sudo systemctl start occam-web-service`
  * Restart the nginx service.
    * `sudo systemctl restart nginx`

