#!/usr/bin/env bash

# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2018 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

_OCCAM_VENV_PATH=$(realpath $(dirname $(realpath $0))/../python)
_OCCAM_SRC_PATH=$(realpath $(dirname $(realpath $0))/../src)

# Source the virtual environment, if not set, and use the internal python
if [ "$VIRTUAL_ENV" != "$_OCCAM_VENV_PATH" ]; then
  if [ -d "$_OCCAM_VENV_PATH" ]; then
    source $_OCCAM_VENV_PATH/bin/activate
  fi
fi

# Carry forth
if [ ${BASH_VERSINFO[0]} -gt 4 ] || ( [ ${BASH_VERSINFO[0]} -eq 4 ] && [ ${BASH_VERSINFO[1]} -ge 4 ] ); then
  # For new versions of bash, use the expansion routine built in
  function expandq() { for i; do echo ${i@Q}; done; }
else
  # For old bash, use printf to escape quotes correctly for us
  function expandq() { for i; do printf '%q' "$i"; done; }
fi
which python3

ARGS=$(expandq "$@")
OCCAM_BIN=$(realpath $0) LD_LIBRARY_PATH="$_OCCAM_SRC_PATH/../vendor/lib":$LD_LIBRARY_PATH exec python3 "$_OCCAM_SRC_PATH"/occam/main.py "$@"
