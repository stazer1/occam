#!/bin/bash

# Discover known nodes
./bin/occam nodes discover https://occam.cs.pitt.edu
./bin/occam nodes discover https://occam.software
./bin/occam nodes discover https://archived.software

# Pull a Singularity environment
./bin/occam objects pull https://occam.cs.pitt.edu/QmZdi2nN7zzX6UL314BP1bPPDzTpZySTyCmvMFtxiuBF6T -f $@

# Pull the Python template
./bin/occam objects pull https://occam.cs.pitt.edu/QmdRGhAMjqpisdzmU1xFwHLc7LWU76ZpTuNEARBYoCiXuT -f $@

# Pull in Numpy
./bin/occam objects pull https://occam.cs.pitt.edu/QmWhaivtuNG5C7KRN6uyShrtke3HexG2jenoYCQuBtssxQ/5dsWVbBwH72u2iL6RHVEHGy257kuts -f $@

# Pull the CSV viewer
./bin/occam objects pull https://occam.cs.pitt.edu/QmRrdYaUjxSfU7Raq8FcGDjiAYMXBk9Skj9tWGrp6gNuMw -f $@

# Pull the image viewer
./bin/occam objects pull https://occam.cs.pitt.edu/QmeGqNVNWoR9XPSa33aZMXUrsADWYnM6ZTxKgjZxetgehk -f $@

# Pull a PDF viewer... (PDF.js)
./bin/occam objects pull https://occam.cs.pitt.edu/QmNooP5Lxn3jGbFvUtMzphJeTBUykVix9SDSSkT8rzeNt1 -f $@

# Pull a text editor... (Ace Editor)
./bin/occam objects pull https://occam.cs.pitt.edu/QmSk23u78xfHA2RVSBVdDeDJGpoHWQUYWWVD8dAgmanY4g -f $@

# Pull in the blank experiment
./bin/occam objects pull https://occam.cs.pitt.edu/QmZD2m1uU3hw3QpasYdcTMD7QCBcAEahsJRaBrQevv3Uz2 -f $@

# Pull in the blank object
./bin/occam objects pull https://occam.cs.pitt.edu/QmcveUc6VKvxeXjSHqmgQ8yTZmck8rd9dxre36uXi983fa -f $@
