# Add key type to each key
from occam.databases.manager import up

import occam.keys.records.identity

@up("identities")
def addKeyTypeToIdentities():
  # The type of key, which can interpret the text field
  # An empty string is the default which is RSA
  return {
    "key_type": {
      "type": "string",
      "length": 10,
      "default": ""
    }
  }

import occam.keys.records.identity_key

@up("identity_keys")
def addKeyTypeToIdentityKeys():
  # The type of key, which can interpret the text field
  # An empty string is the default which is RSA
  return {
    "key_type": {
      "type": "string",
      "length": 10,
      "default": ""
    }
  }

import occam.keys.records.verify_key

@up("signing_keys")
def addKeyTypeToSigningKeys():
  # The type of key, which can interpret the text field
  # An empty string is the default which is RSA
  return {
    "key_type": {
      "type": "string",
      "length": 10,
      "default": ""
    }
  }

import occam.keys.records.signing_key

@up("verify_keys")
def addKeyTypeToVerifyKeys():
  # The type of key, which can interpret the text field
  # An empty string is the default which is RSA
  return {
    "key_type": {
      "type": "string",
      "length": 10,
      "default": ""
    },

    # The type of signature, which defaults to PKCS1_v1_5 when empty
    "signature_type": {
      "type": "string",
      "length": 16,
      "default": ""
    },

    # The hash digest for the token message
    "signature_digest": {
      "type": "string",
      "length": 10,
      "default": ""
    },
  }

import occam.keys.records.signature

@up("signatures")
def addSignatureTypeToSignatures():
  # The type of signature, which can interpret the text field
  # An empty string is the default which is PKCS1_v1_5
  return {
    "signature_type": {
      "type": "string",
      "length": 16,
      "default": ""
    },

    # The hash digest for the token message
    "signature_digest": {
      "type": "string",
      "length": 10,
      "default": ""
    },
  }

import occam.versions.records.version

@up("versions")
def addSignatureTypeToVersions():
  # The type of signature, which can interpret the text field
  # An empty string is the default which is PKCS1_v1_5
  return {
    "signature_type": {
      "type": "string",
      "length": 16,
      "default": ""
    },

    # The hash digest for the token message
    "signature_digest": {
      "type": "string",
      "length": 10,
      "default": ""
    },
  }
