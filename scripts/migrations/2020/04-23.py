# Add key type to each key
from occam.databases.manager import up, patch

from occam.keys.records.identity_key import IdentityKeyRecord
from occam.keys.records.identity import IdentityRecord
from occam.keys.records.verify_key import VerifyKeyRecord

@up("identity_keys")
def addPublishedToIdentityKeys():
  # The type of key, which can interpret the text field
  # An empty string is the default which is RSA
  return {
    "published": {
      "type": "datetime"
    }
  }

@up("identities")
def addPublishedToIdentityKeys():
  # The type of key, which can interpret the text field
  # An empty string is the default which is RSA
  return {
    "published": {
      "type": "datetime"
    }
  }

import occam.builds.records.build

@up("builds")
def addSignatureTypeToBuilds():
  # The type of signature, which can interpret the text field
  # An empty string is the default which is PKCS1_v1_5
  return {
    "signature_type": {
      "type": "string",
      "length": 16,
      "default": ""
    },

    # The hash digest for the token message
    "signature_digest": {
      "type": "string",
      "length": 10,
      "default": ""
    },
  }

@patch("identity_keys")
def fixPublishedForExistingKeys(database):
  # For each identity key that does not have a published date, it will be the
  # published date for the earliest verifying key.

  from occam.databases.manager import table

  @table("identity_keys")
  class IdentityKeyRecord:
    schema = {
      "uri": {
        "type": "string",
        "length": 256,
        "primary": True
      },
      "signing_key_id": {
        "foreign": {
          "key":   "id",
          "table": "signing_keys"
        }
      },
      "key": {
        "type": "text"
      },
      "key_type": {
        "type": "string",
        "length": 10,
        "default": ""
      },
      "published": {
        "type": "datetime"
      }
    }

  @table("verify_keys")
  class VerifyKeyRecord:
    schema = {
      "id": {
        "type": "string",
        "length": 256,
        "primary": True
      },
      "uri": {
        "foreign": {
          "key": "uri",
          "table": "identities"
        }
      },
      "revokes": {
        "type": "string",
        "length": 256
      },
      "key": {
        "type": "text"
      },
      "key_type": {
        "type": "string",
        "length": 10,
        "default": ""
      },
      "published": {
        "type": "datetime",
      },
      "signature": {
        "type": "binary",
        "length": 512
      },
      "signature_type": {
        "type": "string",
        "length": 16,
        "default": ""
      },
      "signature_digest": {
        "type": "string",
        "length": 10,
        "default": ""
      },
    }

  @table("identities")
  class IdentityRecord:
    schema = {
      "uri": {
        "type": "string",
        "length": 256,
        "primary": True
      },
      "key": {
        "type": "text"
      },
      "key_type": {
        "type": "string",
        "length": 10,
        "default": ""
      },
      "published": {
        "type": "datetime"
      }
    }

  import sql, sys

  session = database.session()

  identity_keys = sql.Table("identity_keys")
  identities = sql.Table("identities")
  verify_keys = sql.Table("verify_keys")

  query = identity_keys.select()
  database.execute(session, query)
  rows = database.many(session, size = 1000)

  for row in rows:
    row = IdentityKeyRecord(row)
    if row.published is None:
      # Find the earliest verify key, and use the published there
      print("Patching identity key... ", end="")
      sys.stdout.flush()

      query = verify_keys.select()
      query.where = verify_keys.uri == row.uri
      query.order_by = sql.Asc(verify_keys.published)
      database.execute(session, query)
      key = VerifyKeyRecord(database.fetch(session))

      row.published = key.published
      database.update(session, row)
      database.commit(session)

      print("Done.")

  query = identities.select()
  database.execute(session, query)
  rows = database.many(session, size = 1000)

  for row in rows:
    row = IdentityRecord(row)
    if row.published is None:
      # Find the earliest verify key, and use the published there
      print("Patching identity record... ", end="")
      sys.stdout.flush()

      query = verify_keys.select()
      query.where = verify_keys.uri == row.uri
      query.order_by = sql.Asc(verify_keys.published)
      database.execute(session, query)
      key = VerifyKeyRecord(database.fetch(session))

      row.published = key.published
      database.update(session, row)
      database.commit(session)

      print("Done.")
