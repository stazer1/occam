#!/bin/bash
set -eu

mkdir -p ./vendor

export LD_LIBRARY_PATH=$PWD/vendor/lib:${LD_LIBRARY_PATH-}
export PATH=$PWD/vendor/bin:${PATH-}

cd vendor

if [ ! -d Python-3.8.2 ]; then
	wget https://www.python.org/ftp/python/3.8.2/Python-3.8.2.tgz
	tar xvf Python-3.8.2.tgz
	cd Python-3.8.2
	./configure --with-ensurepip=install --prefix=$PWD/.. --enable-loadable-sqlite-extensions LDFLAGS="-L$PWD/../lib" CPPFLAGS="-I$PWD/../include"
	make -j4
	make install
	cd ..
fi

cd ..
