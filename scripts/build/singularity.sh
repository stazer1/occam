#!/bin/bash
set -eu

mkdir -p ./vendor
mkdir -p ./vendor/bin

export LD_LIBRARY_PATH=$PWD/vendor/lib:${LD_LIBRARY_PATH-}
export PATH=$PWD/vendor/bin:${PATH-}

VERSION=1.13
OS=linux
ARCH=amd64

cd vendor

# Install go
if [ ! -d go ]; then
  wget https://dl.google.com/go/go$VERSION.$OS-$ARCH.tar.gz
  tar -xvf go$VERSION.$OS-$ARCH.tar.gz
  rm go$VERSION.$OS-$ARCH.tar.gz    # Deletes the ``tar`` file
  ln -s $PWD/go/bin/go bin/go
fi

cd ..

# Build Singularity
VERSION=3.10.0

cd vendor

if [ ! -d singularity ]; then
  wget https://github.com/sylabs/singularity/archive/refs/tags/v${VERSION}.tar.gz
  tar -xzf v${VERSION}.tar.gz
  cd singularity-${VERSION}
  ./mconfig --prefix=$PWD/..
  make -C builddir
  make -C builddir install
  cd ..
fi

cd ..
