# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import json
import datetime

from occam.config import Config

from occam.manager import uses, manager
from occam.log import loggable

from occam.databases.manager   import DatabaseManager
from occam.storage.manager     import StorageManager
from occam.permissions.manager import PermissionManager
from occam.caches.manager      import CacheManager
from occam.resources.manager   import ResourceManager

@loggable
@uses(DatabaseManager)
@uses(StorageManager)
@uses(PermissionManager)
@uses(ResourceManager)
@uses(CacheManager)
@manager("links")
class LinkManager:
  """ Manages relationships among objects and synchronization of objects and local clones.

  Essentially, some objects can be created in a directory and then modified
  outside of that context. Perhaps, through a web-based client. This manager
  establishes a link between objects and their local content ensuring that if
  they are updated, they change locally as well.

  This is useful for caching and ensuring that, for instance, a workset that
  is being modified isn't cloned repeatedly. It also gives a single point for
  all collaborators to modify an object.

  Other relationships tie objects together. One such type of relationship is
  one between a Person and an Object. Such as a recently used items list,
  bookmarks, and any active work. A client can make links and use them to
  organize objects appropriately.

  Links are not propagated across the federation.
  """

  def _cacheKey(self, object_id, relationship,
                                 order = "ascending",
                                 reverse = False):
    """ Internal method to produce the cache key for the given object and relationship.

    Arguments:
      object_id (string): The object id of the source or target of the relationship.
      relationship (string): The 'kind' of link.
      order (string): The order of the list.
      reverse (bool): Whether the source object was being sought.
    """

    searching_for = "t"

    if reverse:
      searching_for = "s"

    return f"links-{searching_for}-{object_id}-{relationship}-{order}"

  def retrieve(self, source_db, relationship, target_object_db=None, target_revision=None):
    """ Retrieves a list of LinkRecords for the given source and relationship.

    You may optionally provide the target to filter to just the single link
    (if it exists) between source and target. It will return a list of one
    link (or an empty list.)
    """

    from occam.objects.records.object import ObjectRecord
    from occam.keys.records.identity import IdentityRecord

    sourceObjectId = None
    sourceIdentityUri = None

    if isinstance(source_db, ObjectRecord):
      sourceObjectId = source_db.id
    elif isinstance(source_db, IdentityRecord):
      sourceIdentityUri = source_db.uri
    else:
      raise TypeError("source_db not ObjectRecord or IdentityRecord")

    if target_object_db and not isinstance(target_object_db, ObjectRecord):
      raise TypeError("Target parameter not type of ObjectRecord")

    if target_revision and not isinstance(target_revision,str):
      target_revision = str(target_revision)

    targetObjectId = None

    if not isinstance(relationship, str):
      relationship = str(relationship)

    if target_object_db:
      targetObjectId = target_object_db.id

    return self.datastore.retrieveLinks(sourceObjectId       = sourceObjectId,
                                        sourceIdentityUri    = sourceIdentityUri,
                                        targetObjectId       = targetObjectId,
                                        targetObjectRevision = target_revision,
                                        relationship         = relationship)

  def retrieveResources(self, source_db=None,
                              target_db=None,
                              relationship=None,
                              order="ascending",
                              sourceRevision=None,
                              targetRevision=None,
                              reverse=False):
    """ Retrieves an ObjectRecord list for the links for the given source and relationship.
    """

    from occam.objects.records.object import ObjectRecord
    from occam.resources.records.resource import ResourceRecord
    from occam.keys.records.identity import IdentityRecord

    sourceObjectId = None
    sourceIdentityUri = None
    # Interpret source_db. It must be in a known format or optionally None if
    # we are performing a reverse lookup.
    if isinstance(source_db, ObjectRecord):
      sourceObjectId = source_db.id
    elif isinstance(source_db, IdentityRecord):
      sourceIdentityUri = source_db.uri
    elif not (source_db is None and reverse):
      raise TypeError("source_db not ObjectRecord or IdentityRecord")

    # When doing reverse lookup the target is required.
    if reverse and not isinstance(target_db, ObjectRecord):
      raise TypeError("target_db not ObjectRecord or IdentityRecord")

    if relationship:
      cacheObjId = sourceObjectId or sourceIdentityUri
      # If we are searching by target use that to construct the cache key.
      if reverse:
        cacheObjId = target_db.id
      cacheKey = self._cacheKey(cacheObjId, relationship, order, reverse)

      data = self.cache.get(cacheKey)

      if not data is None:
        import codecs
        return [ObjectRecord(x) for x in json.loads(codecs.decode(data, 'utf8'))]

    session = self.database.session()

    import sql

    links     = sql.Table('links')
    resources = sql.Table('resources')

    join = links.join(resources)
    if reverse:
      join.condition = (links.source_object_id == join.right.id)
    else:
      join.condition = (links.target_resource_id == join.right.id)

    query = join.select(links.id.as_('link_id'),
                        links.source_revision,
                        links.target_revision,
                        links.relationship,
                        links.local_link_id,
                        join.right.id,
                        join.right.uid,
                        join.right.resource_type,
                        join.right.identity_uri,
                        join.right.source,
                        join.right.name)

    query.where = sql.Literal(True)
    if sourceObjectId:
      query.where = (links.source_object_id == sourceObjectId)
    elif sourceIdentityUri:
      query.where = (links.source_identity_uri == sourceIdentityUri)

    if sourceRevision:
      query.where = query.where & (links.source_revision == sourceRevision)

    if targetRevision:
      query.where = query.where & (links.target_revision == targetRevision)

    if relationship:
      query.where = query.where & (links.relationship == relationship)

    if target_db:
      query.where = query.where & (links.target_object_id == target_db.id)

    if order == "ascending":
      query.order_by = sql.Asc(links.id)
    else:
      query.order_by = sql.Desc(links.id)

    self.database.execute(session, query)

    ret = self.database.many(session, size=100)

    if relationship:
      self.cache.set(cacheKey, json.dumps(ret))

    return [ResourceRecord(x) for x in ret]

  def retrieveObjects(self, source_db=None,
                            target_db=None,
                            relationship=None,
                            order="ascending",
                            sourceRevision=None,
                            targetRevision=None,
                            reverse=False):
    """ Retrieves an ObjectRecord list for the links for the given source and relationship.
    """

    from occam.objects.records.object import ObjectRecord
    from occam.keys.records.identity import IdentityRecord

    sourceObjectId = None
    sourceIdentityUri = None
    # Interpret source_db. It must be in a known format or optionally None if
    # we are performing a reverse lookup.
    if isinstance(source_db, ObjectRecord):
      sourceObjectId = source_db.id
    elif isinstance(source_db, IdentityRecord):
      sourceIdentityUri = source_db.uri
    elif not (source_db is None and reverse):
      raise TypeError("source_db not ObjectRecord or IdentityRecord")

    # When doing reverse lookup the target is required.
    if reverse and not isinstance(target_db, ObjectRecord):
      raise TypeError("target_db not ObjectRecord or IdentityRecord")

    if relationship:
      cacheObjId = sourceObjectId or sourceIdentityUri
      # If we are searching by target use that to construct the cache key.
      if reverse:
        cacheObjId = target_db.id
      cacheKey = self._cacheKey(cacheObjId, relationship, order, reverse)

      data = self.cache.get(cacheKey)

      if not data is None:
        import codecs
        return [ObjectRecord(x) for x in json.loads(codecs.decode(data, 'utf8'))]

    session = self.database.session()

    import sql

    links   = sql.Table('links')
    objects = sql.Table('objects')

    join = links.join(objects)
    if reverse:
      join.condition = (links.source_object_id == join.right.id)
    else:
      join.condition = (links.target_object_id == join.right.id)

    query = join.select(links.id.as_('link_id'),
                        links.source_revision,
                        links.target_revision,
                        links.relationship,
                        links.local_link_id,
                        join.right.id,
                        join.right.object_type,
                        join.right.subtype,
                        join.right.name,
                        join.right.organization,
                        join.right.description,
                        join.right.updated,
                        join.right.published,
                        join.right.file,
                        join.right.tags,
                        join.right.environment,
                        join.right.architecture,
                        join.right.capabilities)

    query.where = sql.Literal(True)
    if sourceObjectId:
      query.where = (links.source_object_id == sourceObjectId)
    elif sourceIdentityUri:
      query.where = (links.source_identity_uri == sourceIdentityUri)

    if sourceRevision:
      query.where = query.where & (links.source_revision == sourceRevision)

    if targetRevision:
      query.where = query.where & (links.target_revision == targetRevision)

    if relationship:
      query.where = query.where & (links.relationship == relationship)

    if target_db:
      query.where = query.where & (links.target_object_id == target_db.id)

    if order == "ascending":
      query.order_by = sql.Asc(links.id)
    else:
      query.order_by = sql.Desc(links.id)

    self.database.execute(session, query)

    ret = self.database.many(session, size=100)

    if relationship:
      self.cache.set(cacheKey, json.dumps(ret))

    return [ObjectRecord(x) for x in ret]

  def path(self):
    """ Returns the path to the local tracked-object store.
    """

    return self.configuration.get('path', os.path.join(Config.root(), 'links'))

  def basePathFor(self, link, path):
    """ Returns the stage page when the path is within a staging path.

    If you pass this some kind of path generated by `pathFor`, it will yield the
    base path for the stage. This matches the exact result returned by `pathFor`
    prior to this call.

    If one moves the staging paths and also reconfigures such that `pathFor`
    would generate a path in the new staging path, this would respond just as
    though the path were generated from this configuration even though it was
    moved there. Although, local links that are tracked store absolute paths to
    the stage even when they are staged in the internal staging space for the
    system.

    Returns:
      str: The base path of the stage, if an internal stage, otherwise None.
    """

    comparePath = self.path()
    if os.path.commonpath([comparePath, path]) == comparePath:
      return self.pathFor(link)

    return None

  def pathFor(self, link):
    """ Returns the path for the tracked object base on the link specified.

    There is no guarantee that the path returned actually exists.

    The `path` parameter must be at least 4 characters.

    Arguments:
      link (str): The local link identifier.

    Returns:
      str: The expected path to the root of the tracked object.
    """

    if len(link) < 4:
      raise ValueError("link identifier must be at least 4 characters")

    # Find the full path to the stored invocation metadata
    # Ex: .occam/links/2-code/2-code/full-code

    code = link
    code1 = code[0:2]
    code2 = code[2:4]

    path = os.path.join(self.path(), code1, code2, code)
    os.makedirs(path, exist_ok = True)

    return path

  def localPathFor(self, object):
    """ Retrieves the working directory for running the given staged object.

    Ensures the path exists.

    Arguments:
      object (Object): The staged object to query.

    Returns:
      str: The path to the object's local path.
    """

    # Determine the task path from the object's path
    basePath = os.path.join(object.path, "..")
    localPath = os.path.join(basePath, "local")

    # Ensure the path exists
    os.makedirs(localPath, exist_ok = True)

    # Return the path
    return localPath

  def stagePathFor(self, object):
    """ Retrieves the stage path for the given staged object.

    Ensures the path exists.

    Arguments:
      object (Object): The staged object to query.

    Returns:
      str: The path to the object's stage path.
    """

    # Determine the task path from the object's path
    basePath = os.path.join(object.path, "..")
    stagePath = os.path.join(basePath, "stage")

    # Ensure the path exists
    os.makedirs(stagePath, exist_ok = True)

    # Return the path
    return stagePath

  def taskPathFor(self, object, build=False):
    """ Returns the path to the given staged object's build or run task.

    The path does not need to actually contain a valid task.

    Arguments:
      object (Object): The staged object whose task path should be reported.
      build (bool): When True, retrieves a build task.

    Returns:
      str: The path to the object's build or run task.
    """

    # Determine the task path from the object's path
    basePath = os.path.join(object.path, "..")
    taskPath = os.path.join(basePath, "run")
    if build:
      taskPath = os.path.join(basePath, "build")

    # Ensure the path exists
    os.makedirs(taskPath, exist_ok = True)

    # Return the path
    return taskPath

  def taskFor(self, object, build=False):
    """ Retrieves the run or build task for the given staged object, if it exists.

    Arguments:
      object (Object): The staged object to query.
      build (bool): When True, retrieves the build task.

    Returns:
      dict: The task manifest or None if it does not exist.
    """

    taskPath = self.taskPathFor(object, build = build)
    taskFile = os.path.join(taskPath, "object.json")

    task = None
    if os.path.exists(taskFile):
      with open(taskFile, "r") as f:
        try:
          task = json.load(f)
        except json.JSONDecodeError as e:
          # OK, fine. An error means there is no task.
          pass

    return task

  def logFor(self, object, build=False):
    """ Returns the run log for the run or build for the staged object.

    Arguments:
      object (Object): The staged object whose build run path will be reported.
      build (bool): When True, retrieves the staged build log.

    Returns:
      bytes: An IO stream for the log bytes.
    """

    runPath = self.runPathFor(object, build = build)
    logPath = os.path.join(runPath, "output.log")

    return open(logPath, 'rb')

  def metadataFor(self, object, build=False):
    """ Returns the run metadata for the run or build for the staged object.

    Arguments:
      object (Object): The staged object whose build run path will be reported.
      build (bool): When True, retrieves the staged build metadata.

    Returns:
      bytes: An IO stream for the metadata bytes.
    """

    runPath = self.runPathFor(object, build = build)
    metadataPath = os.path.join(runPath, "metadata.json")
    return open(metadataPath, 'rb')

  def buildPathFor(self, object):
    """ Returns the staged build package path for the given staged object.

    Arguments:
      object (Object): The staged object whose build path will be reported.

    Returns:
      str: The path to the object's build package.
    """

    return os.path.join(object.path, "..", "build", "result")

  def runPathFor(self, object, build=False):
    """ Returns the path to the given staged object's build or run path.

    This path contains the virtual environment in which the object is built or
    runs. It is instantiated by the JobManager when a job is deployed during
    the job's allocated time.

    The path does not need to actually contain an instantiated environment.

    Arguments:
      object (Object): The staged object whose build run path will be reported.
      build (bool): When True, retrieves the staged build path.

    Returns:
      str: The path to the object's build run path.
    """

    # Determine the task path from the object's path
    basePath = os.path.join(object.path, "..")
    runPath = os.path.join(basePath, "run", "vm")
    if build:
      runPath = os.path.join(basePath, "build", "vm")

    # Ensure the path exists
    os.makedirs(runPath, exist_ok = True)

    # Return the path
    return runPath

  def retrieveLocalLinks(self, obj, path=None, linkId=None):
    """ Retrieves the LocalLink, if it exists, for the given object.

    This should be done for every read/write of an Object that might be done.
    If there is a local link, it serves as a cached repository for that Object.

    That is, and in particular collaborations or large project worksets, this
    is used not only for command line usage that goes along with any web or GUI
    client... it is also used internally or by clients to create a quicker way
    of modifying an object repeatedly. That way, the object does not need to be
    cloned and then modified and then stored each time.
    """

    import sql

    session = self.database.session()

    from occam.links.records.local_link import LocalLinkRecord

    links = sql.Table("links")
    subQuery = links.select(links.local_link_id, where = (links.id == linkId))

    localLinks = sql.Table("local_links")
    objects    = sql.Table("objects")

    query = localLinks.select(where = (localLinks.target_object_id == obj.id))

    if linkId:
      query.where = query.where & (localLinks.id == linkId)

    if path:
      query.where = query.where & (localLinks.path == path)

    self.database.execute(session, query)

    return [LocalLinkRecord(x) for x in self.database.many(session)]

  def __retrieveFileStatFrom(self, path):
    """ Returns the item info for the given file.

    Arguments:
      path (str): The path to a local file.

    Returns:
      dict: The item metadata.
    """

    itemInfo = {
      'name': os.path.basename(path),
      'type': 'tree' if os.path.isdir(path) else 'blob'
    }

    if os.path.islink(path):
      stat = os.lstat(path)
      itemInfo['mode'] = stat.st_mode
      itemInfo['link'] = 'symbolic'
      itemInfo['size'] = os.lstat(path).st_size
    else:
      stat = os.stat(path)
      itemInfo['mode'] = stat.st_mode
      itemInfo['size'] = os.path.getsize(path)

    itemInfo['date'] = datetime.datetime.fromtimestamp(stat.st_mtime).isoformat() + "Z"

    itemInfo['mime'] = self.storage.mimeTypeFor(itemInfo['name'])

    return itemInfo

  def retrieveDirectoryFrom(self, obj, path="/", build=False):
    """ Retrieves a directory listing for the given local object.

    Arguments:
      obj (Object): The object to use. It should be local (have a link field.)
      path (str): The file path to list.

    Returns:
      dict: The directory listing metadata (or None if the path is not found.)
    """

    if not obj.link:
      return None

    objectPath = os.path.join(obj.path, path[1:])
    if build:
      objectPath = os.path.join(obj.path, "..", "build", "result", path[1:])

    if not os.path.exists(objectPath):
      return None

    data = {'items': []}

    for file in os.listdir(objectPath):
      itemPath = os.path.join(objectPath, file)
      itemInfo = self.__retrieveFileStatFrom(itemPath)
      data['items'].append(itemInfo)

    return data

  def retrieveFileFrom(self, obj, path="/", build=False):
    """ Retrieves file data from a local link for the given path.

    Arguments:
      obj (Object): The object to use. It should be local (have a link field.)
      path (str): The file path to view.

    Returns:
      File: A stream of the file data.
    """

    if not obj.link:
      return None

    objectPath = os.path.join(obj.path, path[1:])
    if build:
      objectPath = os.path.join(obj.path, "..", "build", "result", path[1:])

    if not os.path.exists(objectPath):
      return None

    return open(objectPath, "rb")

  def retrieveFileStatFrom(self, obj, path="/", build=False):
    """ Retrieves file data from a local link for the given path.

    Arguments:
      obj (Object): The object to use. It should be local (have a link field.)
      path (str): The file path to view.

    Returns:
      File: A stream of the file data.
    """

    if not obj.link:
      return None

    objectPath = os.path.join(obj.path, path[1:])
    if build:
      objectPath = os.path.join(obj.path, "..", "build", "result", path[1:])

    if not os.path.exists(objectPath):
      return None

    return self.__retrieveFileStatFrom(objectPath)

  def infoFor(self, object):
    """ Retrieves the current object metadata from a staged object.
    """

    info = None
    with open(os.path.join(object.path, "object.json"), "r") as f:
      info = json.load(f)

    return info

  def syncRequiredFor(self, object):
    """ Determines and returns a list of any files that need to be synchronized.

    Returns:
      list: A set of strings of relative paths of updated files.
    """

    return []

  def stageRequiredFor(self, object):
    """ Determines if the object metadata has changed and requires restaging.

    Returns:
      bool: True when a restage is required.
    """

    # If the stage path does not exist, then it is also required
    stagePath = os.path.join(object.path, "..", "stage")
    if not os.path.exists(stagePath):
      return True

    resourcesPath = os.path.join(object.path, "..", "metadata", "object.resources.json")

    oldResources = []
    if os.path.exists(resourcesPath):
      with open(resourcesPath, "r+") as f:
        oldResources = json.load(f)

    # Get the current metadata
    info = self.infoFor(object)

    # Gather the resources
    resources = self.resources.resourcesFor(info, build = ('build' in info))

    # Look for changes between this metadata and the cached metadata
    # Decide on what parts of the caches to update.

    # Retain the current resource list
    with open(resourcesPath, "w+") as f:
      f.write(json.dumps(resources))

  def taskRequiredFor(self, object):
    """ Determines if a new task must be created for the object.

    Normally, the task is cached are re-used for staged objects. This is to
    more quickly run an object when only data files have changed and not the
    metadata.

    Arguments:
      object (Object): The staged object whose task cache will be evaluated.
    """

    # Gather the object metadata
    info = self.infoFor(object)

    # Read the former cached manifest
    manifestPath = os.path.join(object.path, "..", "metadata", "object.json")

    oldInfo = {}
    if os.path.exists(manifestPath):
      with open(manifestPath, "r+") as f:
        oldInfo = json.load(f)

    # Do not rebuild the task if the metadata has not changed
    if json.dumps(info) == json.dumps(oldInfo):
      return False

    # TODO: do not remove the cache if only descriptive metadata has changed
    # This would just mean removing the descriptive metadata from the info block

    # Make a copy of the current object manifest to the metadata path
    with open(manifestPath, "w+") as f:
      f.write(json.dumps(info))

    # Report that we want the task rebuilt
    return True
