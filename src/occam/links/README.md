# The Links Component

This component manages links between objects and local directories.

## Traditional Relationships

To manage lists of objects and ties between them, a `LinkRecord` can be created
for that purpose. The link is a simple record with a source ID (which can be an
object identifier or actor identity), a target ID (again, either object or actor
identity), and a relationship "kind" which is some string that identifies the
type of relationship.

For instance, the bookmarks list consists of all records between the source ID
for an actor's identity of the "bookmark" relationship. The "active"
relationship holds the current objects that are part of a person's collection.
Also, objects can create relationships between them, such as supplemental
material (manuals, documentation, screenshots, etc) when necessary and clients
can then render those lists as they wish.

```python
self.links.create()
```

## Local Links

The LinkManager also keeps track of local instances of Occam objects. Occam can
manage a directory that holds in-progress work in an active object. Such links
are internally called "tracked" links.

Here, a local identifier is used to redirect requests for an object to this
managed directory. The identifier, here, is `3`, but this is generally very
simplified. A true identifier would likely be a `uuid` and much longer. See
the section about "Staged Objects" for a more accurate portrayal.

The actions (listing, viewing, modifying, etc) made to an object get, instead,
made to this copy when the link identifier is specified. For instance, the 
ollowing commands perform actions on a tracked object:

```shell
# View a file in a tracked object
occam objects view QmTUJxV1VKFZzHp4zKCatxvUHZGWa6v9fjafj2atnjC37Y#3/src/main.c

# Update metadata in a tracked object
occam objects set QmTUJxV1VKFZzHp4zKCatxvUHZGWa6v9fjafj2atnjC37Y#3/object.json -i summary "This is my object!"

# Build a tracked object
occam builds new QmTUJxV1VKFZzHp4zKCatxvUHZGWa6v9fjafj2atnjC37Y#3
```

The tracked object itself is staged where edits are contained within the stage
and not immediately visible to the global object store. The staged object itself
can reference other objects as children. These are then also staged within the
context of the tracked object. Therefore, a tracked object can have multiple
staged objects. Referencing them is done the normal way:

```shell
# View a file in a staged object child of the tracked object
occam objects view QmTUJxV1VKFZzHp4zKCatxvUHZGWa6v9fjafj2atnjC37Y#3[0]/src/main.c
```

Local objects act very differently than objects that are properly kept in the
object repositories. Specifically, all tasks are generated within their tracked
directory and are cached there. When a build is performed, the build is done
incrementally. That is, it reuses the task and virtual environment from one
invocation of the build to another. This mimics the normal development cycle
of `make` where it only builds the content that changes which allows for a more
convenient development process.

Yet, this runs counter to the goals of software preservation. Therefore, special
consideration is made for staged objects. They cannot be used in any situation
that requires preservation quality, such as workflows. They can only be ran in
special situations and their output is, itself, not preserved. There are extra
flags for deploying tasks that can clean the environment to ensure that the
object properly builds and runs from a known state. When the object is
"committed," the object will then have to be built from such a clean state in
order to be used in a public-facing situation, such as a workflow. This is
elaborated further below.

## Staged Objects

Staged objects have a directory structure that maintains both the working index
of the object, and it has several caches for metadata and task directories.

A 'linked' staged object will exist in the links path, by default
`$OCCAM_ROOT/links`, under a subdirectory that matches the link's random id.
Let's say one such directory would be
`$OCCAM_ROOT/links/1e/d6/1ed6087c-b7af-4708-8c7c-414be552f39a`. Within this
path are a set of directories that manage all of the objects within the tracked
object. Each of these objects is considered "staged" in that the files within
are not stored, but rather in-progress. They are the only record of these edits
until a snapshot is committed to the proper object store via a `pull` command.

The `data/` path contains the actual working index of the object. This is the
main record of the data within the object. This is represented by the `git`
repository for the object itself which matches and is linked to the one found
in the object store. When files are modified by the client, the files are also
modified within this directory and a `git add` command is issued to track that
edit.

When a publish happens via a `pull` command with the staged object as its
target, the `git commit` command happens and the index is then written to the
object store proper. This means the current index is then reflecting no new
edits. That is, there is now nothing staged.

When running (or building) the object, there needs to be a place for the object
data to exist that can also exist within the running environment. Normally, this
means mounting a path to a container from which the object can be invoked. This
is the `stage/` path. This directory must be synchronized with the `data/` path
when a run is deployed. (We will not worry about the active race condition that
might occur when an actor edits a file after pressing run... it is fine since
the synchronization must be queued.)

When the run is deployed, the `stage/` directory is synchronized with the
`data/` path (unless reasonable assumption that there is no need). The resources
linked by the object via the `install` field within the `object.json` for the
object are also synchronized as needed. This path is then mounted into the
runtime environment in place of the normal mount to the object store. When it is
a build environment, this mount is a writable local directory. Otherwise, the
mount is read-only. This follows the same rule as all globally invoked objects.

For build environments, the `stage/` path might be modified. We keep these
modifications and only purge the stage path when metadata changes reflect a
need (such as adding a new external resource). We do not need to re-stage when
a dependency is added, for instance. We only need to regenerate a task in this
case. Build stages are modifiable with their changes kept in order to
faciliate the common need to perform iterative development. That is, when you
compile a project and encounter an error, there is an expectation that fixing
the file with the error will mean that recompilation picks up from the point it
left off. This is possible only when the build scripts are properly written, but
it is nonetheless common with even the most basic `make` scripts.

A special command can be used to ensure that the build stage is cleared prior to
being invoked. This can help faciliate an easy `make clean` style operation to
inspect if build repeatability is achieved. In the end, a published object must
be built from a known fresh starting point, so it is wise to always attempt to
build from a clean stage prior to publishing, from the actor's point-of-view.

The `metadata/` path contains hashes and other metadata that are useful in the
determination of the staleness of caches. For instance, the system might place a
file within the `metadata/` path that stores the current knowledge of resources
that exist within the `stage/` path. If the current object metadata differs from
this file, we know we have a stale `stage/` and we need to rebuild it prior to
invocation.

The `contains/` path is a place where collected objects that are descendents of
the tracked object are themselves recursively staged. The contents of this path
are a set of sub-directories with names matching object identifiers. Each of
these are similar to the root of the tracked object consisting of its own
`data/`, `stage/`, `contains/` and `metadata/` sub-paths.

The `tasks/` path, finally, is the path in which generated tasks and their
running environments are found.

### Staged File System Overview

Here is a larger point of view of the tracked object path and staged objects:

```
$OCCAM_ROOT/links/1e/d6/1ed6087c-b7af-4708-8c7c-414be552f39a/
  data/
    my-file.txt
    object.json
  metadata/
    object.json
    object.resources.json
  stage/
    my-file.txt
    object.json
    my-resource.csv
  task/
    task.json
  contains/
    QmXZDPLmTbygvajDPsUq1dwmC9UTQC8PAnHzroq5jgikCa/
      data/
        my-child.txt
        object.json
      metadata/
        object.json
        object.resources.json
      stage/
        my-child.txt
        object.json
        my-resource.zip
        my-resource/
          some-unzipped-file.txt
      task/
        task.json
```
