# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import json
import shutil
import subprocess

from occam.config import Config
from occam.git_repository import GitRepository

from occam.manager import uses, manager
from occam.log     import loggable

from occam.databases.manager   import DatabaseManager
from occam.storage.manager     import StorageManager
from occam.permissions.manager import PermissionManager
from occam.caches.manager      import CacheManager
from occam.links.manager       import LinkManager
from occam.objects.manager     import ObjectManager
from occam.resources.manager   import ResourceManager

@loggable
@uses(DatabaseManager)
@uses(StorageManager)
@uses(PermissionManager)
@uses(ObjectManager)
@uses(CacheManager)
@uses(ResourceManager)
@uses(LinkManager)
@uses(ObjectManager)
@manager("links.write", reader=LinkManager)
class LinkWriteManager:
  """ This manages the creation and updating of links.
  """

  def create(self, source_db,
                   relationship,
                   target_db,
                   target_revision,
                   limit = None,
                   local_link_id = None,
                   person = None,
                   source_revision = None):
    """ Creates a link between two objects.

    This is used primarily to connect a Person to an Object. For instance, a
    "bookmark" link will let people save objects at a particular revision to
    a list. This way, they can recall them at some point.

    The main list of active work for a Person is the "collected" relation. When
    one removes an item from their dashboard, that item then becomes "archived"
    for one particular example of this might be used by an Occam client.

    If the link already exists, the relationship will be updated.

    You cannot add a link to an object you cannot see.
    You cannot add a link from an object you cannot edit.

    Args:
      source_db (ObjectRecord, IdentityRecord): The object or identity record to link from.
      relationship (str): The relationship between the objects.
      target_db (ObjectRecord, ResourceRecord): The object record to link to.
      target_revision (str): The revision of the target object.
      limit (int): The maximum number of links for this relation.
        Defaults to None which implies an infinite number. When the limit is
        set, it will add this link, and if the limit is exceeded, truncate the
        oldest stored link.
      local_link_id (str): The identifier of the local link.
      person (Person): The person adding the link, for permission checking.
      source_revision (str): The revision of the source object.

    Returns:
      LinkRecord: The resulting link record or None if the record cannot be created.
    """

    from occam.objects.records.object import ObjectRecord

    sourceObjectId = None
    sourceIdentityUri = None

    if isinstance(source_db, ObjectRecord):
      if not self.permissions.can("write", source_db.id, person=person):
        LinkWriteManager.Log.error(f"Person {person.id} has no write permission for {source_db.id}")
        return None

      sourceObjectId = source_db.id
    else:
      # Pull the identity record uri
      if person.identity == source_db.uri:
        sourceIdentityUri = source_db.uri

      # Cannot set a link if the actor is not the one linking
      if sourceIdentityUri is None:
        LinkWriteManager.Log.error(f"Person {person.id} cannot link to an actor that is not themselves")
        return None

    # Need to have read access to the object being linked
    targetObjectId = None
    targetResourceId = None
    if isinstance(target_db, ObjectRecord):
      if not self.permissions.can("read", target_db.id, person = person):
        LinkWriteManager.Log.error(f"Person {person.id} has no read permission for {target_db.id}")
        return None

      targetObjectId = target_db.id
    else:
      targetResourceId = target_db.id

    # Create a new LinkRecord

    import datetime
    published = datetime.datetime.now()
    link = self.datastore.write.createLink(sourceObjectId       = sourceObjectId,
                                           sourceIdentityUri    = sourceIdentityUri,
                                           sourceRevision       = source_revision,
                                           targetObjectId       = targetObjectId,
                                           targetResourceId     = targetResourceId,
                                           targetObjectRevision = target_revision,
                                           relationship         = relationship,
                                           published            = published,
                                           trackingId           = local_link_id)

    if limit and limit > 0:
      self.datastore.write.truncateLinks(sourceObjectId       = sourceObjectId,
                                         sourceIdentityUri    = sourceIdentityUri,
                                         relationship         = relationship,
                                         limit                = limit)

    # Invalidate the cache key
    self.cache.delete(self.links._cacheKey(sourceObjectId or sourceIdentityUri, relationship, order = "ascending"))
    self.cache.delete(self.links._cacheKey(sourceObjectId or sourceIdentityUri, relationship, order = "descending"))

    return link

  def createLocalLink(self, db_object, revision, path, link=None):
    """ Creates a link between a local data object and a local path.
    
    This record will allow changes that happen within the system
    or seen by the outside to be reflected at the current path automatically
    in some situations.

    When path is None, the tracked object is placed in a tracked object pool
    somewhere within the occam system's file storage. These are system-tracked
    objects and have some special semantics. For instance, they collapse
    whenever revisions eventually match other tracked objects. They also
    regenerate when they are missing, as opposed to non-system local links
    which are destroyed when the path they track goes away.

    Returns the LocalLinkRecord created.

    Arguments:
    Returns:
    """

    from uuid import uuid4
    if link is None:
      link = str(uuid4())

    # Create a suitable path outside of the object store.
    if path is None:
      path = self.links.pathFor(link = link)

    from occam.links.records.local_link import LocalLinkRecord

    session = self.database.session()

    local_link = LocalLinkRecord()

    # Assign keys to local link.
    local_link.id = link
    local_link.path = path
    local_link.target_object_id = db_object.id
    local_link.revision = revision

    LinkManager.Log.noisy("creating local link between object (%s) at %s" % (db_object.id, path))

    self.database.update(session, local_link)
    self.database.commit(session)

    return local_link

  def updateLocalLink(self, link, newRevision):
    """ Updates the status of the local version based on a stored revision.

    It will collapse internal tracked objects when the newRevision is one that
    is already known and tracked elsewhere. This can happen when objects are
    reverted. Essentially, it just deletes it and lets the current LocalLink
    handle tracking the object from now on.
    """

    import sql

    session = self.database.session()

    link.revision = newRevision

    self.database.update(session, link)
    self.database.commit(session)

    # Update any minor links that are attached to this local link
    links = sql.Table('links')

    query = links.update(where = (links.local_link_id == link.id), columns = [links.target_revision], values = [link.revision])

    self.database.execute(session, query)
    self.database.commit(session)

    query = links.select(where = (links.local_link_id == link.id))
    
    self.database.execute(session, query)
    self.database.commit(session)
    
    links = self.database.many(session)

    from occam.links.records.link import LinkRecord
    for link in links:
      # TODO: simplify this as a query instead
      # TODO: or just retrieve the last row when we can
      link = LinkRecord(link)
      self.cache.delete(self.links._cacheKey(link.source_object_id, link.relationship, order = "ascending"))
      self.cache.delete(self.links._cacheKey(link.source_object_id, link.relationship, order = "descending"))

    return link

  def destroy(self, link_db):
    """ Destroys the given link.
    """

    ret = self.datastore.write.deleteLinks(id = link_db.id)

    # Invalidate the cache key
    self.cache.delete(self.links._cacheKey(link_db.source_object_id, link_db.relationship, order = "ascending"))
    self.cache.delete(self.links._cacheKey(link_db.source_object_id, link_db.relationship, order = "descending"))

    return ret

  def destroyLocalLink(self, link, path = None):
    """ Destroys any local link (if it exists) for the given link.
    """

    import sql
    
    session = self.database.session()

    localLinks = sql.Table("local_links")

    # Get the staging path
    path = self.links.basePathFor(link, path)

    # If the path is an internal stage, destroy it too
    if path and os.path.exists(path):
      shutil.rmtree(path)

    # Delete the record
    query = localLinks.delete(where = (localLinks.id == link))

    self.database.execute(session, query)
    self.database.commit(session)

    return True

  def initializeStage(self, object, basePath):
    """ Initializes the given staged object at the given base path.

    Arguments
      object (Object): The staged object to initialize.
      basePath (str): The path in which to initialize the staged object.
    """

    # Create the data path
    dataPath = os.path.join(basePath, "data")

    self.objects.clone(object, dataPath, increment = False)

    # Create a metadata directory
    metadataPath = os.path.join(basePath, "metadata")
    os.makedirs(metadataPath, exist_ok = True)

  def syncUpdate(self, object):
    """ A callback when the staged object metadata changes.

    Arguments:
      object (Object): The staged object whose metadata has been updated.
    """

  def syncDeleteFile(self, object, path):
    """ Called when a file is deleted in the index.

    Arguments:
      object (Object): The staged object within which some file was deleted.
      path (str): The relative path within the object of the deleted file.
    """

  def syncUpdateData(self, object, path):
    """ Called when a file is updated in the index.

    Arguments:
      object (Object): The staged object within which some file was updated.
      path (str): The relative path within the object of the updated file.
    """

  def clearStage(self, object):
    """ Clears the stage path of the staged object at the given path.

    Arguments:
      object (Object): The staged object whose staged data should be cleared.
    """

    # Look for the stage path off of the object path
    basePath = os.path.join(object.path, "..")
    stagePath = os.path.join(basePath, "stage")

    # If it exists, mercilessly destroy it
    if os.path.exists(stagePath):
      shutil.rmtree(stagePath)

  def stage(self, object, force=False):
    """ Synchronizes the files within the staged object at the given path.

    A staged object has two main data paths. The first is the one at `data/`
    which contains the working index. The next is the one at `stage/` which
    contains the current running context for the object. The files are synced
    whenever the object is being run.

    Resources are only listed within the `stage/` path. When the resources are
    updated, this forces a flush when the object is run or built next. In many
    situations, the resources are not moved around again.

    Arguments:
      object (Object): The staged object whose staged data will be synchronized.

    Returns:
      bool: Whether or not the stage was cleared and restaged.
    """

    if not force and not self.links.stageRequiredFor(object):
      return False

    # Gather the relevant paths
    basePath = os.path.join(object.path, "..")
    dataPath = os.path.join(basePath, "data")
    stagePath = self.links.stagePathFor(object)

    # Clear the stage path
    self.clearStage(object)

    # Clone the repository, shallowly
    source = GitRepository(dataPath)
    git = source.clone(stagePath, shared = True)
    git.reset(source.head(), hard = True)

    # Remove the `.git` path
    shutil.rmtree(os.path.join(stagePath, ".git"))

    # Sync the changed files
    self.sync(object)

    # Install the resources
    info = self.objects.infoFor(object)
    resources = self.objects.resourcesFor(object, build = ('build' in info))
    self.resources.installAll(resources, stagePath)

    return True

  def syncRemote(self, object):
    """ Synchronize our local index to match a remote index.
    """

    # TODO: this should reset the local data path and fetch from the remote
    #       data path.

    # Gather the relevant paths
    basePath = os.path.join(object.path, "..")
    dataPath = os.path.join(basePath, "data")

    # Get our own repository instance
    git = GitRepository(object.path)

    # Ask the remote for its revision
    remoteRevision = None # TODO: query for remote revision somehow

    # If the revision is different, we must reset
    if remoteRevision != git.head():
      # Fetch those changes by adding a remote to the remote URL
      from uuid import uuid4
      remote = str(uuid4())
      git.addRemote(remote, url)

      # Then `git fetch` to pull the changes to the repository
      git.fetch(remote)

      # Then remove that random remote name
      git.rmRemote(remote)

      # Then a reset to the requested revision
      git.reset(remoteRevision, hard = True)

    # Synchronize the updated files by asking the remote for what was staged
    staged = [] # TODO: remote query for file list

    # TODO: Actually perform the synchronization over the network somehow

  def sync(self, object):
    """ Synchronize the local staged path with changes found in the local index.
    """

    # Gather the relevant paths
    basePath = os.path.join(object.path, "..")
    dataPath = os.path.join(basePath, "data")
    metadataPath = os.path.join(basePath, "metadata")
    stagePath = self.links.stagePathFor(object)

    # Get a reference to the git repository of the object
    git = GitRepository(object.path)

    # Open the prior staged file listing
    # This list cross-reference a list of files 'staged' last time. Any that are
    # now not in this list are truly changed files. Consider what happens if a
    # file is edited, then it is synchronized, and then that change is reverted.
    # The git index in `data/` would not report that file as staged any longer,
    # and so it would not be synchronized back. Therefore, the prior staged list
    # is merged with the current to form a list of all files that should be
    # checked.
    stagedFile = os.path.join(metadataPath, "staged.txt")
    files = []
    if os.path.exists(stagedFile):
      with open(stagedFile, "rb") as f:
        for path in f:
          path = path.strip()
          if path != b'':
            files.append(path)

    # Get the staged file listing
    staged = git.staged()

    # Retrieve all unique files across both old and new sets
    synclist = list(set(staged) | set(files))

    # Preserve this current file listing
    with open(stagedFile, "wb+") as f:
      for path in staged:
        f.write(path)
        f.write(b'\n')

    # Synchronize the files
    # `--delete-missing-args` requires at least version 3.1.0 (September, 2013)
    #                         it will delete files in the destination path when
    #                         they are missing at the source path
    # `--files-from` is a list of files to process which we give it the list of
    #                staged files at the git repo at dataPath

    # TODO: use the remote rsync implementation so we don't gotta tunnel
    args = ['rsync', '-ah', '--delete-missing-args',
                            '--files-from', '-', dataPath, stagePath]

    # Perform the `rsync` call
    p = subprocess.Popen(args, stdin = subprocess.PIPE,
                               stdout = subprocess.DEVNULL,
                               stderr = subprocess.DEVNULL,
                               cwd = stagePath)

    # Feed rsync the list of changed files
    for path in synclist:
      p.stdin.write(path)
      p.stdin.write(b'\n')

    p.stdin.close()
    p.communicate()

  def clearTask(self, object):
    """ Clears the task path of the staged object at the given path.

    Arguments:
      object (Object): The staged object whose staged data should be cleared.

    Returns:
      bool: Returns True when the path was found and destroyed.
    """

    # Return False if the path does not exist
    taskPath = self.links.runPathFor(object)
    if not os.path.exists(taskPath):
      return False

    # Remove the task directory and return True
    shutil.rmtree(taskPath)
    return True

  def updateTaskFor(self, object, task):
    """ Writes the given run phase task manifest to the object stage.
    """

    taskPath = os.path.join(self.links.taskPathFor(object), "object.json")

    with open(taskPath, "w+") as f:
      json.dump(task, f)

    taskPath = os.path.join(self.links.taskPathFor(object), "update.txt")

    with open(taskPath, "w+") as f:
      f.write("delete")

    return task

  def updateBuildTaskFor(self, object, task):
    """ Writes the given build phase task manifest to the object stage.
    """

    taskPath = os.path.join(self.links.taskPathFor(object, build = True), "object.json")

    with open(taskPath, "w+") as f:
      json.dump(task, f)

    return task
