# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.manager import uses

from occam.links.write_manager import LinkWriteManager
from occam.keys.manager        import KeyManager
from occam.objects.manager     import ObjectManager

from occam.commands.manager import command, option, argument

@command('links', 'delete',
  category      = 'Link Management',
  documentation = "Deletes an existing link between two objects on the local system.")
@argument("source", type="object")
@argument("relationship", type=str)
@argument("target", type="object")
@option("-i", "--identity", action = "store_true",
                            help = "interpret source as an identity URI")
@uses(LinkWriteManager)
@uses(KeyManager)
@uses(ObjectManager)
class DeleteCommand:
  def do(self):
    Log.header("Deleting an existing %s link" % self.options.relationship)

    source_db = None
    name = None

    if self.options.identity:
      source_db = self.keys.retrieve(self.options.source.id)

      if source_db is None:
        raise Exception(f"Cannot find source identity ({self.options.source.id})")
        return -1

      name = source_db.uri
    else:
      source = self.objects.resolve(self.options.source, person = self.person)

      if source is None:
        Log.error("Cannot find source object (%s)" % (self.options.source.id))
        return -1

      rows = self.objects.search(id = source.id)
      source_db = rows[0]

      name = source_db.name

    target = self.objects.resolve(self.options.target, person = self.person)

    if target is None:
      Log.error("Cannot find target object (%s)" % (self.options.target.id))
      return -1

    rows = self.objects.search(id = target.id)
    target_db = rows[0]

    links = self.links.retrieve(source_db, self.options.relationship, target_db, self.options.target.revision)
    if len(links) > 0:
      for link in links:
        Log.write("Removing link id %s" % (link.id))
        self.links.write.destroy(link)

    Log.done("Successfully deleted relationship %s between %s and %s" % (self.options.relationship,
                                                                         name,
                                                                         target_db.name))
    return 0
