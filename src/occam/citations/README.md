# The Citations Component

This component handles the generation of citations for objects. Available
citations may be extended by adding new citations to the plugins directory.
