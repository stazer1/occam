# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sql
import sql.aggregate

from occam.log import loggable
from occam.databases.manager import uses, datastore

@loggable
@datastore("manifests")
class BuildDatabase:
  """ Manages the database interactions for keeping track of generated tasks.
  """

  def queryTask(self, id=None, revision=None, taskId=None, taskRevision=None, section=None, buildId=None, identity=None, secondaryIdentity=None):
    tasks = sql.Table("tasks")

    query = tasks.select()

    query.where = sql.Literal(True)
    if id is not None:
      query.where = (tasks.id == id)

    if taskId is not None:
      query.where = (tasks.task_id == taskId)

    if revision is not None:
      query.where = query.where & (tasks.revision == revision)

    if buildId is not None:
      query.where = query.where & (tasks.build_id == buildId)

    if identity:
      subQuery = (tasks.identity_uri == identity)

      if secondaryIdentity:
        subQuery = subQuery | (tasks.identity_uri == secondaryIdentity)

      query.where = query.where & subQuery

    if section:
      query.where = query.where & (tasks.section == section)

    # Order by newest (signature)
    query.order_by = sql.Desc(tasks.signed)
    return query

  def retrieveTask(self, id, revision, section, buildId, identity, secondaryIdentity):
    """
    """

    from occam.manifests.records.task import TaskRecord

    # Form a query to retrieve the task
    query = self.queryTask(id, revision = revision, section = section,
                               buildId = buildId, identity = identity, secondaryIdentity = secondaryIdentity)

    session = self.database.session()

    # Retrieve the raw data
    self.database.execute(session, query)
    task = self.database.fetch(session)

    # Normalize to a TaskRecord
    if task:
      task = TaskRecord(task)

    # Return (will return None if not found)
    return task

  def createTask(self, id,
                       revision,
                       taskId,
                       taskRevision,
                       section,
                       buildId,
                       identity,
                       verifyKeyId,
                       published,
                       signed,
                       signature,
                       signature_type,
                       signature_digest):
    """ Creates a task record for the provided metadata.
    """

    from occam.manifests.records.task import TaskRecord

    session = self.database.session()

    r = TaskRecord()

    r.id               = id
    r.revision         = revision
    r.task_id          = taskId
    r.task_revision    = taskRevision
    r.build_id         = buildId
    r.section          = section
    r.identity_uri     = identity
    r.verify_key_id    = verifyKeyId
    r.published        = published
    r.signed           = signed
    r.signature        = signature
    r.signature_type   = signature_type
    r.signature_digest = signature_digest

    self.database.update(session, r)
    self.database.commit(session)

    return r
