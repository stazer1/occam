# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log              import Log
from occam.object           import Object
from occam.manager          import uses

from occam.objects.manager   import ObjectManager
from occam.manifests.manager import ManifestManager
from occam.backends.manager  import BackendManager

from occam.commands.manager   import command, option, argument

import base64
import json

@command('manifests', 'list',
  category      = 'Task Generation',
  documentation = "Displays the known task manifests for the given object.")
@argument("object", type = "object")
@option("-p", "--phase",
  action  = "store",
  dest    = "section",
  help    = "which section to generate ('run' or 'build') default: 'run'",
  default = "run")
@uses(ObjectManager)
@uses(ManifestManager)
@uses(BackendManager)
class ManifestsListCommand:
  """ This command displays a list of tasks known for the given object.
  """

  def do(self):
    object = self.objects.resolve(self.options.object, person = self.person)
    if object is None:
      # Cannot find the object
      Log.error("Cannot find the given object.")
      return -1

    # Find task manifests
    tasks = []
    task, buildId, record = self.manifests.retrievePartialTaskFor(object, section = self.options.section,
                                                                          person = self.person)
    if task is not None:
      info = {
        'id': task.id,
        'uid': task.uid,
        'name': task.name,
        'revision': task.revision,
        'identity': task.identity,
        'published': record.published.isoformat() + "Z",
        'signature': {
          'key': record.verify_key_id,
          'signed': record.signed.isoformat() + "Z",
          'data': base64.b64encode(record.signature).decode('utf-8'),
          'encoding': "base64",
          'format': record.signature_type,
          'digest': record.signature_digest,
        }
      }

      if buildId:
        info['build'] = {
          'id': buildId
        }

      tasks.append(info)

    Log.output(json.dumps(tasks), padding="")
    return 0
