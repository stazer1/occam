# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.manager import uses

from occam.accounts.manager    import AccountManager
from occam.permissions.manager import PermissionManager
from occam.objects.manager     import ObjectManager

from occam.commands.manager import command, option, argument

@command('permissions', 'remove',
  category      = 'Access Management',
  documentation = "Removes access to the given object from the given identity")
@argument("object", type="object")
@argument("identity", type=str)
@option("-c", "--children", action  = "store_true",
                            dest    = "children",
                            default = False,
                            help    = "when given, removes access for children of this object.")
@uses(ObjectManager)
@uses(AccountManager)
@uses(PermissionManager)
class PermissionsRemoveCommand:
  def do(self):
    if (self.person is None or not hasattr(self.person, 'id')):
      Log.error("Must be authenticated to update objects")
      return -1

    # Resolve the acting identity
    identity = self.options.identity

    # Resolve the object
    obj = self.objects.resolve(self.options.object, person = self.person)
    if obj is None:
      Log.error("The object could not be found.")
      return -1

    # Remove the permission record for the given identity
    if self.person and ('administrator' in self.person.roles or self.permissions.can('write', obj.id, person = self.person)):
       self.permissions.destroyAccessControl(id       = obj.id,
                                             identity = identity,
                                             children = self.options.children)
    else:
      Log.error("Access denied.")
      return -1

    return 0
