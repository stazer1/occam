# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.objects.manager    import ObjectManager
from occam.resources.manager  import ResourceManager
from occam.ingest.manager     import IngestManager

from occam.object import Object
from occam.log import Log

import os

@command('ingest', 'maven-package',
  category      = 'Importing Other Packages',
  documentation = "Imports a given Maven package as an Occam object.")
@argument("name", type = str)
@argument("version", nargs="?", default = None)
@option("-f", "--force", action = "store_true",
                         dest   = "force",
                         help   = "when specified, it will ingest this object even if it already exists.")
@option("-p", "--pomfile", action = "store",
                           dest   = "pomfile",
                           help   = "specifies a pom.xml file to use to satisfy its dependencies.")
@uses(ObjectManager)
@uses(ResourceManager)
@uses(IngestManager)
class MavenPackageCommand:
  """ The maven-package command will import a Maven package.
  """

  def do(self):
    if self.person is None:
      Log.error("Must be authenticated to store objects")
      return -1

    packageName = self.options.name

    if packageName == "-":
      obj = self.ingest.pullAll("maven-package", person = self.person,
                                                 force = self.options.force,
                                                 pomfile = self.options.pomfile)
    else:
      obj = self.ingest.pull("maven-package", packageName, person = self.person,
                                                           force = self.options.force,
                                                           version = self.options.version)

      if obj is None:
        Log.error("Failed to import.")
        return -1

      ret = {}
      ret["updated"] = []

      # Report the object status
      ret["updated"].append({
        "id": obj.id,
        "uid": obj.uid,
        "revision": obj.revision,
        "position": obj.position,
      })

      import json
      Log.output(json.dumps(ret))

      Log.done("imported %s" % (packageName))
    return 0
