# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.objects.manager    import ObjectManager
from occam.resources.manager  import ResourceManager
from occam.ingest.manager     import IngestManager

from occam.object import Object
from occam.log import Log

import os

@command('ingest', 'js-package',
  category      = 'Importing Other Packages',
  documentation = "Imports a given JavaScript package as an Occam object.")
@argument("packageName", type = str)
@argument("version", nargs="?", default = None)
@option("-f", "--force", action = "store_true",
                         dest   = "force",
                         help   = "When specified, it will ingest this object even if it already exists.")
@option("-l", "--lockfile", action = "store",
                            dest   = "lockfile",
                            help   = "A package-lock.json file to parse.")
@option("-p", "--packagefile", action = "store",
                               dest   = "packagefile",
                               help   = "A package.json file to parse.")
@uses(ObjectManager)
@uses(ResourceManager)
@uses(IngestManager)
class JSPackageCommand:
  """ The js-package command will import one or more JavaScript packages.
  """

  def do(self):
    if self.person is None:
      Log.error("Must be authenticated to store objects")
      return -1

    libraryName = self.options.packageName

    if libraryName == "-":
      objs = self.ingest.pullAll("js-package", person = self.person,
                                               force = self.options.force,
                                               lockfile = self.options.lockfile,
                                               packagefile = self.options.packagefile)
      Log.done("imported all")
    else:
      obj = self.ingest.pull("js-package", libraryName, person = self.person,
                                                        version = self.options.version,
                                                        force = self.options.force)
      if obj is None:
        Log.error("Failed to import.")
        return -1

      Log.done("imported %s" % (libraryName))

    return 0
