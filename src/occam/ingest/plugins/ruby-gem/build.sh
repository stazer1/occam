pkgname=$1
_gemname=$1
pkgver=$2
pkgdir=$PWD/../build
srcdir=$PWD

# Determine the gem directory
_gemdir="$(ruby -e'puts Gem.default_dir')"

# Install using RubyGems
HOME="/tmp" GEM_HOME="$_gemdir" GEM_PATH="$_gemdir" gem install -N --no-user-install --ignore-dependencies \
  -i "$pkgdir/$_gemdir" -n "$pkgdir/usr/bin" "$_gemname-$pkgver.gem" || exit 1

# Install a license file, if it exists
install -D -m644 "$pkgdir/$_gemdir/gems/$_gemname-$pkgver/COPYING" "$pkgdir/usr/share/licenses/$pkgname/COPYING"
install -D -m644 "$pkgdir/$_gemdir/gems/$_gemname-$pkgver/LICENSE.md" "$pkgdir/usr/share/licenses/$pkgname/LICENSE.md"
install -D -m644 "$pkgdir/$_gemdir/gems/$_gemname-$pkgver/LICENSE.txt" "$pkgdir/usr/share/licenses/$pkgname/LICENSE.txt"
install -D -m644 "$pkgdir/$_gemdir/gems/$_gemname-$pkgver/LICENSE" "$pkgdir/usr/share/licenses/$pkgname/LICENSE"

# Install any man files
if [ -d "$pkgdir/$_gemdir/gems/$_gemname-$pkgver/man" ]; then
  mkdir -p $pkgdir/usr/share/man/man1
  install -D -m644 "$pkgdir/$_gemdir/gems/$_gemname-$pkgver/man/*.1" "$pkgdir/usr/share/man/man1"
  mkdir -p $pkgdir/usr/share/man/man3
  install -D -m644 "$pkgdir/$_gemdir/gems/$_gemname-$pkgver/man/*.3" "$pkgdir/usr/share/man/man3"
fi

# Remove the cached copy of the gem
rm -rf "$pkgdir/$_gemdir/cache/$_gemname-$pkgver.gem"
