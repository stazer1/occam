# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.ingest.manager import IngestManager, package

from occam.network.manager import NetworkManager
from occam.objects.manager import ObjectManager
from occam.resources.write_manager import ResourceWriteManager

from occam.manager import uses

from occam.git_repository import GitRepository
from occam.semver import Semver

import uuid as UUID
import os, re, json, datetime

@package("r-package")
@uses(ObjectManager)
@uses(NetworkManager)
@uses(ResourceWriteManager)
class RPackage:
  """ This aids in importing R packages into Occam for preservation.
  """

  # The public identifier for the account managing the CRAN mirror.
  CRAN_IDENTITY = "6MuxYVLPHen4sanXdvzAm95utbTPxJhD5xYzkfV3i61HkyA"

  # These pages list the packages. Another route is to pull the package names
  # from the ARCHIVE_SOURCE URL.
  INDEX_SOURCE = "https://cran.r-project.org/web/packages/available_packages_by_date.html"
  INDEX_URL = "https://cran.r-project.org/web/packages/available_packages_by_date.html"

  # The archive contains just tarballs, so you'll have to open them and read
  # DESCRIPTION for metadata.
  ARCHIVE_SOURCE = "https://cran.r-project.org/src/contrib/Archive/%s"

  # Otherwise, we just assume the package info page is the URL for any package.
  # This will be used to generate the object identifier.
  CANONICAL_SOURCE = "https://cran.r-project.org/package=%s"

  def __reformVersion(self, version):
    """ Converts a CRAN version to an Occam version.

    Arguments:
      version (str): The CRAN version.

    Returns:
      str: The corresponding Occam version.
    """

    if version is None:
      return None

    # Do nothing, for now
    return version

  def __queryAll(self):
    """ Retrieves a listing of all packages.
    """

    RPackage.Log.write(f"Parsing package listing at {RPackage.INDEX_SOURCE}")

    ret = []

    # The package index is a git repository we need to clone
    #data, content_type, size = self.network.get(RPackage.INDEX_SOURCE)

    #RPackage.Log.writePercentage("Parsing...")

    #position = 0
    #for line in data.readlines():
    #  position += len(line)
    #  RPackage.Log.updatePercentage(position / size * 100)

    #  line = line.strip()
    #  if line.startswith(b'<a'):
    #    name = line.split(b'>', 1)[1].split(b'<', 1)[0].decode('utf-8')
    #    ret.append(name)

    #RPackage.Log.updatePercentage(100)

    return ret

  def __queryMetadata(self, packageName, version = None):
    """ Retrieves the package metadata for the given R package that matches the given version.

    Returns:
      None when the data cannot be found.
    """

    version = self.__reformVersion(version)

    # Get the metadata listing
    metadata = self.__retrieveMetadata(packageName, version = version)

    # Bail if the package cannot be found
    if metadata is None:
      return None

    # Reform metadata (date)
    if "published" in metadata:
      # Get time/date into iso8601 form (we assume all dates are UTC)
      parsed = False
      try:
        metadata["published"] = datetime.datetime.strptime(metadata["published"], "%Y-%m-%d").isoformat() + "Z"
        parsed = True
      except ValueError:
        pass

      if not parsed:
        try:
          metadata["published"] = datetime.datetime.strptime(metadata["published"], "%Y-%m-%d %H:%M:%S").isoformat() + "Z"
          parsed = True
        except ValueError:
          pass

      if not parsed:
        try:
          metadata["published"] = datetime.datetime.strptime(metadata["published"], "%Y-%m-%d %H:%M:%S UTC").isoformat() + "Z"
          parsed = True
        except ValueError:
          pass

    return metadata

  def __gatherArchivedMetadata(self, packageName, version = None):
    """ Retrieves a package metadata from the CRAN archive.

    This is useful when the normal CRAN repository does not have the history
    version requested. However, the archives are just a file repository of the
    source tarballs for the packages listed by version.

    The metadata is in the DESCRIPTION file within the tarball. This requires
    downloading at least enough of the tarball (or just storing it
    opportunistically as a resource) to get at the DESCRIPTION file and yield
    that here.
    """

    # Perform a lookup of all historic versions
    url = RPackage.ARCHIVE_SOURCE % (packageName)
    try:
      data, _, size = self.network.get(url)
      data = data.read()
    except Exception as e:
      ret = None

    # Parse HTML for metadata
    import html.parser

    class Parser(html.parser.HTMLParser):
      def handle_starttag(self, tag, attrs):
        if not hasattr(self, 'versions'):
          self.versions = []
          self.urls = {}

        # Remember data within <a>
        if tag == "a":
          for k,v in attrs:
            if k == 'href' and v.startswith(packageName):
              extension = ".tar.gz"
              version = v[len(packageName) + 1:-len(extension)]
              self.versions.append(version)
              self.urls[version] = url + "/" + v

    parser = Parser()
    parser.feed(data.decode('utf-8'))

    # Sort versions
    releases = Semver.sortVersionList(parser.versions)

    # Match one of these (the latest that matches) against the given query.
    getting = None
    resolved = None
    for release in reversed(releases):
      if not version or Semver.resolveVersion(version, [release]) or Semver.resolveVersion(version + ".x", [release]):
        getting = parser.urls[release]
        resolved = release

    # Download the tarball as a resource and get the id of the resource back.
    # (This might return immediately if the resource data is already known)
    resource = None
    if getting:
      # Download the resource and store it in the resource store already
      resource = self.resources.write.pull(resourceInfo = {
                                             "source": getting,
                                             "type": "resource",
                                             "subtype": "application/gzip",
                                             "name": f"{packageName} R Package {resolved} Source Distribution",
                                             "to": f"{packageName}_{resolved}.tar.gz",
                                             "actions": {
                                               "unpack": "."
                                             }
                                           },
                                           identity = RPackage.CRAN_IDENTITY)

    # Get the DESCRIPTION file within the tarball via the resource.
    data = None
    if resource:
      id, uid, revision, subDependencies, data_path = resource
      file = self.resources.retrieveFileFrom(id, uid, revision, "application/gzip", f"{packageName}/DESCRIPTION")
      data = file.readlines()

    # Parse the DESCRIPTION file and return the metadata as a dict.
    metadata = None
    if data:
      metadata = {}

      tag = None
      value = ""
      for line in data:
        line = line.decode('utf-8')
        try:
          index = line.index(":")
        except:
          index = -1

        if index > 0:
          if tag is not None:
            # Store last tag
            metadata[tag] = value.strip()

          value = line.split(":", 1)[1].strip()
          tag = line.split(":", 1)[0].strip()
        elif line.startswith(" "):
          # Continuing
          value = value + " " + line.strip()

      if tag is not None:
        # Store last tag
        metadata[tag] = value
        value = ""

      print(metadata)

    if metadata:
      ret = {}
      ret["version"] = resolved
      ret["downloads"] = []
      ret["downloads"].append({"type": "package", "url": getting})
      if "License" in metadata:
        ret["license"] = metadata["License"]
      if "Author" in metadata:
        # Remove things between brackets '[' and ']'
        ret["authors"] = re.sub(r"\s*[\[].+?[\]]\s*", "", metadata["Author"]).split(',')
        ret["authors"] = list(map(lambda x: x.strip(),
                                  re.sub(r"\s*[\[].+?[\]]\s*", "", metadata["Author"]).split(',')))
      if "Date/Publication" in metadata:
        ret["published"] = metadata["Date/Publication"].split(";", 1)[0]
      if "Title" in metadata:
        ret["summary"] = metadata["Title"]
      if "Description" in metadata:
        ret["description"] = metadata["Description"]
      if "NeedsCompilation" in metadata:
        ret["compiled"] = metadata["NeedsCompilation"] == "yes"
      if "Package" in metadata:
        ret["name"] = metadata["Package"]

    return ret

  def __retrieveMetadata(self, packageName, version = None):
    """ Retrieves the package metadata for the given R package at the given package version.

    Returns:
      None when the data cannot be found.
    """

    # Look up the metadata from the R package index
    url = RPackage.CANONICAL_SOURCE % packageName

    RPackage.Log.noisy(f"Pulling metadata at {url}")

    ret = []

    try:
      data, _, size = self.network.get(url)
      data = data.read()
    except Exception as e:
      ret = None

    if ret is None:
      return None

    # Parse HTML for metadata
    import html.parser

    class Parser(html.parser.HTMLParser):
      def handle_starttag(self, tag, attrs):
        self.tag = tag
        self.attrs = attrs

        if tag == "tr":
          self.sawtr = True
          self.lasttd = ""

        if tag == "a" and self.lasth4 == "Downloads:":
          # Download link
          for k,v in attrs:
            if k == "href":
              self.lasthref = self.network.joinURL(url, v)

      def handle_endtag(self, tag):
        if self.tag == tag:
          self.tag = None

      def handle_data(self, data):
        data = data.strip()
        if data == "":
          return

        if not hasattr(self, 'metadata'):
          self.metadata = {}
          self.lasttd = ""
          self.lasth4 = ""
          self.tag = ""
          self.lasthref = None
          self.lasth2 = False

        if self.tag == "h2":
          # Get summary and name
          self.metadata['name'], self.metadata['summary'] = list(map(lambda x: x.strip(),
                                                           data.split(':', 1)))
          self.lasth2 = True
          return

        if self.tag == "p" and self.lasth2:
          # Get description
          self.metadata['description'] = " ".join(list(map(lambda x: x.strip(),
                                                           data.split('\n'))))
          self.lasth2 = False

        # Handle table contents
        if self.lasttd == "Version:":
          self.metadata["version"] = data
        elif self.lasttd.startswith("Author"):
          self.metadata["authors"] = list(map(lambda x: x.strip(),
                                              re.sub(r"\s*[\[].+?[\]]\s*", "", data).split(',')))
        elif self.lasttd == "Date/Publication:":
          self.metadata["published"] = data.split(';', 1)[0]
        elif self.lasttd == "Published:":
          self.metadata["published"] = data.split(';', 1)[0]
        elif self.lasttd == "License:":
          self.metadata["license"] = data
        elif self.lasttd == "Title:":
          self.metadata["summary"] = data
        elif self.lasttd == "Description:":
          self.metadata["description"] = data
        elif self.lasttd == "NeedsCompilation:":
          self.metadata["compiled"] = data == "yes"
        elif self.lasttd == "Package:":
          self.metadata["name"] = data
        elif self.lasttd == "Depends:" or self.lasttd == "Imports:":
          dependencyInfo = {}
          item = None
          if self.tag == "a":
            # Link to a package
            item = data;
          else:
            # Text (comma or an R dependency)
            if data.strip() == ",":
              return

            if data.startswith("("):
              # Version info from the last item
              pass
            elif not data.startswith("R "):
              return
            item = data

          if item:
            dependencyInfo['name'] = item.split('(', 1)[0].strip()
            try:
              dependencyInfo['version'] = item.split('(', 1)[1].split(')', 1)[0].strip().replace("≥", ">=").replace("≤", "<=").replace("==", "=")
            except:
              pass

            if dependencyInfo['name'] != "":
              self.metadata["dependencies"] = self.metadata.get("dependencies", [])
              self.metadata["dependencies"].append(dependencyInfo)
            elif "version" in dependencyInfo:
              self.metadata["dependencies"][-1]["version"] = dependencyInfo["version"]
        elif self.lasth4 == "Downloads:":
          self.metadata["downloads"] = self.metadata.get("downloads", [])
          if self.lasttd.startswith("Package") and self.lasthref is not None:
            self.metadata["downloads"].append({"type": "package", "url": self.lasthref})
            self.lasthref = None

        # Remember data within tags
        if self.tag == "td" and self.sawtr:
          self.lasttd = data
          self.sawtr = False
        elif self.tag == "h4":
          self.lasth4 = data

    parser = Parser()
    parser.network = self.network
    parser.feed(data.decode('utf-8'))

    release = parser.metadata.get('version')
    if version and not Semver.resolveVersion(version, [release]):
      ret = self.__gatherArchivedMetadata(packageName, version)
    else:
      ret = parser.metadata

    if ret and ret.get('version'):
      RPackage.Log.noisy(f"Found package at version {ret.get('version')}")

    return ret

  def sourceURLFor(self, packageName):
    """ Returns the canonical source url.
    """

    sourceURL = RPackage.CANONICAL_SOURCE % (packageName)

    return sourceURL

  def uidFor(self, occamType, occamName, packageName, sourceURL):
    """ Returns the uid for a particular source package name.
    """

    return self.objects.uidFor(None, objectInfo = {
      "name": occamName,
      "type": occamType,
      "source": sourceURL
    })

  def idFor(self, identity, occamType, occamName, packageName, sourceURL):
    """ Returns the id for a particular source package name.
    """

    return self.objects.idFor(None, identity, objectInfo = {
      "name": occamName,
      "type": occamType,
      "source": sourceURL
    })

  def queryAll(self, identity, lockfile=None, tomlfile=None):
    """ Returns a list of all packages.
    """

    if lockfile:
      return self.__queryLockfile(identity, lockfile, tomlfile)

    return self.__queryAll()

  def query(self, identity, packageName, pending = None, version = None):
    """ Returns the id and initial metadata for the package.
    """

    # Retrieve metadata
    metadata = self.__queryMetadata(packageName, version = version)

    print("REFORMED", metadata)

    # Get the canonical name
    name = metadata['name']
    version = self.__reformVersion(metadata['version'])

    # Ensure 'R' is a dependency.
    hasR = False
    for dependency in metadata.get('dependencies', []):
      if dependency.get('name') == "R":
        hasR = True

    if not hasR:
      metadata['dependencies'] = metadata.get('dependencies', [])
      metadata['dependencies'].append({
        'name': 'R',
        'version': 'x'
      })

    # Get the canonical URL
    sourceURL = self.sourceURLFor(name)

    download = metadata['downloads'][0]
    tarSource = download['url']
    tarFilename = os.path.basename(download['url'])

    # Now we generate the object metadata
    objectInfo = {
      "name": name,
      "type": "r-package",
      "environment": "linux",
      "architecture": "x86-64",
      "source": sourceURL,
      "dependencies": [
      ],
      "build": {
        "install": [
          {
            "type": "resource",
            "subtype": "application/gzip",
            "source": tarSource,
            "to": tarFilename,
            "name": f"{name} {version} Distribution"
          }
        ],
        "command": ["/usr/bin/R", "CMD", "INSTALL", tarFilename, "-l", "../build"]
      },
      "init": {
        "link": [
          {
            "source": name,
            "to": f"/usr/lib/R/library/{name}"
          }
        ]
      }
    }

    if metadata.get('compiled'):
      # Add build dependencies here
      objectInfo["build"]["dependencies"] = [{
        "id": "QmPxP6yHYWbSA3z3sFVQVp67xUmAmKAD7kLUCc7j3JJjvi",
        "type": "library",
        "name": "png",
        "version": "1.x"
      }, {
        "type": "collection",
        "name": "build",
        "id": "QmQ1i5VjdxdU7dWCkhpM7ccVDCBNPX2swTLQSHRiCW8sK1",
        "version": "1.0"
      }, {
        "type": "compiler",
        "name": "gfortran",
        "id": "QmPZqTJFGdfgeA5CoVcYeQCEyxtjgWFGJTDYQicerHJtHf",
        "version": ">5"
      }, {
        "type": "library",
        "name": "tk",
        "id": "QmXvu76BoVptAisCWL1WHybV9oS2CEZDRRnk5PwmsxctPJ",
        "version": "8.x"
      }, {
        "type": "application",
        "name": "unzip",
        "id": "QmPvYt44E95j78i7MSi3rJrMZ6K3Jyj677Yg3Q8wruEcKE",
        "version": "6.x"
      }, {
        "id": "QmSPK9zLkxwtcj2QsrZNVz3sXH5zwR2nAaBiWVSUcXg64S",
        "version": "x",
        "name": "tzdata",
        "type": "data"
      }, {
        "type": "compiler",
        "name": "g++",
        "id": "QmVseooVZ4dn2Vo4ikwKXzmBcqDH9avt4YWYtKmujP4KC8",
        "version": ">5",
        "inject": "ignore"
      }]

    id = self.idFor(identity, occamType = "r-package",
                              occamName = name,
                              packageName = name,
                              sourceURL = sourceURL)

    uid = self.uidFor(occamType = "r-package",
                      occamName = name,
                      packageName = name,
                      sourceURL = sourceURL)

    RPackage.Log.write("uid: {}".format(uid))
    RPackage.Log.write(" id: {}".format(id))

    ingestRequests = []

    # Gather dependencies
    for dependency in metadata.get('dependencies', []):
      info = {}
      info['type'] = "r-package"
      info['name'] = dependency['name']

      info['id'] = self.idFor(identity, occamType = "r-package",
                                        occamName = info['name'],
                                        packageName = dependency['name'],
                                        sourceURL = self.sourceURLFor(dependency['name']))
      if dependency['name'] == "R":
        info['type'] = "language"
        info['id'] = "QmTb7tZsxwKHtYvXp7bJmZ1eUNiCW175JWgMNwVZ8ymzHh"

      info['version'] = self.__reformVersion(dependency.get('version', 'x'))
      info['replace'] = 'never'

      objectInfo["dependencies"].append(info)

      if not info['id'] in (pending or {}):
        if info['type'] == "r-package":
          ingestRequests.append({
            "packageType": info['type'],
            "packageName": info['name']
          })

          if 'version' in info and info['version'] != 'x':
            ingestRequests[-1]['version'] = info['version']

    print(objectInfo)

    return objectInfo, {
      "ingest": ingestRequests,
      "build": True,
      "tag": version
    }

  def report(self, identity, runType, options, objectInfo, packageName, data, runReport, pending = None, version = None):
    """ Finishes the process.
    """

    return None

  def craft(self):
    """ Creates an Occam object from the given package resource.
    """
