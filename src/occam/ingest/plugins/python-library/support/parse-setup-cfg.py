import sys, os
from configparser import ConfigParser
import json

if len(sys.argv) > 2 and "--ignore-setup-dependencies" in sys.argv:
  exit(0)

path = sys.argv[1]

data = ConfigParser()
data.read(path)

output = {}
if 'options' in data:
  if 'setup_requires' in data['options']:
    if os.path.exists("_output"):
      with open("_output", 'r') as f:
        output = json.load(f)

    setup_requires = data['options']['setup_requires']
    if not isinstance(setup_requires, list):
      setup_requires = [setup_requires]

    output['setup_requires'] = output.get('setup_requires', [])
    output['setup_requires'].extend(setup_requires)

    with open("_output", 'w+') as f:
      f.write(json.dumps(output))
