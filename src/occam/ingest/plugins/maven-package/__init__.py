# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.ingest.manager import IngestManager, package

from occam.network.manager import NetworkManager
from occam.objects.manager import ObjectManager

from occam.manager import uses

from occam.semver import Semver

import xml.etree.ElementTree as XML

import uuid as UUID
import os

@package("maven-package")
@uses(ObjectManager)
@uses(NetworkManager)
class MavenPackage:
  """ This aids in importing maven packages into Occam for preservation.
  """

  # The URL that will be used to create the unique identifiers
  CANONICAL_SOURCE = "https://repo1.maven.org/maven2/%s"

  # Some known mirrors
  MIRRORS=[
    "https://repo1.maven.org/maven2/%s",
    "http://builds.archive.org/maven2/%s",
    "https://repo.maven.apache.org/maven2/%s",
    "http://uk.maven.org/maven2/%s",
    "https://repo.scala-sbt.org/scalasbt/maven-releases/%s",
    "https://clojars.org/repo/%s",
    #"https://repo.spring.io/plugins-release/%s"
  ]

  def __convertVersion(self, version):
    """ Converts the Maven version tag to an Occam semver tag.

    Maven versions have the following syntax:

    "1.0" - Soft requirement. Use this version if no stronger form is specified.

    This one is a bad one. What does that mean?? Yuck!

    Well, we have "soft" versions, too, and they are bad. But, here we are.

    Reforms to "*1.0".

    "[1.0]" - Hard requirement. Use exactly this version.

    This reforms to "=1.0".

    "(,1.0]" - Hard requirement for any version <= 1.0.

    This reforms to "<=1.0".

    "[1.2,1.3]" - Hard requirement for any version between 1.2 and 1.3 inclusive.

    Reforms to "1.2...1.3"

    "[1.0,2.3)" - Hard requirement between 1.0 and, exclusively, 2.3.

    Reforms to ">=1.0, <2.3"

    "[1.5,)" - Hard requirement, no upper bound.

    Reforms to ">=1.5"

    Commas are used as "OR" operators, whereas in Occam they are "AND".

    "(,1.0],[1.2,)" - Anything <= 1.0 or >= 1.2.

    Reforms to "<=1.0 || >=1.2"

    "(,1.1),(1.1,)" - Omits the version 1.1

    Reforms to "<1.1 || >1.1" when applying convenience, yet also "!=1.1".

    Maven requires a reformation in tags to conform to semantic versioning,
    which is described in their own specification at
    https://maven.apache.org/pom.html#version-order-specification as follows:

    When version strings do not follow semantic versioning, a more complex set
    of rules is required. The Maven coordinate is split in tokens between dots
    ('.'), hyphens ('-') and transitions between digits and characters. The
    separator is recorded and will have effect on the order. A transition
    between digits and characters is equivalent to a hyphen. Empty tokens are
    replaced with "0". This gives a sequence of version numbers (numeric tokens)
    and version qualifiers (non-numeric tokens) with "." or "-" prefixes.

    So, we must inject hyphens to make things as expected.

    It also trims versions down when "1." becomes "1", etc.

    Arguments:
      version (str): The version specified by Maven.

    Returns:
      str: The reformed Occam semver tag.
    """

    if version is None:
      return 'x'

    ranges = []

    # Parse the version string into its OR sections
    parts = map(lambda x: x.strip(), version.split(","))

    # For example: (1.1,1.2],(1.3,1.4]
    #   has parts: '(1.1' '1.2]' '(1.3' '1.4]'
    withinRange = False
    range = ""
    for part in parts:
      if part.startswith("("):
        withinRange = True
        range = f">{part[1:]}"
      elif part.startswith("["):
        withinRange = True
        if part.endswith("]"):
          range = f"={part[1:-1]}"
          ranges.append(range)
          range = ""
        else:
          range = f">={part[1:]}"
      elif part == ")":
        # This is something like [1.1,) which just implies no upper bound
        ranges.append(range)
        range = ""
        withinRange = False
      elif part.endswith(")"):
        range = range + f", <{part[:-1]}"
        ranges.append(range)
        range = ""
        withinRange = False
      elif part == "]":
        # This is something like [1.1,] which I'm not sure is valid, but let's
        # just assume it is the same as [1.1,)
        ranges.append(range)
        range = ""
        withinRange = False
      elif part.endswith("]"):
        range = range + f", <={part[:-1]}"
        ranges.append(range)
        range = ""
        withinRange = False
      else:
        # A "soft" version
        if part == "LATEST":
          # Or maybe a 'latest' tag
          ranges.append('x')
        elif part.startswith('*'):
          ranges.append(part)
        else:
          ranges.append(f"*{part}")

    return " || ".join(ranges)

  def __parseDependency(self, tag, properties, management, latest=False):
    """ Parses the dependency rooted at the given tag.

    Arguments:
      tag (xml.etree.ElementTree): The document element for the dependency.
      properties (dict): The current set of properties we can parse.
      management (dict): The dependencyManagement structure.
      latest (Boolean): When True, this will resolve empty versions with latest.

    Returns:
      dict: Dependency information.
    """

    dependency = {}

    artifactId = tag.findall('./{http://maven.apache.org/POM/4.0.0}artifactId')[0].text.strip()
    artifactId = self.__parseTags(artifactId, properties)
    dependency['artifact'] = artifactId

    # Combine group and artifact into a name we can use
    try:
      groupId = tag.findall('./{http://maven.apache.org/POM/4.0.0}groupId')[0].text.strip()
    except:
      # Attempt to determine the group from the name, if a special case
      # (Needed for maven-plugin-tools-3.6.0 for maven-enforcer-plugin)
      import re

      if re.match(r"^maven-.*-plugin$", artifactId):
        groupId = "org.apache.maven.plugins"

    groupId = self.__parseTags(groupId, properties)
    dependency['group'] = groupId

    # The != here prevents the exceptional "commons-io.commons-io" from causing problems
    #if artifactId.startswith(groupId) and artifactId != groupId:
    #  dependency['name'] = artifactId
    #else:
    dependency['name'] = groupId + "." + artifactId

    # Get the scope, maybe
    scope = None
    for scopeTag in tag.findall('./{http://maven.apache.org/POM/4.0.0}scope'):
      scope = scopeTag.text.strip()

    if scope:
      dependency['scope'] = scope

    subVersion = None
    for versionTag in tag.findall('./{http://maven.apache.org/POM/4.0.0}version'):
      subVersion = versionTag.text.strip()
      subVersion = self.__parseTags(subVersion, properties)

    # TODO: reform version to our semver standard
    if subVersion:
      dependency['version'] = subVersion

    # If it does not have a version, we get it from the dependency
    # management section of this or the parent.
    if 'version' not in dependency:
      if dependency['name'] in management and 'version' in management[dependency['name']]:
        dependency['version'] = management[dependency['name']]['version']
      elif latest:
        # We need to resolve the latest version
        dependencyMetadata = self.__retrieveMetadata(dependency['name'])
        dependency['version'] = dependencyMetadata.get('latest')

    # We reform the dependency
    if 'version' in dependency:
      dependency['version'] = self.__convertVersion(dependency['version'])

    return dependency

  def __parseTags(self, data, properties):
    """ Parses the ${} tags that are within maven POM xml files.

    These keys, such as ${maven.version}, etc, have values that are listed once
    in <properties> sections within the POM xml file. In this case, we would
    see:

      <properties>
        <maven.version>3.0</maven.version>
      </properties>

    Arguments:
      data (str): The data to parse.
      properties (dict): A dictionary of property keys with their values.

    Returns:
      str: The parsed data.
    """

    # Find ${
    try:
      index = 0
      start = data.index("${", index)

      while start >= 0:
        end = data.index("}", start)
        key = data[start+2:end].strip()

        replacement = ""
        if key in properties:
          replacement = properties[key]

        try:
          data = data[0:start] + replacement + data[end+1:]
        except:
          pass

        # Adjust where we look next
        index = start + len(replacement)

        # Pull the index of the next ${ section
        # If there isn't one, it will fire an exception
        start = data.index("${", index)

    except ValueError as e:
      pass

    return data

  def __queryMetadata(self, name, version = None):
    """ Retrieves the XML POM metadata for the given package that matches the given version.

    Returns:
      None when the data cannot be found.
    """

    # Get the baseline
    metadata = self.__retrieveMetadata(name)

    # Gather the versions
    versions = metadata.get('versions', [])

    if version is None:
      version = metadata.get('release', metadata.get('latest'))
    elif version.startswith("="):
      version = version[1:]

    # Go through the releases until we find something we can use
    found = False
    for release in reversed(versions):
      # If this version is acceptable...
      if not version or Semver.resolveVersion(version, [release]):
        # We will get the individual metadata
        metadata.update(self.__retrieveMetadata(name, version = release))
        found = True
        version = release
        break

    if not found:
      # Try to get the metadata *anyway* if we have a version we know we want
      if version:
        attempt = self.__retrieveMetadata(name, version = version)
        if attempt:
          metadata.update(attempt)
        else:
          metadata = None
      else:
        metadata = None

    # TODO: reform version to our semver standard
    if metadata and not 'version' in metadata:
      metadata['version'] = version

    return metadata

  def __parsePOM(self, xmldata, version = None, group = None, artifact = None, baseURL = None):
    ret = {}

    parser = XML.XMLParser()
    parser.entity['oslash'] = '\u00D8'
    preamble = """<?xml version="1.0" encoding="UTF-8"?>"""
    # Ignore UTF BOM as well
    if xmldata[0:2] == b"<?" or xmldata[3:5] == b"<?":
      _, xmldata = xmldata.split(b'>', 1)
    doctype = """<!DOCTYPE data PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" [
    <!ENTITY nbsp ' '>
    ]>"""
    try:
      root = XML.fromstringlist([preamble, doctype, xmldata], parser = parser)
    except Exception as e:
      MavenPackage.Log.error(f"XML data: {xmldata[0:100]}")
      raise e

    # TODO: Get license information, organization, year, potentially the scm, and authors/developers

    if version is None:
      version = root.findall('./{http://maven.apache.org/POM/4.0.0}version')[0].text.strip()

    if artifact is None:
      artifact = root.findall('./{http://maven.apache.org/POM/4.0.0}artifactId')[0].text.strip()
      group = root.findall('./{http://maven.apache.org/POM/4.0.0}groupId')[0].text.strip()

    if group is None:
      name = artifact
      artifact = name.split('.')[-1]
      group = name[0:-len(artifact) - 1]

    name = group + "." + artifact

    # Properties are defined in <properties> and define the text search and
    # replace used in the POM within ${} braces.
    properties = {}
    # There is one that we already know (I'm not sure if it is used to
    # retrieve the parent or not.)
    properties['project.version'] = version
    properties['pom.version'] = version

    # Get the parent, if any
    try:
      parentGroup = root.findall('./{http://maven.apache.org/POM/4.0.0}parent/{http://maven.apache.org/POM/4.0.0}groupId')[0].text.strip()
      parentId = root.findall('./{http://maven.apache.org/POM/4.0.0}parent/{http://maven.apache.org/POM/4.0.0}artifactId')[0].text.strip()
    except:
      parentGroup = None
      parentId = None

    parentMetadata = {}
    if parentId and parentGroup:
      # Add parent POM as a resource we require (we need to also put it in
      # the correct place in the path)

      # If the <relativePath> is empty, it expects that the POM is installed
      # in the local maven repository. So, it would have to, then, be a
      # dependency instead.
      tag = root.findall('./{http://maven.apache.org/POM/4.0.0}parent')[0]
      dependency = self.__parseDependency(tag, properties, {})

      relativePath = root.findall('./{http://maven.apache.org/POM/4.0.0}parent/{http://maven.apache.org/POM/4.0.0}relativePath')
      if relativePath:
        relativePath = (relativePath[0].text or "").strip()

      # Combine group and artifact into a name we can use
      dependency['group'] = parentGroup
      dependency['artifact'] = parentId

      # The != here prevents the exceptional "commons-io.commons-io" from causing problems
      if parentId.startswith(parentGroup + ".") and parentId != parentGroup:
        dependency['name'] = parentId
      else:
        dependency['name'] = parentGroup + "." + parentId

      # It is only a dependency if the relativePath is empty.
      # Another WACKY Maven thing...
      # (Not sure if it is strictly required to pull down)
      if relativePath == "" or relativePath == []:
        ret['dependencies'] = ret.get('dependencies', [])
        ret['dependencies'].append(dependency)

      # We want the 'dependencyManagement' group for dependency versions.
      # The parent can define the versions of some dependencies here.

      # This means that we need to download the POM
      parentVersion = dependency.get('version')
      if parentVersion[0] == "*":
        parentVersion = parentVersion[1:]
      parentMetadata = self.__retrieveMetadata(dependency['name'], version = parentVersion)

      # We can pull out properties now.
      properties.update(parentMetadata.get('properties', {}))

      # Fix the well-known project.version (in case the parent overwrote it)
      properties['project.version'] = version
      properties['pom.version'] = version

      # We will pull out the dependency management later on.
      # For now, we are done with the parent.

    try:
      groupId = root.findall('./{http://maven.apache.org/POM/4.0.0}groupId')[0].text.strip()
    except:
      try:
        groupId = root.findall('./{http://maven.apache.org/POM/4.0.0}parent/{http://maven.apache.org/POM/4.0.0}groupId')[0].text.strip()
      except:
        # Create the group from the given name (drop the last token)
        groupId = ".".join(name.split(".")[0:-1])

    ret['group'] = groupId
    properties['project.groupId'] = groupId # Matches: ${project.groupId}
    properties['pom.groupId'] = groupId # Matches: ${pom.groupId}

    try:
      artifactId = root.findall('./{http://maven.apache.org/POM/4.0.0}artifactId')[0].text.strip()
    except:
      artifactId = artifact

    ret['artifact'] = artifactId
    properties['project.artifactId'] = artifactId # Matches: ${project.artifactId}
    properties['pom.artifactId'] = artifactId # Matches: ${pom.artifactId}

    # Get the name
    for tag in root.findall('./{http://maven.apache.org/POM/4.0.0}name'):
      ret['name'] = tag.text.strip()

    ret['name'] = ret.get('name', artifactId)

    # Then we parse <properties> for extra tags.
    try:
      propertiesRoot = root.findall('./{http://maven.apache.org/POM/4.0.0}properties')[0]
    except:
      propertiesRoot = []

    try:
      for propertyItem in propertiesRoot:
        if propertyItem.text:
          tag = propertyItem.tag.split('}')[-1].strip()
          properties[tag] = self.__parseTags(propertyItem.text.strip(), properties)
    except Exception as e:
      raise e

    # Get the description
    for tag in root.findall('./{http://maven.apache.org/POM/4.0.0}description'):
      if tag.text is None:
        continue

      ret['summary'] = tag.text.strip()

    # Get the website
    for tag in root.findall('./{http://maven.apache.org/POM/4.0.0}url'):
      if tag.text is None:
        continue

      ret['website'] = tag.text.strip()

    # Get the organization
    for tag in root.findall('./{http://maven.apache.org/POM/4.0.0}organization/{http://maven.apache.org/POM/4.0.0}name'):
      if tag.text is None:
        continue

      ret['organization'] = tag.text.strip()

    # Get the developers
    for tag in root.findall('./{http://maven.apache.org/POM/4.0.0}developers/{http://maven.apache.org/POM/4.0.0}developer/{http://maven.apache.org/POM/4.0.0}name'):
      if tag.text is None:
        continue

      # Do not include the organization as an author if listed twice.
      if tag.text.strip() == ret.get('organization'):
        continue

      # Otherwise, append the author
      ret['authors'] = ret.get('authors', [])
      ret['authors'].append(tag.text.strip())

    # Get the license type
    for tag in root.findall('./{http://maven.apache.org/POM/4.0.0}licenses/{http://maven.apache.org/POM/4.0.0}license/{http://maven.apache.org/POM/4.0.0}name'):
      if tag.text is None:
        continue

      ret['license'] = ret.get('license', [])
      ret['license'].append(tag.text.strip())

    # Get the dependency "management" which can list dependency versions.
    # Well... it /sometimes/ does.
    dependencyManagement = (parentMetadata or {}).get('dependencyManagement', {}).copy()
    for tag in root.findall('./{http://maven.apache.org/POM/4.0.0}dependencyManagement/{http://maven.apache.org/POM/4.0.0}dependencies/{http://maven.apache.org/POM/4.0.0}dependency'):
      dependency = self.__parseDependency(tag, properties, dependencyManagement)

      if dependency.get('scope') == "import":
        # We have to import the dependencyManagement from this POM, too.
        dependencyMetadata = self.__retrieveMetadata(dependency['name'], version = dependency.get('version'))
        dependencyManagement.update(dependencyMetadata.get('dependencyManagement', {}))

      dependencyManagement[dependency.get('name')] = dependency

    # Retain it (so it can be returned from a parent)
    ret['dependencyManagement'] = dependencyManagement

    # "Plugins" are a kind of build dependency which is inherited from
    # parents.

    # Get the plugin "management" which can list plugin versions/groups.
    pluginManagement = (parentMetadata or {}).get('pluginManagement', {}).copy()
    for tag in root.findall('./{http://maven.apache.org/POM/4.0.0}build/{http://maven.apache.org/POM/4.0.0}pluginManagement/{http://maven.apache.org/POM/4.0.0}plugins/{http://maven.apache.org/POM/4.0.0}plugin'):
      dependency = self.__parseDependency(tag, properties, pluginManagement)

      if dependency.get('scope') == "import":
        # We have to import the dependencyManagement from this POM, too.
        dependencyMetadata = self.__retrieveMetadata(dependency['name'], version = dependency.get('version'))
        pluginManagement.update(dependencyMetadata.get('pluginManagement', {}))

      pluginManagement[dependency.get('name')] = dependency

    # Retain it (so it can be returned from a parent)
    ret['pluginManagement'] = pluginManagement

    # Get the plugins
    for tag in root.findall('./{http://maven.apache.org/POM/4.0.0}build/{http://maven.apache.org/POM/4.0.0}plugins/{http://maven.apache.org/POM/4.0.0}plugin'):
      dependency = self.__parseDependency(tag, properties, pluginManagement, latest=True)

      ret['plugins'] = ret.get('plugins', [])
      ret['plugins'].append(dependency)

    # Get the dependencies
    for tag in root.findall('./{http://maven.apache.org/POM/4.0.0}dependencies/{http://maven.apache.org/POM/4.0.0}dependency'):
      dependency = self.__parseDependency(tag, properties, dependencyManagement)

      ret['dependencies'] = ret.get('dependencies', [])
      ret['dependencies'].append(dependency)

    rootPath = os.path.join("repository", *group.split('.'), artifact)

    if baseURL is None:
      baseURL = MavenPackage.MIRRORS[0]

    # Add the POM file as a resource
    url = baseURL % (group.replace('.', '/') + "/" + artifact)
    url = url + f"/{version}/{artifact}-{version}.pom"
    ret['install'] = ret.get('install', [])
    ret['install'].append({
      "name": f"{artifact} {version} POM",
      "type": "resource",
      "subtype": "file",
      "to": f"{rootPath}/{version}/{artifact}-{version}.pom",
      "source": url
    })

    # Look for actual packages
    url = baseURL % (group.replace('.', '/') + "/" + artifact)
    url = url + f"/{version}/{artifact}-{version}.jar.sha1"
    data, type, size = self.network.get(url, suppressError=True)

    if data is not None:
      url = baseURL % (group.replace('.', '/') + "/" + artifact)
      url = url + f"/{version}/{artifact}-{version}.jar"
      ret['install'].append({
        "name": f"{artifact} {version} Distribution",
        "type": "resource",
        "subtype": "application/java-archive",
        "to": f"{rootPath}/{version}/{artifact}-{version}.jar",
        "source": url
      })

      url = baseURL % (group.replace('.', '/') + "/" + artifact)
      url = url + f"/{version}/{artifact}-{version}.jar.sha1"
      ret['install'].append({
        "name": f"{artifact} {version} Distribution Checksum",
        "type": "resource",
        "subtype": "file",
        "to": f"{rootPath}/{version}/{artifact}-{version}.jar.sha1",
        "source": url
      })

    # Look for source code distribution
    url = baseURL % (group.replace('.', '/') + "/" + artifact)
    url = url + f"/{version}/{artifact}-{version}-sources.jar.sha1"
    data, type, size = self.network.get(url, suppressError=True)

    if data is not None:
      url = baseURL % (group.replace('.', '/') + "/" + artifact)
      url = url + f"/{version}/{artifact}-{version}-sources.jar"
      ret['install'].append({
        "name": f"{artifact} {version} Source Distribution",
        "type": "resource",
        "subtype": "application/java-archive",
        "to": f"{rootPath}/{version}/{artifact}-{version}-sources.jar",
        "source": url
      })

      url = baseURL % (group.replace('.', '/') + "/" + artifact)
      url = url + f"/{version}/{artifact}-{version}-sources.jar.sha1"
      ret['install'].append({
        "name": f"{artifact} {version} Source Distribution Checksum",
        "type": "resource",
        "subtype": "file",
        "to": f"{rootPath}/{version}/{artifact}-{version}-sources.jar.sha1",
        "source": url
      })

    # Look for any testing code
    url = baseURL % (group.replace('.', '/') + "/" + artifact)
    url = url + f"/{version}/{artifact}-{version}-test-sources.jar.sha1"
    data, type, size = self.network.get(url, suppressError=True)

    if data is not None:
      url = baseURL % (group.replace('.', '/') + "/" + artifact)
      url = url + f"/{version}/{artifact}-{version}-test-sources.jar"
      ret['install'].append({
        "name": f"{artifact} {version} Source Test Distribution",
        "type": "resource",
        "subtype": "application/java-archive",
        "to": f"{rootPath}/{version}/{artifact}-{version}-test-sources.jar",
        "source": url
      })

      url = baseURL % (group.replace('.', '/') + "/" + artifact)
      url = url + f"/{version}/{artifact}-{version}-test-sources.jar.sha1"
      ret['install'].append({
        "name": f"{artifact} {version} Source Test Distribution Checksum",
        "type": "resource",
        "subtype": "file",
        "to": f"{rootPath}/{version}/{artifact}-{version}-test-sources.jar.sha1",
        "source": url
      })

    # Look for maven-metadata.xml
    url = baseURL % (group.replace('.', '/') + "/" + artifact)
    url = url + f"/maven-metadata.xml.sha1"
    data, type, size = self.network.get(url, suppressError=True)

    if data is not None:
      url = baseURL % (group.replace('.', '/') + "/" + artifact)
      url = url + f"/maven-metadata.xml"
      ret['install'].append({
        "name": f"{artifact} {version} maven-metadata.xml File",
        "type": "resource",
        "subtype": "file",
        "to": f"{rootPath}/maven-metadata-central.xml",
        "source": url
      })

      url = baseURL % (group.replace('.', '/') + "/" + artifact)
      url = url + f"/maven-metadata.xml.sha1"
      ret['install'].append({
        "name": f"{artifact} {version} maven-metadata.xml Checksum",
        "type": "resource",
        "subtype": "file",
        "to": f"{rootPath}/maven-metadata-central.sha1",
        "source": url
      })

    # Finally, look for documentation
    url = baseURL % (group.replace('.', '/') + "/" + artifact)
    url = url + f"/{version}/{artifact}-{version}-javadoc.jar.sha1"
    data, type, size = self.network.get(url, suppressError=True)

    if data is not None:
      url = baseURL % (group.replace('.', '/') + "/" + artifact)
      url = url + f"/{version}/{artifact}-{version}-javadoc.jar"
      ret['install'].append({
        "name": f"{artifact} {version} Documentation Distribution",
        "type": "resource",
        "subtype": "application/java-archive",
        "to": f"{rootPath}/{version}/{artifact}-{version}-javadoc.jar",
        "source": url
      })

      url = baseURL % (group.replace('.', '/') + "/" + artifact)
      url = url + f"/{version}/{artifact}-{version}-javadoc.jar.sha1"
      ret['install'].append({
        "name": f"{artifact} {version} Documentation Distribution Checksum",
        "type": "resource",
        "subtype": "file",
        "to": f"{rootPath}/{version}/{artifact}-{version}-javadoc.jar.sha1",
        "source": url
      })

    # Retain properties (so we can inherit them in children.)
    ret['properties'] = properties

    return ret

  def __retrieveMetadata(self, artifact, group=None, version = None):
    """ Retrieves the XML POM metadata for the given package at the given version.

    Arguments:
      artifact (str): The name of the package.
      group (str): The group the package belongs to.
      version (str): The version requested, if not known, queries versions instead.

    Returns:
      None when the data cannot be found.
    """

    ret = {}

    if group is None:
      name = artifact
      artifact = name.split('.')[-1]
      group = name[0:-len(artifact) - 1]

    name = group + "." + artifact

    for baseURL in MavenPackage.MIRRORS:
      url = baseURL % (group.replace('.', '/') + "/" + artifact)
      if version:
        url = url + f"/{version}/{artifact}-{version}.pom"
      else:
        url = url + "/maven-metadata.xml"

      data, type, size = self.network.get(url, suppressError=True)
      if data is None:
        continue

      MavenPackage.Log.noisy(f"Pulling metadata at {url}")

      if version:
        # Read POM
        ret = self.__parsePOM(data.read(), baseURL = baseURL,
                                           version = version,
                                           group = group,
                                           artifact = artifact)
        ret['package'] = artifact
      else:
        ret['package'] = artifact

        root = XML.parse(data).getroot()

        # Get latest version tags
        latest = None
        for tag in root.findall('./versioning/latest'):
          latest = tag.text.strip()

        # Get release tag
        release = None
        for tag in root.findall('./versioning/release'):
          release = tag.text.strip()

        # Get a list of all versions
        versions = []
        for tag in root.findall('./versioning/versions/version'):
          versions.append(tag.text.strip())

        # Sort versions
        versions = Semver.sortVersionList(versions)

        if latest:
          ret['latest'] = latest
        if release:
          ret['release'] = release

        ret['versions'] = versions

      break

    if not ret:
      # Hmm, attempt to try adding a '.' to the package name
      # This will aid those cases like `javax.inject.javax.inject` where the
      # group is `javax.inject` as is the artifact. Yikes.
      if '.' in group:
        artifact = group.split('.')[-1] + "." + artifact
        group = name[0:-len(artifact) - 1]
        return self.__retrieveMetadata(artifact, group = group, version = version)

    return ret

  def sourceURLFor(self, name):
    """ Returns the canonical source url.
    """

    return MavenPackage.CANONICAL_SOURCE % name.replace('.', '/')

  def uidFor(self, name):
    """ Returns the id for a particular source package name.
    """

    sourceURL = self.sourceURLFor(name)

    return self.objects.uidFor(None, objectInfo = {
      "name": name,
      "type": "maven-package",
      "source": sourceURL
    })

  def idFor(self, identity, name):
    """ Returns the id for a particular source package name.
    """

    sourceURL = self.sourceURLFor(name)

    return self.objects.idFor(None, identity, objectInfo = {
      "name": name,
      "type": "maven-package",
      "source": sourceURL
    })

  def __queryPOM(self, identity, pomfile):
    """
    """

    data = None
    with open(pomfile, "r") as f:
      data = f.read()

    ret = []
    if data:
      pom = self.__parsePOM(data)

      ret = list(map(lambda x: {
        'name': x.get('name'),
        'version': x.get('version')
      }, pom.get('dependencies', [])))

      # Add plugins for good measure... we often want to build
      # TODO: maybe behind a flag to disable
      ret = ret + list(map(lambda x: {
        'name': x.get('name'),
        'version': x.get('version')
      }, pom.get('plugins', [])))

      for package in ret:
        id = self.idFor(identity, package['name'])
        MavenPackage.Log.output('{')
        MavenPackage.Log.output(f'  "name": "{package["name"]}",')
        MavenPackage.Log.output(f'  "type": "maven-package",')
        MavenPackage.Log.output(f'  "id": "{id}",')
        MavenPackage.Log.output(f'  "dependencies": [],')
        MavenPackage.Log.output(f'  "mount": "ignore",')
        MavenPackage.Log.output(f'  "version": "{package["version"]}",')
        MavenPackage.Log.output(f'  "replace": "never",')
        MavenPackage.Log.output(f'  "inject": "ignore"')
        MavenPackage.Log.output('},')

    return ret

  def __queryAll(self):
    """
    """

    return []

  def queryAll(self, identity, pomfile=None):
    """ Returns a list of all packages.
    """

    if pomfile:
      return self.__queryPOM(identity, pomfile)

    return self.__queryAll()

  def query(self, identity, name, pending = None, version = None):
    """ Returns the id and initial metadata for the package.
    """

    if version and version.startswith('*'):
      version = version[1:]

    uid = self.uidFor(name)
    id = self.idFor(identity, name)

    MavenPackage.Log.write(f"Ingesting {name} {version or ''}")
    MavenPackage.Log.write(f"uid: {uid}")
    MavenPackage.Log.write(f" id: {id}")

    sourceURL = self.sourceURLFor(name)

    metadata = self.__queryMetadata(name, version = version)
    if metadata is None:
      # Cannot find the metadata
      MavenPackage.Log.warning(f"cannot find metadata for {name} {version or ''}")
      return None

    version = metadata.get('version')

    # Build object manifest
    objectInfo = {
      "type": "maven-package",
      "name": name,
      "source": sourceURL,
      "architecture": "x86-64",
      "environment": "linux",
      "init": {
        "copy": [
          {
            "source": "repository",
            "to": "/opt/maven/repository"
          }
        ]
      }
    }

    # Format dependencies
    ingestRequests = []

    # Go through the entire list of dependencies
    for dependency in metadata.get('dependencies', []):
      if dependency.get('scope') == "provided":
        continue

      if dependency.get('scope') == "test":
        continue

      if dependency.get('scope') == "system":
        continue

      name = dependency.get('name')
      subVersion = dependency.get('version')

      dependencyUid = self.uidFor(name)
      dependencyId  = self.idFor(identity, name)

      if id == dependencyId:
        MavenPackage.Log.warning(f"ignoring dependency {name}@{subVersion or 'latest'} which is itself")
        continue

      MavenPackage.Log.write(f"Found dependency {name}@{subVersion or 'latest'}")

      if not dependencyId in (pending or {}):
        ingestInfo = {
          "packageType": "maven-package",
          "packageName": name,
        }
        ingestRequests.append(ingestInfo)

        if subVersion:
          ingestVersion = subVersion
          if subVersion.startswith('*'):
            ingestVersion = subVersion[1:]
          ingestRequests[-1]['version'] = ingestVersion
      else:
        # This package does the UNTHINKABLE and requires a library that itself
        # requires us! Why does Maven allow such atrocities?
        MavenPackage.Log.warning(f"not ingesting dependency {name}@{subVersion or 'latest'} which creates a cycle")

      dependencyInfo = {
        "id": dependencyId,
        "uid": dependencyUid,
        "name": name,
        "mount": "ignore",
        "replace": "never",
        "type": "maven-package"
      }

      if subVersion:
        dependencyInfo['version'] = subVersion
      else:
        dependencyInfo['version'] = "x"

      objectInfo['dependencies'] = objectInfo.get('dependencies', [])
      objectInfo['dependencies'].append(dependencyInfo)

    # Add resources
    objectInfo['install'] = metadata['install']

    # Add other metadata
    for key in ['website', 'license', 'summary', 'authors', 'organization']:
      if key in metadata:
        objectInfo[key] = metadata[key]

    #import json
    #print(json.dumps(objectInfo, indent=2, separators=(',', ': ')))

    # Return the ingested object information
    return objectInfo, {
      # Recursively ingest these packages
      "ingest": ingestRequests,

      # The version to tag this as
      "tag": version
    }

  def report(self, identity, runType, options, objectInfo, name, data, runReport, pending = None, version = None):
    return None

  def craft(self):
    """ Creates an Occam object from the given package resource.
    """
