# UNUSED at the moment... we just... kinda store the jar files now.

# This script builds and packages a Maven package from its sources/test-sources
# jar files that are stored in a Maven repository.

# This file is in the public domain.

import os, sys
import shutil
import hashlib

# We first build out the expected maven directory structure

base = sys.argv[1]
groupId = sys.argv[2]
artifactId = sys.argv[3]
version = sys.argv[4]

path = "./src"
build = True
if not os.path.exists(path):
  # No sources!
  build = False
  os.mkdir(path)

path = "./src/main"
if not os.path.exists(path):
  os.mkdir(path)

path = "./src/main/java"
if not os.path.exists(path):
  os.mkdir(path)

path = "./src/main/resources"
if not os.path.exists(path):
  os.mkdir(path)

# We go into the resources directory and unpack there
os.chdir(path)

# Now we walk the files and move the ".java" files to the java path
sourcePath = "../java"
for root, dirs, files in os.walk("."):
  path = os.path.join(sourcePath, root)

  for file in files:
    if file.split('.')[-1] == "java":
      subPath = "../java"
      for part in root.split(os.sep):
        if part == "." or part == "..":
          continue

        subPath = os.path.join(subPath, part)
        if not os.path.exists(subPath):
          os.mkdir(subPath)

      shutil.move(os.path.join(root, file), os.path.join(path, file))

os.chdir("../../..")

# If we see a test package, let's deal with that as well

if os.path.exists("./src/test"):
  path = "./src/test/java"
  if not os.path.exists(path):
    os.mkdir(path)

  path = "./src/test/resources"
  if not os.path.exists(path):
    os.mkdir(path)

  os.chdir(path)

  # Now we walk the files and move the ".java" files to the java path
  sourcePath = "../java"
  for root, dirs, files in os.walk("."):
    path = os.path.join(sourcePath, root)

    for file in files:
      if file.split('.')[-1] == "java":
        subPath = "../java"
        for part in root.split(os.sep):
          if part == "." or part == "..":
            continue

          subPath = os.path.join(subPath, part)
          if not os.path.exists(subPath):
            os.mkdir(subPath)

        shutil.move(os.path.join(root, file), os.path.join(path, file))

  os.chdir("../../..")

# Copy POM
if os.path.exists(base + ".pom"):
  shutil.copy(base + ".pom", "pom.xml")

# Build, if required
if build:
  code = os.system('mvn package -Dmaven.test.skip=true -Dmaven.compiler.source=1.8 -Dmaven.compiler.target=1.8 -e')

  if code != 0:
    exit(1)

# Now, package
if not os.path.exists("../build"):
  os.mkdir("../build")

# Determine correct directory
path = "../build/repository"
if not os.path.exists(path):
  os.mkdir(path)

for package in groupId.split('.'):
  path = os.path.join(path, package)
  if not os.path.exists(path):
    os.mkdir(path)

path = os.path.join(path, artifactId)
if not os.path.exists(path):
  os.mkdir(path)

path = os.path.join(path, version)
if not os.path.exists(path):
  os.mkdir(path)

# Copy in the POM and jar files (if there are any)
shutil.copy(base + ".pom", os.path.join(path, base + ".pom"))
if build:
  shutil.copy(os.path.join("target", base + ".jar"), os.path.join(path, base + ".jar"))

# And package the SHA1 files
for subpath in [os.path.join(path, base + ".pom"), os.path.join(path, base + ".jar")]:
  if not os.path.exists(subpath):
    continue

  targetPath = subpath + ".sha1"

  # The following code is from:
  # https://stackoverflow.com/questions/22058048/hashing-a-file-in-python
  # Thanks, stackoverflow, for doing the basic stuff.

  BUF_SIZE = 65536

  sha1 = hashlib.sha1()

  with open(subpath, 'rb') as f:
    while True:
      data = f.read(BUF_SIZE)
      if not data:
        break

      sha1.update(data)

  # Save the hash in a file without any whitespace
  with open(targetPath, 'w+') as f:
    f.write(sha1.hexdigest())
