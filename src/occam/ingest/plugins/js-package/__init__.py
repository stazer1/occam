# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.ingest.manager import IngestManager, package

from occam.network.manager import NetworkManager
from occam.objects.manager import ObjectManager
from occam.resources.write_manager import ResourceWriteManager

from occam.manager import uses

from occam.git_repository import GitRepository
from occam.semver import Semver

import uuid as UUID
import os, re, json, datetime

@package("js-package")
@uses(ObjectManager)
@uses(NetworkManager)
@uses(ResourceWriteManager)
class JSPackage:
  """ This aids in importing JavaScript (node) packages into Occam for preservation.
  """

  # The public identifier for the account managing the NPM mirror.
  NPM_IDENTITY = "6Mv2TwtQgXWAboSjModskEzdu4q1wZAzGDmh5xw8U2tHgc5"

  # These pages list the packages. Another route is to pull the package names
  # from the ARCHIVE_SOURCE URL.
  INDEX_SOURCE = "https://registry.npmjs.org"
  INDEX_URL = "https://registry.npmjs.org"

  # Otherwise, we just assume the package info page is the URL for any package.
  # This will be used to generate the object identifier.
  CANONICAL_SOURCE = "https://registry.npmjs.org/%s"

  def __base64ToHex(self, b64):
    """ Converts a base64 encoded string to a string representing its hex dump.

    Args:
      b64 (str): The base64 encoded string.

    Returns:
      str: The re-encoded hex dump of the same byte sequence.
    """

    import codecs
    import base64

    bytes = base64.b64decode(b64)
    hexbytes = codecs.encode(bytes, 'hex')
    hexdump = hexbytes.decode('utf-8')

    return hexdump

  def __reformVersion(self, version):
    """ Converts a NPM version to an Occam version.

    Arguments:
      version (str): The NPM version.

    Returns:
      str: The corresponding Occam version.
    """

    if version is None:
      return None

    # Occam uses npm-style semantic versions (with added features for Java
    # ecosystems) so we don't have to do anything!
    return version

  def __queryAll(self):
    """ Retrieves a listing of all packages.
    """

    JSPackage.Log.write(f"Parsing package listing at {JSPackage.INDEX_SOURCE}")

    ret = []

    # The package index is a git repository we need to clone
    #data, content_type, size = self.network.get(JSPackage.INDEX_SOURCE)

    #JSPackage.Log.writePercentage("Parsing...")

    #position = 0
    #for line in data.readlines():
    #  position += len(line)
    #  JSPackage.Log.updatePercentage(position / size * 100)

    #  line = line.strip()
    #  if line.startswith(b'<a'):
    #    name = line.split(b'>', 1)[1].split(b'<', 1)[0].decode('utf-8')
    #    ret.append(name)

    #JSPackage.Log.updatePercentage(100)

    return ret

  def __queryMetadata(self, packageName, version = None):
    """ Retrieves the package metadata for the given JS package that matches the given version.

    Returns:
      None when the data cannot be found.
    """

    version = self.__reformVersion(version or "x")

    # Get the metadata listing
    metadata = self.__retrieveMetadata(packageName, version = version)

    # Bail if the package metadata cannot be found
    if metadata is None:
      return None

    # If version exists, we find that version
    versions = metadata.get('versions', {}).keys()
    versions = Semver.sortVersionList(versions)

    if not '.' in version:
      version = "^" + version
    tag = Semver.resolveVersion(version, versions)

    return metadata.get('versions', {}).get(tag)

  def __retrieveMetadata(self, packageName, version = None):
    """ Retrieves the package metadata for the given JS package at the given package version.

    Returns:
      None when the data cannot be found.
    """

    # Look up the metadata from the package index
    url = JSPackage.CANONICAL_SOURCE % packageName

    JSPackage.Log.noisy(f"Pulling metadata at {url}")

    ret = []

    try:
      data = self.network.getJSON(url)
    except Exception as e:
      ret = None

    if ret is None:
      return None

    return data

  def sourceURLFor(self, packageName):
    """ Returns the canonical source url.
    """

    sourceURL = JSPackage.CANONICAL_SOURCE % (packageName)

    return sourceURL

  def uidFor(self, occamType, occamName, packageName, sourceURL):
    """ Returns the uid for a particular source package name.
    """

    return self.objects.uidFor(None, objectInfo = {
      "name": occamName,
      "type": occamType,
      "source": sourceURL
    })

  def idFor(self, identity, occamType, occamName, packageName, sourceURL):
    """ Returns the id for a particular source package name.
    """

    return self.objects.idFor(None, identity, objectInfo = {
      "name": occamName,
      "type": occamType,
      "source": sourceURL
    })

  def __queryLockfile(self, identity, lockfile=None, packagefile=None):
    """ Queries the contents of either a lock file or a package.json file.

    It will ensure that the dependencies within can be resolved with the
    packages that currently exist.
    """

    ret = []

    if lockfile:
      with open(lockfile, "r") as f:
        data = json.load(f)

      # The list of packages and versions is listed within `packages`
      packages = filter(lambda x: x != "" and "node_modules/" in x and data['packages'][x].get('version'),
                        data.get('packages').keys())

      ret = list(map(lambda x: {
        'name': x[x.rindex("node_modules")+13:],
        'version': data['packages'][x]['version']
      }, packages))
    elif packagefile:
      with open(packagefile, "r") as f:
        data = json.load(f)

    for package in ret:
      # Ok, now present the occam dependency
      sourceURL = self.sourceURLFor(package['name'])
      id = self.idFor(identity, occamType = "js-package",
                                occamName = package['name'],
                                packageName = package['name'],
                                sourceURL = sourceURL)
      JSPackage.Log.output('{')
      JSPackage.Log.output(f'  "name": "{package["name"]}",')
      JSPackage.Log.output(f'  "type": "js-package",')
      JSPackage.Log.output(f'  "id": "{id}",')
      JSPackage.Log.output(f'  "version": "{self.__reformVersion(package["version"])}",')
      JSPackage.Log.output(f'  "dependencies": [],')
      JSPackage.Log.output(f'  "inject": "ignore",')
      JSPackage.Log.output(f'  "mount": "ignore",')
      JSPackage.Log.output(f'  "replace": "never"')
      JSPackage.Log.output('},')

    return ret

  def queryAll(self, identity, lockfile=None, packagefile=None):
    """ Returns a list of all packages.
    """

    if lockfile or packagefile:
      return self.__queryLockfile(identity, lockfile, packagefile)

    return self.__queryAll()

  def query(self, identity, packageName, pending = None, version = None):
    """ Returns the id and initial metadata for the package.
    """

    # Retrieve metadata
    metadata = self.__queryMetadata(packageName, version = version)

    if metadata is None:
      JSPackage.Log.write(f"Cannot find metadata for {packageName} {version if version else ''}")
      return None

    # Get the canonical name
    name = metadata['name']
    version = self.__reformVersion(metadata['version'])

    # Get the canonical URL
    sourceURL = self.sourceURLFor(name)

    distSource = metadata['dist']['tarball']
    distFilename = os.path.basename(distSource)
    distType = "application/gzip"
    if distFilename.endswith('.tar.gz') or distFilename.endswith('.tgz'):
      distType = "application/gzip"
    elif distFilename.endswith('.bz'):
      distType = "application/x-bzip2"
    elif distFilename.endswith('.xz'):
      distType = "application/x-xz"

    # Generate the SHA hash cache path
    cachePath = "_cacache/content-v2"
    try:
      distHash = metadata['dist']['integrity']
      digestType, digest = distHash.split('-')
      digest = self.__base64ToHex(digest)
    except:
      digestType = "sha1"
      digest = metadata['dist']['shasum']
    cachePath = os.path.join(cachePath, digestType, digest[0:2], digest[2:4], digest[4:])

    # Now we generate the object metadata
    objectInfo = {
      "name": name,
      "type": "js-package",
      "environment": "linux",
      "architecture": "x86-64",
      "source": sourceURL,
      "dependencies": [
      ],
      "metadata": {
        "npm": metadata
      },
      "authors": [],
      "install": [
        {
          "type": "resource",
          "subtype": distType,
          "source": distSource,
          "to": distFilename,
          "name": f"{name} {version} Distribution"
        }
      ],
      "init": {
        "copy": [
          {
            "source": distFilename,
            "to": f"/opt/npm/cache/{cachePath}"
          }
        ]
      }
    }

    if metadata.get('license'):
      objectInfo['license'] = metadata['license']

    if metadata.get('description'):
      objectInfo['summary'] = metadata['description']

    if metadata.get('author'):
      author = metadata['author']

      if isinstance(author, dict):
        author = author.get('name')

      if author:
        objectInfo['authors'].append(author)

    if metadata.get('contributors'):
      for author in metadata['contributors']:
        if isinstance(author, dict):
          author = author.get('name')

        if author:
          if author not in objectInfo['authors']:
            objectInfo['authors'].append(author)

    if metadata.get('keywords'):
      # Purge duplicates using list(set( ... ))
      objectInfo['tags'] = list(set(metadata['keywords']))

    if metadata.get('repository'):
      # This has a git repository maybe
      repositoryURL = None
      if isinstance(metadata['repository'], dict) and metadata['repository'].get('type') == "git" and 'url' in metadata['repository']:
        repositoryURL = metadata['repository']['url']

      if repositoryURL:
        if repositoryURL.startswith("git+"):
          repositoryURL = repositoryURL.split("+", 1)[1]
        if repositoryURL.startswith("git:"):
          repositoryURL = 'https:' + repositoryURL[4:]

        if self.network.isGitAt(repositoryURL):
          # Mirror it
          objectInfo['install'].append({
            "type": "resource",
            "subtype": "git",
            "source": repositoryURL,
            "to": "repository",
            "mount": "ignore",
            "name": f"{repositoryURL} Source Repository"
          });

          if metadata.get('gitHead'):
            objectInfo['install'][-1]['revision'] = metadata['gitHead']

    if metadata.get('maintainers'):
      for author in metadata['maintainers']:
        if isinstance(author, dict):
          author = author.get('name')

        if author:
          objectInfo['collaborators'] = objectInfo.get('collaborators', [])
          objectInfo['collaborators'].append(author)

    if metadata.get('homepage'):
      objectInfo['website'] = metadata['homepage']

    id = self.idFor(identity, occamType = "js-package",
                              occamName = name,
                              packageName = name,
                              sourceURL = sourceURL)

    uid = self.uidFor(occamType = "js-package",
                      occamName = name,
                      packageName = name,
                      sourceURL = sourceURL)

    JSPackage.Log.write("uid: {}".format(uid))
    JSPackage.Log.write(" id: {}".format(id))

    ingestRequests = []

    # Gather dependencies
    for dependencyName, dependencyVersion in metadata.get('dependencies', {}).items():
      info = {}
      info['type'] = "js-package"
      info['name'] = dependencyName

      info['id'] = self.idFor(identity, occamType = "js-package",
                                        occamName = info['name'],
                                        packageName = dependencyName,
                                        sourceURL = self.sourceURLFor(dependencyName))

      info['version'] = self.__reformVersion(dependencyVersion)
      info['replace'] = 'never'
      info['mount'] = 'ignore'

      objectInfo["dependencies"].append(info)

      if not info['id'] in (pending or {}):
        if info['type'] == "js-package":
          ingestRequests.append({
            "packageType": info['type'],
            "packageName": info['name']
          })

          if 'version' in info and info['version'] != 'x':
            ingestRequests[-1]['version'] = info['version']

    return objectInfo, {
      "ingest": ingestRequests,
      "tag": version
    }

  def report(self, identity, runType, options, objectInfo, packageName, data, runReport, pending = None, version = None):
    """ Finishes the process.
    """

    return None

  def craft(self):
    """ Creates an Occam object from the given package resource.
    """
