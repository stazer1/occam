# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os          # path functions
import json

from occam.log    import Log
from occam.object import Object

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.objects.write_manager import ObjectWriteManager
from occam.permissions.manager   import PermissionManager

@command('objects', 'clone',
  category      = 'Object Management',
  documentation = "Creates a copy of the given object.")
@argument("object", type = "object", help = "the object to clone")
@argument("path",   type = str,      help = "path to clone the object", nargs = "?")
@option("--to",             action = "store",
                            dest   = "to_object",
                            type   = "object",
                            help   = "the object id to clone into")
@option("-o", "--override", dest    = "override",
                            action  = "store",
                            help    = "the object info to override provided as a JSON string")
@option("-n", "--name",     action = "store",
                            dest   = "name",
                            help   = "the new name for the cloned object")
@option("-t", "--type",     action = "store",
                            dest   = "type",
                            help   = "the new type for the cloned object")
@option("-s", "--subtype",  action = "append",
                            dest   = "subtype",
                            nargs  = "?",
                            help   = "the new subtype for the cloned object")
@option("-p", "--temporary",action = "store_true",
                            dest   = "temporary",
                            help   = "create a temporary path (you are responsible for deleting)")
@option("-c", "--no-cloned-from", action = "store_true",
                                  dest   = "no_cloned_from",
                                  help   = "suppresses the clonedFrom tag")
@option("-i", "--internal", action = "store_true",
                            dest   = "store",
                            help   = "build the clone in the object store instead of making a local path")
@option("-l", "--linked",   action = "store_true",
                            dest   = "linked",
                            help   = "whether or not to link to the original object instead of creating a new object with a new id")
@option("-j", "--json",     dest    = "to_json",
                            action  = "store_true",
                            help    = "returns result as a json document")
@option("-f", "--full",     dest    = "hard",
                            action  = "store_true",
                            help    = "creates a fresh history")
@uses(ObjectWriteManager)
@uses(PermissionManager)
class CloneCommand:
  def do(self):
    if self.person is None and (self.options.object is None or self.options.object.id == "."):
      Log.error(key="occam.objects.errors.mustBeAuthenticated")
      return -1

    Log.header("Cloning object")

    obj = self.objects.resolve(self.options.object, person = self.person)

    if obj is None:
      Log.error(key="occam.objects.errors.objectNotFound")
      return -1

    to_object = None
    if self.options.to_object:
      to_object = self.objects.resolve(self.options.to_object, person = self.person)

      if to_object is None:
        Log.error(key="occam.objects.commands.clone.errors.toObjectNotFound")
        return -1

    obj_type = self.options.type or self.objects.infoFor(obj).get('type', 'object')
    obj_name = self.options.name or self.objects.infoFor(obj).get('name', 'object')
    obj_subtype = self.options.subtype or self.objects.infoFor(obj).get('subtype', [])
    if not isinstance(obj_subtype, list):
      obj_subtype = [obj_subtype]

    path = self.options.path
    if path is None or path == "":
      path = os.path.join('.', Object.slugFor(obj_type) + '-' + Object.slugFor(obj_name))

    path = os.path.realpath(path)

    # Clone
    new_obj  = None
    # XXX: It's not really clear why this triggers when self.options.name is not None but not store
    if self.options.store or self.options.name:
      if self.options.hard:
        # Create a new object instead (based on this one)
        if obj.owner_id != obj.id:
          raise Exception("cannot clone --full from subobject")
        info = self.objects.retrieveJSONFrom(obj, "object.json", fromObject=True, includeResources=False)
        if obj_subtype:
          if obj_subtype[0] is None:
            info['subtype'] = []
          else:
            info['subtype'] = obj_subtype

        # Override any options passed in
        if self.options.override:
          override = json.loads(self.options.override)
          info.update(override)

        # Creates a temporary Object history locally somewhere
        new_obj = self.objects.write.create(name=obj_name, object_type=obj_type, root=None, path=None, identity = self.person.identity, info=info)

        # Add author
        if self.person:
          self.objects.write.addAuthorTo(new_obj, self.person.identity, self.person)

        # Add files (from old object)
        # Walk the object... and then read in the files
        def addDirectory(fromObj, toObj, path):
          for item in self.objects.retrieveDirectoryFrom(fromObj, path)['items']:
            fullPath = os.path.join(path, item['name'])
            if fullPath == "/object.json":
              continue

            if item['type'] == 'tree':
              addDirectory(fromObj, toObj, fullPath)
            else:
              stream = self.objects.retrieveFileFrom(fromObj, fullPath)
              # TODO: fix addFileTo to be able to take a stream
              self.objects.write.addFileTo(toObj, fullPath[1:], stream.read())

        addDirectory(obj, new_obj, '/')

        # Commit files
        self.objects.write.commit(new_obj, self.person.identity, message="Adds files from template.")

        # Store new object
        self.objects.write.store(new_obj, self.person.identity)
      else:
        # Clone the object within the provided object
        # We need the space to do this, so make a temp clone of to_object
        info = {
          "name": obj_name,
          "type": obj_type
        }

        if obj_subtype:
          info['subtype'] = obj_subtype

        # Override any options passed in
        if self.options.override:
          override = json.loads(self.options.override)
          info.update(override)

        new_obj = self.objects.write.clone(obj, to = to_object,
                                                identity = self.person.identity,
                                                person = self.person,
                                                info = info,
                                                noClonedFrom = self.options.no_cloned_from)

      # Set initial permissions
      self.permissions.update(new_obj.id, canRead=False, canWrite=False, canClone=False, canRun=False)

      if self.person:
        Log.write(key="occam.objects.commands.clone.settingPermissions")
        self.permissions.update(new_obj.id, identity=self.person.identity, canRead=True, canWrite=True, canClone=True, canRun = True)
      else:
        Log.write(key="occam.objects.commands.clone.settingAllPermissions")
        self.permissions.update(new_obj.id, canRead=True, canWrite=True, canClone=True, canRun=True)

      # Clone into a directory by default
      if not self.options.store:
        new_obj = self.objects.retrieve(new_obj.id, new_obj.revision, person = self.person)

        info = None

        # Override any options passed in
        if self.options.override:
          override = json.loads(self.options.override)
          info = self.objects.infoFor(new_obj)
          info.update(override)

        new_obj = self.objects.clone(new_obj, path = path, info = info)
    else:
      info = None

      # Override any options passed in
      if self.options.override:
        override = json.loads(self.options.override)
        info = self.objects.infoFor(obj)
        info.update(override)

      new_obj = self.objects.clone(obj, path = path, info = info)
      Log.write(key="occam.objects.commands.clone.clonedTo", path=new_obj.path)

    if new_obj is None:
      Log.error(key="occam.objects.commands.clone.errors.failed")
      return -1

    ret = {}
    ret["updated"] = []

    for x in (new_obj.roots or []):
      ret["updated"].append({
        "id": x.id,
        "uid": x.uid,
        "revision": x.revision,
        "position": x.position,
      })

    ret["updated"].append({
      "id": new_obj.id,
      "uid": new_obj.uid,
      "revision": new_obj.revision,
      "position": new_obj.position,
    })

    Log.done(key="occam.objects.commands.clone.cloned", type=obj_type, name=obj_name, id=new_obj.id)
    Log.output(json.dumps(ret))

    return 0
