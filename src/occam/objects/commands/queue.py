# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log    import Log
from occam.object import Object

import json
from types import SimpleNamespace

from occam.commands.manager import command, option, argument

from occam.objects.manager         import ObjectManager
from occam.manifests.manager       import ManifestManager, BuildRequiredError
from occam.jobs.manager            import JobManager
from occam.links.write_manager     import LinkWriteManager

from occam.manager import uses

@command('objects', 'queue',
  category      = 'Running Objects',
  documentation = "Queues a run of the given object.")
@argument("object", type="object", nargs="?")
@option("-f", "--from-clean",  dest   = "from_clean",
                               action = "store_true",
                               help   = "will run from a clean environment.")
@option("-t", "--target", action = "store",
                          dest   = "target",
                          help   = "Scheduler target to use for job scheduling. 'null' for an unqueued job.")
@uses(ObjectManager)
@uses(ManifestManager)
@uses(LinkWriteManager)
@uses(JobManager)
class ObjectsQueueCommand:
  def do(self):
    if self.person is None:
      Log.error("Must be authenticated to run.")
      return -1

    Log.header("Queuing run")

    # Determine the object to run
    if self.options.object is None:
      # Default id is '+'
      obj = self.objects.resolve(SimpleNamespace(id       = "+",
                                                 revision = None,
                                                 version  = None,
                                                 uid      = None,
                                                 path     = None,
                                                 index    = None,
                                                 link     = None,
                                                 identity = None),
                                 person = self.person)
    else:
      obj = self.objects.resolve(self.options.object, person = self.person)

    if obj is None:
      # Cannot resolve the object
      Log.error("Could not find the object.")
      return -1

    # Handle staged runs and options within
    taskInfo = None
    buildTask = None
    updated = False
    if obj.link:
      # Determine if we have an existing task stored
      taskInfo = self.links.taskFor(obj)

      if not taskInfo or self.links.taskRequiredFor(obj):
        # Force task recreation
        taskInfo = None

        # And then determine if we have a build task... which we need to use for
        # task creation.
        buildTask = self.links.taskFor(obj, build = True)

    if not taskInfo:
      # Generate or pull a cached build task, if we don't have an existing one
      # Also pass in the build task, if known
      taskInfo = self.manifests.taskFor(obj, buildTask = buildTask,
                                             person = self.person)

      updated = True

    # Store the task, if it is a repository build
    if obj.link:
      # Indicate that the job should clean the build stage, if desired
      taskInfo['runs']['stage']['clean'] = bool(self.options.from_clean)

      taskInfo['id'] = obj.id
      taskInfo['revision'] = '#' + obj.link

      # Write out the task to the stage
      if updated or self.options.from_clean:
        self.links.write.updateTaskFor(obj, taskInfo)
    else:
      # Store the task into the object store
      task = self.manifests.store(taskInfo, person = self.person)

      # Update the task to have the task's new id/revision
      taskInfo['id'] = task.id
      taskInfo['revision'] = task.revision

    # Create the job
    job = self.jobs.create(taskInfo, taskId      = taskInfo.get('id'),
                                     revision    = taskInfo.get('revision'),
                                     interactive = True,
                                     person      = self.person,
                                     object      = obj,
                                     target      = self.options.target)

    # Output the job identifier
    ret = {
      "id": job.id
    }

    Log.output(json.dumps(ret))
    return 0
