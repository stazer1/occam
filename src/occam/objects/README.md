# The Objects Component

This component manages the object storage and retrieval.

## Commands

### `objects` `new`

### `objects` `queue`

### `objects` `view`

### `objects` `list`

### `objects` `status`

### `objects` `history`

### `objects` `set`

### `objects` `append`

### `objects` `pull`

### `objects` `run`

### `objects` `clone`

### `objects` `install`

### `objects` `verify`

### `objects` `sign`

### `objects` `delete`

### `objects` `search`

### `objects` `providers`

### `objects` `viewers`

### `objects` `editors`

## Philosophy

Objects are the basic representation of something preserved within the system.
It is assumed that anything within the system might change, but that any prior
state of that object might be important. Therefore, the design of the system
will accommodate a history of changes to any object in the system.

## Metadata

Every object contains at least one file named `object.json` which is a JSON
document that describes the metadata for the object. This metadata defines not
just descriptive aspects (the name, for instance), but also how the object is
built and/or invoked. This includes dependencies, the commands to invoke, and
any external resources that are needed.

The `id` that identifies the object is not written as part of its metadata.
It is generated using some of the metadata, which then allows it to be
verified upon retrieving the object across machines. The exact mechanism used
to generate the identifier is discussed later on in the identifiers section.

Any version attached to an object is also not written to the metadata. If it
were, it would quickly become confusing if the version was written to different
revisions of the object. Instead, the versions are handled external to the
object data and cryptographically signed by the actor owning the object. This is
discussed in detail in the versioning section later on.

### Descriptive

`name`: The name of the object. Example: `"grep"`

`type`: The type of the object. Example: `"application"`

`organization`: The name of the organization or group that created the object.

`website`: A URL that serves as the website describing or hosting the object.

`summary`: A short description of the purpose of the object.

`description`: A long description of the object. This may be a set of metadata
consisting of a path within the object.

`license`: A string denoting the software or data license the object is using.
Can be a list of strings when multiple are used or a set of metadata consisting
of a path within the object that serves as the license text.

`authors`: A string or set of strings consisting of the names of authors or
identity URIs that match actors known to the federation that serve as creators
of the object.

`collaborators`: Same form as `author`, but for actors that have collaborated
or maintained smaller aspects of the object. The purpose of this field is to
denote people who are not actively involved in the main development of the
object.

`images`: A set of strings that resolve to paths within the object that serve
image data representing the object. These are rendered by conforming clients as
visual representations of the object on pages for the object or search results.

### Behavioral

`build`: Describes how something runs within the build/compile phase.

`run`: Describes how to run the object.

`init`: Describes how to initialize the running object within the machine. This
is not applied during the build phase. It is always applied during the run phase
and also when the object is attached to the machine as a dependency of another.

Within these sections, metadata is used to describe the exact details about how
to invoke the object and what also needs to be placed within these phases.

`environment`: A string denoting the expected system or software environment
needed to invoke the given phase. May be placed in the root of the object
metadata in which case it is the default for any phase where it is not
specified. Example: `"linux"`

`architecture`: A string denoting the expected hardware capability required
to invoke the given phase. May be placed in the root of the object metadata in
which case it is the default for any phase where it is not specified. Example:
`"x86-64"`

`install`: A set of object identifiers for object or resource data that should
be made available alongside this object's normal data. This is usually used to
place the contents of large files or compressed archives (containing data or
source code) alongside the scripts contained within the object itself.

`dependencies`: A list of object identifiers denoting other objects that need to
exist within the machine in order to build or run this object. May be placed in
the root section in which case it applies to all phases.

`command`: A string or set of strings that denote the command to invoke to run
the object. When it is in the form of a list of strings, it denotes the set of
arguments that make up the command. Example: `["/usr/bin/grep", "--version"]`

`script`: A string or set of strings, in the same style as `command`, that
denotes how to invoke a command that will generate metadata that describes
which commands to run. This is used when the exact command is only known after
looking at the input data.

`link`: A set of metadata that contains information about symlinks to create
in the machine's filesystem. Each item has a `source` field which contains a
relative path to a file within the object (or its built package) that will be
pointed to by the symlink created by the absolute path specified by the `to`
field. If the underlying system cannot create a symlink, the file will be a
copy.

`copy`: Specified just like the `link` field, this instead copies the file
indicated by the relative path to the object (or built package) in `source` to
the absolute path represented by `to`. This file can be modified freely by the
running object. The modifications will be thrown out at the resolution of the
object's runtime.

### Sub-objects

The `includes` field can contain a list of object descriptions consisting of
metadata that will describe completely independently identified objects that
reuse this object's history and, if the main object provides, build.

The point of this is to provide specifically isolated objects for each
program or library that might be provided by a larger collection. For instance,
`gcc` is compiled and provides `gcc` and `g++` separately along with `libgcc`
and a c++ runtime. All of these can be separate objects that share a build
package.

Since it reuses the build phase, if provided, of the overall object, the
sub-object cannot have its own `build` phase, but may specify its own `run`
phase. The sub-object inherits the `environment` and `architecture` of the main
object, which can be overwritten in the sub-object's metadata or within the
sub-object's `run` description, if provided.

Some other metadata is inherited including descriptive metadata such as the
`organization`, `authors`, `website`, `summary`, `description`, and `license`.
All of these fields may be overwritten if provided in the sub-object itself.

## Identifiers

Objects are identified uniquely based on several properties of the object that
are hashed into a single identifier. There are two identifiers, the `uid`, which
denotes a unique identifier for the object metadata and history, and the proper
`id` which also includes the identity of the actor that created or maintains it.

The `uid` is crafted as a hash of the `name` and `type` of the object, which are
provided by the metadata and are freeform fields, and the revision of the first
git commit in the object's history along with the original metadata found in the
`object.json` at that initial commit.

If the object has a `source` given, the `uid` is calculated in a different way.
It uses the `name` and `type` along with the `source` field and ignores the
initial commit. This means that objects have identifiers that are tied to a URL
instead.

The `id`, then, is the `uid` hashed along with the `identity` of the actor
providing the object. If the object is a fork, the `id` also combines the time
it was cloned and the revision it was cloned from.

The identifiers are typically represented by a multihash determined by Base58
encoding the SHA256 hash of the tokens generated with the metadata described
above. This produces `uid` and `id` identifiers that resemble an identifier
like this one:

```
QmZZ39tjcMZgPo8qsKr7FELJYNBXKdrbasA4YQeHGWGDsz
```

### Tokens

The tokens, prior to hashing, look like this for the `uid` with a `source`:

```
type:<type>
name:<name>
source:<source>
```

Without a source, it incorporates the `git` metadata, as described prior, in
this form:

```
type:<type>
name:<name>
root:<root revision>
info:<initial object.json data>
```

When the object being considered is a sub-object, the `uid` of the owning object
that contains the `includes` field is the basis with extra metadata prepended as
follows, with the sub-object's `name` and `type` field being used:

```
subobjecttype:<subobject type>
subobjectname:<subobject name>
<remaining uid token>
```

Although this means that the sub-objects have unique `uid` identifiers (and,
thus, unique `id` identifiers,) they are not stored in isolation. They are
stored using their owning object's `uid` instead. When a sub-object `id` is
used, it will resolve to this token which explains that it is a sub-object.
The owning object is determined from the remaining token data, which yields
the `uid` used to store and retrieve the actual data.

The `id` token is built as such:

```
uid:<uid>
identity:<identity uri>
<uid token>
```

If it was cloned from an existing object, the `id` is prepended with the cloned
object metadata:

```
clonedfrompublished:<clonedFrom.published>
clondedfromrevision:<clonedFrom.revision>
<remaining id token>
```

Everything within `< ... >` brackets above is replaced with the appropriate
data. Newlines are exactly where they exist in these examples above. The
tokens are not ended with any whitespace or newlines.

In all of the above examples of tokens, the `name`, `type`, `source` fields come
from the object metadata found in the `object.json` file data at the current
revision of the object. If these fields are changed, the `uid` and `id` of the
object will change which means the file data would be copied when stored in the
object repository.

The `<root revision>` is the multihash encoding of the `git` revision that marks
the initial (root) commit of the current revision being stored in the
repository. If you somehow make a new commit that is detached from the prior
root commit, it will create a new `uid` and `id` revision (except in the case
where `source` is provided.) This actually happens on purpose when an object
is cloned with a particular flag. This is useful when cloning from a template
in order to purposefully create a unique identifier. Otherwise, all derivations
of that template may have the same `uid` and, thus, stored together.

The `<initial object.json data>` is the object.json as read at the root
revision. So, with the object's original name and type.

The `<identity uri>` is the hash of the public key of the actor that is creating
and storing the object. You will notice that this is only encoded within the
`id` tag and not the `uid` tag. When somebody clones an object, and assuming it
keeps the same `name` and `type`, the `uid` will be the same. When it is a
different actor making the fork, the `id` will now differ. This is important
when considering the storage of objects, which will be discussed later. Objects
are stored by their `uid`, so forked objects are stored in a single commit pool.

When the object is cloned, the `<clonedFrom.published>` and
`<clonedFrom.revision>` are taken directly from the object metadata. Typically
datetimes are encoded as ISO8601 strings, but technically it will use whatever
happens to be written to the field. The `revision` is the multihash that is
written to that metadata and similarly uses whatever happens to be recorded in
the field. The `<clonedFrom.published>` is found within the `published` subfield
of the `clonedFrom` field's data, which is itself part of the root document. The
`revision` is found similarly within the `clonedFrom` data.

## Histories

Objects are stored as a `git` repository. This is a commonplace technology that
can track changes to the object's metadata and data over time. It keeps track of
a tree that depicts those changes. Each change, called a "commit", has a parent
"commit" that serves as the prior state. That commit, then, has a parent, and so
on until it goes back to its original state. It is a tree and not just a linear
timeline since two separate changes could be made from the same state, which
`git` handles naturally. This is sometimes called a "fork", much like a fork in
the road stems from the same origin but goes off in two or more directions.

These 'commits' each represent a single moment of time. They are themselves
represented by a set of characters that are a hash of the metadata for that
particular commit. Objects in the system use that same hash to refer to the
commit as well. These are referred to as "revisions" and, in our system, are
represented as a multihash that looks like `5drq9jgCsmEcGEVsbQk3VDMn3xXzHv`.

An object identifier will identify the entire history of an object. This
identifier alongside a revision hash refers to a specific point in time for that
object.

## Versioning

Since revisions are long hash strings, they serve as very poor human
representations of the linear history of an object. Typically, software and data
is assigned some human-readable value that represents what version of the object
that state of the object represents. For instance, Windows has version numbers
such as "3.1", "95", "98", and "10". As once can see here, this quick example
illustrates how confusing they can be. "Windows 95" succeeds "3.1" but precedes
"Windows 10". Typically, thankfully, versions maintain a reasonable sorted order
where new versions are larger than previous versions.

A version can be created that associates such a string of characters, like
`"3.1"`, with a revision hash. Since these are arbitrary strings and they might
be reassigned at some point, the versions are signed by the actor that creates
them. If a version is revoked, it is done by creating a version with the same
tag as an existing one but with a later date. These cannot be forged since the
signature can only be reliably crafted by the original author.

These are registered and revoked upon discovery and verification against the
public verification key attached to the given author.

You can find more information about how versions are represented, sorted, and
queried within the documentation of the
[version component](../versions/README.md).

## Git Storage

As mentioned, objects are represented at a low-level by a `git` repository.

A deeper discussion of the difficulty and mechanism used to do preservation of a
`git` repository is discussed as part of the [git component](../git/README.md).

## Discovery

The object identifiers are hashes that uniquely identify the object across all
machines. With this in mind, these identifiers can be used to find specific
objects across a federation of conforming machines and servers. It is open-ended
how such a lookup is done, but there are several protocols and systems that can
perform that function.

When such a machine is found, the object data can be retrieved using the `git`
tool since the object metadata is represented as a `git` repository. The way the
object repositories are stored (see previous section) multiple histories can be
easily accommodated since the commits are stored in a single pool. There is no
single history that is deemed the official history.
