# The Caches Component

This component manages cache (key/value store/retrieve) management.

Caching is currently a WIP and when active causes misbehavior in the
overall system.

Different caching plugins can be added to be used to manage the pulling of
documents or object metadata. This is to subvert the speed of pulling data
from the network (at worst) or from the local filesystem (most common) or
from the database (at best).

Pulling from the cache layer could result in never loading the database layer
at all. Hopefully, (and caching is always a hope, isn't it?) this results in
the drastic improvement of any repeated tasks.
