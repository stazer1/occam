# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.databases.manager import table, index

@table('services')
class ServiceRecord:
  schema = {

    # Foreign Key to Object

    "internal_object_id": {
      "foreign": {
        "table": "objects",
        "key":   "id"
      }
    },

    # Attributes

    "revision": {
      "type": "string",
      "length": 128
    },

    "backend": {
      "type": "string",
      "length": 128
    },

    "environment": {
      "type": "string",
      "length": 128
    },

    "architecture": {
      "type": "string",
      "length": 128
    },

    "service": {
      "type": "string",
      "length": 128
    },

    # Constraints
    "relationship": {
      "constraint": {
        "type": "unique",
        "columns": ["internal_object_id", "environment", "architecture", "service"],
        "conflict": "replace"
      }
    },

    "backend_unique": {
      "constraint": {
        "type": "unique",
        "columns": ["service", "backend"],
        "conflict": "replace"
      }
    }
  }

@index(ServiceRecord, "environment")
class ServiceEnvironmentIndex:
  ids = ['environment']

@index(ServiceRecord, "architecture")
class ServiceArchitectureIndex:
  ids = ['architecture']

@index(ServiceRecord, "backend")
class ServiceBackendIndex:
  ids = ['backend']

@index(ServiceRecord, "service")
class ServiceServiceIndex:
  ids = ['service']
