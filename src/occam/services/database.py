# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sql

from occam.log import loggable

from occam.databases.manager import uses, datastore

@loggable
@datastore("services")
class ServiceDatabase:
  """ Manages the database interactions for managing available services.
  """

  def query(self, environment=None, architecture=None, service=None):
    """ Returns a query that selects services based on the given criteria.

    Args:
      environment: The requested environment.
      architecture: The requested architecture.
      service: The requested service.

    Returns:
      sql.Query: The SQL query that satisfies this request.
    """

    services = sql.Table('services')

    query = services.select()

    query.where = sql.Literal(True)

    if environment is not None:
      query.where = query.where & (services.environment == environment)

    if architecture is not None:
      query.where = query.where & (services.architecture == architecture)

    if service is not None:
      query.where = query.where & (services.service.ilike("%" + service + "%"))

    return query

  def retrieve(self, environment=None, architecture=None, service=None):
    """ Returns a set of ServiceRecord entries that match the given criteria.

    Args:
      environment: The requested environment.
      architecture: The requested architecture.
      service: The requested service.

    Returns:
      list: A list of ServiceRecord items that match or an empty list if none are found.
    """

    from occam.services.records.service import ServiceRecord

    # Create transaction
    session = self.database.session()

    query = self.query(environment, architecture, service)
    self.database.execute(session, query)
    
    rows = self.database.many(session, size=100)
    if rows:
      rows = [ServiceRecord(row) for row in rows]

    return rows
