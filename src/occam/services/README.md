# The Services Component

This component handles retrieval of objects that provide services for other
objects.
