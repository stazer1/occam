# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log    import Log
from occam.object import Object

from types import SimpleNamespace

from occam.commands.manager import command, option, argument

from occam.objects.manager import ObjectManager
from occam.builds.manager  import BuildManager
from occam.links.manager   import LinkManager

from occam.manager import uses

@command('builds', 'list',
  category      = 'Build Management',
  documentation = "Lists builds for the object or directories within.")
@argument("object", type="object", nargs="?", help="A buildable object")
@option("-b", "--build-id", dest   = "build_id",
                            action = "store",
                            help   = "the specific build id to target (default is most recent)")
@option("-j", "--json", dest   = "to_json",
                        action = "store_true",
                        help   = "returns result as a json document")
@option("-l", "--long", dest   = "list_details",
                        action = "store_true",
                        help   = "lists the metadata for each file in a table listing")
@uses(ObjectManager)
@uses(BuildManager)
@uses(LinkManager)
class BuildsListCommand:
  def listBuilds(self, obj):
    Log.header("Listing known builds")

    # List existing tags
    builds = self.builds.retrieveAll(obj)

    if not builds and not self.options.to_json:
      Log.write("No builds")

    if self.options.to_json:
      import json
      import base64

      ret = {
        "builds": []
      }

      for build in builds:
        buildInfo = {
          "id":        build.build_id,
          "uid":       build.build_uid,
          "revision":  build.build_revision,
          "elapsed":   build.elapsed,
          "backend":   build.backend,
          "hash":      build.build_hash,
          "host":      build.host,
          "port":      build.port,
          "identity":  build.identity_uri,
          "published": build.published.isoformat() + "Z",
        }
        if build.signed:
          buildInfo["signature"] = {
            "signed": build.signed.isoformat() + "Z",
            "data": base64.b64encode(build.signature).decode('utf-8'),
            "encoding": "base64",
            "format": "PKCS1_v1_5",
            "digest": "SHA512",
            "key": build.verify_key_id
          }
        ret["builds"].append(buildInfo)
      Log.output(json.dumps(ret))
    else:
      for build in builds:
        Log.write("%s: %s" % (build.published.isoformat(), build.build_id,))

    return 0

  def listBuildDirectory(self, obj, task, path):
    taskId = None
    if task:
      taskId = task.id

    data = self.builds.retrieveDirectoryFrom(obj, taskId, path)

    if data is None:
      Log.error("Could not find the given path.")
      return -1

    if self.options.to_json:
      import json

      if self.options.list_details:
        Log.output(json.dumps(data['items']))
      else:
        Log.output(json.dumps([item['name'] for item in data['items']]))
      return 0

    for i, item in enumerate(data['items']):
      if i > 0:
        Log.output("  ", end="", padding="")
      Log.output(item['name'], end="", padding="")

    Log.output("\n", end="", padding="")

    return 0

  def do(self):
    if self.options.object is None:
      obj = self.objects.resolve(ObjectManager.parseObjectIdentifier("+"),
                                 person = self.person)
    else:
      obj = self.objects.resolve(self.options.object,
                                 person = self.person)

    if obj is None:
      Log.error("Could not find the object.")
      return -1

    obj = self.objects.ownerFor(obj, person = self.person)

    if not self.options.object.path:
      # List existing tags
      builds = self.builds.retrieveAll(obj)
    else:
      task = None
      build = None
      build_id = self.options.build_id
      if not obj.link:
        if not build_id:
          # Get latest build instead
          build = self.builds.retrieveAll(obj)[0]
          build_id = build.build_id

        task = self.objects.retrieve(id = build_id, person = self.person)


      if obj.link is None and task and build is None:
        build = self.builds.retrieve(obj, task)

      if obj.link is None and build is None:
        Log.error("Could not find a build.")
        return -1
      elif build is not None:
        obj.revision = build.revision

      return self.listBuildDirectory(obj, task, self.options.object.path or "/")

    return self.listBuilds(obj)
