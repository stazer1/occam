# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

from occam.log    import Log
from occam.object import Object

from occam.commands.manager import command, option, argument

from occam.objects.manager   import ObjectManager
from occam.builds.manager    import BuildManager
from occam.manifests.manager import ManifestManager
from occam.links.manager     import LinkManager

from occam.manager import uses

@command('builds', 'status',
  category      = 'Build Management',
  documentation = "Retrieves build metadata or file metadata within.")
@argument("object", type="object", help="A buildable object")
@option("-b", "--build-id", dest   = "build_id",
                            action = "store",
                            help   = "the specific build id to target (default is most recent)")
@option("-j", "--json", dest   = "to_json",
                        action = "store_true",
                        help   = "returns result as a json document")
@uses(ObjectManager)   # For pulling base object information
@uses(ManifestManager) # For pulling partial tasks related to the build
@uses(BuildManager)    # For pulling the build
@uses(LinkManager)     # For pulling staged build information
class BuildsStatusCommand:
  def do(self):
    # Determine the referenced object
    obj = self.objects.resolve(self.options.object,
                               person = self.person)

    # The object must exist
    if obj is None:
      Log.error("Could not find the object.")
      return -1

    # Retrieve the explicit object owner of the object
    obj = self.objects.ownerFor(obj, person = self.person)

    task = None
    build = None
    build_id = self.options.build_id
    if not obj.link:
      if not build_id:
        # Get latest build instead
        try:
          build = self.builds.retrieveAll(obj)[0]
          build_id = build.build_id
        except:
          build = None

      task = self.objects.retrieve(id = build_id, person = self.person)

    if obj.link is None and task and build is None:
      build = self.builds.retrieve(obj, task)

    if obj.link is None and build is None:
      Log.error("Could not find a build.")
      return -1
    elif build is not None:
      obj.revision = build.revision

    if self.options.object.path:
      # If there is a file referenced within the task, view the file
      return self.viewFileStatus(obj, task, self.options.object.path or "/")

    # Otherwise pull the build task metadata
    if obj.link:
      # Retrieve the staged metadata
      return self.viewStagedBuildStatus(obj)

    # Retrieve the normal metadata for an existing published build
    return self.viewBuildStatus(obj, task)

  def viewStagedBuildStatus(self, obj):
    """ Retrieve the staged build status.
    """

    Log.header("Listing build status")

    taskInfo = self.links.taskFor(obj, build = True)

    build = json.load(self.builds.metadataFor(obj, None))

    ret = {
      "elapsed": build.get('elapsed', 0),
      "host": build.get('occam', {}).get('host', 'unknown'),
      "port": build.get('occam', {}).get('host', 'unknown'),
      "backend": taskInfo.get('backend', 'unknown'),
      "identity": self.person.identity,
      "metadata": build,
      "stage": {
        "id": obj.link
      }
    }
    
    if 'built' in build:
      ret['published'] = build['built']

    # Dump the metadata
    Log.output(json.dumps(ret))
    return 0

  def viewBuildStatus(self, obj, task):
    """ Retrieve the published object's build status.
    """

    Log.header("Listing build status")

    # Retrieve the build record associated with the given object and task
    build = self.builds.retrieve(obj, task)

    # That build must be known
    if build is None:
      Log.error("Cannot find build.")
      return -1

    # The basic metadata for the build
    ret = {
      "id":        build.build_id,
      "uid":       build.build_uid,
      "revision":  build.build_revision,
      "hash":      build.build_hash,
      "elapsed":   build.elapsed,
      "host":      build.host,
      "port":      build.port,
      "backend":   build.backend,
      "identity":  build.identity_uri,
      "published": build.published.isoformat(),
      "compress":  self.builds.compressInfo(obj.uid, build.revision, build.build_id),
      "metadata":  json.load(self.builds.metadataFor(obj, task.id))
    }

    # Retrieve any known run task for the build
    runTask, buildId, _ = self.manifests.retrievePartialTaskFor(obj, buildId = task.id,
                                                                     person = self.person)

    # If such a task is already known, report it within the metadata
    if runTask:
      ret["tasks"] = {
        "run": {
          "id": runTask.id,
          "uid": runTask.uid,
          "identity": runTask.identity,
          "revision": runTask.revision
        }
      }

      if buildId:
        ret["tasks"]["run"]["build"] = {
          "id": buildId
        }

    # Dump the metadata
    Log.output(json.dumps(ret))
    return 0

  def viewFileStatus(self, obj, task, path):
    """ Retrieve metadata for the file at the given path in the given object.
    """

    taskId = None
    if task:
      taskId = task.id

    # Retrieve the file stat info
    data = self.builds.retrieveFileStatFrom(obj, taskId, path)

    # Dump it
    Log.output(json.dumps(data))
    return 0
