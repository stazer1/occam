# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import copy
import json

from occam.log    import Log
from occam.object import Object

from occam.commands.manager import command, option, argument

from occam.objects.manager import ObjectManager
from occam.builds.write_manager  import BuildWriteManager
from occam.manifests.manager import ManifestManager, BuildRequiredError, DependencyUnresolvedError
from occam.jobs.manager      import JobManager
from occam.links.write_manager import LinkWriteManager

from occam.manager import uses

@command('builds', 'new',
  category      = 'Build Management',
  documentation = "Starts a build of the given object.")
@argument("object", type="object", nargs="?")
@option("-r", "--recursive", action  = "store_true",
                             dest    = "recursive",
                             help    = "when specified, builds all necessary objects when needed.")
@option("-i", "--interactive", action = "store_true",
                               help   = "runs in interactive mode. no stdout or stderr are generated.",
                               dest   = "interactive")
@option("-b", "--backend",     action = "store",
                               help   = "forces the use of the given backend.",
                               dest   = "backend")
@option("-f", "--force",       action = "store_true",
                               help   = "forces the build when it already exists",
                               dest   = "force")
@option("-t", "--new-task",    action = "store_true",
                               help   = "forces the creation of a new task when one already exists",
                               dest   = "new_task")
@option("-l", "--lock",        action = "append",
                               nargs  = 2,
                               help   = "locks a requirement to a particular version.",
                               dest   = "locks")
@option("-c", "--command",     action = "store",
                               help   = "overrides the given build command.")
@option("-n", "--from-clean",  dest   = "from_clean",
                               action = "store_true",
                               help   = "will build from a clean environment.")
@uses(ObjectManager)
@uses(BuildWriteManager)
@uses(ManifestManager)
@uses(LinkWriteManager)
@uses(JobManager)
class BuildsNewCommand:
  def build(self, obj, penalties, local, buildStack = []):
    info = self.objects.infoFor(obj)

    locks = {}

    if self.options.locks:
      for lock in self.options.locks:
        locks[lock[0]] = lock[1]

    task = None
    resolved = False
    while not resolved:
      try:
        depth = 0
        for buildingObj, buildingInfo in buildStack + [(obj, info,)]:
          Log.write("%sBuilding %s %s %s[@%s]" % (" " * depth, buildingInfo.get('type'), buildingInfo.get('name'), buildingObj.version + " " if buildingObj.version else "", buildingObj.revision))
          depth = depth + 1

        task = self.manifests.build(obj, id = obj.id, revision = obj.revision, local = local, penalties = copy.deepcopy(penalties), backend = self.options.backend, locks = locks, person = self.person, force = self.options.new_task)
        resolved = True
      except BuildRequiredError as e:
        if self.options.recursive:
          try:
            penalties[e.objectInfo.get('id')] = penalties.get(e.objectInfo.get('id'), [])
            penalties[e.objectInfo.get('id')].append(e.objectInfo.get('revision'))
            ret = self.build(e.requiredObject, copy.deepcopy(penalties), local = False, buildStack = buildStack + [(obj, info,)])
            if ret != 0:
              return ret
          except DependencyUnresolvedError as e:
            # Cannot resolve a dependency
            # Therefore, we cannot build this object
            # Penalize it
            Log.warning(f"Rerouting build due to recursive requirement on {e.objectInfo.get('type')} {e.objectInfo.get('name')}")
            penalties[e.objectInfo.get('id')] = penalties.get(e.objectInfo.get('id'), [])
            penalties[e.objectInfo.get('id')].append(e.objectInfo.get('revision'))
        else:
          if len(buildStack) == 0:
            raise e
          penalties[e.objectInfo.get('id')] = penalties.get(e.objectInfo.get('id'), [])
          penalties[e.objectInfo.get('id')].append(e.objectInfo.get('revision'))
          task = None

    if task is None:
      Log.error("Could not generate a build task.")
      return -2

    # Retrieve the task manifest and pass along its id, uid, revision
    taskInfo = self.objects.infoFor(task)
    originalTaskId = task.id
    taskInfo['id'] = task.id
    taskInfo['uid'] = task.uid
    taskInfo['revision'] = task.revision

    # Determine the working path
    cwd = None
    if local:
      cwd = obj.path

    # Deploy the job
    opts = self.jobs.deploy(taskInfo, revision = task.revision, local=local, person = self.person, interactive=self.options.interactive, command = self.options.command, cwd=cwd)

    # Invoke the job and wait for its completion
    report = self.jobs.execute(*opts)

    # Determine the job metadata for the resulting build paths
    elapsed = report['time']
    buildPath = report.get('paths').get(taskInfo.get('builds').get('index')).get('buildPath')
    buildLogPath = os.path.join(os.path.dirname(report.get('paths').get('task')), 'task', 'objects', str(taskInfo.get('builds').get('index')), 'stdout.0')

    import datetime
    built = datetime.datetime.utcnow()

    # Write the build metadata
    buildMetadata = report.get('machineInfo', {})
    buildMetadata['occam'] = report.get('occamInfo', {})
    buildMetadata['elapsed'] = elapsed
    buildMetadata['built'] = built.isoformat()
    buildMetadataPath = os.path.join(os.path.dirname(report.get('paths').get('task')), 'build.json')
    with open(buildMetadataPath, 'w+') as f:
      f.write(json.dumps(buildMetadata))

    # If the job failed for any reason, the build failed.
    # Do not continue nor store the build.
    if not 'runReport' in report or report['runReport'].get('status') == 'failed':
      Log.error("Build failed.")
      return -1

    # When the build is based off of the global store, we can store the build
    if not local and not self.options.command:
      # Store the build
      Log.write(f"Storing build {task.id}")
      self.builds.write.store(self.person, obj, task, buildPath = buildPath,
                                                      elapsed = elapsed,
                                                      built = built,
                                                      buildLogPath = buildLogPath,
                                                      buildMetadataPath = buildMetadataPath)

      # Compress the build
      Log.write(f"Compressing build {task.id}")
      self.builds.compress(obj.uid, obj.revision, task.id, buildPath = buildPath)

    # Remove the run path
    Log.write(f"Deleting build environment files")
    self.jobs.removeRunFolder(taskInfo, person = self.person)

    # When the build is based off of the global store, we can generate run tasks
    if not local and not self.options.command:
      # Reset the id/uid/revision to their base values
      taskInfo['id'] = task.id
      taskInfo['uid'] = task.uid
      taskInfo['revision'] = task.revision

      # Create a run task (if needed)
      if info.get('run'):
        try:
          Log.write(f"Creating a run task for {task.id}")
          self.manifests.partialTaskFor(obj, buildTask = taskInfo,
                                             person = self.person)
        except DependencyUnresolvedError as e:
          Log.warning("Cannot create a run task; a dependency is unresolvable.")

      # Look at creating run tasks (if needed) for sub-objects
      ownerInfo = self.objects.ownerInfoFor(obj)
      for included in ownerInfo.get('includes', []):
        if included.get('run'):
          Log.write(f"Creating a run task for {task.id} for {included.get('id')} ({included.get('name')})")

          # Gather the sub-object itself
          subObject = self.objects.retrieve(id = included.get('id'),
                                            revision = obj.revision,
                                            ownerId = obj.id,
                                            person = self.person)

          # Create a partial run task for running this sub-object
          try:
            self.manifests.partialTaskFor(subObject, buildTask = taskInfo,
                                                     person = self.person)
          except DependencyUnresolvedError as e:
            Log.warning("Cannot create a run task; a dependency is unresolvable.")

    return 0

  def do(self):
    if self.person is None:
      Log.error("Must be authenticated to build.")
      return -1

    local = False
    if self.options.object is None or self.options.object.id == '.' or self.options.object.link:
      local = True

    obj = self.objects.resolve(self.options.object,
                               person = self.person)

    if obj is None:
      # Cannot resolve the object
      Log.error("Could not find the object.")
      return -1

    # Get a task
    penalties = {}

    ret = -1

    Log.header("Starting build")

    # Check to see if a build already exists
    builds = self.builds.retrieveAll(obj)
    if not local and len(builds) > 0 and not self.options.force:
      Log.error("Build already exists.")
      return 1

    success = False
    lastObj = None
    while not success:
      try:
        ret = self.build(obj, penalties, local)
        success = True
      except DependencyUnresolvedError as e:
        # Try again
        if lastObj == e.objectInfo:
          # Already tried this
          if not self.options.recursive:
            if isinstance(e.objectInfo, dict):
              Log.warning(f"Need to provide a build for {e.objectInfo.get('type')} {e.objectInfo.get('name')}")
            Log.error("A dependency also needs to be built first. Use the --recursive flag to build all.")
            return -1

          # Otherwise, bail
          raise e

        lastObj = e.objectInfo
        penalties = {}
      except BuildRequiredError as e:
        if isinstance(e.objectInfo, dict):
          Log.warning(f"Need to provide a build for {e.objectInfo.get('type')} {e.objectInfo.get('name')}")
        Log.error("A dependency also needs to be built first. Use the --recursive flag to build all.")
        return -1

    return ret
