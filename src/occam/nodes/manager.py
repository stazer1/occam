# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import codecs
import os
import json

from types import SimpleNamespace

from occam.config         import Config
from occam.git_repository import GitRepository
from occam.log            import loggable
from occam.object         import Object

from occam.manager import uses, manager

from occam.backends.manager import BackendManager
from occam.network.manager  import NetworkManager
from occam.storage.manager  import StorageManager

from urllib.parse import urlparse, quote

class NetworkOptions(SimpleNamespace):
  """ A set of options that dictate how content is accessed on the network.
  """

  def __init__(self, externalToken = None, processingTag = None):
    """ Constructs an option set overriding default values when given.

    Arguments:
      externalToken (String): An access token to use for the request, if any.
      processingTag (String): Tag for selecting the right daemon to handle the
        request.
    """

    super().__init__(externalToken = externalToken,
                     processingTag = processingTag)

@loggable
@uses(NetworkManager)
@uses(StorageManager)
@uses(BackendManager)
@manager("nodes")
class NodeManager:
  """ This OCCAM manager handles the Node listing, Node discovery, and Node lookup.

  It contains methods for retrieving content from other Occam nodes using various
  protocols.
  """

  CERTIFICATE_FILENAME = 'ssl.crt'

  def __init__(self):
    """ Initialize the node manager.
    """

  def partialFromURL(self, url):
    """ Returns the host and port for the given URL.

    This method will take a fully realized URL to a resource on an OCCAM node
    and return the canonical name (domain,port) for that node. Port will be
    omitted when it is a normal port (80, 443)
    """

    if url is None:
      url = ""

    # Make sure it *is* a full URL
    url = self.urlFromPartial(url)

    # urlparse works great on real URLs
    urlparts = urlparse(url)

    # we want the host:port (unless port is 80 or 443)
    url = urlparts.hostname

    if urlparts.port and urlparts.port != 80 and urlparts.port != 443:
      url = url + "," + str(urlparts.port)

    return url

  def urlFromPartial(self, url, scheme="https"):
    """ This method will return a full URL from any partial url given as a Node URI.

    You can give the method a 'scheme' which will be applied to the URL if it
    does not have a scheme already.
    """

    if url is None:
      url = ""

    if "//" not in url:
      url = "//" + url

    urlparts = urlparse(url)
    if urlparts.scheme == "":
      url = "%s:%s" % (scheme, url)
      urlparts = urlparse(url)

    if urlparts.netloc == "":
      return None

    return url

  def parseURL(self, url):
    """ Returns a tuple of components of the given URL.

    Arguments:
      url (String): A full URL that will be parsed.

    Returns
      tuple: A set of parameters in the form [host, port, scheme] for the URL.
    """

    urlparts = urlparse(url)
    host = urlparts.netloc.split(':')[0]

    port = "80"
    if ':' in urlparts.netloc:
      port = urlparts.netloc.split(':')[1]

    elif urlparts.scheme == "https":
      port = "443"

    return [host, port, urlparts.scheme]

  def list(self):
    """ Returns the list of known nodes.
    """

    nodes = [{
      "host": node.host,
      "port": node.port,
      "protocol": node.protocol
    } for node in self.datastore.retrieveList()]

    return nodes

  def forget(self, url):
    """ Removes the given node from the list of known nodes.
    """

    url = self.urlFromPartial(url)
    host, port, scheme = self.parseURL(url)

    return self.datastore.remove(host, port)

  def search(self, url):
    """ Returns the database entry for the given node url.
    """

    url = self.urlFromPartial(url)
    host, port, scheme = self.parseURL(url)

    return self.datastore.retrieve(url, host, port, protocol = scheme)

  def retrieveInfo(self, url, options=None):
    """ Polls and retrieves the node info from the Node.
    """

    url = self.urlFromPartial(url)
    host, port, scheme = self.parseURL(url)

    # We need to determine the certificate to use SSL for commands
    cert = None

    # Default Web API URL
    nodeInfoURL = url + "/system"
    if scheme == "occam":
      # We craft an occam command to retrieve information about the node
      nodeInfoURL = url + "/system/view"
    else:
      # We will attempt to use HTTPS by default
      scheme = scheme or "https"

      # Get certificate
      # TODO: allow for daemon commands as well
      if not self.retrieveCertificate(url):
        return None

      cert = self.certificatePathFor(url)

    # Query the url and retrieve node information
    headers = self.headersFromOptions(options)

    node_info = self.network.getJSON(nodeInfoURL,
                                     scheme = scheme,
                                     suppressError = True,
                                     cert = cert,
                                     headers = headers)

    # Negotiate for HTTPS
    # TODO: Flag on the system that never allows non-HTTPS
    if node_info is None and scheme == "https":
      scheme = "http"
      try:
        headers = self.headersFromOptions(options)

        node_info = self.network.getJSON(
          nodeInfoURL, scheme='http', suppressError=True, cert=None,
          headers=headers
        )
      except:
        node_info = None

    if node_info is None:
      return None

    if not isinstance(node_info, dict):
      # We can't query node information, but that's OK
      node_info = {}

    node_info['scheme'] = scheme

    return node_info

  def pathFor(self, url):
    """ Retrieves the system path for the given node.
    """

    nodePath = self.configuration.get('path', os.path.join(Config.root(), "nodes"))
    nodePath = os.path.join(nodePath, self.partialFromURL(url))

    return nodePath

  def createPathFor(self, url):
    """ Creates a path for storing node keys and metadata.

    Retrieves the configuration root path and create a directory with it doesnt exist
    Then, join this path with the host and port of given URL and if it doesn't exist
    create a new directory with it.
    """
    nodePath = self.configuration.get('path', os.path.join(Config.root(), "nodes"))
    if not os.path.exists(nodePath):
      os.mkdir(nodePath)

    nodePath = os.path.join(nodePath, self.partialFromURL(url))
    if not os.path.exists(nodePath):
      os.mkdir(nodePath)

    return nodePath

  def retrieveCertificate(self, url, options = None):
    """ Retrieves the public key from the given node.
    """

    import ssl, socket
    urlparts = urlparse(url)
    host = urlparts.hostname

    socket.setdefaulttimeout(10)
    verified = False
    try:
      verified = ssl.get_server_certificate((host, 443,))
    except:
      verified = None

    if verified:
      return True

    url = self.urlFromPartial(url)
    url = url + "/public_key"

    # Download that key
    headers = self.headersFromOptions(options)

    response, content_type, size = self.network.get(
      url, scheme="https", doNotVerify=True, suppressError=True,
      headers=headers
    )

    if response is None:
      headers = self.headersFromOptions(options)

      response, content_type, size = self.network.get(
        url, scheme="http", doNotVerify=True, suppressError=True,
        headers=headers
      )

    if response is None:
      return None

    reader = codecs.getreader('utf-8')
    data = reader(response).read()

    if not data:
      return False

    certPath = os.path.join(self.createPathFor(url), NodeManager.CERTIFICATE_FILENAME)
    with open(certPath, 'w+') as f:
      f.write(data)

    return True

  def certificatePathFor(self, url):
    """ Gets the certificate path for the given node url.
    """

    nodePath = self.pathFor(url)
    nodePath = os.path.join(nodePath, NodeManager.CERTIFICATE_FILENAME)

    if not os.path.exists(nodePath):
      return None

    return nodePath

  def discover(self, url, untrusted=True, quiet=False):
    """ This method will lookup or append a new Node record for the given address.

    Returns None when the node cannot be reached.
    """

    url = self.urlFromPartial(url)
    host, port, scheme = self.parseURL(url)

    existing_node = self.search(url)

    NodeManager.Log.noisy("attempting to discover node at %s" % (url))

    node_info = self.retrieveInfo(url)
    if node_info is None:
      if not quiet:
        NodeManager.Log.error("No response from the node specified")
      return None

    # Look up any information we have on the Node

    # Create a record for the Node, if it doesn't exist
    if existing_node:
      # Update Node
      if not quiet:
        NodeManager.Log.write("Updating a known node")
      db_node = self.datastore.update(existing_node, node_info, host, port)
    else:
      # Create Node
      NodeManager.Log.write("Found a new node (%s)" % (url))

      db_node = self.datastore.insert(node_info, host, port)

    # TODO: add untrusted boolean to node table

    # Tell subsystems that might care
    if 'storage' in node_info:
      self.storage.discoverNode(node_info['storage'])

    # Return the Node record
    return db_node

  def urlForNode(self, node, path):
    """ Returns a URL for the given path for the given node.
    """

    scheme = node.protocol
    host = node.host
    port = node.port

    return f"{scheme}://{host}:{port}/{path}"

  def urlForObject(self, node, id, revision=None, path=None):
    """ Returns a URL for the given object id for the given node.
    """

    fullPath = id

    if not revision is None:
      fullPath = "%s/%s" % (fullPath, revision)

    if path:
      fullPath = "%s/%s" % (fullPath, path)

    return self.urlForNode(node, fullPath)

  def findIdentity(self, uri, person = None):
    """ Reports nodes that hold an identity by looking at random known nodes.
    """

    # Naive, at the moment. Just go through the node list and query.

    for node in self.datastore.retrieveList():
      NodeManager.Log.noisy("looking for identity in %s (%s)" % (node.name, node.host))

      result = self.identityFrom(node, uri)

      if result is not None:
        NodeManager.Log.noisy("found identity at %s (%s)" % (node.name, node.host))
        return [node]

    return []

  def findObject(self, id, revision=None, options=None):
    """
    When we are in a bind to find an object, we can call this method to go
    and query for which node has this object. Will return either an empty
    array when the search comes up empty, or at least one Node which has the
    requested object.
    """

    # Naive, at the moment. Just go through the node list and query.

    headers = self.headersFromOptions(options)

    for node in self.datastore.retrieveList():
      NodeManager.Log.noisy("looking in %s (%s)" % (node.name, node.host))

      result = self.statusFrom(node, id)

      if result is not None:
        NodeManager.Log.noisy("found object at %s (%s)" % (node.name, node.host))
        return [node]

    return []

  def retrieveObjectInfoFrom(self, node, id, revision=None, person=None, options=None):
    """ This will retrieve a stream of the object info from the server.
    """

    objectPath = self.urlForObject(node, id, revision=revision)

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(objectPath)

    objectInfoPath = objectPath

    headers = self.headersFromOptions(options)

    response, content_type, size = self.network.get(
      objectInfoPath, 'application/json', cert=cert, suppressError=True,
      headers=headers
    )

    if revision is None:
      # We have to pull a revision first
      # What we just got was the status
      revision = json.loads(response.read()).get('revision')
      return self.retrieveObjectInfoFrom(node, id, revision, person)

    return response

  def headersFromOptions(self, options):
    """ Converts a NetworkOptions object into a dict of header entries.
    """
    headers = {}
    if options is not None:
      if options.externalToken:
        headers['X_OCCAM_TOKEN'] = options.externalToken
      if options.processingTag:
        headers['X_OCCAM_PROCESSING_TAG'] = options.processingTag

    return headers

  def pullObjectInfoFrom(self, node, id, revision=None, options=None):
    """ This will pull the object info from the server.
    """

    if node.protocol == "occam":
      objectPath = f"objects/view/{id}"
      if revision:
        objectPath += f"@{revision}"
      objectPath = self.urlForNode(node, objectPath)
    else:
      objectPath = self.urlForObject(node, id, revision=revision)

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(objectPath)

    objectInfoPath = objectPath

    headers = self.headersFromOptions(options)

    response, content_type, size = self.network.get(
      objectInfoPath, 'application/json', cert=cert, suppressError=True,
      headers=headers
    )

    if response is None:
      NodeManager.Log.write(f"No response from {objectInfoPath}")
      return None

    # Parse the json from the stream
    reader = codecs.getreader('utf-8')
    try:
      objectInfo = json.load(reader(response))
    except json.decoder.JSONDecodeError:
      objectInfo = {}

    return objectInfo

  def pullBuildInfoFrom(self, node, id, revision, options=None):
    """ This will pull the build information known about this revision of an object from the given node.
    """

    if node.protocol == "occam":
      objectPath = f"builds/list/{id}"
      if revision:
        objectPath += f"@{revision}"
      objectPath += "?j"
      objectBuildsPath = self.urlForNode(node, objectPath)
    else:
      objectPath = self.urlForObject(node, id, revision=revision)
      objectBuildsPath = objectPath + "/builds"

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(objectPath)

    headers = self.headersFromOptions(options)

    response, content_type, size = self.network.get(url = objectBuildsPath,
                                                    accept = 'application/json',
                                                    cert = cert,
                                                    suppressError = True,
                                                    headers = headers)

    if response is None:
      NodeManager.Log.write(f"No response from {objectBuildsPath}")
      return None

    # Parse the json from the stream
    reader = codecs.getreader('utf-8')
    try:
      objectBuilds = json.load(reader(response))
    except json.decoder.JSONDecodeError:
      objectBuilds = {}

    return objectBuilds

  def manifestsFrom(self, node, id, revision, phase="run", options=None):
    """ Pulls the manifest listing for the given object.
    """

    # Craft URL to retrieve version information
    if node.protocol == "occam":
      objectPath = f"manifests/list/{id}@{revision}?p={phase}"
      objectManifestsPath = self.urlForNode(node, objectPath)
    else:
      objectPath = self.urlForObject(node, id)
      objectManifestsPath = f"{objectPath}/{phase}/tasks"

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(objectPath)

    NodeManager.Log.noisy(f"Pulling manifest info for {id}@{revision} from "
                          f"{node.host} using path {objectManifestsPath}")

    headers = self.headersFromOptions(options)

    response = self.network.getJSON(url = objectManifestsPath,
                                    cert = cert,
                                    suppressError = True,
                                    headers = headers)

    if response is None:
      NodeManager.Log.warning(f"Failed to pull manifest info for {id}@{revision} from {node.host}")
      return {}

    return response

  def pullVersionInfoFrom(self, node, id, options=None):
    """ This will pull the version information known about this object from the given node.
    """

    # Craft URL to retrieve version information
    if node.protocol == "occam":
      objectPath = f"versions/list/{id}?j"
      objectVersionsPath = self.urlForNode(node, objectPath)
    else:
      objectPath = self.urlForObject(node, id)
      objectVersionsPath = f"{objectPath}/versions"

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(objectPath)

    NodeManager.Log.noisy(f"Pulling version info for {id} from "
                          f"{node.host} using path {objectVersionsPath}")

    headers = self.headersFromOptions(options)

    response = self.network.getJSON(url = objectVersionsPath,
                                    cert = cert,
                                    suppressError = True,
                                    headers = headers)

    if response is None:
      NodeManager.Log.warning(f"Failed to pull version info for {id} from {node.host}")
      return {}

    return response

  def repositoryFrom(self, node, id, revision=None, options=None):
    if node.protocol == "occam":
      objectPath = self.urlForObject(node, id, revision=revision)
      objectPath = objectPath.replace("occam://", "http://")
    else:
      objectPath = self.urlForObject(node, id, revision=revision)

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(objectPath)

    NodeManager.Log.noisy(f"Cloning {id} at revision {revision} from {objectPath}")

    # Clone a temporary copy of this object
    externalToken = None
    if options and options.externalToken:
      externalToken = options.externalToken

    return GitRepository(objectPath, cert=cert, externalToken=externalToken)

  def buildLogFrom(self, node, id, revision, buildId, destination, options=None):
    """ Pull the build log from the given node to the given destination.
    """

    # Get the URL for the build
    if node.protocol == "occam":
      objectPath = f"builds/view/{id}"
      if revision:
        objectPath += f"@{revision}"
      buildLogPath = objectPath + f"?b={buildId}"
      buildLogPath = self.urlForNode(node, buildLogPath)
    else:
      buildLogPath = self.urlForObject(node, id, revision=revision)
      buildLogPath = buildLogPath + "/builds/" + buildId + "/log"

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(buildLogPath)

    # Pull down an archive of the build
    headers = self.headersFromOptions(options)

    reader, content_type, size = self.network.get(
      buildLogPath, 'application/octet-stream', cert=cert, suppressError=True,
      headers=headers
    )

    logfile = os.path.join(destination, "log")

    if reader is None:
      NodeManager.Log.write(f"No response from {buildLogPath}")
      return None

    bytesTotal = size
    bytesSoFar = 0
    bytesRead = 1
    with open(logfile, 'wb+') as f:
      while bytesRead > 0:
        chunk = reader.read(8196*4)
        bytesRead = len(chunk)
        bytesSoFar += bytesRead
        #NodeManager.Log.write(f"Read {bytesRead} bytes of log...")
        f.write(chunk)

    return logfile

  def buildMetadataFrom(self, node, id, revision, buildId, destination, options=None):
    """ Pull the build metadata from the given node to the given destination.
    """

    # Get the URL for the build
    if node.protocol == "occam":
      objectPath = f"builds/view/{id}"
      if revision:
        objectPath += f"@{revision}"
      buildLogPath = objectPath + f"?b={buildId}&t=metadata"
      buildLogPath = self.urlForNode(node, buildLogPath)
    else:
      buildLogPath = self.urlForObject(node, id, revision=revision)
      buildLogPath = buildLogPath + "/builds/" + buildId + "/metadata"

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(buildLogPath)

    # Pull down an archive of the build
    headers = self.headersFromOptions(options)

    reader, content_type, size = self.network.get(
      buildLogPath, 'application/octet-stream', cert=cert, suppressError=True,
      headers=headers
    )

    logfile = os.path.join(destination, "metadata")

    if reader is None:
      NodeManager.Log.write(f"No response from {buildLogPath}")
      return None

    bytesTotal = size
    bytesSoFar = 0
    bytesRead = 1
    with open(logfile, 'wb+') as f:
      while bytesRead > 0:
        chunk = reader.read(8196*4)
        bytesRead = len(chunk)
        bytesSoFar += bytesRead
        #NodeManager.Log.write(f"Read {bytesRead} bytes of log...")
        f.write(chunk)

    return logfile

  def buildFrom(self, node, id, revision, buildId, identity, destination, options=None):
    """ Pull the given build for the given object from the given node.

    Arguments:
      node (NodeRecord): The node to pull it from.
      id (string): The id of the object.
      revision (string): The revision of the object.
      buildId (string): The id of the task manifest for the build.
      identity (string): The identity of the actor that owns the object.
      destination (string): The path to place the build. The build itself goes
                            into an 'unpack' directory. The compressed file is
                            found in the destination path as 'build.tar.xz' or
                            'build.zip' depending on the compression used.

    Returns:
      dict: Information about the pulled build. The 'buildPath' contains the
            directory with the unpacked files. The 'buildPackagePath' contains
            the path to the compressed package.
    """

    # Get the URL for the build
    if node.protocol == "occam":
      objectPath = f"builds/view/{id}"
      if revision:
        objectPath += f"@{revision}"
      buildPath = objectPath + f"/{buildId}{quote('/', safe='')}?compress=txz"
      buildPath = self.urlForNode(node, buildPath)
    else:
      buildPath = self.urlForObject(node, id, revision=revision)
      buildPath = buildPath + "/builds/" + buildId + "/files/"

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(buildPath)

    # Pull down an archive of the build
    headers = self.headersFromOptions(options)

    reader, content_type, size = self.network.get(
      buildPath, 'application/x-tar+xz', cert=cert, suppressError=True,
      headers=headers
    )

    if reader is None:
      NodeManager.Log.write(f"No response from {buildPath}")
      return None

    # Extract it as we download it to a temporary space
    import tarfile
    import zipfile
    import tempfile
    import time

    tmpfile = os.path.join(destination, "build.tar.xz")

    bytesTotal = size
    bytesSoFar = 0
    bytesRead = 1

    NodeManager.Log.writePercentage("Downloading build (size: %s)" % (bytesTotal))

    with open(tmpfile, 'wb+') as f:
      while bytesRead > 0:
        chunk = reader.read(8196*4)
        bytesRead = len(chunk)
        bytesSoFar += bytesRead
        f.write(chunk)
        if bytesTotal > 0:
          NodeManager.Log.updatePercentage((bytesSoFar / bytesTotal) * 100)
        else:
          NodeManager.Log.updatePercentage(100)

    # What the HECK, ZipFile does not extract with permissions
    # So executables don't have execute bits... what THE HECK.
    # INCONCEIVABLE!! (That reminds me, Gina Gershon is a VERY
    # underrated actress imho... I've been watching Riverdale
    # and Brooklyn 99. I know, I know... but she was also good
    # in Elementary! Also, Lucy Liu's on set fashion person or
    # whatever they are called is amazing.)
    # Advised from https://stackoverflow.com/questions/39296101/python-zipfile-removes-execute-permissions-from-binaries
    # This feels flimsy, but, yanno.
    # Like, what happens when _extract_member changes? Yikes!
    class ZipFileX(zipfile.ZipFile):
      """ Custom ZipFile class handling file permissions.
      """
      def _extract_member(self, member, targetpath, pwd):
        if not isinstance(member, zipfile.ZipInfo):
          member = self.getinfo(member)

        # Normally extract
        targetpath = super()._extract_member(member, targetpath, pwd)

        # Handle symlinks
        # With help from: https://mail.python.org/pipermail/python-list/2005-June/322179.html
        if (member.external_attr & 0xA0000000) == 0xA0000000:
          # In this case, the newly created file will contain the target
          # path of the symbolic link
          with open(targetpath, 'rb') as f:
            linkpath = f.read()
          os.remove(targetpath)
          os.symlink(linkpath, targetpath)
        else:
          attr = member.external_attr >> 16
          if attr != 0:
            os.chmod(targetpath, attr)

        # Preserve modification time
        date_time = time.mktime(member.date_time + (0, 0, -1))
        os.utime(targetpath, (date_time, date_time), follow_symlinks = False)
        return targetpath

    buildPath = os.path.join(destination, 'unpack')
    if not os.path.exists(buildPath):
      os.mkdir(buildPath)

    # IF IT IS A ZIP FILE
    #with ZipFileX(tmpfile, 'r') as zipStream:
    #  zipStream.extractall(destination)

    # OTHERWISE, TAR FILE
    with tarfile.open(tmpfile) as tf:
      tf.extractall(buildPath)

    return {
        "buildPath": buildPath,
        "buildPackagePath": tmpfile
    }

  def taskFor(self, fromEnvironment=None, fromArchitecture=None,
                    toEnvironment=None, toArchitecture=None,
                    toBackend=None, obj=None):
    """ Queries for a task from known OCCAM nodes.

    This will ask known nodes how it would construct a VM for the given object
    for the given backend. It returns a VM object with enough metadata to
    discover required resources to replicate the VM on this node.
    """

    # Naive, at the moment. Just go through the node list and query.

    for node in self.datastore.retrieveList():
      NodeManager.Log.noisy("looking in %s (%s)" % (node.name, node.host))

      return self.taskForFrom(node, fromEnvironment, fromArchitecture, toEnvironment, toArchitecture, toBackend=toBackend, obj=obj)

    return None

  def taskForFrom(self, node, fromEnvironment=None, fromArchitecture=None,
                              toEnvironment=None, toArchitecture=None,
                              toBackend=None, obj=None, options=None):
    """ This will ask the given node how it would construct a VM for the given object.
    
    It does so for the given backend. It returns a VM object with enough metadata
    to discover required resources to replicate the VM on this node.
    """

    # TODO: occam api protocol
    if obj is None:
      taskPath = self.urlForNode(node,
        "task?fromEnvironment=%s&fromArchitecture=%s&toEnvironment=%s&toArchitecture=%s" % (
          fromEnvironment, fromArchitecture, toEnvironment, toArchitecture
        )
      )
    else:
      # TODO: pass along revision and env/arch goals?
      taskPath = self.urlForNode(node,
        "task?fromObject=%s" % (
          obj.objectInfo().get('id')
        )
      )

    if not toBackend is None:
      taskPath = "%s&toBackend=%s" % (taskPath, toBackend)

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(taskPath)

    headers = self.headersFromOptions(options)

    return self.network.getJSON(
      taskPath, "application/json", cert=cert, suppressError=True,
      headers=headers
    )

  def retrieveDirectoryFrom(self, node, id, revision = None, path = "", person = None, options=None):
    """ Retrieves the directory metadata for the given path from the given node.
    """

    if node.protocol == "occam":
      url = f"objects/list/{id}"
      if revision:
        url += f"@{revision}"
      url += quote(path or "/", safe = '')
      url += "?j"
    else:
      token = id
      if revision:
        token = "%s/%s" % (token, revision)
      url = self.urlForNode(node, f"{token}/files/{path or ''}")

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(url)

    headers = self.headersFromOptions(options)

    items = self.network.getJSON(
      url, "application/json", cert=cert, suppressError=True,
      headers=headers
    )

    if not items:
      return None

    return items

  def retrieveFileStatFrom(self, node, id, revision = None, path = "object.json", person = None, options=None):
    """ Retrieves the given file from the given node.
    """

    if node.protocol == "occam":
      url = f"objects/status/{id}"
      if revision:
        url += f"@{revision}"
      url += quote(path or "/object.json", safe = '')
      url += "?j"
      url = self.urlForNode(node, url)
    else:
      token = id
      if revision:
        token = "%s/%s" % (token, revision)
      url = self.urlForNode(node, f"{token}/files/{path or 'object.json'}")

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(url)

    headers = self.headersFromOptions(options)

    return self.network.getJSON(
      url, "application/json", cert=cert, suppressError=True,
      headers=headers
    )

  def retrieveJSONFrom(self, node, id, revision=None, path="object.json", person=None, options=None):
    """ Retrieves the JSON content from the given object on the given node.
    """

    return self.retrieveFileFrom(node, id, revision, path, person, options, json = True)

  def retrieveFileFrom(self, node, id, revision=None, path="object.json", person=None, options=None, json=False):
    """ Retrieves the given file from the given node.
    """

    if path is None:
      path = "/object.json"

    if not path.startswith("/"):
      path = "/" + path

    if node.protocol == "occam":
      url = f"objects/view/{id}"
      if revision:
        url += f"@{revision}"
      url += quote(path or "object.json", safe = '')
      url = self.urlForNode(node, url)
    else:
      token = id
      if revision:
        token = "%s/%s" % (token, revision)
      url = self.urlForNode(node, f"{token}/raw{path}")

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(url)

    headers = self.headersFromOptions(options)

    if json:
      response = self.network.getJSON(url, cert = cert,
                                           suppressError = True,
                                           headers = headers)
    else:
      response, content_type, size = self.network.get(url, cert = cert,
                                                           suppressError = True,
                                                           headers = headers)

    return response

  def historyFrom(self, node, id, revision = None, person = None, options=None):
    """ Returns the object status for the given object at the given node.
    """

    if node.protocol == "occam":
      url = f"objects/history/{id}"
      if revision:
        url += f"@{revision}"
      url += "?j"
    else:
      token = id
      if revision:
        token = "%s/%s" % (token, revision)
      url = self.urlForNode(node, f"{token}/history")

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(url)

    headers = self.headersFromOptions(options)

    return self.network.getJSON(
      url, "application/json", cert=cert, suppressError=True,
      headers=headers
    )

  def logForJob(self, node, jobId, logType=None, start=0, tail=False, person=None, options=None):
    """ Gets the log for a job on a remote node.

    Arguments:
      node (NodeRecord): The node to retrieve the information from.
      jobId (String): The job identifier for the local job.
      logType (str): The type of log. May be None for standard out, 'events' for
                     the event log, 'network' for the network info, or 'task'
                     for the task manifest.
      tail (bool): Whether or not we want it to block until new log content.
      start (number): The starting position of the log in bytes.
      person (Object): The actor making the request.
      options (NetworkOptions): Flags that dictate how to make the request.

    Returns:
      String: The contents of the log.
    """

    # The basic arguments
    queryParams = f"?s={start}"

    # Whether or not we are retrieving the whole log
    if tail:
      queryParams += "&t"

    # Negotiating the log type
    if logType == 'events':
      queryParams += "&e"
    elif logType == 'task':
      queryParams += "&k"
    elif logType == 'network':
      queryParams += "&n"

    # FIXME: Must use uid and revision so we can verify the person requesting
    # the log information has permissions for it. (wilkie: why? the access toke
    # is good enough.)
    urlPath = f"jobs/view/{jobId}{queryParams}"
    if node.protocol == "occam":
      url = self.urlForNode(node, urlPath)
    else:
      # TODO: The front-end web API needs a general job query API route.
      raise NotImplementedError("Tried to view the log for a job on a node whose protocol is HTTP.")

    headers = None

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(url)

    if node.protocol == "occam" and tail:
      # We need an open socket, so we special case it here :(
      return self.network.postOccam(url, None, accept = "application/octet-stream",
                                               headers = headers,
                                               promoted = True)

    return self.network.get(url, accept = "application/octet-stream",
                                 cert = cert,
                                 suppressError = True,
                                 headers = headers)

  def pingForJob(self, node, remoteJobId, fromUri, status="queued", jobId=None, person=None, token=None, options=None):
    """ Pings the node that a job has a changed status.

    Arguments:
      node (NodeRecord): The node to retrieve the information from.
      remoteJobId (str): The job identifier on the node we are pinging.
      status (str): The new job status.
      jobId (str): The job identifier for the local job.
      person (Object): The actor making the request.
      token (str): The token provided with the ping allowing access this local node.
      options (NetworkOptions): Flags that dictate how to make the request.

    Returns:
      dict: The response from that coordinator.
    """

    from urllib.parse import quote
    urlPath = f"jobs/ping/{remoteJobId}/{quote(fromUri, safe='')}/{status}"
    if node.protocol == "occam":
      queryParams = ""

      # Give the other node the token it will need to pull objects from us.
      if options and options.externalToken:
        queryParams = f"?T={options.externalToken}"

      if token:
        queryParams = f"{queryParams}{'&' if queryParams else '?'}e={token}"

      # If we have a local job id the other node can pull information from, give that as well.
      if jobId:
        queryParams = f"{queryParams}{'&' if queryParams else '?'}r={jobId}"

      urlPath = f"{urlPath}{queryParams}"
      url = self.urlForNode(node, urlPath)
    else:
      # TODO: Enable this on the web frontend as well.
      #url = self.urlForNode(node, f"jobs/{remoteJobId}/ping?remoteJobId={jobId}")
      raise NotImplementedError("Tried to ping for a job on a node whose protocol is HTTP.")

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(url)

    headers = None

    return self.network.getJSON(url, accept = "application/json",
                                     cert = cert,
                                     suppressError = True,
                                     headers = headers)

  def sendToJob(self, node, remoteJobId, data, person=None, options=None):
    """ Pings the node that a job has a changed status.

    Arguments:
      node (NodeRecord): The node to which we are sending data.
      remoteJobId (str): The job identifier for the local job that will get the data.
      data (bytes): The data.
      person (Object): The actor making the request.
      options (NetworkOptions): Flags that dictate how to make the request.

    Returns:
      dict: The response from that coordinator.
    """

    from urllib.parse import quote
    urlPath = f"jobs/send/{remoteJobId}"
    if node.protocol == "occam":
      url = self.urlForNode(node, urlPath)
    else:
      # TODO: Enable this on the web frontend as well.
      #url = self.urlForNode(node, urlPath)
      raise NotImplementedError("Tried to send data to a job on a node whose protocol is HTTP.")

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(url)

    headers = self.headersFromOptions(options)

    self.network.post(url, data, cert = cert,
                                 suppressError = True,
                                 headers = headers)

    return True

  def jobStatusFrom(self, node, jobId, person=None, options=None):
    """ Returns the job status from the given node.

    Arguments:
      node (NodeRecord): The node to retrieve the information from.
      jobId (String): The job identifier.
      person (Object): The actor making the request.
      options (NetworkOptions): Flags that dictate how to make the request.

    Returns:
      dict: The job information or None if it could not be found.
    """

    if node.protocol == "occam":
      url = self.urlForNode(node, f"jobs/status/{jobId}?j")
    else:
      url = self.urlForNode(node, f"jobs/{jobId}")

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(url)

    headers = self.headersFromOptions(options)

    return self.network.getJSON(url, accept = "application/json",
                                     cert = cert,
                                     suppressError = True,
                                     headers = headers)

  def statusFrom(self, node, id, revision=None, person=None, options=None):
    """ Returns the object status for the given object at the given node.
    """

    token = id
    if revision:
      if node.protocol == "occam":
        token = f"{token}@{revision}"
      else:
        token = f"{token}/{revision}"

    if node.protocol == "occam":
      statusPath = self.urlForNode(node, f"objects/status/{token}?j")
    else:
      statusPath = self.urlForNode(node, f"{token}/status")

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(statusPath)

    headers = self.headersFromOptions(options)

    try:
      return self.network.getJSON(statusPath, accept = "application/json",
                                              cert = cert, 
                                              suppressError = True,
                                              headers = headers)
    except ConnectionResetError:
      raise
    except Exception as e:
      NodeManager.Log.error(f"Unable to get status from {statusPath}: {e}")
      return None

  def identityFrom(self, node, uri, options=None):
    """ Retrieve identity information from the given node.

    Arguments:
      node (NodeRecord): The node to query.
      uri (str): The identity URI to query.
      options (NetworkOptions): Flags that dictate how to make the request.

    Returns:
      dict: The identity metadata.
    """

    if node.protocol == "occam":
      identityPath = self.urlForNode(node, f"keys/view/{uri}?a")
    else:
      identityPath = self.urlForNode(node, f"identities/{uri}")

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(identityPath)

    headers = self.headersFromOptions(options)

    identityInfo = self.network.getJSON(identityPath, accept = 'application/json',
                                                      cert = cert,
                                                      suppressError = True,
                                                      headers = headers)

    return identityInfo

  def personFrom(self, node, uri, options=None):
    """ Retrieve the person associated with the given identity.
    """

    personPath = self.urlForNode(node, f"people/{id}")

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(personPath)

    headers = self.headersFromOptions(options)

    personInfo = self.network.getJSON(
      personPath, 'application/json', cert=cert, suppressError=True,
      headers=headers
    )

    return personInfo

  def viewersFor(self, node, type, subtype, person=None, options=None):
    """ Retrieve viewers information for the given node given the type and subtype of the viewer
    """

    if node.protocol == "occam":
      url = f"objects/viewers"
      url += "?type={quote(type, safe='')}&subtype={quote(subtype, safe='')}"
    else:
      url = self.urlForNode(node, f"viewers?type={type}&subtype={subtype}")

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(url)

    headers = self.headersFromOptions(options)

    return self.network.getJSON(
      url, "application/json", cert=cert, suppressError=True,
      headers=headers
    )

  def providersFor(self, node, environment, architecture, person=None, options=None):
    """ Retrieve provider information for the given node given the environment and architecture requested.
    """

    if node.protocol == "occam":
      url = f"objects/providers"
      url += "?environment={quote(environment, safe='')}"
      url += "&architecture={quote(architecture, safe='')}"
    else:
      url = self.urlForNode(node, "providers?environment={environment}&architecture={architecture}")

    # Retrieve the certificate file path for the node referenced by this url
    cert = self.certificatePathFor(url)

    headers = self.headersFromOptions(options)

    return self.network.getJSON(
      url, "application/json", cert=cert, suppressError=True,
      headers=headers
    )
