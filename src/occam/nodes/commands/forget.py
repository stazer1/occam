# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.manager import uses

from occam.commands.manager import command, option, argument
from occam.nodes.manager    import NodeManager

@command('nodes', 'forget',
  documentation = "Forget an Occam node.")
@argument("url", action = "store")
@uses(NodeManager)
class ForgetCommand:
  """
  This command will remove an OCCAM node from the list of nodes used
  to discover objects.

  Usage:

  occam forget <HOST>
  occam forget <HOST>:<PORT>

  Examples:

  Removal by hostname:

  $ occam forget occam.cs.pitt.edu

  Removal by ip address and port:

  $ occam forget 10.0.0.1:9292
  """

  def do(self):
    """
    Perform the command.
    """

    url = self.options.url

    if self.nodes.forget(url):
      Log.done("Successfully forgot %s" % (url))
    else:
      Log.done("%s was not in the list of known nodes" % (url))

    return 0
