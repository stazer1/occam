# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import os
import subprocess
import datetime
import json

from occam.log     import loggable

from occam.manager import uses

from occam.jobs.manager      import JobManager
from occam.databases.manager import DatabaseManager
from occam.caches.manager    import CacheManager
from occam.accounts.manager  import AccountManager

@loggable
@uses(JobManager)
@uses(DatabaseManager)
@uses(CacheManager)
@uses(AccountManager)
class JobDaemon:
  """ The JobDaemon keeps track of running jobs as long as there are jobs.

  When all jobs are done, the JobDaemon can finally sleep. When a job is queued,
  the JobDaemon will run once more. This daemon simply dispatches work when it
  can (or queue it in an auxiliary scheduler that already exists on the machine)
  and keeps track of those running tasks.

  When a job completes, the daemon will respond by cleaning up the task and
  recording output, etc. If the job fails, the daemon will record this as well.

  Internally, job dependencies are kept and jobs are dispatched and scheduled
  such that this ordering is preserved.
  """

  def __init__(self):
    """ Initializes the job daemon.
    """
    self.initializeInfoDirectory()

  def path(self):
    """ Returns the path to the daemon metadata.
    """

    from occam.config import Config

    basePath = Config.root()
    config = Config.load()
    config = config.get("jobs", {})
    basePath = config.get("path", os.path.join(basePath, "job-daemon"))
    return basePath

  @staticmethod
  def popen(command, stdout=None, stdin=None, stderr=None, cwd=None, env=None, start_new_session=False):
    import subprocess

    if stdout is None:
      stdout = subprocess.DEVNULL

    if stderr is None:
      stderr = subprocess.DEVNULL

    return subprocess.Popen(command, stdout=stdout,
                                     stdin=stdin,
                                     stderr=stderr,
                                     cwd=cwd,
                                     env=env,
                                     start_new_session=start_new_session)

  def initializeJob(self, job, token):
    """ Run job initialization for the given job.

    Arguments:
      job (JobRecord): The job to initialize.

    Returns:
      bool: Whether or not initialization succeeded.
    """

    tokenArgs = []
    if token:
      tokenArgs = ['-T', token]

    tagArgs = []
    if job.initialize_tag:
      tagArgs = [job.initialize_tag]

    occamCommand = self.occamBin()
    command = [occamCommand, job.initialize or 'jobs', 'job-init', str(job.id)] + tagArgs + tokenArgs
    print("[%s] job-initialize %s" % (datetime.datetime.today(), command))
    p = JobDaemon.popen(command, cwd=self.path())
    p.communicate()

    # If initialization fails, mark the job as failed.
    return p.returncode == 0

  def finalizeJob(self, job, token):
    """ Initiates job finalization if a finalize step exists for the
    given job.
    """

    tokenArgs = []
    if token:
      tokenArgs = ['-T', token]

    tagArgs = []
    if job.finalize_tag:
      tagArgs = [job.finalize_tag]

    occamCommand = self.occamBin()
    command = [occamCommand, job.finalize or 'jobs', 'job-done', str(job.id)] + tagArgs + tokenArgs
    JobManager.Log.write(f"job-finalize {command}")

    p = JobDaemon.popen(command, cwd=self.path())
    p.communicate()

    return p.returncode == 0

  def idle(self):
    """ Retrieve a job record and schedule it.
    """

    print("[%s] started daemon" % (datetime.datetime.today()))

    while(self.run()):
      print("[%s] ran" % (datetime.datetime.today()))

    print("[%s] finished queue" % (datetime.datetime.today()))

  def initializeInfoDirectory(self):
    """ Initializes the directory for the metadata for running daemons.
    """

    path = self.path()

    if not os.path.exists(path):
      os.mkdir(path)

  def run(self):

    # Look for jobs, quit if there aren't any
    # Only jobs whose scheduler is "occam" will be pulled
    job = self.jobs.pull("occam")

    if job is None:
      # Exit the loop when we can't find a job to run
      return False

    print("[%s] deploying job %s" % (datetime.datetime.today(), job.id))

    # Spawn a process to run this task through current scheduler extension
    # TODO: do through extension
    # If the scheduler is not "occam", will not launched through job daemon.
    # Queue command will call "occam jobs schedule" to schedule the job through
    # the specified scheduler

    stdinPath = os.path.join(self.path(), "%s-stdin" % (job.id))

    # Create the stdin pipe
    if os.path.exists(stdinPath):
      os.remove(stdinPath)
    os.mkfifo(stdinPath)

    stdinFile = os.open(stdinPath, os.O_NONBLOCK | os.O_TRUNC | os.O_RDWR)

    # Get the acting person
    token = None
    person = self.jobs.personFor(job)

    if person:
      account = self.accounts.retrieveAccount(username = person.name)
      token = self.accounts.generateToken(account, person, "person")

    if not self.initializeJob(job, token):
      job.status = "failed"
      self.jobs.failed(job)

      print(
        "[%s] failed to initialize %s" % (
          datetime.datetime.today(), job.initialize
        )
      )
      return True

    tokenArgs = []
    if token:
      tokenArgs = ['-T', token]

    command = None
    occamCommand = self.occamBin()
    command = [occamCommand, 'jobs', 'run', '--log', str(job.id)] + tokenArgs
    if job.interactive:
      command.append('-i')

    print("[%s] running command %s" % (datetime.datetime.today(), command) )
    p = JobDaemon.popen(command, stdin=stdinFile, cwd=self.path(), start_new_session=True)

    # Write the metadata for the job we just launched.
    self.jobs.writeMetadataFor(job.id, pid=p.pid, args=p.args)

    print("[%s] process %s" % (datetime.datetime.today(), p.pid))

    p.communicate()
    if p.returncode != 0:
      print("[%s] Job failed: %d" % (datetime.datetime.today(), job.id))

      # Force the failure of the job due to the return code
      job.status = "failed"
      self.jobs.failed(job)
    else:
      print("[%s] Job completed: %d" % (datetime.datetime.today(), job.id))

    if self.finalizeJob(job, token):
      print("[%s] job-finalized" % (datetime.datetime.today()))
    else:
      print("[%s] failed to finalize job" % (datetime.datetime.today()))

    return True

  def occamBin(self):
    """ Returns the command to invoke OCCAM.
    """

    return os.getenv("OCCAM_BIN") or "occam"

  def spawnWorker(self):
    """ Spawns a background job worker as a separate process.
    """

    occamCommand = self.occamBin()

    env = os.environ

    command = [occamCommand, "jobs", "daemon", "-b"]

    # We must use stdin otherwise the occam command's stdin will be None.
    # The command manager expects to be able to set stdin = sys.stdin.buffer.
    p = JobDaemon.popen(command, cwd=self.path(), env=env, stdin=subprocess.PIPE)
    p.communicate()

    if p.returncode != 0:
      self.Log.error("[%s] Failed to spawn worker" % (datetime.datetime.today()))
      return p.returncode

    return 0

  def start(self):
    """ Starts the job daemon process.
    """

    from occam.daemon.daemon import Daemon

    path = self.path()

    try:
      pid = os.fork()
    except OSError as e:
      raise(Exception, "%s [%d]" % (e.strerror, e.errno))

    if (pid == 0):
      # The new child process
      jobDaemon = Daemon(workingDir=path, logFile="job-daemon", pty=True)
      jobDaemon.create()

      # Force reinitializing a database connection
      self.database.disconnect()

      # Force reinitializing a cache connection
      self.cache.disconnect()

      # Run the daemon
      self.idle()

      # Exit the process
      exit(0)

    return pid


  def stop(self):
    pass

  def restart(self):
    self.stop()
    self.start()
