# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.datetime import Datetime

from occam.log import Log

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.discover.manager     import DiscoverManager
from occam.jobs.manager         import JobManager
from occam.nodes.manager        import NodeManager, NetworkOptions
from occam.objects.manager      import ObjectManager
from occam.permissions.manager  import PermissionManager
from occam.builds.write_manager import BuildWriteManager

from occam.config import Config
from occam.connection_logic import retryOnReset

import datetime
import json
import os

@command('jobs', 'ping',
    category = "Job Management",
    documentation = "Reports a completed job on another node.")
@argument("job_id", action = "store",
                    help = "the job id to update")
@argument("from_uri", action = "store",
                      help = "the URI of the node signaling this node")
@argument("status", action = "store",
                    help = "the new job status")
@option("-e", "--external-token", action = "store",
                                  dest = "external_token",
                                  help = "the access token to use")
@option("-r", "--remote-job-id", action = "store",
                                 dest = "remote_job_id",
                                 help = "the job id on the signaling node")
@uses(JobManager)
@uses(ObjectManager)
@uses(DiscoverManager)
@uses(BuildWriteManager)
@uses(NodeManager)
@uses(PermissionManager)
class PingCommand:
  """ A remotely initiated command updating the status of a local job running elsewhere.
  """

  def do(self):
    # The ping command is not initiated directly by a client, but rather by a
    # compute node. Since network failures here can't be retried by the compute
    # node, we have to retry when we detect a connection reset.
    #
    # Since the changes implementing a connection between daemons introduced a
    # window where connections could be accepted and dropped before the first
    # command gets through, we especially need to retry in the event of a
    # connection drop.

    job = self.jobs.jobFromId(self.options.job_id)
    if not job:
      Log.error("Cannot find the local job.")
      return 1

    # Acquire the local authority for this job.
    personForJob = self.jobs.personFor(job)

    # It is possible the job status changed before a remote job ID was
    # generated. Because we don't have the job ID on the remote node, we can't
    # update anything other than the local job's status.
    if self.options.remote_job_id is None:
      self.jobs.updateJob(id = job.id, status = self.options.status)
      Log.output(json.dumps({}))
      return 0

    # Get the status of the completed job. It may have completed in a success
    # or failure.
    discover = lambda: self.nodes.discover(self.options.from_uri)
    node = retryOnReset(discover)

    statusFrom = lambda: self.nodes.jobStatusFrom(node,
                                                  self.options.remote_job_id)
    statusInfo = retryOnReset(statusFrom)

    # Set the status of the main job to be the same as the node local job.
    startTime = statusInfo.get('startTime', None)
    if startTime:
      startTime = Datetime.from8601(startTime)

    finishTime = statusInfo.get('finishTime', None)
    if finishTime:
      finishTime = Datetime.from8601(finishTime)

    # The scheduler 'identifier' will be the URI of that remote job
    schedulerId = f"{self.options.from_uri}/jobs/{self.options.remote_job_id}"

    # Update the job record to match the node local job information we just
    # gathered.
    #
    # TODO: Where do we change the output's person owner?
    #
    job.status = self.options.status
    self.jobs.updateJob(id = job.id,
                        status = self.options.status,
                        startTime = startTime,
                        finishTime = finishTime,
                        schedulerId = schedulerId)

    # Pull all of the results and stitch them into the objects of their origin
    # as if they ran locally.
    outputs = statusInfo.get('outputs', [])

    networkOptions = None
    if self.options.external_token:
      networkOptions = NetworkOptions(externalToken = self.options.external_token)

    # Pull all output objects back to this object store
    for wireIndex, wireOutputs in enumerate(statusInfo.get('outputs', [])):
      for itemIndex, outputInfo in enumerate(wireOutputs):
        outputObject = self.objects.retrieve(id = outputInfo['id'],
                                             revision = outputInfo['revision'],
                                             person = personForJob)

        if outputObject is None:
          id = outputInfo['id']
          revision = outputInfo['revision']
          discover = lambda: self.discover.discover(id,
                                                    revision = revision,
                                                    networkOptions = networkOptions,
                                                    nodes = [node])
          outputObject = retryOnReset(discover)

        if not outputObject:
          Log.error(f"Failed to get output object {outputInfo['id']}")
          return -1

        self.jobs.createJobOutput(job, outputObject, wireIndex, itemIndex)

        # Update the output object to reflect that the person who created this
        # object has permissions to it.
        self.permissions.update(outputObject.id, identity = self.person.identity,
                                                 canRead  = True,
                                                 canWrite = True,
                                                 canClone = True)

        # Add the output object as an output to each generator. This is what
        # enables the frontend to show the outputs under the object's page.
        outputObjectInfo = self.objects.infoFor(outputObject)
        for i, generator in enumerate(outputObjectInfo.get("generator", [])):
          generatorObj = self.objects.retrieve(generator.get('id'),
                                               revision = generator.get('revision'),
                                               person = personForJob)

          if generatorObj:
            relationship = "outputOf"

            if i > 0:
              relationship = "generatedBy"

            self.jobs.addOutput(generatorObj, outputObject,
                                              self.jobs.personFor(job),
                                              relationship = relationship)

    # If the job was to run a build task, we want to pull back the build
    task = self.jobs.taskFor(job, person = personForJob)
    if task:
      taskInfo = self.objects.infoFor(task)
      buildInfo = taskInfo.get('builds')
      if buildInfo and not buildInfo.get('stage'):
        obj = self.objects.retrieve(buildInfo.get('id'), revision = buildInfo.get('revision'),
                                                         person = personForJob)

        if obj:
          pullBuild = lambda: self.discover.pullBuild(node,
                                                      obj,
                                                      buildId = task.id,
                                                      verify = False)
          build = retryOnReset(pullBuild)

          # Resign build
          Log.write("Signing build")
          published = build.get('published')
          if published:
            published = Datetime.from8601(published)
          self.builds.write.sign(self.person.identity, obj, task, published)

    # Copy the job files to this node.
    try:
      logForJob = lambda: self.nodes.logForJob(node, self.options.remote_job_id)
      remoteLogHandle, _, _ = retryOnReset(logForJob)
      if remoteLogHandle:
        with open(self.jobs.logPathFor(self.options.job_id), 'w+b') as fh:
          fh.write(remoteLogHandle.read())
    except Exception as e:
      Log.error(f"Failed to copy the remote job's log: {e}")

    # Tell the local scheduler plugin about the ping
    self.jobs.ping(job)

    # Output the JSON of the updated status
    Log.output(json.dumps(statusInfo))
    return 0
