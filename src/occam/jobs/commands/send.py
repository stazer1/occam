# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.objects.manager   import ObjectManager
from occam.jobs.manager      import JobManager

@command('jobs', 'send',
    category = "Job Management",
    documentation = "Sends data to the given job")
@argument("job_id", type=str, help = "The identifier for the job you wish to send data.")
@uses(JobManager)
class SendCommand:
  """ Sends data to the given job.
  """

  def do(self):
    job_id = self.options.job_id
    job = self.jobs.jobFromId(job_id)

    if job is None:
      Log.error(f"Job {job_id} does not exist.")
      return 1

    data = self.stdin.read()

    if not self.jobs.writeTo(job, data):
      Log.error(f"Failed to send data to job {job.id}.")

    return 0
