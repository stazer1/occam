# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime

from occam.log import Log

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.objects.manager   import ObjectManager
from occam.jobs.manager      import JobManager

@command('jobs', 'status',
    category = "Job Management",
    documentation = "Lists information about the given job.")
@argument("job_id", type=str, help = "The identifier for the job you wish to list.")
@option("-j", "--json",   action  = "store_true",
                          dest    = "to_json",
                          help    = "Report the output in JSON format.")
@uses(JobManager)
@uses(ObjectManager)
class StatusCommand:
  """ Gives the status of the job.
  """

  def structureOutputs(self, outputs):
    """ Converts a list of job output objects into a list of lists of
    dictionaries containing the id and revision of output jobs.

    Arguments:
      outputs (list): List of job output records.

    Returns:
      list: A list of lists of dictionaries, where each list represents a wire
            output and contains a dictionary for each output from that wire in order
            of creation.
    """
    structuredOutputs = []
    intermediate = {}
    maxWire = 0

    # Start with an intermediate dictionary of dictionaries so we can sort the
    # values cheaply.
    for output in outputs:
      maxWire = max(output.output_index, maxWire)
      if output.output_index not in intermediate.keys():
        intermediate[output.output_index] = {}
      intermediate[output.output_index][output.output_item_index] = {
        "id": output.object_uid, "revision": output.object_revision
      }

    # Because the client will infer the wire index from the array, we have to
    # have empty arrays for wires without outputs. This isn't necessary for the
    # output items themselves, because their relative order being maintained is
    # sufficient.
    for wireIndex in range(maxWire + 1):
      intermediate.setdefault(wireIndex, {})

    # Convert to a sorted list of sorted lists of dictionaries.
    for outputIndex in sorted(intermediate.keys()):
      # Remember, we are intentionally inserting blank arrays for wires without
      # outputs to preserve the wireIndex when clients parse the output.
      outputItemList = []
      for outputItemIndex in sorted(intermediate[outputIndex].keys()):
        outputItemList.append(intermediate[outputIndex][outputItemIndex])
      structuredOutputs.append(outputItemList)

    return structuredOutputs

  def do(self):
    Log.header("Listing job %s" % (self.options.job_id))

    jobs = self.jobs.retrieve(job_id = self.options.job_id)

    if len(jobs) == 0:
      Log.error("Job not found.")
      return -1

    job = jobs[0]

    ret = {
      "id": job.id,
      "task": {
        "id": job.task_uid,
        "revision": job.task_revision,
        "backend": job.task_backend
      },
      "kind": job.kind,
      "running": self.jobs.isRunning(job),
      "status": job.status,
      "scheduler": job.scheduler,
      "outputs": [],
    }

    if job.initialize:
      ret["initialize"] = {
          "component": job.initialize,
          "argument": job.initialize_tag
      }

    if job.object_uid:
      ret["object"] = {
        "id": job.object_uid,
        "revision": job.object_revision
      }

    task = self.jobs.taskFor(job, person = self.person)
    taskInfo = self.objects.infoFor(task)

    # For jobs run on compute nodes, we need a way to find all of the job
    # outputs to pull back to the backend. To aid this aim, we put all of the
    # output object ids/revisions in the status as well.
    #
    # Per Wilkie, we want job status outputs in the format of a list of lists
    # of dictionaries out of convenience for the user who may wish to walk over
    # the outputs by item by wire.
    outputs = self.jobs.outputsFor(job)

    ret["outputs"] = self.structureOutputs(outputs)

    if job.queue_time:
      ret["queueTime"] = job.queue_time.isoformat()

    if job.init_time:
      ret["initTime"] = job.init_time.isoformat()

    if job.start_time:
      ret["startTime"] = job.start_time.isoformat()

    if job.run_time:
      ret["runTime"] = job.run_time.isoformat()

    if job.finish_time:
      ret["finishTime"] = job.finish_time.isoformat()

      if job.status == "failed":
        ret["failure_time"] = job.finish_time.isoformat()

    if self.options.to_json:
      import json
      Log.output(json.dumps(ret))
    else:
      Log.output(str(ret))

    return 0
