# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import datetime

from occam.key_parser import KeyParser

from occam.log import Log

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.objects.manager   import ObjectManager
from occam.jobs.manager      import JobManager

@command('jobs', 'list',
    category = "Job Management",
    documentation = "Lists tasks queued.")
@option("-s", "--status", action = "append",
                          dest   = "filter",
                          help   = "filter by the status of the listed jobs",
                          nargs  = "?")
@option("-t", "--target", action = "append",
                          dest   = "targets",
                          help   = "filter by the target of the listed jobs",
                          nargs  = "?")
@option("-a", "--aggregate", action = "store_true",
                             dest   = "aggregate",
                             help   = "reports the aggregate statistics")
@option("-j", "--json",   action = "store_true",
                          dest   = "to_json",
                          help   = "report the output in JSON format")
@uses(JobManager)
class JobsListCommand:
  """ Lists all tasks within the job queue.
  """

  def do(self):
    statusFilter = self.options.filter or []
    Log.header("Listing %s tasks" % (', '.join(statusFilter) or "all"))

    if self.options.aggregate:
      # Query for job statistics
      ret = self.jobs.retrieveCount(status = self.options.filter,
                                    targets = self.options.targets)

      # Output the data for the aggregate counts
      Log.output(json.dumps(ret))
    else:
      # Query for the set of jobs
      jobs = self.jobs.retrieve(status = self.options.filter,
                                targets = self.options.targets)

      # Reform the data and output it
      if self.options.to_json:
        ret = []

        # Convert dates to iso8601
        for job in jobs:
          for k, v in job._data.items():
            if isinstance(v, datetime.datetime):
              job._data[k] = v.isoformat()

          # Reform tags to convention
          jobInfo = {}
          for key, value in job._data.items():
            key = KeyParser.keySnakeToLowerCamelCase(key)
            jobInfo[key] = value

          ret.append(jobInfo)

        # Output the data for all of the job records
        Log.output(json.dumps(ret))
      else:
        for job in jobs:
          time = job.finish_time

          if job.status == "queued":
            time = job.queue_time or time

          if job.status == "started":
            time = job.start_time or time

          if not time is None:
            Log.write("%s: %s %s %s at %s" % (job.id, job.kind, job.task_uid, job.status, time.isoformat()))
          else:
            Log.write("%s: %s %s %s at %s" % (job.id, job.kind, job.task_uid, job.status, None))

    return 0
