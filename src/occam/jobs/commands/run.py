# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys, os

from datetime import datetime

from occam.log import Log

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.objects.manager   import ObjectManager
from occam.jobs.manager      import JobManager

@command('jobs', 'run',
    category = "Job Management",
    documentation = "Runs the given job")
@argument("job_id", type=str, help = "The identifier for the job you wish to run.")
@option("-i", "--interactive", action = "store_true",
                               help   = "Deploys the job to run as an interactive process")
@option("-l", "--log", action = "store_true",
                       dest   = "write_to_log",
                       help   = "Writes job output to a log file.")
@uses(JobManager)
@uses(ObjectManager)
class RunCommand:
  """ Immediately runs the given job.
  """

  def do(self):
    print("Running job %s" % self.options.job_id)
    Log.header("Running job %s" % (self.options.job_id))

    job = self.jobs.jobFromId(self.options.job_id)

    if not job:
      Log.error("Job not found.")
      return -1

    # Ensure the job process metadata exists
    if not self.jobs.metadataFor(job.id):
      # Write out metadata
      print("DEBUG WORKFLOW - do (Ensure the job process metadata exists) - occam/src/occam/jobs/run.py")
      print("DEBUG WORKFLOW - Metadata: %s" % self.jobs.writeMetadataFor(job.id))
      self.jobs.writeMetadataFor(job.id)

    # If we have yet to officially 'start' this job, mark it initialized,
    # if it is not already
    if job.status != "initialized":
      self.jobs.initialized(job)

    # Determine where the output goes
    if self.options.write_to_log:
      sys.stdout = open(self.jobs.logPathFor(job.id), "w+b")

    # Start the task
    #print("DEBUG WORKFLOW - do (Start the task) - occam/src/occam/jobs/run.py")
    opts = self.jobs.run(job, interactive = self.options.interactive,
                              person = self.person)

    # Retrieve the task
    #print("DEBUG WORKFLOW - do (Retrieve the task) - occam/src/occam/jobs/run.py")
    taskInfo = self.jobs.taskInfoFor(job)

    # Job failures are run failures.
    if opts is None:
      #print("DEBUG WORKFLOW - do (Job FAILED!) - occam/src/occam/jobs/run.py")
      self.jobs.failed(job)
      self.jobs.removeRunFolder(taskInfo, person = self.person)
      return -1

    # If a task was executed, rake output
    if opts[0]:
      Log.write("Raking Output")
      #print("DEBUG WORKFLOW - do (If a task was executed, rake output) - occam/src/occam/jobs/run.py")
      self.jobs.rakeOutput(task       = taskInfo,
                           taskPath   = opts[1]['home'],
                           generators = [],
                           person     = self.person,
                           job        = job)

    # Update the runtime completion time for this job.
    self.jobs.ran(job)
    return 0
