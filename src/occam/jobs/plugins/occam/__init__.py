# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.config import Config
from occam.jobs.manager import scheduler, JobManager
from occam.log import loggable
from occam.manager import uses

from occam.jobs.daemon import JobDaemon
from occam.network.manager import NetworkManager

@loggable
@uses(JobManager)
@uses(NetworkManager)
@scheduler("occam")
class OccamScheduler:
  """ The native Occam scheduler.
  """

  def __init__(self, configuration):
    """ Initializes the driver.
    """

  def launchDaemon(self):
    """ Retrieves an instance of the daemon class.

    It will spawn one if it does not already exist.
    """

    # Spawn the daemon
    return JobDaemon().spawnWorker()

  def cancel(self, job):
    """ Cancels the given job.

    Arguments:
      job (JobRecord): The job to cancel.

    Returns:
      bool: True upon success.
    """

    # The job is local and the JobManager can kill it via a backend.
    return True

  def status(self, targetInfo):
    """ Reports the status of the given target.
    """

    # Gather the host name
    config = Config().load()
    host = self.network.hostname()
    thisHost = config.get("daemon", {}).get("externalHost", host)

    # Just report it as available
    return {
      "available": True,
      "nodes": [
        {
          "host": thisHost,
          "available": True
        }
      ]
    }

  def queue(self, job, person, targetInfo):
    """ Queues the given occam command.

    Arguments:
      job (JobRecord): The job to be queued.
      person (Person): The actor queuing the job.
      targetInfo (dict): The target to queue upon and its configuration.

    Returns:
      bool: Whether or not the job was queued successfully. Meaning that all of
            the sbatch commands queued, not that the job execution succeeded.
    """

    # We just ensure that a job worker is running since the job record is already
    # written to the database. The job worker simply polls the database until
    # no jobs exist and then powers down.
    launchRetCode = self.launchDaemon()

    # Not fatal, but the job won't run until a job daemon picks it up.
    if launchRetCode != 0:
      OccamScheduler.Log.error("failed to start job daemon")
      return False

    OccamScheduler.Log.write("Started a job daemon")
    return True

  def ping(self, job):
    """ Responds to job status changes.

    Arguments:
      job (JobRecord): The job information to be used in the ping.

    Returns:
      bool: True upon success.
    """

    # Do nothing, successfully
    return True

  def isRunning(self, job):
    """ Returns True when the given job is currently running.
    """

    # If it is locally known, and has a pid, then it is running
    return self.jobs.pidFor(job) is not None
