# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.jobs.manager import scheduler, JobManager
from occam.log import loggable
from occam.manager import uses

@loggable
@uses(JobManager)
@scheduler("null")
class NullScheduler:
  """ The null scheduler runs objects immediately.
  """

  def __init__(self, configuration):
    """ Initializes the driver.
    """

    # We don't do anything because this driver is trivial.

  def cancel(self, job):
    """ Cancels the given job.

    Arguments:
      job (JobRecord): The job to cancel.

    Returns:
      bool: True upon success.
    """

    # The job is local and the JobManager can kill it via a backend.
    return True

  def status(self, targetInfo):
    """ Reports the status of the given target.
    """

    # Just report it as available
    return {
      "available": True
    }

  def queue(self, job, person, targetInfo):
    """ This would normally queue the job, but this driver runs the job right away.
    """

    # Do nothing, successfully
    return True

  def isRunning(self, job):
    """ Returns True when the given job is currently running.
    """

    # If it is locally known, and has a pid, then it is running
    return self.jobs.pidFor(job) is not None

  def ping(self, job):
    """ Responds to job status changes.

    Arguments:
      job (JobRecord): The job information to be used in the ping.

    Returns:
      bool: True upon success.
    """

    # Do nothing, successfully
    return True
