# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re
import os
import subprocess

from occam.config import Config
from occam.log import loggable
from occam.manager import uses

from occam.jobs.manager import scheduler

from occam.accounts.manager import AccountManager
from occam.jobs.manager import JobManager
from occam.network.manager import NetworkManager
from occam.objects.manager import ObjectManager
from occam.accounts.manager import AccountManager

# FIXME (john): We need some sort of mechanism that will detect that a job
# needs to be picked up and will keep retrying.
#
# The job daemon failing to launch is fine, because the next one that
# launches will pick the job up, but in this case nothing is trying to
# pick up unscheduled slurm jobs. Perhaps we can push this down a level
# and make the daemon do the scheduling.

@loggable
@uses(AccountManager)
@uses(JobManager)
@uses(NetworkManager)
@uses(ObjectManager)
@uses(AccountManager)
@scheduler("slurm")
class SlurmScheduler:
  """ The Slurm scheduler.

  This plugin enables scheduling Occam jobs to be run in a distributed
  environment via Slurm (https://slurm.schedmd.com/overview.html).

  We will refer to the computing resource that initially requests the job to be
  run as the coordinator. The computing resource that is allocated for the
  batch execution of our sbatch command will be referred to as the compute
  node. They may be the same resource.

  The Occam job on the coordinator will be referred to as the main job.
  The Occam job on the compute node will be referred to as the
  compute-node-local job.

  Jobs run over Slurm are submitted via sbatch, and are deployed over the
  following stages:

    Preparation:
      First the task manifest generated during the creation of the main job is
      pulled onto the compute node. The compute-node-local job is then created
      to track task execution. The main job is reported has having been
      accepted onto the compute node. All of the task's dependencies are then
      pulled onto the compute node.

    Run:
      The job is reported as started, and the compute-node-local job is run.

    Report:
      When execution has finished, the final job status is reported. The
      coordinator is now responsible for pulling results and storing them
      long term.

  Along with launching an sbatch job to execute the main job's task, a failure
  reporting Slurm job is launched. The failure reporting Slurm job is dependent
  on the sbatch job failing. If the sbatch job fails for any reason, the main
  job is reported as having failed. This way even if a Slurm node goes down,
  the job is not left hanging. If the job completes and a report makes it back
  to the main node, the failure reporting Slurm job is cancelled.

  The Slurm plugin assumes compute nodes have a working Occam instance.
  """

  @staticmethod
  def executeSlurmCommand(command, stdout=None, stderr=None, stdin=None):
    if stdout is None:
      stdout = subprocess.DEVNULL

    if stderr is None:
      stderr = subprocess.DEVNULL

    return subprocess.Popen(command, stdout=stdout, stdin=stdin, stderr=stderr)

  def __init__(self, configuration):
    """ Creates a Slurm interface with the provided configuration.
    """

    # Retain configuration options
    self._configuration = configuration

  def executeSlurmBatchCommand(self, command, occamCommand):
    """ Runs the given slurm command that presumably is an sbatch command.

    Prepends occamCommand with the shell shebang line and shell commands needed
    to load the environment variables required to find Occam.

    The sbatch command returns a job id with '--parsable' specified. This will
    parse the job identifier and return it once Slurm has successfully scheduled
    the job.
    """

    process = SlurmScheduler.executeSlurmCommand(command, stdin = subprocess.PIPE,
                                                          stdout = subprocess.PIPE)

    finalCommand = (
      f"#!/bin/bash\n"

      f"{occamCommand}"
    )

    stdout, _ = process.communicate(finalCommand.encode('utf-8'))
    if process.returncode != 0:
      raise RuntimeError(f"sbatch finished with returncode {process.returncode}")
    
    # Pull out stdout and clean up whitespace.
    jobId = stdout.strip()

    # Return the job id
    return jobId

  def prepareSlurmBatchCommand(self, jobDescription=None, output="/dev/null",
                                     openMode="append", depend=None,
                                     comment=None, targetInfo={}):
    """ Generate the command to run the desired occam command.

    The `jobDescription` field is normally in the form "{job.id}-some-details".

    Note that the `output` field is a path on the compute node (remote) and not
    a path on the existing server. If the output file cannot be created on this
    remote node, the job will fail.

    Arguments:
      jobDescription (str): Describes the Occam job being submitted.
      output (str): A path to store the output of the slurm job.
      openMode (str): Will append to an existing log when "append" is specified.
      depend (str): The slurm depend rule for this job.
      comment (str): Arbitrary string that serves as a job's comment field.

    Returns:
      list: The sbatch command in list form. It may be passed to subprocess for
        running.
    """

    # Retrieve configuration settings from the target description
    config = targetInfo.get('configuration', {})
    partitionName = config.get("partition", None)
    reservationName = config.get("reservation", None)
    nodelist = config.get("nodelist", None)

    slurmCommand = [
      "sbatch",
      "--parsable",
      "--output", output,
      "--open-mode", openMode,
    ]

    # Some Slurm setups will need additional arguments to make the environment Occam friendly.
    # For example, the PSC Bridges environment requires "-C EGRESS" to be
    # specified in order to reach off-node servers.
    sbatchAdditionalArgs = config.get("sbatchAdditionalArgs", [])
    slurmCommand.extend(sbatchAdditionalArgs)

    if partitionName:
      slurmCommand.extend(("--partition", partitionName))

    if reservationName:
      slurmCommand.extend(("--reservation", reservationName))

    if nodelist:
      slurmCommand.extend(("--nodelist", nodelist))

    if jobDescription:
      slurmCommand.extend(("--job-name", f"occam-job-{jobDescription}"))

    if depend:
      slurmCommand.extend(("--depend", depend))

    if comment:
      slurmCommand.append("".join(["--comment=\"", f"{comment}", "\""]))

    return slurmCommand

  def slurmJobInfo(self, job, postfix=''):
    """ Using the given job, find the Slurm job info.
    """

    # Fetch the job resource (not including job steps) status.
    # -o : filters output (%A Ids, %T State, %N Nodelist)
    # -h : Hide headers
    command = ["squeue", f"--name=occam-job-{job.id}{postfix}", '-o', '%A %T %N', '-h']
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)

    stdout = process.communicate()[0].decode('utf-8')

    if not (process.returncode == 0 and stdout):
      if process.returncode:
        SlurmScheduler.Log.error(f"squeue failed with code: {process.returncode}")

      return {}

    # NOTE: We can't be sure we are the first occam daemon to ever use the local
    # slurm controller, so without more internal accounting of job ids, we need
    # to always get the last job listed. If we want to have multiple backends
    # with different sets of job ids running in the same slurm ecosystem, we
    # need to have a unique identifier in the job names. (like a hostname?)
    lines = stdout.strip().split("\n")
    if len(lines) == 0:
      # No output
      return {}

    stats = lines[-1].split(" ")
    if len(stats) < 2:
      SlurmScheduler.Log.error(f"Unexpected squeue output: {lines[-1]}")
      return {}

    node = None
    if len(stats) >= 3:
      # Get node
      node = stats[2]

    if node and (',' in node or '[' in node):
      SlurmScheduler.Log.warning(
        "Multiple nodes listed for an Occam job submitted to Slurm."
      )
      node = None

    info = {
      "slurmJobId": stats[0],
      "slurmJobState": stats[1],
      "slurmJobNode": node
    }

    return info

  def slurmCancel(self, slurmJobId):
    """ Cancels the given slurm job.
    """
    
    command = ["scancel", slurmJobId]
    process = subprocess.Popen(command)
    process.communicate()

  def cancel(self, job):
    """ Cancels the given job.

    Arguments:
      job (JobRecord): The job to cancel.

    Returns:
      bool: True upon success.
    """

    slurmJobInfo = self.slurmJobInfo(job)

    if slurmJobInfo:
      self.slurmCancel(slurmJobInfo['slurmJobId'])

    return True

  def status(self, targetInfo):
    """ Reports the status of the given target.
    """

    # Retrieve configuration settings from the target description
    config = targetInfo.get('configuration', {})
    partitionName = config.get("partition", None)

    command = ['sinfo', '--noheader']

    # Add pattern
    command.append('-o')
    command.append('%P %.5a %.10l %.10s %.4r %.8h %.10g %.6D %.11T %N %e %d %c %B %E')

    if partitionName:
      command.append('-p')
      command.append(partitionName)

    process = self.executeSlurmCommand(command, stdout = subprocess.PIPE)

    # These options give this tabular info:
    # PARTITION AVAIL TIMELIMIT JOB_SIZE ROOT OVERSUBS GROUPS NODES STATE NODELIST
    stdout, _ = process.communicate('')

    # By default, we assume it is not available
    ret = {
      "available": False
    }

    # sinfo failed to run if the return code is not 0
    if process.returncode == 0:
      # Assume it is unavailable to begin with
      available = False

      # We want information for each node in the cluster
      ret["nodes"] = []

      # Parse the information
      for line in stdout.split(b'\n'):
        # Each line is a set of nodes
        tokens = re.split(rb"\s+", line.strip())
        if len(tokens) > 8:
          # Read Reason
          reason = b' '.join(tokens[14:]).decode('utf-8')

          # Read STATE
          state = tokens[8]

          if state == b"down*" or state == b"down" or state == b"idle*":
            state = False
          else:
            state = True

          # Read NODELIST
          nodelist = tokens[9]

          # The nodelist is a string with a numeric range in brackets
          # We will parse the nodelist string.
          parts = re.split(rb"\[|\]", nodelist)
          base = parts[0].decode('utf-8')
          if len(parts) > 1:
            subparts = parts[1].split(b"-", 1)
            if len(subparts) == 1:
              subparts.append(subparts[0])

            try:
              suffix = parts[2].decode('utf-8')
              start = int(subparts[0])
              end = int(subparts[1])

              for idx in range(start, end + 1):
                ret["nodes"].append({
                  "host": base + str(idx) + suffix,
                  "available": state,
                  "reason": reason
                })
            except:
              pass
          else:
            ret["nodes"].append({
              "host": base,
              "available": state,
              "reason": reason
            })

          # Read "AVAIL"
          if tokens[1] == b'up':
            # If any are 'up', the cluster is technically available
            available = True

      ret["available"] = available

    return ret

  def queue(self, job, person, targetInfo):
    """ Sends the task associated with this job to a compute node.

    Arguments:
      job (JobRecord): The job to be run on a compute node.
      person (Person): The actor requesting this job.
      targetInfo (dict): The target parameters.

    Returns:
      bool: Whether or not the job was queued successfully. Meaning that all of
            the sbatch commands queued, not that the job execution succeeded.
    """

    import json
    import uuid

    from occam.config import Config

    # Get the configuration elements required to execute using Slurm.
    config = Config().load()
    host = self.network.hostname()
    thisHost = config.get("daemon", {}).get("externalHost", host)
    thisPort = config.get("daemon", {}).get("port", None)

    if not (thisHost and thisPort):
      SlurmScheduler.Log.error("Slurm support requires configuring daemon host and port.")
      return False

    # The compute node performs several steps:
    # - Prepare: It pulls the task and its associated objects.
    # - Run:
    #   - Create: It creates a local job with a null scheduler on the compute
    #             node. The job must be run directly with 'occam jobs run'.
    #   - Run: It runs job-init, and then the local job, and then job-done.
    # - Report: It pings this coordinator via 'jobs report' which will execute
    #           'jobs ping' to pull job results.

    # Get the acting person
    person = self.jobs.personFor(job)

    # Attempt to procure a token so any private objects can still be run over
    # Slurm.
    tokenArg = ""
    if person:
      account = self.accounts.retrieveAccount(username = person.name)
      token = self.accounts.generateToken(account, person, "person")
      tokenArg = f"-e {token}"

    # Get the task to run
    task = self.jobs.taskFor(job, person = person)

    account = self.accounts.retrieveAccount(identity = person.identity)
    readOnlyToken = self.accounts.generateToken(account, task, "readOnly")
    accessToken = self.accounts.generateToken(account)

    # Determine the log path.
    config = targetInfo.get("configuration", {})
    slurmLogPath = f"{config.get('slurmLogDir', '/home/occam')}/{job.id}.log"

    try:
      slurmCommand = self.prepareSlurmBatchCommand(jobDescription = f"{job.id}",
                                                   output = slurmLogPath,
                                                   targetInfo = targetInfo)

      script = []

      # Exit the script on the first failure
      script.append("set -e")

      # Export all of the environment customizations done in the config at the
      # start of the script. These will help find the binaries for Occam as
      # well as control the effective Occam root folder. The environment
      # variables are specified as a list of dictionaries.
      scriptSetup = config.get('scriptSetup', [])
      for line in scriptSetup:
        script.append(line)

      # FIXME: add --ntasks-per-node=1 --cpus-per-task=1 --nodes=1
      # to prevent multiple occam command runs when sbatch has more than one
      # core allocated to it.

      # Ensure the system is initialized.
      script.append(f"srun --job-name=\"occam-job-{job.id}-system-initialize\" "
                    f"--ntasks-per-node=1 --cpus-per-task=1 --nodes=1 "
                    f"occam system initialize")

      # Create the temporary account for this job
      # This lets us sign work and keep objects private within this node
      account = f"job-{job.id}-{task.id}"
      script.append(f"srun --job-name=\"occam-job-{job.id}-new-account\" "
                    f"--ntasks-per-node=1 --cpus-per-task=1 --nodes=1 "
                    f"occam accounts new {account} -p {task.id}")

      # Get the token for that account
      script.append(f"OCCAM_TOKEN=`srun --job-name=\"occam-job-{job.id}-token\" "
                    f"occam accounts login {account} -p {task.id} -t --filter token`")

      # The Prepare Step (Pull the task (shallow))
      script.append(f"srun --job-name=\"occam-job-{job.id}-pull\" "
                    f"--ntasks-per-node=1 --cpus-per-task=1 --nodes=1 "
                    f"occam objects pull occam://{thisHost}:{thisPort}/{task.id} -T $OCCAM_TOKEN -e {readOnlyToken}")

      # The Create Step (Creates local job)
      script.append(f"LOCAL_JOB_ID=`srun --job-name=\"occam-job-{job.id}-create\" "
                    f"--ntasks-per-node=1 --cpus-per-task=1 --nodes=1 "
                    f"occam jobs new {task.id} --target null -T $OCCAM_TOKEN`")

      # Start a daemon.
      script.append(f"occam daemon start -P job-$LOCAL_JOB_ID -s '0.0.0.0'")

      # Report Accepting the Job
      # Will get the external token to pull the deep task
      script.append(f"srun --job-name=\"occam-job-{job.id}-report-prepare\" "
                    f"--ntasks-per-node=1 --cpus-per-task=1 --nodes=1 "
                    f"occam jobs report {job.id} "
                    f"occam://{thisHost}:{thisPort} --job-id $LOCAL_JOB_ID -T $OCCAM_TOKEN -e {readOnlyToken}")

      # The Prepare Step Phase II (Pull the task (deep))
      # TODO: external token (need to retrieve it from the coordinator... not encode it here)
      script.append(f"srun --job-name=\"occam-job-{job.id}-pull\" "
                    f"--ntasks-per-node=1 --cpus-per-task=1 --nodes=1 "
                    f"occam objects pull occam://{thisHost}:{thisPort}/{task.id} -p -T $OCCAM_TOKEN -e {accessToken}")

      taskInfo = self.objects.infoFor(task)
      objInfo = taskInfo.get("runs", taskInfo.get("builds",{}))
      if objInfo:
        stageId = objInfo.get("stage", {}).get("id")
        objectId = objInfo.get("id")
        if objectId is not None and stageId is not None:
          script.append(f"srun --job-name=\"occam-job-{job.id}-update-link\" "
                        f"--ntasks-per-node=1 --cpus-per-task=1 --nodes=1 "
                        f"occam links track {objectId} --id {stageId} -T $OCCAM_TOKEN -I {job.identity}\n")

      # Report the job reaching its initialize phase
      script.append(f"srun --job-name=\"occam-job-{job.id}-report-initialize\" "
                    f"--ntasks-per-node=1 --cpus-per-task=1 --nodes=1 "
                    f"occam jobs report {job.id} "
                    f"occam://{thisHost}:{thisPort} --status started --job-id $LOCAL_JOB_ID -T $OCCAM_TOKEN -e {readOnlyToken}")

      # The Initialize Step
      if job.initialize:
        script.append(f"srun --job-name=\"occam-job-{job.id}-init\" "
                      f"--ntasks-per-node=1 --cpus-per-task=1 --nodes=1 "
                      f"occam {job.initialize} job-init $LOCAL_JOB_ID {job.initialize_tag or ''} --remote-job {job.id} -T $OCCAM_TOKEN -I {job.identity}")

      # Report the job starting its run phase
      script.append(f"srun --job-name=\"occam-job-{job.id}-report-run\" "
                    f"--ntasks-per-node=1 --cpus-per-task=1 --nodes=1 "
                    f"occam jobs report {job.id} "
                    f"occam://{thisHost}:{thisPort} --status initialized --job-id $LOCAL_JOB_ID -T $OCCAM_TOKEN -e {readOnlyToken}")

      # The Run Step
      script.append(f"srun --job-name=\"occam-job-{job.id}-run\" "
                    f"--ntasks-per-node=1 --cpus-per-task=1 --nodes=1 "
                    f"occam jobs run $LOCAL_JOB_ID --log -T $OCCAM_TOKEN -I {job.identity}")

      # Report the job is going into the finalize phase
      script.append(f"srun --job-name=\"occam-job-{job.id}-report-finalize\" "
                    f"--ntasks-per-node=1 --cpus-per-task=1 --nodes=1 "
                    f"occam jobs report {job.id} "
                    f"occam://{thisHost}:{thisPort} --status ran --job-id $LOCAL_JOB_ID -T $OCCAM_TOKEN -e {readOnlyToken}")

      # The Finalize Step
      if job.finalize:
        script.append(f"srun --job-name=\"occam-job-{job.id}-done\" "
                      f"--ntasks-per-node=1 --cpus-per-task=1 --nodes=1 "
                      f"occam {job.finalize} job-done $LOCAL_JOB_ID {job.finalize_tag or ''} --remote-job {job.id} -T $OCCAM_TOKEN -I {job.identity}")

      # Report the job completion
      script.append(f"srun --job-name=\"occam-job-{job.id}-report-finish\" "
                    f"--ntasks-per-node=1 --cpus-per-task=1 --nodes=1 "
                    f"occam jobs report {job.id} "
                    f"occam://{thisHost}:{thisPort} --status finished --job-id $LOCAL_JOB_ID -T $OCCAM_TOKEN -e {readOnlyToken}")

      # Stop the daemon.
      script.append(f"occam daemon stop -P job-$LOCAL_JOB_ID")

      #print("\n".join(script))
      # TODO: We then need to remove the temporary account

      # Create the actual slurm job
      createJobId = self.executeSlurmBatchCommand(slurmCommand, "\n".join(script))
      SlurmScheduler.Log.noisy(f"Slurm create step job id: {createJobId}")

      # If the main Slurm job for running the Occam job fails for any reason
      # (cancelled etc.), we need to make sure the coordinator knows the job
      # failed.
      slurmCommand = self.prepareSlurmBatchCommand(jobDescription = f"{job.id}-on-fail",
                                                   output = "/dev/null",
                                                   depend = f"afternotok:{createJobId.decode('utf-8')}",
                                                   targetInfo = targetInfo)

      # Create the 'afternotok' slurm job to watch for failure
      failCommand = f"occam jobs report {job.id} occam://{thisHost}:{thisPort} --status failed -e {readOnlyToken}"
      onFailId = self.executeSlurmBatchCommand(slurmCommand, failCommand)
      SlurmScheduler.Log.noisy(f"Slurm on job fail id: {onFailId}")
    except RuntimeError as e:
      SlurmScheduler.Log.error(e)
      return False

    return True

  def ping(self, job):
    """ Responds to job status changes.

    Arguments:
      job (JobRecord): The job information to be used in the ping.

    Returns:
      bool: True upon success.
    """

    # Is the job, effectively, not running?
    if job.status == "finished" or job.status == "failed":
      # Remove the failure catch job
      slurmJobInfo = self.slurmJobInfo(job, postfix = '-on-fail')

      # Cancel it
      if slurmJobInfo:
        self.slurmCancel(slurmJobInfo['slurmJobId'])

    return True

  def isRunning(self, job):
    """ Returns True when the given job is currently running.
    """

    slurmJobStatus = self.slurmJobInfo(job)
    return slurmJobStatus.get("slurmJobState", "MISSING") == "RUNNING"
