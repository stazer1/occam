# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import json
import os
import re
import select
import shlex
import shutil
import sys
import time

from functools import reduce

from occam.config import Config
from occam.log    import loggable

from occam.manager import manager, uses

from occam.databases.manager     import DatabaseManager, DataNotUniqueError
from occam.accounts.manager      import AccountManager
from occam.backends.manager      import BackendManager
from occam.builds.manager        import BuildManager
from occam.links.write_manager   import LinkWriteManager
from occam.network.manager       import NetworkManager
from occam.nodes.manager         import NodeManager
from occam.objects.manager       import ObjectManager
from occam.objects.write_manager import ObjectWriteManager
from occam.permissions.manager   import PermissionManager
from occam.storage.manager       import StorageManager
from occam.system.manager        import SystemManager

# Modifies the configuration records when recording output
from occam.configurations.write_manager import ConfigurationWriteManager

@loggable
@uses(AccountManager)
@uses(BackendManager)
@uses(DatabaseManager)
@uses(NetworkManager)
@uses(NodeManager)
@uses(ObjectManager)
@uses(ObjectWriteManager)
@uses(StorageManager)
@uses(SystemManager)
@uses(BuildManager)
@uses(PermissionManager)
@uses(LinkWriteManager)
@uses(ConfigurationWriteManager)
@manager("jobs")
class JobManager:
  """ This OCCAM manager handles dispatching and controlling running tasks.
  """

  drivers = {}

  @staticmethod
  def register(adapter, driverClass):
    """ Adds a new scheduler driver to the possible drivers.
    """
    JobManager.drivers[adapter] = driverClass

  def __init__(self):
    """ Initialize the job manager.
    """

  def configurationFor(self, scheduler):
    """ Returns the configuration section for the given scheduler.

    Returns the configuration for the given scheduler that is found within the
    occam configuration (config.yml) under the "jobs" section under the
    "schedulers" key and then under the given scheduler plugin name.
    """

    config = self.configuration
    subConfig = config.get('schedulers', {}).get(scheduler, {})

    return subConfig

  def driverFor(self, scheduler):
    """ Returns an instance of a driver for the given scheduler backend.

    Arguments:
      scheduler (String): The name of the scheduler.
    """

    if not scheduler in JobManager.drivers:
      # Attempt to find the driver plugin
      import importlib

      try:
        importlib.import_module("occam.jobs.plugins.%s" % (scheduler))
      except ImportError as e:
        # On error, we return None
        JobManager.Log.warning(f"Failed to import scheduler plugin for {scheduler}: {e}")
        return None

      if not scheduler in JobManager.drivers:
        # If the import does not register the driver correctly, we also error out.
        return None

    return JobManager.drivers[scheduler](self.configurationFor(scheduler))

  def updateRunSection(self, task, runSection, root = None):
    """ Modifies the given task to replace the run section with the given run section.
    """

    if root is None:
      root = task

    # For this object, add its install paths to root
    if 'index' in task and task['index'] == root['runs']['index']:
      task['run'] = task.get('run', {})
      task['run'].update(runSection)

      if 'cwd' in runSection:
        task['paths']['cwd'] = runSection['cwd']

      return root

    for wire in task.get('inputs', []):
      for inputObject in wire.get('connections', []):
        # Recursively find paths to subobjects
        self.updateRunSection(inputObject, runSection, root)

    for subTask in task.get('running', []):
      for inputObject in subTask.get('objects', []):
        # Recursively find paths to subobjects
        self.updateRunSection(inputObject, runSection, root)

    return root

  def updateJob(self, id, path=None, status=None, startTime=None, finishTime=None, initTime=None, runTime=None, schedulerId=None):
    return self.datastore.updateJob(id, path = path,
                                        status = status,
                                        startTime = startTime,
                                        finishTime = finishTime,
                                        initTime = initTime,
                                        runTime = runTime,
                                        schedulerId = schedulerId)

  def writeTask(self, task, taskPath):
    import json

    # Create an stdin file/socket to communicate to the object
    stdinPath = os.path.join(taskPath, "stdin")
    if not os.path.exists(stdinPath):
      with open(stdinPath, "w+") as f:
        pass

    # Output the whole task to path/object.json
    taskManifestPath = os.path.join(taskPath, "task", "object.json")

    if not os.path.exists(taskManifestPath):
      f = open(taskManifestPath, "w+")
      json.dump(task, f, indent=2, separators=(',', ': '))
      f.close()

  def writeNetwork(self, network, taskPath):
    import json

    # Output the whole task to path/object.json
    networkManifestPath = os.path.join(taskPath, "network.json")

    if not os.path.exists(networkManifestPath):
      f = open(networkManifestPath, "w+")
      json.dump(network, f, indent=2, separators=(',', ': '))
      f.close()

  def deployBuild(self, task, revision=None, local=False, command=None, runSection=None, person=None, uuid = None, interactive=False, cwd = None):
    """ Deploys a build task.
    """

    originalTaskId = task.get('id')

    if uuid is None:
      from uuid import uuid1
      uuid = str(uuid1())

    task['id'] = uuid

    # Get the object properties for the object we are building
    buildsId = task.get('builds', {}).get('id')
    buildsRevision = task.get('builds', {}).get('revision')
    buildsLink = task.get('builds', {}).get('stage', {}).get('id')

    # Get a reference to the object info for the object that is building
    buildingObject = self.getObjectFromTask(task, buildsId, buildsRevision)

    if buildingObject is None:
      raise ValueError("Task does not contain the object requested to run.")

    # Update the task to force the given command
    if command:
      buildingObject['run'] = buildingObject.get('run', {})
      buildingObject['run']['command'] = shlex.split(command)

    # TODO: instantiate a writable version of the object for the build
    #       delete that if it isn't local
    # TODO: record the run parameters and build information in the metadata
    taskPath = None
    if buildsLink:
      # Get the staged object
      instantiatedObject = self.objects.retrieve(buildsId, revision = buildsRevision,
                                                           link = buildsLink,
                                                           person = person)

      # Get the task path already instantiated for this build
      taskPath = self.links.runPathFor(instantiatedObject, build = True)

      # Get any other staged parameters
      buildsClean = task.get('builds', {}).get('stage', {}).get('clean')

      # If we are told to clean the stage, we will reinitialize the stage
      # Otherwise, we might reuse the current stage
      if buildsClean:
        self.links.write.clearStage(instantiatedObject)

      # If we need to re-stage the object, ensure that it does so
      if not self.links.write.stage(instantiatedObject, force = True):
        # If we did not restage, ensure we synchronize the files
        self.links.write.sync(instantiatedObject)

    if not taskPath:
      taskPath = self.createPathFor(task['id'])
    self.updateTaskWithRuntimeRequests(task, interactive)

    taskRootPath = os.path.join(taskPath, "task")
    os.makedirs(taskRootPath, exist_ok=True)

    objectInfo = task.get('builds')

    if buildsLink:
      local = True

      if not cwd:
        instantiatedObject = self.objects.retrieve(buildsId, revision = buildsRevision,
                                                             link = buildsLink,
                                                             person = person)
        if not instantiatedObject:
          raise RuntimeError(f"Unable to find staged object {buildsId}#{buildsLink}")

        cwd = instantiatedObject.path

    self.prepareTaskPath(task, taskRootPath, local = local,
                                             person = person)

    if local:
      # Local objects get built inside the local directory they are in
      # They get placed in a ".occam/local" path.
      buildPath = cwd

      # If we manage the space such that we know we have a parent path, then
      # we can put the ".occam" directory off to the side in the 'metadata'
      # directory.
      if buildsLink:
        localTaskPath = os.path.join(cwd, "..", "build", "vm")
        buildPath = os.path.realpath(os.path.join(cwd, "..", "build", "result"))
        cwd = os.path.realpath(os.path.join(cwd, "..", "stage"))
      else:
        localTaskPath = os.path.join(buildPath, ".occam", "local")
        buildPath = os.path.join(buildPath, ".occam", "local", "build")

      os.makedirs(buildPath, exist_ok = True)
      os.makedirs(localTaskPath, exist_ok = True)

      # Write local copy of the build task
      localTaskPath = os.path.join(localTaskPath, "object.json")
      with open(localTaskPath, "w+") as f:
        f.write(json.dumps(task))

      deployInfo = self._deploy(task, taskPath, cwd=cwd, pathsFor=[
          {'id': objectInfo.get('id'), 'revision': objectInfo.get('revision'), 'path': cwd}
        ], buildPath=buildPath, runSection = runSection, person = person, interactive=interactive)
    else:
      # Get the object
      buildPath = self.builds.pathFor(uid = objectInfo.get('owner', objectInfo).get('uid'),
                                      revision = objectInfo.get('revision'),
                                      buildId = originalTaskId,
                                      create = True)

      # FIXME: Is there a reason we abandon cwd in this case?
      # deployRun() still passes it along.
      deployInfo = self._deploy(task, taskPath, runSection = runSection, buildPath = buildPath, person = person, interactive=interactive)

    return deployInfo

  def deployRun(self, task, revision=None, arguments = None, command = None, local = False, runSection = None, person = None, stdin = None, stdout = None, uuid = None, taskPath = None, interactive=False, cwd=None):
    """ Deploys a run task.
    """

    initializeTaskPath = uuid is None

    if uuid is None:
      from uuid import uuid1
      uuid = str(uuid1())

    task['id'] = uuid

    runsId = task.get('runs', {}).get('id')
    runsRevision = task.get('runs', {}).get('revision')
    runsLink = task.get('runs', {}).get('stage', {}).get('id')

    # Get a reference to the running object metadata within this task
    runningObject = self.getObjectFromTask(task, runsId, runsRevision)

    if runningObject is None:
      raise ValueError("Task does not contain the object requested to run.")

    # Add arguments
    if command:
      runningObject['run'] = runningObject.get('run', {})
      runningObject['run']['command'] = shlex.split(command)

    if 'run' in runningObject and not arguments is None:
      runningObject['run']['arguments'] = arguments

      if 'command' in runningObject['run']:
        runningObject['run']['command'].extend(arguments)

    # We always prepare the task if it is not staged
    prepare = runsLink is None

    # If it is staged, we may need to prepare the task if it is not cached within the stage
    # This means we will have a task path that is co-located with the staged data
    if not taskPath and runsLink:
      # We want to create and reuse the task path at the given stage
      # Assume we already have a valid, prepared task
      prepare = False

      # Now, we want to get the object and determine if we need to re-stage and
      # regenerate the task.
      instantiatedObject = self.objects.retrieve(runsId, runsRevision, link = runsLink,
                                                                       person = person)
      if not instantiatedObject:
        raise RuntimeError(f"Unable to find staged object {runsId}@{runsRevision}")

      taskPath = self.links.runPathFor(instantiatedObject)
      updatePath = os.path.join(taskPath, "..", "update.txt")
      if os.path.exists(updatePath):
        os.remove(updatePath)
        self.links.write.clearTask(instantiatedObject)
        prepare = True

    # If we do not have an existing task path, we create one in the 'runs' path.
    if not taskPath:
      taskPath = self.createPathFor(task['id'])

    # Override the run section with the given one.
    if runSection:
      task = self.updateRunSection(task, runSection)

    # Update the task, as invoked, with any extra requests
    self.updateTaskWithRuntimeRequests(task, interactive)

    # Ensure that the `task/` sub-directory exists within the task path
    taskRootPath = os.path.join(taskPath, "task")
    os.makedirs(taskRootPath, exist_ok=True)

    # Build out the task's root filesystem in that `task/` sub-directory
    if prepare:
      self.prepareTaskPath(task, taskRootPath, person = person)

    # Whether or not we need to initialize the task filesystem
    # This will actually populate the task's root filesystem
    # Obviously, this must be done if we had already needed to 'prepare'
    initializeTaskPath = initializeTaskPath and prepare

    # Determine the working directory of the staged object
    mountPath = None
    if runsLink:
      local = True

      # Get any other staged parameters
      runsClean = task.get('runs', {}).get('stage', {}).get('clean')

      # If we are told to clean the stage, we will reinitialize the stage
      # Otherwise, we might reuse the current stage
      if runsClean:
        self.links.write.clearStage(instantiatedObject)

      # If we need to re-stage the object, ensure that it does so
      if not self.links.write.stage(instantiatedObject, force = True):
        # If we did not restage, ensure we synchronize the files
        self.links.write.sync(instantiatedObject)

      if cwd is None:
        cwd = self.links.localPathFor(instantiatedObject)

      mountPath = self.links.stagePathFor(instantiatedObject)

    if local:
      # Local objects get built inside the local directory they are in
      # They get placed in a ".occam/local" path.
      buildPath = cwd

      # If we manage the space such that we know we have a parent path, then
      # we can put the ".occam" directory off to the side in the 'metadata'
      # directory.
      if runsLink:
        buildPath = os.path.realpath(os.path.join(cwd, "..", "build", "result"))
        localTaskPath = os.path.join(cwd, "..", "run", "vm")
      elif buildPath is None:
        localTaskPath = os.path.join(cwd, ".occam", "local")
      else:
        localTaskPath = os.path.join(buildPath, ".occam", "local")
        buildPath = os.path.join(buildPath, ".occam", "local", "build")

      if not os.path.exists(buildPath):
        buildPath = None

      ret = self._deploy(task, taskPath, cwd=cwd, mountPath=mountPath, buildPath=buildPath, runSection = runSection, person = person, stdin = stdin, stdout = stdout, interactive=interactive, initializeTaskPath=initializeTaskPath)
    else:
      ret = self._deploy(task, taskPath, cwd=cwd, runSection = runSection, person = person, stdin = stdin, stdout = stdout, interactive=interactive, initializeTaskPath=initializeTaskPath)

    return ret

  def _deploy(self, task, taskPath, cwd=None, mountPath=None, pathsFor=[], buildObject=None, buildPath=None, runSection = None, person = None, stdin = None, stdout = None, interactive=False, initializeTaskPath=True):
    """ Deploys the given task.
    """

    # Ask the StorageManager for paths for each object at its revision
    paths = self.pathsForTask(task,
                              buildObject    = buildObject,
                              buildPath      = buildPath,
                              person         = person)

    rootPath = os.path.join(taskPath, "task", "root")

    running = task.get('runs', task.get('builds', {}))

    paths['root'] = rootPath
    paths['task'] = os.path.join(taskPath, "task")
    paths['home'] = taskPath

    # Determine the host path for the current working directory
    if cwd is not None:
      # Allow the override of the CWD as given to the function
      paths['cwd'] = cwd
    else:
      # Form the CWD to point to the run path's local directory for the
      # running object.
      paths['cwd'] = running.get('paths', {}).get('cwd')
      taskLocal = running.get('paths', {}).get('taskLocal')
      paths['cwd'] = paths['cwd'][len(taskLocal):]
      if paths['cwd'][0] == '/':
        paths['cwd'] = paths['cwd'][1:]
      paths['cwd'] = os.path.join(paths['task'], paths['cwd'])

    if initializeTaskPath:
      self.installTask(rootPath, task, task, paths)

    for k, v in paths.items():
      # Skip the base entries such as paths['root']
      if isinstance(v, str):
        continue

      for item in pathsFor:
        if item.get('id') == v.get('id') and item.get('revision') == v.get('revision'):
          v['path'] = item['path']

      if v.get('index') == task.get('runs', task.get('builds', {})).get('index'):
        if mountPath is not None:
          v['path'] = mountPath

        if cwd is not None:
          v['localPath'] = cwd
          localMount  = task.get('runs', task.get('builds', {})).get('paths', {}).get('localMount')
          localMounts = task.get('runs', task.get('builds', {})).get('paths', {}).get('local', [])
          if len(localMounts) > 0:
            arch = task.get('provides', {}).get('architecture')
            environ = task.get('provides', {}).get('environment')
            localMount = localMounts.get(arch, {}).get(environ, localMount)
          v['localMount'] = os.path.join(localMount, "local")

        # Install stdin file if we have data
        if stdin is not None:
          runningTaskLocal = os.path.join(taskPath, "objects", str(v['index']))
          if not os.path.exists(runningTaskLocal):
            os.makedirs(runningTaskLocal, exist_ok=True)
          stdinPath = os.path.join(runningTaskLocal, "stdin")
          with open(stdinPath, "w+") as f:
            f.write(stdin)

    network = self.installNetwork(task)

    self.writeTask(task, taskPath)
    self.writeNetwork(network, taskPath)

    return (task, paths, network, stdout)

  def execute(self, task, paths, network, stdout, ignoreStdin = False):
    # Keep track of date
    date = datetime.datetime.utcnow().isoformat()

    taskPath = paths['home']

    # Get stdin stream
    stdin = None
    #if not ignoreStdin:
    #  stdinPath = os.path.join(taskPath, "stdin")
    #  stdin = open(stdinPath, "rb")

    # Run that task (keep track of time)
    elapsed = time.perf_counter()
    backendExitCode = self.backends.run(task, paths, network, stdout, stdin)
    elapsed = time.perf_counter() - elapsed

    self.uninstallNetwork(network)

    self.cleanupTaskOutputFilenames(task, taskPath)

    # Look at the run.json if it exists
    runReport = self.pullRunReport(task, taskPath)

    exitCode = runReport.get('code', backendExitCode)

    if exitCode:
      self.updateRunReportOnError(task, taskPath, runReport=runReport)

    return {
      'time': elapsed,
      'date': date,
      'code': exitCode,
      'runReport': runReport,
      'machineInfo': self.system.machineInfo(),
      'occamInfo': self.system.localInfo(),
      'paths': paths,
      'id': task.get('id')
    }

  def getRunReportPath(self, task, taskPath):
    objectIndex = task.get('runs', task.get('builds', {}))['index']
    return os.path.join(taskPath, "task", "objects", str(objectIndex), "run.json")

  def updateRunReportOnError(self, task, taskPath, runReport=None):
    if runReport is None:
      runReport = self.pullRunReport(task, taskPath)

    # Write a modified report if an error occurred during execution, but
    # wasn't noted in the run report.
    if runReport.get('status', '') != 'failed':
      runReport['status'] = 'failed'
      runReportPath = self.getRunReportPath(task, taskPath)
      try:
        with open(runReportPath, 'w+') as f:
          json.dump(runReport, f)
      except:
        # FIXME: Log errors at least.
        pass

  def updateTaskWithRuntimeRequests(self, task, interactive, root = None):
    """ Inserts runtime tags into each provider in the task.

    The interactive parameter indicates that the task wants to run as an
    interactive process.

    The root parameter should be None as it is only used when the function
    recurses.
    """

    if root is None:
      root = task

    if interactive:
      task['interactive'] = interactive

    for subA in task.get('running', []):
      for subB in subA.get('objects', []):
        if "provides" in subB and "run" in subB:
          self.updateTaskWithRuntimeRequests(subB, interactive, root)

  def pullRunReport(self, task, taskPath):
    """ Returns the array of run sections found within a run.json if it exists.
    """

    runReportPath = self.getRunReportPath(task, taskPath)

    if not os.path.exists(runReportPath):
      return {}

    import json

    ret = {}

    with open(runReportPath) as f:
      try:
        ret = json.load(f)
      except:
        ret = {}

    if isinstance(ret, list):
      ret = {
        "phases": ret
      }

    return ret

  def create(self, task, taskId=None, revision=None, person=None,
                         initialize=None, initializeTag=None, finalize=None,
                         finalizeTag=None, interactive=False, existingPath=None,
                         target=None, object=None):
    identity = None
    if person:
      identity = person.identity

    # Choose the default target from the occam configuration
    if target is None:
      target = 'default'

    if target == 'null':
      targetInfo = {
        'scheduler': 'null'
      }
    else:
      targetsConfig = self.configuration.get('targets', {})
      targetInfo = targetsConfig.get(target)

    # We must have a target to queue a job
    if targetInfo is None:
      raise RuntimeError(f"No job scheduler targets available")

    # The default scheduler is the occam scheduler
    scheduler = targetInfo.get('scheduler', 'occam')

    # TODO: Merge in base configuration from `jobs.plugins.{scheduler}`

    kind = ""
    if 'builds' in task and 'type' in task and task['type'] == "task":
      kind = "build"
    elif 'type' in task and task['type'] == "task":
      kind = "run"

    # Get the object this job is attached to
    objectId = None
    objectRevision = None
    if object:
      objectId = object.id

      if object.link:
        objectRevision = "#" + object.link
      else:
        objectRevision = object.revision

    job = self.datastore.createJob(
      taskId         = taskId or task.get('id'),
      taskBackend    = task.get('backend', ''),
      taskName       = task.get('name', "NoName"),
      taskRevision   = revision,
      objectId       = objectId,
      objectRevision = objectRevision,
      path           = existingPath,
      initialize     = initialize or "jobs",
      initializeTag  = initializeTag,
      interactive    = interactive,
      finalize       = finalize or "jobs",
      finalizeTag    = finalizeTag,
      scheduler      = scheduler,
      target         = target,
      identity       = identity,
      status         = "queued",
      kind           = kind,
      queueTime      = datetime.datetime.now()
    )

    # Report the job to the scheduler itself.
    driver = self.driverFor(job.scheduler)
    if driver:
      if not driver.queue(job, person, targetInfo):
        self.failed(job)
        return None

    return job

  def retrieve(self, status=None, targets=None, kind=None, task=None, job_id=None):
    """ Retrieves a list of JobRecord items for the given criteria.

    By default, if status and/or targets, etc, are not given, the resulting listing
    will not filter by that respective field.

    Arguments:
      status (list): A set of job status strings to filter the listing.
      targets (list): A set of target strings to filter the listing.
      kind (str): The kind of job: run or build.
      task (str): The task identifier.
      job_id (str): The job identifier.

    Returns:
      list: A set of JobRecord items matching the given filters.
    """

    return self.datastore.retrieveJobs(status = status, targets = targets,
                                       kind = kind, task_id = task, id = job_id)

  def retrieveCount(self, status=None, targets=None):
    """ Retrieves the aggregate statistics for the optionally filtered jobs.

    By default, if status and/or targets are not given, the resulting listing
    will not filter by that respective field.

    Arguments:
      status (list): A set of job status strings to filter the listing.
      targets (list): A set of target strings to filter the listing.

    Returns:
      dict: A dictionary keyed by job status that contains aggregate information.
    """

    return self.datastore.retrieveJobCounts(status = status, targets = targets)

  def pull(self, scheduler="occam"):
    """ Atomically retrieves a job to run.

    This will pull a job while also marking it as 'started'.

    Returns:
      JobRecord: An available Job or None if no jobs are on the queue.
    """

    return self.datastore.pullJob(scheduler = scheduler)

  def initialized(self, job_or_job_id):
    """ Marks the job as having done its init phase.
    """

    import datetime

    job = self.jobFromId(job_or_job_id)

    self.datastore.updateJob(job.id, status = "initialized",
                                     initTime = datetime.datetime.now())

  def ran(self, job_or_job_id):
    """ Marks the job as having ran.
    """

    import datetime

    job = self.jobFromId(job_or_job_id)

    self.datastore.updateJob(job.id, status = "ran",
                                     runTime = datetime.datetime.now())

  def finished(self, job_or_job_id):
    """ Marks the job as complete.
    """

    import datetime

    job = self.jobFromId(job_or_job_id)

    self.datastore.updateJob(job.id, status = "finished",
                                     finishTime = datetime.datetime.now())

  def failed(self, job_or_job_id):
    """ Marks the job as failed.
    """

    import datetime

    job = self.jobFromId(job_or_job_id)

    self.datastore.updateJob(job.id, status = "failed",
                                     finishTime = datetime.datetime.now())

  def ping(self, job):
    """ Report the job status to its scheduler.
    """

    driver = self.driverFor(job.scheduler)
    if driver:
      driver.ping(job)

    # Pings always return success
    return True

  def personFor(self, job):
    """ Gets the Person that queued this task.
    """

    person = None
    if job.identity:
      person = self.accounts.retrievePerson(job.identity)

    return person

  def queue(self, task, person, interactive, finalize=None, target=None):
    """ Queue a task to be run.

    When the target is not specified, it will use the 'default' target
    specified in the Occam configuration under 'jobs.targets.default'.

    Arguments:
      task (Object): The task to queue.
      person (Person): The actor requesting the job.
      interactive (bool): Whether or not the job is interactive.
      target (str): The target to use for queuing the job.
    """

    if task is None:
      raise TypeError("queue requires a task")

    #print("DEBUG JOB- queue")
    # Gather information about the task being queued
    taskInfo = self.objects.infoFor(task)
    taskInfo['id'] = task.id

    JobManager.Log.write("Queuing: %s" % (taskInfo.get('name', 'unnamed')))

    # Get the interactivity of the requested job
    interactive = interactive or taskInfo.get('interactive', False)

    # Create a Job record to track the job progress
    job = self.create(taskInfo, task.id, revision = task.revision,
                                         person = person,
                                         interactive = interactive,
                                         finalize = finalize,
                                         target = target)

    return job.id

  def run(self, job, interactive=False, stdout=None, person=None):
    """ Deploy the phases for the given job, then run the job.

    Arguments:
      job (JobRecord): The job that should be run.
      interactive (bool): If stdin input needs to be used for this run.
                          Ultimately determines if stdin gets piped into the
                          process through the backend.
      stdout (BytesIO): An optional stream to which it will write job output.
      person (Person): The actor running the job.

    Returns:
      None if the job could not be run, otherwise a tuple of
      [task, paths, network, stdout].
    """

    print("DEBUG JOB - run")
    # Get the task itself
    taskObject = self.taskFor(job, person)

    # Now, get the task info
    task = None
    if taskObject:
      task = self.objects.infoFor(taskObject)
    else:
      task = self.stagedTaskFor(job, person = person)

    if task is None:
      # Error
      job.status = "failed"
      print("DEBUG JOB - run - FAILED!")
      return None

    taskPath = None
    if job.path:
      # Deploy within an existing path
      taskPath = os.path.realpath(os.path.join(job.path, ".."))

      # If that path does not exist, then we don't
      if not os.path.exists(taskPath):
        taskPath = None

    taskPath = None

    revision = None
    if taskObject:
      revision = taskObject.revision

    # Ensure the id is in the task manifest, if known
    # It is used to determine the eventual build path
    if taskObject:
      task['id'] = taskObject.id

    # Deploy each phase
    task, paths, network, stdout = self.deploy(task,
                                               revision    = revision,
                                               person      = person,
                                               taskPath    = taskPath,
                                               interactive = interactive,
                                               stdout      = stdout)

    # Set the job's path
    if paths:
      taskPath = paths['home']

      self.updatePath(job, paths['task'])

      # Execute the task
      run = self.execute(task, paths, network, stdout)

      # Write run report
      runReportPath = self.getRunReportPath(task, taskPath)
      with open(runReportPath, 'w+') as f:
        f.write(json.dumps(run))

      if run.get('code', 0) or run.get('runReport', {}).get('code', 0):
        job.status = 'failed'
        return None

      # Deploy secondary phases
      for runSection in run['runReport'].get('phases', []):
        data = self.deploy(task,
                           revision    = taskObject.revision,
                           runSection  = runSection,
                           person      = person,
                           uuid        = run['id'],
                           taskPath    = taskPath,
                           interactive = interactive,
                           stdout      = stdout)
        run = self.execute(*data)
        if run.get('code', 0) or run.get('runReport', {}).get('code', 0):
          job.status = 'failed'
          return None

    return task, paths, network, stdout

  def removeRunFolder(self, taskInfo, taskPath = None, person = None):
    """ Remove the run folder of the indicated task object.

    The run folder will not be removed if the task belongs to a staged object.

    Arguments:
      taskInfo (dict): The task information the indicates the run folder via id.
      taskPath (str): The exact task path.
      person (Person): Person who ran the job.
    """

    # Get the metadata for the object being built or invoked within this task
    objectInfo = taskInfo.get('runs', taskInfo.get('builds', {}))

    # If there is a 'stage' identifier (link id) do not remove the run path
    if objectInfo.get('stage', {}).get('id') is not None:
      JobManager.Log.warning(f"Not deleting run folder of staged object")
      return

    # Retrieve the logical path for the task
    taskPath = taskPath or self.pathForUUID(taskInfo.get('id'))
    if taskPath is None:
      return

    try:
      shutil.rmtree(taskPath)
      JobManager.Log.noisy(f"Deleted run folder {taskPath}")
    except FileNotFoundError as e:
      # This is fine. We just wanted the directory gone.
      JobManager.Log.noisy(f"Can't delete non-existing run folder of job")
    except (OSError, IOError) as e:
      JobManager.Log.warning(f"Failed to remove run path {taskPath}: {e}")

  def deploy(self, task, revision, local=False, arguments = None, command = None, person = None, runSection = None, stdin = None, stdout = None, uuid = None, taskPath = None, interactive=False, cwd = None):
    """ Deploys the given task.

    Returns:
      int: The exit code of the deployed process.
    """

    try:
      if not task.get('builds') is None:
        return self.deployBuild(task, revision=revision, local=local, command = command, runSection = runSection, person = person, interactive=interactive, cwd=cwd)
      elif not task.get('runs') is None:
        return self.deployRun(task, revision=revision, local=local, arguments = arguments, command = command, person = person, runSection = runSection, stdin = stdin, stdout = stdout, uuid = uuid, taskPath = taskPath, interactive=interactive, cwd=cwd)
    except ValueError as e:
      JobManager.Log.error(f"Couldn't deploy task: {e}")

    # We don't understand this task
    return [None, None, None, None]

  def signal(self, task, signal = 9):
    """ Submits the given signal to the running task.
    """

    if task is None:
      return None

    self.backends.signal(task, signal)

  def terminate(self, job):
    """ Terminates the given job.
    """

    # If this is a remote job, we cancel via the scheduler
    # Determine the task so that the job process can be found
    task = self.taskInfoFor(job.id)
    if task:
      # Having a task means that the job was deployed.
      # Have the backend kill the task
      ret = self.backends.terminate(task)

      # If the backend fails to kill the task,
      # we then kill any scheduler worker that is running it.
      if ret != 0:
        pid = self.pidFor(job = job)
        if pid:
          import os
          import signal as UNIXSignal
          os.killpg(pid, UNIXSignal.SIGKILL)
    else:
      driver = self.driverFor(job.scheduler)
      if driver:
        driver.cancel(job)
        
    # Canceled jobs are failures
    self.failed(job.id)
    return 0

  def pathForUUID(self, uuid):
    """ Return the absolute path for the directory of the given UUID if it were
    stored under the given subpath.

    This will be something like
    '$OCCAM_ROOT/runs/d30154cc-8ef3-11ea-9f43-0242ac11005f'.
    """

    if uuid is None:
      return None

    globalPaths = self.configuration.get('paths', {})
    absSubPath = globalPaths.get('runs', os.path.join(Config.root(), 'runs'))

    if absSubPath is None:
      # The configuration is in a state where we don't know where we can store
      # runs for this subpath.
      return None

    return os.path.join(absSubPath, uuid)

  def createPathFor(self, uuid):
    """ Creates a directory in which a runtime environment will be instantiated.

    Does not delete existing directories.

    Arguments:
      uuid (str): A unique identifier representing this particular invocation.

    Returns:
      str: The absolute path to the directory.
    """

    if uuid is None:
      return None

    path = self.pathForUUID(uuid)
    os.makedirs(path, exist_ok=True)

    return path

  def cleanupTaskOutputFilenames(self, task, path, root=None):
    """ Rename the output files to indicate which phase it belonged to.
    """

    if root is None:
      root = task

    # Move stdout
    # Look at running task
    runningObject = root.get('runs', root.get('builds'))
    runningTaskLocal = os.path.join(path, "task", "objects", str(runningObject['index']))

    stdoutPath = os.path.join(runningTaskLocal, "stdout")
    stderrPath = os.path.join(runningTaskLocal, "stderr")

    # Rename the stdout and stderr files to differentiate different run outputs.
    for oldPath in [stdoutPath, stderrPath]:
      if os.path.exists(oldPath):
        i = 0
        while True:
          newPath = oldPath + ".%s" % (str(i))
          i += 1

          if not os.path.exists(newPath):
            break

        os.rename(oldPath, newPath)

  def _prepareTaskObjectPath(self, task, path, root=None, build=None, local=False, person=None):
    """ Create the directories that the given task's object requires.
    """

    import json

    objectPath = os.path.join(path, "objects", str(task['index']))
    os.makedirs(objectPath, exist_ok=True)

    inputsPath  = os.path.join(objectPath, "inputs")
    outputsPath = os.path.join(objectPath, "outputs")

    inputSymlinkPath  = os.path.join(objectPath, "input")
    outputSymlinkPath = os.path.join(objectPath, "output")

    localPath = os.path.join(objectPath, "local")

    os.makedirs(inputsPath, exist_ok=True)
    os.makedirs(outputsPath, exist_ok=True)
    os.makedirs(localPath, exist_ok=True)

    # When this is a build task, there is also a place for
    # built version of objects to go.
    if build and task['index'] == root['builds']['index']:
      buildPath = os.path.join(objectPath, "build")

      os.makedirs(buildPath, exist_ok=True)

      # Instantiate the object in the local path
      if not local:
        self.storage.instantiate(task['uid'], task['revision'], localPath)
        buildObject = self.objects.retrieve(task['id'], revision=task['revision'], person = person)

        self.objects.install(buildObject, localPath, build = True)

    for idx, outputInfo in enumerate(task.get('outputs', [])):
      subOutputPath = os.path.join(outputsPath, str(idx))

      os.makedirs(subOutputPath, exist_ok=True)

      # Allow for at least one output
      subSubOutputPath = os.path.join(subOutputPath, "0")

      os.makedirs(subSubOutputPath, exist_ok=True)

    # Link inputs to their object contents when applicable
    for idx, wire in enumerate(task.get('inputs', [])):
      subInputPath = os.path.join(inputsPath, str(idx))

      os.makedirs(subInputPath, exist_ok=True)

      for inputIndex, inputInfo in enumerate(wire.get('connections', [])):
        subSubInputPath = os.path.join(subInputPath, str(inputIndex))

        if inputInfo.get('id') and inputInfo.get('revision') and inputInfo.get('paths'):
          if not os.path.lexists(subSubInputPath):
            os.symlink(inputInfo['paths'].get('mount'), subSubInputPath)
        else:
          os.makedirs(subSubInputPath, exist_ok=True)

    # Link ../input to ../inputs/0/0 as a convenience
    if not os.path.lexists(inputSymlinkPath):
      os.symlink(os.path.join("inputs", "0", "0"), inputSymlinkPath)

    # Link ../output to ../outputs/0/0 as a convenience
    if not os.path.lexists(outputSymlinkPath):
      os.symlink(os.path.join("outputs", "0", "0"), outputSymlinkPath)

    # TODO: link inputs to output paths when appropriate

    subTaskManifestPath = os.path.join(objectPath, "task.json")

    # Place the task component into this path
    f = open(subTaskManifestPath, "w+")
    json.dump(task, f, indent=2, separators=(',', ': '))
    f.close()

  def prepareTaskPath(self, task, path, root=None, build=None, local=False, person=None):
    """ Creates all folders and files needed for the given task.
    """

    if build is None:
      build = 'builds' in task

    if root is None:
      root = task

    # Create a path that represents contents in root
    rootPath = os.path.join(path, "root")
    os.makedirs(rootPath, exist_ok=True)

    # Create a path that holds local data for the objects
    objectsPath = os.path.join(path, "objects")
    os.makedirs(objectsPath, exist_ok=True)

    # Create an events file/socket to communicate any actions of objects
    eventsPath = os.path.join(path, "events")
    if not os.path.exists(eventsPath):
      with open(eventsPath, "w+") as f:
        pass

    # For this object, add its object path
    if 'index' in task and ('run' in task or (build and task['index'] == root['builds']['index'])):
      self._prepareTaskObjectPath(task, path, root, build, local, person)

    # For each object, create the directory it runs in
    for wire in task.get('inputs', []):
      for obj in wire.get('connections', []):
        self.prepareTaskPath(obj, path, root=root, build=build, local=local, person = person)

    for subTask in task.get('running', []):
      for obj in subTask.get('objects', []):
        self.prepareTaskPath(obj, path, root=root, build=build, local=local, person = person)

  def _buildPathForOwner(self, obj, revision, owner_uid, cache):
    # Discover the built version of this object (or build it?? or report that
    # we need to build it??)
    if obj is None:
      return None, None

    builds = self.builds.retrieveAll(obj)

    path = None
    buildId = None

    # For every build we know, we attempt to find that build's content.
    for build in builds:
      path = self.builds.pathFor(owner_uid, revision, build.id, cache = cache)

      if path is not None:
        buildId = build.id
        break

    return path, buildId

  def _pathForBuildTask(self, task, root, owner_uid, revision, buildPath=None):
    localMount  = root.get('builds', {}).get('paths', {}).get('localMount')
    localMounts = root.get('builds', {}).get('paths', {}).get('local')

    if localMounts is not None and len(localMounts) > 0:
      arch = task.get('provides', {}).get('architecture')
      environ = task.get('provides', {}).get('environment')
      localMount = localMounts.get(arch, {}).get(environ, localMount)

    localPath = localMount
    if localPath is not None:
      localPath = os.path.join(localPath, "build")

    if buildPath is None:
      # Object would have already been discovered?
      # TODO: Assess the truth of the above haha
      buildPath = self.builds.pathFor(owner_uid, revision = revision,
                                                 buildId = root.get('id'),
                                                 create = True)

    return {"buildMount": localPath, "buildPath": buildPath}

  def pathsForObject(self, inputObject, task, root = None, buildPath = None, person=None):
    """ Used internally to gather the paths for the given object within a task.
    """

    if root is None:
      root = task

    id = inputObject.get('id')
    uid = inputObject.get('uid')

    # If the input object is a subobject, we want the containing object.
    owner_id = inputObject.get('owner', {}).get('id', id)
    owner_uid = inputObject.get('owner', {}).get('uid', uid)
    revision = inputObject.get('revision')
    buildId  = inputObject.get('build', {}).get('id')

    link = inputObject.get('stage', {}).get('id')

    obj = self.objects.retrieve(id = owner_id, revision = revision, person = person, link = link)
    #ownerInfo = inputObject.get('owner', self.objects.infoFor(obj))

    # This determines if the build should be a thing or not
    # Local builds (and subsequent runs) mess this up so much!

    # TODO: use this when it is a local run!
    #built = not inputObject.get('index') == root.get('builds', root.get('runs', {})).get('index') and 'build' in ownerInfo
    #built = not inputObject.get('index') == root.get('builds', {}).get('index') and 'build' in ownerInfo
    built = False

    # Do we need to install content alongside the build?
    cache = False
    if inputObject.get('install'):
      cache = True

    path = None
    if obj is not None:
      if built:
        JobManager.Log.noisy(f"Looking up build path for object owner {owner_id}")
        path, build = self._buildPathForOwner(obj, revision, owner_uid, cache)
        if path is not None:
          buildId = build
      elif not buildId is None:
        JobManager.Log.noisy(f"Looking up build path for object {owner_id}")
        path = self.objects.buildPathFor(obj, buildId, cache = cache)
      elif link:
        JobManager.Log.noisy(f"Looking up build path for staged object {owner_id}")
        path = obj.path
      else:
        JobManager.Log.noisy(f"Looking up build path for the instantiation of object {owner_id}")
        path = self.objects.instantiate(obj)
    else:
      raise RuntimeError(
        f"Can't retrieve the object {owner_id} with revision {revision}"
      )

    if buildPath is None and path is None:
      raise RuntimeError(
        f"Couldn't find the path for the built object {owner_id} with "
        f"revision {revision} and build id {buildId}"
      )

    mountPath  = inputObject.get('paths', {}).get('mount')
    mountPaths = inputObject.get('paths', {}).get('volume', {})

    if mountPaths:
      arch = root.get('provides', {}).get('architecture')
      environ = root.get('provides', {}).get('environment')
      mountPath = mountPaths.get(arch, {}).get(environ, mountPath)

    pathInfo = {
      "id": owner_id,
      "revision": revision,
      "index": inputObject.get('index'),
      "mount": mountPath,
      "path": path
    }

    if 'builds' in root:
      # This is a build task.
      if root['builds'].get('index') == inputObject.get('index'):
        pathInfo.update(self._pathForBuildTask(task, root, owner_id, revision, buildPath))
    elif 'runs' in root:
      # This is a run task
      if root['runs'].get('index') == inputObject.get('index'):
        if not buildPath is None:
          pathInfo["path"] = buildPath

    return pathInfo

  def pathsForTask(self, task, buildObject=None, root=None, create=False, buildPath=None, person=None):
    """ Returns the paths to each object given in a task.
    """

    if root is None:
      root = task

    ret = {}

    for wire in task.get('inputs', []):
      for inputObject in wire.get('connections', []):
        if 'index' in inputObject:
          pathInfo = self.pathsForObject(inputObject, task, root = root, buildPath = buildPath, person = person)
          ret[inputObject.get('index')] = pathInfo

          # Recursively find paths to subobjects
          ret.update(self.pathsForTask(inputObject, buildObject=buildObject, root=root, create=create, buildPath=buildPath, person = person))

    for subTask in task.get('running', []):
      for inputObject in subTask.get('objects', []):
        if 'index' in inputObject:
          pathInfo = self.pathsForObject(inputObject, task, root = root, buildPath = buildPath, person = person)
          ret[inputObject.get('index')] = pathInfo

          # Recursively find paths to subobjects
          ret.update(self.pathsForTask(inputObject, buildObject=buildObject, root=root, create=create, buildPath=buildPath, person = person))

    return ret

  def installNetwork(self, task, root = None, output = None):
    """ Instantiates the network knowledge for this object.

    This will allocate ports when necessary.
    """

    if root is None:
      root = task

    if output is None:
      output = {
        "ports": []
      }

    # Look for a network section
    for port in task.get('network', {}).get('ports', []):
      if 'bind' in port:
        systemPort = self.network.allocatePort();
        JobManager.Log.write("allocating system port %s to %s" % (systemPort, port.get('bind', 0)))
        port['port'] = systemPort
        output['ports'].append(port)

    for wire in task.get('inputs', []):
      for inputObject in wire.get('connections', []):
        self.installNetwork(inputObject, root, output)

    for subTask in task.get('running', []):
      for inputObject in subTask.get('objects', []):
        self.installNetwork(inputObject, root, output)

    return output

  def uninstallNetwork(self, network):
    """ Unallocates resources allocated by installNetwork.
    """

    for portInfo in network.get('ports', []):
      if 'port' in portInfo:
        systemPort = portInfo['port']
        JobManager.Log.write("freeing system port %s" % (systemPort))
        self.network.freePort(systemPort)

  def installTask(self, rootPath, task, root=None, paths={}):
    """
    """

    if root is None:
      root = task

    # For this object, add its install paths to root
    if 'init' in task and 'index' in task:
      pathInfo = paths.get(task.get('index'))

      if pathInfo:
        JobManager.Log.noisy("Instantiating %s %s %s %s@%s" % (task.get('type', 'object'), task.get('name', 'unnamed'), task['id'], task.get('version', ''), task['revision']))

        # For each install object that is a local path, symlink to its mount path
        mountPath  = task.get('paths', {}).get('mount')
        mountPaths = task.get('paths', {}).get('volume', {})

        if ("builds" in root and root["builds"]["id"] == task["id"] and root["builds"]["revision"] == task["revision"]):
          mountPath = task.get('paths', {}).get('cwd')

        if task.get('mount') == 'ignore':
          # Remove from the mount paths
          del paths[task['index']]
        else:
          if len(mountPaths) > 0:
            arch = root.get('provides', {}).get('architecture')
            environ = root.get('provides', {}).get('environment')
            mountPath = mountPaths.get(arch, {}).get(environ, mountPath)

          # Link files into the system
          for linkInfo in task['init'].get('link', []):
            # TODO: check if this is a resource (it has an 'id' and 'revision')
            # this would be added as a dependency.

            if not 'id' in linkInfo:
              sourcePath = linkInfo.get('source')
              if sourcePath[0] == '/':
                sourcePath = sourcePath[1:]
              installPath = os.path.join(mountPath, sourcePath)
              installDestination = linkInfo.get('to')
              localPath = os.path.join(pathInfo['path'], sourcePath)

              # Create symlinks for each item in the task
              self.linkTaskDirectory(rootPath, localPath, installPath, installDestination)

        # Copy files into the system
        for copyInfo in task['init'].get('copy', []):
          if not 'id' in copyInfo:
            sourcePath = copyInfo.get('source')
            if sourcePath[0] == '/':
              sourcePath = sourcePath[1:]
            installPath = os.path.join(mountPath, sourcePath)
            installDestination = copyInfo.get('to')
            localPath = os.path.join(pathInfo['path'], sourcePath)

            # Determine if the destination path is mounted to the current
            # working directory. If so, we copy the files to that host path.
            cwd = task.get('paths').get('cwd')
            if installDestination.startswith(cwd):
              # Ensure it creates the files in the cwd
              installDestination = installDestination[len(cwd):]
              rootPath = paths['cwd']

            # Create symlinks for each item in the task
            self.copyTaskDirectory(rootPath, localPath, installPath, installDestination)

    for wire in task.get('inputs', []):
      for inputObject in wire.get('connections', []):
        # Recursively find paths to subobjects
        self.installTask(rootPath, inputObject, root, paths)

    for subTask in task.get('running', []):
      for inputObject in subTask.get('objects', []):
        # Recursively find paths to subobjects
        self.installTask(rootPath, inputObject, root, paths)

  def copyTaskDirectory(self, rootPath, localPath, mountPath, destinationPath, recurse=True):
    """ Copies the files from the local path to the given destination.
    """

    # Ensure that the destination path exists
    localDestinationPath = os.path.join(rootPath, destinationPath[1:])

    if not os.path.exists(localPath):
      return

    if os.path.isdir(localPath):
      files = os.listdir(localPath)

      if os.path.lexists(localDestinationPath) and os.path.islink(localDestinationPath):
        os.remove(localDestinationPath)

      os.makedirs(localDestinationPath, exist_ok=True)

      for filename in files:
        copyDestination = os.path.join(localDestinationPath, filename)
        copySource = os.path.join(localPath, filename)
        if os.path.isdir(copySource):
          if recurse:
            self.copyTaskDirectory(rootPath, copySource, None, os.path.join(destinationPath, filename))
        else:
          if os.path.lexists(copyDestination):
            os.remove(copyDestination)
          shutil.copy(copySource, copyDestination, follow_symlinks=False)
    else:
      copyDestination = localDestinationPath
      copySource = localPath

      os.makedirs(os.path.dirname(localDestinationPath), exist_ok=True)

      if os.path.lexists(copyDestination):
        os.remove(copyDestination)

      shutil.copy(copySource, copyDestination, follow_symlinks=False)

  def linkTaskDirectory(self, rootPath, localPath, mountPath, destinationPath, recurse=True):
    """ Creates symlinks at destination that point to the mounted paths by walking the
    localPath.

    The localPath gives a local view on what will be seen inside the
    virtual machine. We can duplicate that structure and point them to each file
    with a path relative to the virtual machine (mountPath)
    """

    # Ensure that the destination path exists
    localDestinationPath = os.path.join(rootPath, destinationPath[1:])

    if os.path.lexists(localPath):
      if not os.path.islink(localPath) and os.path.isdir(localPath):
        files = os.listdir(localPath)

        if os.path.lexists(localDestinationPath) and os.path.islink(localDestinationPath):
          os.remove(localDestinationPath)

        if not os.path.exists(localDestinationPath):
          os.makedirs(localDestinationPath, exist_ok=True)

        for filename in files:
          localSymlink = os.path.join(localDestinationPath, filename)
          localFilePath = os.path.join(localPath, filename)

          # We always replace the symlink with a new one
          if os.path.lexists(localSymlink) and os.path.islink(localSymlink):
            os.remove(localSymlink)

          # If the file to copy in is a directory, then override any link
          # and then recurse into that directory
          if not os.path.islink(localFilePath) and os.path.isdir(localFilePath):
            # TODO: intelligently detect when we can just symlink the directory
            if recurse:
              self.linkTaskDirectory(rootPath, os.path.join(localPath, filename), os.path.join(mountPath, filename), os.path.join(destinationPath, filename))
          else:
            destination = os.path.join(mountPath, filename)

            # We simply link to the same place as a normal link (copy the link)
            if os.path.islink(localFilePath):
              destination = os.readlink(localFilePath)

            # We won't replace a directory with a symlink
            if not os.path.exists(localSymlink):
              os.symlink(destination, localSymlink)
      else:
        destination  = mountPath
        localSymlink = localDestinationPath

        try:
          os.makedirs(os.path.dirname(localDestinationPath))
        except FileExistsError:
          pass

        # We always replace the symlink with a new one
        if os.path.lexists(localSymlink) and os.path.islink(localSymlink):
          os.remove(localSymlink)

        # We simply link to the same place as a normal link (copy the link)
        if os.path.islink(localPath):
          destination = os.readlink(localPath)

        if not os.path.lexists(localSymlink):
          os.symlink(destination, localSymlink)

  def getObjectFromTask(self, task, id, revision):
    """ Finds the first instance of the given object in the task.
    """

    # Gather the objects that are represented within the task
    inputs = task.get('inputs', [])
    inputs += reduce((lambda x, y: x + y), map(lambda x: x.get('objects', []), task.get('running', [])), [])

    # For each object in the current environment layer of the given task,
    # We will see if that object matches the id and revision requested.
    for obj in inputs:
      if obj.get('id') == id and obj.get('revision') == revision:
        return obj
      else:
        # Recursively search the next layer in the task
        ret = self.getObjectFromTask(obj, id, revision)
        if not ret is None:
          return ret

    return None

  def jobFromId(self, job_or_job_id):
    """ Returns the first job that exists corresponding to the job / job_id passed
    in. Returns None otherwise.
    """

    from occam.jobs.records.job import JobRecord

    if isinstance(job_or_job_id, int) or isinstance(job_or_job_id, str):
      jobs = self.retrieve(job_id = job_or_job_id)

      if len(jobs) != 0:
        return jobs[0]
    elif isinstance(job_or_job_id, JobRecord):
      return job_or_job_id

    return None

  def stagedTaskFor(self, job_or_job_id, person=None):
    """ Retrieves the staged task for the given job, if it exists.
    """

    # Our goal is to set this to the task manifest dict
    task = None

    # Retrieve the job record
    job = self.jobFromId(job_or_job_id)

    # Bail if the job could not be found
    if job is None:
      return None

    # Get the revision stored in the job record
    id = job.task_uid
    revision = job.task_revision

    # This starts with a '#' when it is a link identifier
    if revision and revision[0] == '#':
      # When this is true, the id is the object identifier
      # Resolve the object at the stage
      staged = self.objects.retrieve(id, link = revision[1:],
                                         person = person)

      if staged is None:
        return None

      # Get the appropriate task for the desired phase
      task = self.links.taskFor(staged, build = (job.kind == 'build'))

    return task

  def taskFor(self, job_or_job_id, person = None):
    """ Returns the task for the given job as the given Person.
    """

    job = self.jobFromId(job_or_job_id)

    if job is None:
      return None

    # Get the task itself
    id = job.task_uid
    revision = job.task_revision

    if revision and revision[0] == '#':
      # A linked object... the task is not stored
      return None

    return self.objects.retrieve(id, revision=revision, person=person)

  def taskInfoFor(self, job_or_job_id):
    """ Returns the instantiated task for the running job, if it exists.
    """

    import json
    import codecs
    from collections import OrderedDict

    job = self.jobFromId(job_or_job_id)
    f = self.logFor(job, logType = 'task')

    taskInfo = {}

    try:
      taskInfo = json.loads(codecs.decode(f.read(), 'utf8'), object_pairs_hook=OrderedDict)
    except:
      pass

    return taskInfo

  def taskIdFor(self, job_or_job_id):
    """ Returns the instantiated id for the running job, if it exists.
    """

    taskInfo = self.taskInfoFor(job_or_job_id)

    if taskInfo is None:
      return None

    return taskInfo.get('id')

  def pidFor(self, job):
    """ Returns the process id for the given job, if it is running.

    Returns:
      process_id (int): The process id for the job runner or None if the job is
                        not running.
    """

    metadata = self.metadataFor(job.id)

    if metadata is None:
      return None

    # Get the running job's process id
    pid = metadata.get("pid")

    # Get the command name for the given process
    cmd_check = None
    try:
      with open(os.path.join("/", "proc", str(pid), "cmdline")) as f:
        cmd_check = f.read()
    except:
      pass

    # We truncate just the occam arguments since 'exec' might happen at some point
    if cmd_check is not None:
      cmd_check = '\0'.join(cmd_check.split('\0')[2:])

    # We use endswith since it may append the full path to the initial process
    # name, which we may not know
    if cmd_check is None or metadata is None or not metadata.get("check", "").endswith(cmd_check):
      return None

    return pid

  def isRunning(self, job):
    """ Returns True when the given job is currently running.

    Arguments:
      job (JobRecord): The job to query.

    Returns:
      bool: Returns True when the job is known to be running and False if it is
            not, or if it cannot be queried.
    """

    driver = self.driverFor(job.scheduler)
    if driver:
      return driver.isRunning(job)

    # We couldn't find this job's driver for some reason, so technically we
    # don't know if the job is running or not. However, callers of this
    # function usually only call to find out if they can do something to a job
    # that is running. If we can't find the driver, neither can they.
    return False

  def updatePath(self, job, path):
    """ Updates the path of the given job.
    """

    job.path = path
    self.datastore.updateJob(job.id, path = path)

  def metadataPathFor(self, job_id):
    """ Returns the absolute path to the given job's process metadata.

    Arguments:
      job_id (number): The job identifier.

    Returns:
      str: The path to the metadata JSON document.
    """

    basePath = Config.root()
    config = self.configuration
    basePath = config.get("path", os.path.join(basePath, "job-daemon"))
    return os.path.join(basePath, "%s.json" % (job_id))

  def metadataFor(self, job_id):
    """ Returns the metadata for the given job.
    """

    metadataPath = self.metadataPathFor(job_id)

    try:
      ret = json.load(open(metadataPath, "r"))
    except:
      ret = None

    return ret

  def writeMetadataFor(self, job_id, pid=os.getpid(), args=sys.argv):
    """ Writes metadata for the running process as part of this job.

    Arguments:
      job_id (number): The job identifier the current process is running.

    Returns:
      dict: The metadata that was written.
    """

    cmd_check = None
    try:
      with open(os.path.join("/", "proc", str(pid), "cmdline")) as f:
        cmd_check = f.read()
    except:
      pass

    metadata = {
      "pid": pid,
      "command": ' '.join(args),
      "check": cmd_check
    }

    metadataPath = self.metadataPathFor(job_id)

    logDir = os.path.dirname(metadataPath)
    os.makedirs(logDir, exist_ok=True)

    with open(metadataPath, "w+") as f:
      json.dump(metadata, f)

    return metadata

  def writeTo(self, job, data):
    """ Writes the given data to the given job.
    """

    pid = self.pidFor(job)

    # If the job is indeed running locally, we send the data to the process
    if pid:
      try:
        with open("/proc/%s/fd/0" % (pid), "wb") as f:
          f.write(data)
        return True
      except Exception as e:
        JobManager.Log.error(f"Could not write to job {job.id}: {e}")
        return False

    # No running process for this job locally.
    # Is it a remote job?
    if self.isRunning(job) and self.network.isURL(job.scheduler_identifier):
      # Get the node responsible
      node, remoteJobId = self.nodeFor(job)
      return self.nodes.sendToJob(node, remoteJobId = remoteJobId, data = data)

    return False

  def logPathFor(self, job_id):
    basePath = Config.root()
    config = self.configuration
    basePath = config.get("path", os.path.join(basePath, "job-daemon"))
    return os.path.join(basePath, "%s.log" % (job_id))

  def logFor(self, job, logType=None, start=0, tail=False):
    """ Retrieves the job output or metadata for the given job.

    You can negotiate the type of log using the logType argument.

    Arguments:
      job (JobRecord): The job we are querying.
      logType (str): The type of log. None specifies the default which is the
                     standard output of the job.
      start (number): The byte position to start from.
      tail (bool): A hint to whether or not we want a stream that blocks for new input.

    Returns:
      BytesIO: A stream representing the log or None if it cannot be found.
    """

    if not job:
      JobManager.Log.warning("Tried to get the log for a job that was None.")
      return None

    # Attempt to find the job log cached locally
    relativePath = None
    if logType == "network":
      relativePath = "../network.json"
    elif logType == "task":
      relativePath = "object.json"
    elif logType == "events":
      relativePath = "events"
    elif logType is not None:
      JobManager.Log.error(f"Unknown log type '{logType}' requested.")
      return None

    # The path to the log we should load is either relative to the job.path or
    # is based on the job id.
    logPath = None
    if relativePath:
      if not job.path:
        JobManager.Log.error(
          f"Can't load the {logType} log: job {job.id} has no path."
        )
        return None

      logPath = os.path.join(job.path, relativePath)
    else:
      logPath = self.logPathFor(job.id)

    logHandle = None
    # If we managed to find the log path, try to load it. If we didn't, or if
    # loading fails, try to fall back on the network if that is an option.
    if logPath:
      try:
        logHandle = open(logPath, "rb")
        logHandle.seek(start)
      except:
        # No log found locally.
        logHandle = None

    # If there is a remote identifier, we can use that to get the log from the
    # remote node.
    if not logHandle and self.network.isURL(job.scheduler_identifier):
      # Get the node responsible
      node, remoteJobId = self.nodeFor(job)

      if node:
        logHandle, _, _ = self.nodes.logForJob(node, remoteJobId, logType = logType,
                                                                  start = start,
                                                                  tail = tail)

    return logHandle

  def nodeFor(self, job):
    """ Returns the node that is running or had ran the given job.

    Arguments:
      job (JobRecord): The job in question.

    Returns:
      tuple: The node that was responsible for the job or None if not known and
             the job identifier for the remote job or None if not known.
    """

    # Get the job log over the network
    remoteJobId = None

    try:
      # Retrieve the job id from the job URI
      remoteJobId = re.search(r"/jobs/(\d+)$", job.scheduler_identifier)[1]
    except:
      pass

    node = None
    if remoteJobId:
      node = self.nodes.search(job.scheduler_identifier)

    return node, remoteJobId

  def connect(self, job):
    """ Connects our own stdin and stdout to that of the running job.

    If the job has already finished, will yield the stdout based on the log
    file. If the job is queued, connect will wait and connect when possible.

    Arguments:
      job (JobRecord): The local job we are connecting to.

    Returns:
      bool: True when the connection closed successfully.
    """

    if job is None:
      raise ValueError("Must pass a valid JobRecord to JobManager#connect")

    # We can't connect to a job until it is running. Wait until it is no longer
    # queued.
    #
    # If it is not already covered by the socket, find the local or remote log
    #
    # FIXME: Busy waiting is bad, it would be nice if we could subscribe to an
    # event that would let us watch the job status and wake on database update.
    import time
    waitIntervalSeconds = 0.5
    while job.status == "queued" or job.status == "started":
      time.sleep(waitIntervalSeconds)
      job = self.jobFromId(job.id)

    # If this is a remote job, we need to connect to the job socket instead
    # of the local job stdin/stdout
    socket = None
    if job.scheduler_identifier:
      node, remoteJobId = self.nodeFor(job)

      if node and remoteJobId:
        # Make the connection. Be sure to tag the command to ensure any
        # cooperating daemons can negotiate who should handle the request.
        cPath = f"jobs/connect/{remoteJobId}?processing-tag=job-{remoteJobId}"
        url = self.nodes.urlForNode(node, cPath)
        socket, _, _ = self.network.postOccam(url, data = None,
                                                   accept = 'application/octet-stream',
                                                   promoted = True)
      else:
        return False

    # Disconnect from the database to not hoard a connection while polling logs
    self.database.disconnect()

    # Negotiate the byte device, when available
    if hasattr(sys.stdin, "buffer"):
      stdin = sys.stdin.buffer
    else:
      stdin = sys.stdin

    # Add the stdin to our readers collection
    readers = [stdin.fileno()]

    # Gather the log file for our readers
    logFile = socket

    # Wait 50s before giving up.
    maxTries = 100
    tries = 0

    while not logFile and tries < maxTries:
      time.sleep(waitIntervalSeconds)
      logFile = self.logFor(job)
      tries += 1

    if logFile:
      # append to watch list
      readers.append(logFile)

    # While the job is running, and one of the readers has data, read it
    while socket or self.isRunning(job):
      r, _, _ = select.select(readers, [], [])

      if sys.stdin.fileno() in r:
        if hasattr(sys.stdin, "buffer"):
          d = os.read(sys.stdin.fileno(), 10240)
        else:
          try:
            d = sys.stdin.read(1024)
          except Exception as e:
            JobManager.Log.output(("Error type: %s" % (sys.exc_info()[0])).encode('utf-8'))

        # Write our stdin to the job
        if socket:
          socket.write(d)
        else:
          self.writeTo(job, d)

      # Pipe out the new log to our stdout
      if logFile and logFile in r:
        if logFile is socket:
          # Read some information from the remote job connection
          data = logFile.read(1000)

          # Is the socket closed?
          if len(data) == 0:
            # Break out of our loop
            break

          # Otherwise, pipe the socket data to our own stdout
          JobManager.Log.output(data, end="", padding="")
        else:
          JobManager.Log.pipe(logFile)

    # Flush the log to our stdout
    if logFile:
      JobManager.Log.pipe(logFile)

    return True

  def outputsFor(self, job, wireIndex = None, itemIndex = None):
    """ Retrieves a list of JobOutputRecord objects for the given job.
    """

    return self.datastore.retrieveJobOutputs(job.id, wireIndex  = wireIndex,
                                                     itemIndex  = itemIndex)

  def outputsForAll(self, jobIds, wireIndex = None, itemIndex = None, finishTime = None):
    """ Retrieves a list of JobOutputRecord objects for the given list of jobs.
    """

    jobOutputs = self.datastore.retrieveJobOutputsForAll(jobIds,
                                                   wireIndex  = wireIndex,
                                                   itemIndex  = itemIndex,
                                                   finishTime = finishTime)

    # Organize into a list of lists
    ret = []

    lastJobId = None
    for jobOutput in jobOutputs:
      if jobOutput.job_id != lastJobId:
        ret.append([])
        lastJobId = jobOutput.job_id

      ret[-1].append(jobOutput)

    return ret

  def retrieveTargets(self, status=False):
    """ Retrieves the target listing from the system configuration.

    Arguments:
      status (bool): When True, also returns target status information.

    Returns:
      dict: A set of string keyed sections depicting possible queue targets.
    """

    config = self.configuration
    targets = config.get('targets')

    # If targets is invalid, return an empty dictionary
    if not isinstance(targets, dict):
      targets = {}

    if status:
      for key, targetInfo in targets.items():
        scheduler = targetInfo.get('scheduler', 'occam')

        # By default, the status will report it as unavailable
        targetInfo['status'] = {'available': False}

        driver = self.driverFor(scheduler)
        if driver:
          targetInfo['status'] = driver.status(targetInfo)

    # Fill in any detected provides if the host is localhost
    for key, targetInfo in targets.items():
      if targetInfo.get('host') == 'localhost':
        for backend in self.backends.available():
          # Add the provided environments
          handler = self.backends.handlerFor(backend)
          for provides in handler.provides():
            found = False
            for current in targetInfo.get('provides', []):
              if current.get('environment') == provides[0] and current.get('architecture') == provides[1]:
                found = True
                break

            if not found:
              targetInfo['provides'] = targetInfo.get('provides', [])
              targetInfo['provides'].append({
                "environment": provides[0],
                "architecture": provides[1]
              })

    return targets

  def createJobOutput(self, job, object, wireIndex, itemIndex):
    """ Adds a record of the output object for this job.

    Args:
      job (JobRecord): The job that produced the output.
      object (Object): The object that was produced and stored.
      wireIndex (int): The index of the output wire.
      itemIndex (int): The position of the item on that wire.

    Returns:
      JobOutputRecord: The saved record.
    """

    JobManager.Log.write("Adding job record for %s %s %s" % (job.id, wireIndex, itemIndex))

    return self.datastore.createJobOutput(id             = job.id,
                                          objectId       = object.id,
                                          objectRevision = object.revision,
                                          wireIndex      = wireIndex,
                                          itemIndex      = itemIndex)

  def addOutput(self, obj, outputObject, person, relationship="outputOf"):
    """ Adds an output to the object.

    This is done by linking the objects together using the 'outputOf'
    relationship.

    The 'generatedBy' relationship links outputs that are generated by
    consequence of the object. For instance, a workflow 'generates' the objects
    that are 'outputs' of the nodes represented in the workflow.

    Arguments:
      obj (Object): The object that created the output.
      outputObject (Object): The output object itself.
      person (Person): The actor recording the output.
      relationship (str): The relationship type.

    Returns:
      bool: Returns True when the requested output link exists or is created.
    """

    # Get the generating object's record.
    rows = self.objects.search(id = obj.id)
    if not isinstance(rows, list) or not len(rows) > 0:
      JobManager.Log.error(f"Could not find generator object {obj.id}")
      return False

    target_db = rows[0]

    # Get the output object record.
    rows = self.objects.search(id = outputObject.id)
    if not isinstance(rows, list) or not len(rows) > 0:
      JobManager.Log.error(f"Could not find output object {outputObject.id}")
      return False

    source_db = rows[0]

    try:
      link_db = self.links.write.create(source_db = source_db,
                                        relationship = relationship,
                                        target_db = target_db,
                                        target_revision = target_db.revision,
                                        person = person,
                                        source_revision = source_db.revision)

      if link_db is None:
        JobManager.Log.error(f"Could not link output {outputObject.id} to generator {obj.id}")
        return False
    except DataNotUniqueError:
      JobManager.Log.noisy(f"Link between {obj.id} and {outputObject.id} already exists")

    return True

  def _rakeSubObjectOutput(self, task, wire, subOutputPath, wireIndex, itemIndex, generators, person, job):
    """ Collates subobject output information.

    Will create an object in the Occam store representing the output for each
    output raked.

    Arguments:
      task (dict): The parent task being raked.
      wire (dict): One of the outputs stored in the task's "outputs" array.
      subOutputPath (str): Path to the wire's "itemIndex"th stored output.
      wireIndex (int): Index of wire in the task's "outputs" array.
      itemIndex (int): The index of this output in the list of the wire's outputs.
      generators (list): List of generators that led to these outputs being created.
      person (Person): The actor running the job.
      job (JobRecord): The job that generated these outputs.

    Returns:
      dict: A dictionary containing "output_index", "object_id", "object_uid", and "object_revision".
    """

    # Rake this object
    outputInfo = wire.copy()

    # Add the output's object.json information if available.
    subOutputJSON = os.path.join(subOutputPath, "object.json")
    if os.path.exists(subOutputJSON):
      definedInfo = {}
      try:
        with open(subOutputJSON, "r") as f:
          import json
          definedInfo = json.load(f)
      except:
        JobManager.Log.warning("Could not open the object.json of the next output.")
        definedInfo = {}

      outputInfo.update(definedInfo)

    # Set defaults
    outputInfo['type'] = outputInfo.get('type', 'object')
    outputInfo['name'] = outputInfo.get('name', 'output')

    # It may be possible for two jobs to create outputs of the same type, name,
    # source, and initial-commit-hash. This is a problem because those are used
    # to generate the unique ID of the output object. To prevent this we add
    # the job id to the name.
    if job and 'source' in outputInfo:
      outputInfo['name'] = f"{outputInfo['name']} job-{job.id}"

    # Handle schema
    if 'schema' in wire and outputInfo['schema'] == wire['schema'] and isinstance(wire['schema'], str):
      outputInfo['schema'] = {
        "id": task['id'],
        "name": wire.get('name'),
        "type": "application",
        "subtype": "application/json",
        "revision": task['revision'],
        "file": wire['schema']
      }

    # Add generator relation (The generator is the object, and any passed along)
    outputInfo['generator'] = [{
      "name": task.get('name'),
      "type": task.get('type'),
      "id":   task.get('id'),
      "uid":  task.get('uid'),
      "revision": task.get('revision'),
      "inputs": task.get('inputs', []),
      "outputs": task.get('outputs', [])
    }]

    # Negotiate the configuration parameters that led to the generation of this
    # output object and store them.
    for inputIndex, wire in enumerate(task.get('inputs', [])):
      for wireIndex, inputObject in enumerate(wire.get('connections', [])):
        if inputObject.get('type') == 'configuration':
          # Resolve the used configuration
          configurationObject = self.objects.retrieve(inputObject.get('id'),
                                                      revision = inputObject.get('revision'),
                                                      person = person)

          if configurationObject:
            # Get the configuration schema and configuration values
            data = self.configurations.dataFor(configurationObject)
            schema = self.configurations.schemaFor(configurationObject, person = person)
            filtered = self.configurations.removeDefaultsFrom(data, schema)

            # Add the set configuration options to this generator
            outputInfo['generator'][-1]['inputs'][inputIndex]['connections'][wireIndex]['data'] = filtered

    for generator in (generators or []):
      outputInfo['generator'].append(generator)

    # If the 'file' is not set, and there is only one file in the directory, then
    # add a 'file' tag to point to that file
    if not 'file' in outputInfo:
      # If there is only one file in the output path
      if len(os.listdir(subOutputPath)) == 1:
        # Then, apply that as the 'file' represented by the object
        outputInfo['file'] = os.listdir(subOutputPath)[0]

    # Do not allow an empty file
    if outputInfo.get('file') == "":
      del outputInfo['file']

    # Add
    JobManager.Log.write("Raking %s %s" % (outputInfo['type'], outputInfo['name']))

    # Create the object
    outputObject = self.objects.write.create(name        = outputInfo['name'],
                                             object_type = outputInfo['type'],
                                             subtype     = outputInfo.get('subtype'),
                                             path        = subOutputPath,
                                             info        = outputInfo,
                                             createPath  = False)

    # Store it in the repository
    self.objects.write.store(outputObject, person.actingIdentity or person.identity)

    # Pull resources
    resources = self.objects.write.pullResources(outputObject, person.actingIdentity or person.identity)

    # Update the output object to reflect that the person who created this
    # object has permissions to it.
    self.permissions.update(outputObject.id, identity = person.identity,
                                             canRead  = True,
                                             canWrite = True,
                                             canClone = True)

    if job:
      # Add the output records
      self.createJobOutput(job, outputObject, wireIndex, itemIndex)

    # Add the output object as an output to each generator.
    for i, generator in enumerate(outputInfo['generator']):
      generatorObj = self.objects.retrieve(generator.get('id'),
                                           revision = generator.get('revision'),
                                           person = person)
      if generatorObj:
        relationship = "outputOf"

        if i > 0:
          relationship = "generatedBy"
        else:
          # Update configurations
          for inputIndex, wire in enumerate(task.get('inputs', [])):
            for wireIndex, inputObject in enumerate(wire.get('connections', [])):
              if inputObject.get('type') == 'configuration':
                filtered = generator['inputs'][inputIndex]['connections'][wireIndex]['data']

                # Also write this out via the ConfigurationWriteManager
                self.configurations.write.store(outputObject, generatorObj, inputIndex, filtered, person = person)

        self.addOutput(generatorObj, outputObject, person, relationship)

    # Return the index and id information needed to reference the newly raked
    # subobject.
    return {
      "output_index": wireIndex,
      "object_id": outputObject.id,
      "object_uid": outputObject.uid,
      "object_revision": outputObject.revision,
    }

  def rakeOutput(self, task, taskPath, generators=None, person=None, job=None):
    """ Rake the output generated for the given task.

    Arguments:
      task (dict): The task manifest metadata.
      taskPath (str): The path of the task's execution environment.
      generators (list): A list of generators, if known.
      person (Person): The actor raking the output.
      job (JobRecord): The JobRecord for the job in question.
    """

    # Look at the possible outputs

    if 'generator' in task:
      if generators is None:
        generators = []

      if not isinstance(task['generator'], list):
        task['generator'] = [task['generator']]
      generators.extend(task['generator'])

    outputs = []

    for wire in task.get('inputs', []):
      for inputObject in wire.get('connections', []):
        if 'index' in inputObject:
          outputs.extend(self.rakeOutput(inputObject, taskPath, generators = generators,
                                                                person = person,
                                                                job = job))

    for subTask in task.get('running', []):
      for inputObject in subTask.get('objects', []):
        if 'index' in inputObject:
          outputs.extend(self.rakeOutput(inputObject, taskPath, generators = generators,
                                                                person = person,
                                                                job = job))

    if 'index' in task and 'outputs' in task:
      objectPath = os.path.join(taskPath, "task", "objects", str(task['index']))
      for wireIndex, wire in enumerate(task.get('outputs', [])):
        outputPath = os.path.join(objectPath, "outputs", str(wireIndex))

        # Create DB entry for output of the node pin
        new_output={}
        new_output["output_index"] = wireIndex
        new_output["output_count"] = 0
        new_output["objects"] = []

        # Look for the existence of any new outputs
        # Remember, the first one is assumed, but if it is empty, ignore
        # Ignore all empty outputs
        i = 0
        while os.path.exists(os.path.join(outputPath, str(i))):
          subOutputPath = os.path.join(outputPath, str(i))
          itemIndex = i
          i += 1

          # Is this directory empty?
          if len(os.listdir(subOutputPath)) == 0:
            continue

          outObject = self._rakeSubObjectOutput(task = task,
                                                wire = wire,
                                                subOutputPath = subOutputPath,
                                                wireIndex = wireIndex,
                                                itemIndex = itemIndex,
                                                generators = generators,
                                                person = person,
                                                job = job)

          new_output["output_count"] += 1
          new_output["objects"].append(outObject)

        outputs.append(new_output)
    return outputs

def scheduler(name: str):
  """ This decorator will register a possible scheduler backend.
  """

  def _scheduler(cls):
    JobManager.register(name, cls)
    return cls

  return _scheduler
