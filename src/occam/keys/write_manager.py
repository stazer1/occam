# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log     import loggable
from occam.manager import manager, uses

import os

from occam.objects.manager   import ObjectManager
from occam.keys.manager      import KeyManager, KeyError
from occam.databases.manager import DataNotUniqueError

from Crypto           import Random
from Crypto.PublicKey import RSA
from Crypto.Cipher    import PKCS1_OAEP
from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash      import SHA512, SHA384, SHA256, SHA, MD5

@loggable
@manager("keys.write", reader=KeyManager)
@uses(ObjectManager)
@uses(KeyManager)
class KeyWriteManager:
  """ This manages the retrieval, use, and creation of private keys.
  """

  def importSigningKey(self, signingKey, signingKeyType, verifyKeyId):
    """
    """

    # Write the signing key
    self.datastore.write.createSigningKey(signingKey, signingKeyType, verifyKeyId)

  def importIdentity(self, uri, type, privateKey, verifyKeyId, published):
    """
    """

    # Write the identity
    identityKeyRecord = self.datastore.write.createIdentityKey(uri, privateKey, type, verifyKeyId, published)

  def createIdentity(self, type):
    """ Creates a new identity.

    Args:
      type (str): The type of keys to generate.

    Returns:
      tuple: The IdentityRecord and IdentityKeyRecord respectively.
    """

    sign_type = type
    if type == "Curve25519":
      sign_type = "Ed25519"

    # Create an identity keypair
    type, publicKey, privateKey = self.keys.newSigningPair(type)

    # Create a signing key
    sign_type, verifyKey, signingKey = self.keys.newSigningPair(sign_type)

    import datetime
    published = datetime.datetime.now()

    # Sign the signing key
    identity  = self.keys.importKey(privateKey, type = type)
    verify_key_id = self.keys.idFor(verifyKey, type = sign_type)

    uri = self.keys.idFor(publicKey, type = type)
    signature_digest, signature_type, signature = self.signKey(uri, verifyKey, published, identity, type = type)

    # Store Identity
    identityRecord = self.datastore.write.createIdentity(uri, publicKey, type, published)

    # Store VerifyKey
    verifyKeyRecord = self.datastore.write.createVerifyKey(uri, verify_key_id, verifyKey, sign_type, signature, signature_type, signature_digest, published)

    # Store SigningKey
    signingKeyRecord = self.datastore.write.createSigningKey(signingKey, sign_type, verifyKeyRecord.id)

    # Store Identity Key
    identityKeyRecord = self.datastore.write.createIdentityKey(uri, privateKey, type, verifyKeyRecord.id, published)

    return (identityRecord, identityKeyRecord,)

  def reSignKey(self, uri, verifyKeyId, verifyKey, published, privateKey):
    """ Updates the signature in the file for the given key.

    Returns:
      boolean: True when the key is updated
    """

    # Sign their public key
    signature_digest, signature_type, signature = self.signKey(uri, verifyKey, published, privateKey)

    # Delete the old key
    self.datastore.write.deleteVerifyKey(uri, verifyKeyId)

    try:
      return self.datastore.write.createVerifyKey(uri, verifyKeyId, verifyKey, signature, signature_type, signature_digest, published)
    except DataNotUniqueError as e:
      # Same signature... so we just return True
      return True

    return True

  def discover(self, uri, publicKey, type, published):
    """ Discovers the given identity.

    Args:
      uri: The identity URI.
      publicKey: The encoded public key.
      type (str): The key type for that public key.
      published (datetime): The original creation date for this identity.
    """

    # Verify public key matches identifier
    if uri != self.keys.idFor(publicKey, type):
      return None

    try:
      return self.datastore.write.createIdentity(uri, publicKey, type, published)
    except DataNotUniqueError as e:
      return True

  def discoverKey(self, uri, id, verifyKey, type, signature, signature_type, signature_digest, published, revokes, publicKey, pub_key_type):
    """ Discovers a new key for the given identity.
    """

    # Verify public signing key matches identifier:
    if id != self.keys.idFor(verifyKey, type):
      return None

    if self.keys.verifyKey(uri, verifyKey, published, signature, signature_type, signature_digest, self.keys.importKey(publicKey, pub_key_type), pub_key_type):
      try:
        return self.datastore.write.createVerifyKey(uri, id, verifyKey, type, signature, signature_type, signature_digest, published)
      except DataNotUniqueError as e:
        return True
    else:
      return None

  def identityKeyFor(self, uri):
    """ Retrieves the private identifying key for the given Person.

    This may be a difficult task if the identifying key is obscured or divided
    amongst many servers. This key is only necessary for the creation of new
    signing keys.

    Returns:
      IdentityKeyRecord: The private key for this actor.
    """

    ret = self.datastore.write.retrieveIdentityKey(uri)

    if ret is None:
      raise KeyIdentityUnknownError(uri)

    ret.key_type = ret.key_type or "RSA"

    return ret

  def signingKeyFor(self, uri, id=None):
    """ Retrieves the private signing key for the given Person.

    This is a private key used to sign objects or other actions or messages.

    If no `id` is given, it retrieves the current signing key.

    Arguments:
      uri (str): The identity uri for the actor.
      id (str): The id for the corresponding verify key.

    Returns:
      SigningKeyRecord: The private key for this Person for signing objects.
    """

    ret = None

    if id is None:
      ret = self.datastore.write.retrieveSigningKeyFor(uri)
    else:
      ret = self.datastore.write.retrieveSigningKey(uri, id)

    if ret is None:
      raise KeyIdentityUnknownError(uri)

    ret.key_type = ret.key_type or "RSA"

    return ret

  def revokeKey(self, person):
    """ Revokes the key for the given person.

    This will invalidate the signing key and create a new signing key for the
    given person. This key will, then, be signed by the identifying key.
    """

  def signKey(self, uri, verifyingKey, published, privateKey, type):
    """ Signs the given key using the given identity.

    Args:
      uri (str): The identity URI.
      verifyingKey (str): The encoded verify key.
      published (datetime): The creation date for this signature.
      privateKey (str): The encoded identity key (private key).
      type (str): The key type for that private key.

    Returns:
      tuple: (str, bytes) where the string is the signature type and bytes is
             a digest representing the derived signature.
    """

    # Sign their public key
    token = self.keys.keyTokenFor(uri, verifyingKey, published)
    return self.sign(token, "SHA512", privateKey, type)

  def decrypt(self, cipherText, privateKey):
    """ Decrypts the given message using the given private key.

    Args:
      cipherText (bytes): The cipher content to decrypt.
      privateKey (Key): The key to use.

    Returns:
      bytes: The decrypted content.
    """

    cipher = PKCS1_OAEP.new(privateKey)
    return cipher.decrypt(cipherText)

  def sign(self, message, signature_digest, privateKey, type):
    """ Signs the given message using the given private key.

    Args:
      message (bytes): The content to sign.
      signature_digest (str): The digest to use to hash the message.
      privateKey (Key): The private key to use.
      type (str): The type of key.

    Returns:
      tuple: (str, str, bytes) where the string is the signature digest followed by
             the signature type and bytes represents the derived signature.
    """

    if signature_digest == "SHA512":
      if type == "RSA":
        digest = SHA512.new()
        digest.update(message)
      elif type == "Curve25519" or type == "Ed25519":
        # Ed25519 already does SHA512
        pass
      else:
        import hashlib
        digest = hashlib.new('sha512')
        digest.update(message)

    if type == "RSA":
      signer = PKCS1_v1_5.new(privateKey)
      return (signature_digest, "PKCS1_v1_5", signer.sign(digest),)
    elif type == "Curve25519":
      from nacl.public import PrivateKey
      encryptKey = PublicKey(privateKey)
      return ("SHA512", "Curve25519", signingKey.sign(message).signature,)
    elif type == "Ed25519":
      from nacl.signing import SigningKey
      signingKey = SigningKey(privateKey)
      return ("SHA512", "Ed25519", signingKey.sign(message).signature,)

  def signObject(self, obj, uri, published = None):
    """ Returns a signature for the given Object using the given private key.

    The signature is a cryptographic hash of the object.json with the revision
    appended.

    Args:
      obj (Object): The object to sign.
      uri (str): The identity to sign as.
      published (datetime): The datetime to say the signature was published.
                            Defaults to the system date.

    Returns:
      str: The identifer of the verify key.
      str: The signature algorithm used.
      bytes: A digest representing the derived signature.
      datetime: The time that the signature was created or the value of published.
    """

    signingKey = self.signingKeyFor(uri)
    privateKey = self.keys.importKey(signingKey.key, signingKey.key_type)

    if published is None:
      import datetime
      published = datetime.datetime.now()

    token = self.keys.tokenFor(obj, uri, published)
    signature_digest, signature_type, signature = self.sign(token, "SHA512", privateKey, signingKey.key_type)

    # Can then check if a key is part of the master key
    verifyingKey = self.keys.verifyingKeyFor(uri, signingKey.id)
    publicKey    = self.keys.importKey(verifyingKey.key, verifyingKey.key_type)

    # Verify that the public can check the signature
    verified = self.keys.verifyObject(obj, signature, signature_type, signature_digest, uri, signingKey.id, published)
    if not verified:
      raise Exception("Could not verify the signature based on the verification key.")

    return signingKey.id, signature_digest, signature_type, signature, published

  def signTask(self, obj, uri, task, section, buildId, published, signed=None):
    """ Returns a signature representing the association of a task and an object.

    Args:
      obj (Object): The object whose task is given.
      uri (str): The identity to sign as.
      task (Object): The task itself.
      section (str): The type of task (build or run).
      buildId (str): The build task id (if it exists).
      published (datetime): When the task was generated.
      signed (datetime): When the build was signed (default: now)

    Returns:
      str: The hashing function used.
      str: The signing algorithm used.
      bytes: A digest representing the derived signature.
      datetime: The time that the signature was created or the value of 'signed'.
      string: The verify key id that was used to sign this signature.
    """

    signingKey = self.signingKeyFor(uri)
    privateKey = self.keys.importKey(signingKey.key, signingKey.key_type)

    if signed is None:
      import datetime
      signed = datetime.datetime.now()

    tag = task.id + task.uid + task.revision + section + (buildId or "") + published.isoformat() + "Z"

    token = self.keys.tokenFor(obj, uri, signed, tag = tag)
    signature_digest, signature_type, signature = self.sign(token, "SHA512", privateKey, signingKey.key_type)

    # Can then check if a key is part of the master key
    verifyingKey = self.keys.verifyingKeyFor(uri, signingKey.id)
    publicKey    = self.keys.importKey(verifyingKey.key, verifyingKey.key_type)

    # Verify that the public can check the signature
    verified = self.keys.verifyTask(obj, verifyingKey.id, signature, signature_type, signature_digest, uri, task, section, buildId, signed, published)
    if not verified:
      raise Exception("Could not verify the signature based on the verification key.")

    return signature_digest, signature_type, signature, signed, signingKey.id

  def signBuild(self, obj, uri, task, buildHash, built, signed=None):
    """ Returns a signature representing the given build.

    Args:
      obj (Object): The object whose build is given.
      uri (str): The identity to sign as.
      task (Object): The build task.
      buildHash (str): The hash of the built binaries.
      built (datetime): When the build occurred.
      signed (datetime): When the build was signed (default: now)

    Returns:
      str: The hashing function used.
      str: The signing algorithm used.
      bytes: A digest representing the derived signature.
      datetime: The time that the signature was created or the value of signed.
      string: The verify key id that was used to sign this signature.
    """

    signingKey = self.signingKeyFor(uri)
    privateKey = self.keys.importKey(signingKey.key, signingKey.key_type)

    if signed is None:
      import datetime
      signed = datetime.datetime.now()

    tag = task.id + task.uid + task.revision + buildHash + built.isoformat() + "Z"

    token = self.keys.tokenFor(obj, uri, signed, tag = tag)
    signature_digest, signature_type, signature = self.sign(token, "SHA512", privateKey, signingKey.key_type)

    # Can then check if a key is part of the master key
    verifyingKey = self.keys.verifyingKeyFor(uri, signingKey.id)
    publicKey    = self.keys.importKey(verifyingKey.key, verifyingKey.key_type)

    # Verify that the public can check the signature
    verified = self.keys.verifyBuild(obj, verifyingKey.id, signature, signature_type, signature_digest, uri, task, buildHash, signed, built)
    if not verified:
      raise Exception("Could not verify the signature based on the verification key.")

    return signature_digest, signature_type, signature, signed, signingKey.id

  def signTag(self, obj, uri, tag, signed=None):
    """ Returns a signature representing the application of the given tag.

    Args:
      obj (Object): The object to sign.
      uri (str): The identity to sign as.
      tag (str): The tag being applied to the object.
      signed (datetime): The datetime to say the signature was signed.
                         Defaults to the system date.

    Returns:
      str: The hashing function used.
      str: The signing algorithm used.
      bytes: A digest representing the derived signature.
      datetime: The time that the signature was created or the value of published.
      string: The verify key id that was used to sign this signature.
    """

    signingKey = self.signingKeyFor(uri)
    privateKey = self.keys.importKey(signingKey.key, signingKey.key_type)

    if signed is None:
      import datetime
      signed = datetime.datetime.utcnow()

    token = self.keys.tokenFor(obj, uri, signed, tag = tag)
    signature_digest, signature_type, signature = self.sign(token, "SHA512", privateKey, signingKey.key_type)

    # Can then check if a key is part of the master key
    verifyingKey = self.keys.verifyingKeyFor(uri, signingKey.id)
    publicKey    = self.keys.importKey(verifyingKey.key, verifyingKey.key_type)

    # Verify that the public can check the signature
    verified = self.keys.verifyTag(obj, signature, signature_type, signature_digest, uri, signingKey.id, tag, signed)
    if not verified:
      raise Exception("Could not verify the signature based on the verification key.")

    return signature_digest, signature_type, signature, signed, signingKey.id

  def store(self, obj, signature, signature_type, signature_digest, identity, verifyKeyID, signed):
    """ Stores the given signature for the given object tied to the identity.

    Args:
      obj (Object): The object that has been signed.
      signature (bytes): The signature.
      signature_type (str): The signing algorithm.
      signature_digest (str): The hashing algorithm used to hash the message.
      identity (str): The Identity URI of the signer.
      verifyKeyID (str): The verification key id.
      signed (datetime): The datetime to say the signature was signed.

    Returns:
      SignatureRecord: The record of the signature.
    """

    # Create a signature in the database if it doesn't already exist

    try:
      return self.datastore.write.createSignature(obj.id, obj.revision, identity, signature, signature_type, signature_digest, verifyKeyID, signed)
    except DataNotUniqueError as e:
      raise KeySignatureExistsError(obj.id, obj.revision, identity)

  def trust(self, trustee, trusted, signature):
    """ Stores a proof that the given identity trusts the second identity.

    Args:
      trustee (str): The Identity URI of the person acting upon the trust.
      privateKey (Key): The acting Identity's private key.
      trusted (str): The Identity URI of the Identity being trusted.

    Returns:
      IdentityVoucherRecord: The record of the trust.
    """

    # Store that signature
    return self.datastore.write.createIdentityVoucher(trustee, trusted, signature)

class KeySignatureExistsError(KeyError):
  """ An attempt to store a signature resulted in a collision.
  """

  def __init__(self, uuid, revision, uri):
    self.uri      = uri
    self.uuid     = uuid
    self.revision = revision

  def __str__(self):
    return "Signature already exists when signing '%s@%s' as %s" % (self.uuid, self.revision, self.uri)
