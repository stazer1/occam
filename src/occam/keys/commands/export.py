# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.keys.write_manager import KeyWriteManager

from occam.object import Object
from occam.log import Log

import os

@command('keys', 'export',
  category      = 'Identity Management',
  documentation = "Exports an Identity")
@argument("identity_uri", help = "The identity URI to export.")
@option("-p", "--private", action  = "store_true",
                           dest    = "export_private",
                           help    = "when specified, will export the private keys as well.")
@uses(KeyWriteManager)
class KeyExportCommand:
  """ This command will import an Identity from a JSON representation.
  """

  def do(self):
    import base64

    ret = {}

    # Public Data
    publicKey = self.keys.identityFor(self.options.identity_uri)
    verifyKeys = self.keys.verifyingKeysFor(self.options.identity_uri)

    ret['public'] = {}

    ret['public']['publicKey'] = {
      'id': self.options.identity_uri,
      'data': base64.b64encode(publicKey.encode('utf-8')).decode('utf-8'),
      'encoding': 'base64',
      'format': 'PEM'
    }

    ret['public']['verifyingKeys'] = [{
      'key': {
        'id': record.id,
        'data': base64.b64encode(record.key.encode('utf-8')).decode('utf-8'),
        'published': record.published.isoformat() + "Z",
        'encoding': 'base64',
        'format': 'PEM'
      },
      'signature': {
        'data': base64.b64encode(record.signature).decode('utf-8'),
        'encoding': 'base64',
        'format': 'PKCS1_v1_5',
        'digest': 'SHA512'
      }
    } for record in verifyKeys]

    if self.options.export_private:
      if self.person is None or self.options.identity_uri != self.person.identity:
        Log.error("Not authorized to view that private key.")
        return -1
      ret['private'] = {}

      privateKey = self.keys.write.identityKeyFor(self.options.identity_uri)
      ret['private']['privateKey'] = {
        'data': base64.b64encode(privateKey.encode('utf-8')).decode('utf-8'),
        'encoding': 'base64',
        'format': 'PEM'
      }

      signingKeys = [self.keys.write.signingKeyFor(self.options.identity_uri)]
      ret['private']['signingKeys'] = [{
        'key': {
          'id': record.id,
          'data': base64.b64encode(record.key.encode('utf-8')).decode('utf-8'),
          'encoding': 'base64',
          'format': 'PEM'
        }
      } for record in signingKeys]

    import json
    Log.output(json.dumps(ret))
    return 0
