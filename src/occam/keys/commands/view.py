# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import os

from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.manager import uses

from occam.keys.manager import KeyManager, KeyFormatError, KeyIdentityUnknownError
from occam.people.manager import PersonManager

@command('keys', 'view',
  category      = 'Identity Management',
  documentation = "Lists the public key for the given identity URI")
@argument("identity_uri", help    = "The identity URI to view.")
@option("-e", "--encoding", help    = "The key encoding to use. (OpenSSH, PEM, base64; depends on key type)",
                          default = None)
@option("-v", "--verify", help    = "Views the verification key for this identity.",
                          dest    = "verify",
                          action  = "store_true")
@option("-a", "--all",    help    = "display all metadata about this identity",
                          dest    = "all",
                          action  = "store_true")
@uses(KeyManager)
@uses(PersonManager)
class KeyViewCommand:
  def do(self):
    if self.options.all:
      import base64

      identity = self.keys.retrieve(self.options.identity_uri)
      publicKey = self.keys.identityFor(self.options.identity_uri)

      key = self.keys.importKey(identity.key, identity.key_type or "RSA")
      encoding, key = self.keys.exportKey(key, identity.key_type or "RSA")

      ret = {}

      # Get the Person associated with this identity, as we know it
      person = self.people.retrieve(self.options.identity_uri, person = self.person)
      if person:
        ret['person'] = {
          'id': person.id,
          'uid': person.uid,
          'revision': person.revision,
        }

      ret['publicKey'] = {
        'id': identity.uri,
        'published': identity.published.isoformat() + "Z",
        'data': key,
        'encoding': encoding,
        'format': identity.key_type or 'RSA'
      }

      verifyKeys = self.keys.verifyingKeysFor(self.options.identity_uri)

      # Pull out signature for this key

      ret['verifyingKeys'] = []
      for record in verifyKeys:
        key = self.keys.importKey(record.key, record.key_type or "RSA")
        encoding, key = self.keys.exportKey(key, record.key_type or "RSA")
        ret['verifyingKeys'].append({
          'key': {
            'id': record.id,
            'data': key,
            'published': record.published.isoformat() + "Z",
            'encoding': encoding,
            'format': record.key_type or 'RSA'
          },
          'signature': {
            'data': base64.b64encode(record.signature).decode('utf-8'),
            'encoding': 'base64',
            'format': record.signature_type or 'PKCS1_v1_5',
            'digest': record.signature_digest or 'SHA512',
          }
        })

      import json
      Log.output(json.dumps(ret))
      return 0

    if self.options.verify:
      identity = self.keys.verifyingKeysFor(self.options.identity_uri)[0]
    else:
      identity = self.keys.retrieve(self.options.identity_uri)

    ret = identity

    key = self.keys.importKey(ret.key, ret.key_type or "RSA")
    encoding, ret = self.keys.exportKey(key, ret.key_type or "RSA", self.options.encoding)

    Log.pipe(ret)
    return 0
