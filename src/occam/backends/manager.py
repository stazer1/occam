# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.log    import loggable
from occam.object import Object
from occam.error  import OccamError
from occam.config import Config

from occam.manager import uses, manager

from occam.objects.manager import ObjectManager

@loggable
@uses(ObjectManager)
@manager("backends")
class BackendManager:
  """ This OCCAM manager handles backend invocations.
  """

  # Keeps track of the backend plugins loaded into the system
  handlers = {}
  instantiated = {}

  def __init__(self):
    """ Initialize the backend manager.
    """

    #import occam.backends.plugins.docker
    import occam.backends.plugins.singularity

    self.handlers = BackendManager.handlers

  @staticmethod
  def register(backendName, handlerClass):
    """ Adds a new backend type.
    """

    BackendManager.handlers[backendName] = { 'class': handlerClass }

  def isAvailable(self, backendName):
    """ Returns True when the backend given is available on this node.
    """

    return backendName in self.handlers

  def available(self):
    """ Returns a list of backend names that are available on this node.
    """

    ret = []
    for k, v in BackendManager.handlers.items():
      handler = None
      try:
        handler = self.handlerFor(k)
      except:
        handler = None

      if handler:
        ret.append(k)

    return ret

  def configurationFor(self, backendName):
    """ Returns the configuration for the given backend.

    This is found within the occam configuration (config.yml) under the
    "backends" section under the given plugin name.
    """

    config = self.configuration
    subConfig = config.get(backendName, {})

    return subConfig

  def handlerFor(self, backendName):
    """ Returns an instance of a handler for the given type.
    """

    if not backendName in BackendManager.handlers:
      raise BackendPluginMissingError(backendName)

    if not backendName in BackendManager.instantiated:
      # Pull the configuration (from the 'backends' subpath and keyed by the name)
      subConfig = self.configurationFor(backendName)

      # Get the storage path for this backend
      occamPath = os.path.join(self.occamPath, backendName)

      # Create a driver instance
      instance = BackendManager.handlers[backendName]['class'](subConfig, occamPath)

      # If there is a problem detecting the backend, cancel
      # This will set the value in the instantiations to None
      try:
        if not instance.detect():
          instance = None
      except:
        instance = None

      # Set the instance
      BackendManager.instantiated[backendName] = instance

    if BackendManager.instantiated.get(backendName) is None:
      # The driver could not be initialized
      raise BackendPluginInitializationError(backendName)

    return BackendManager.instantiated[backendName]

  def handlerCall(self, backendName, name, *args, **kwargs):
    handler = self.handlerFor(backendName)

    if not hasattr(handler, name) or \
       not callable(getattr(handler, name)) or \
       not hasattr(getattr(handler, name), 'registered') or \
       not getattr(handler, name).registered:
      raise BackendPluginUnimplementedError(backendName, name)

    return getattr(handler, name)(*args, **kwargs)

  def initialize(self):
    """ Initializes the backend plugins.
    """

    from occam.services.write_manager import ServiceWriteManager
    services = ServiceWriteManager()

    ret = {}
    for k, v in BackendManager.handlers.items():
      info = self.handlerCall(k, 'initialize')
      if not info is None:
        ret[k] = info

      # Pull out services so we have a record that they exist
      result = self.handlerCall(k, 'services')
      for serviceInfo in result:
        services.storeBackend(backend = k, service = serviceInfo.get('name'))

    return ret

  def run(self, task, paths=[], network = None, stdout = None, stdin = None):
    """ Runs the object represented by the given task.

    Each runnable object has a 'run' section in its metadata. This lists
    everything the ManifestManager needs to build a task for the environment
    needed to run the object.

    This function will take a task and, based on its backend, run that object.

    The run will be marked in an invocation record along with information about
    the runtime or any failures. Other invocations of this object on other
    machines may use that information to make decisions about how to best run
    the object in the future.
    """

    BackendManager.Log.noisy("Running with %s" % (task.get('backend')))
    backend = task.get('backend')
    return self.handlerCall(backend, 'run', task, paths, network, stdout, stdin)

  def terminate(self, task):
    """ Terminates the running task.
    """

    backend = task.get('backend')
    return self.handlerCall(backend, 'terminate', task)

  def signal(self, task, signal):
    """ Sends a signal to the running task.

    Arguments:
      task (dict): The task manifest.
      signal (int): The signal vector to send.
    """

    backend = task.get('backend')
    return self.handlerCall(backend, 'signal', task, signal)

  def providersFor(self, environment = None, architecture = None, capability = None):
    """ Returns rows of objects that provide the given environment and architecture.
    """

    # TODO: remove this function

    return self.datastore.retrieveProvidersFor(environment, architecture, capability)

  def backendsFor(self, environment, architecture):
    """ Returns a list of strings indicating the backends for the given env/arch pair.

    Uses the Environment Map to look up which backends are possible endpoints
    for the given environment/architecture pair. This lets the system or a
    person know what possible options there are to run the object.

    This is exhaustive and determined via the Environment Map which is a
    normalized view of all permutations of environment/architecture pairs and
    available backends.

    The `providing` function is the base case of this function. That function,
    in contrast to this one, only returns the backends that directly support a
    given environment/architecture pair. This function, on the other hand, can
    provide a list of backends that will eventually support an object via other
    Provider objects (emulators, etc) that themselves run within the backend's
    environment.

    Arguments:
      environment (str): The tag representing the source environment.
      architecture (str): The tag representing the source architecture.

    Returns:
      list: An array of strings indicating possible backends to terminate task routes.
    """

    return self.datastore.retrieveBackendsFor(environment, architecture)

  def providerPath(self, environment, architecture, environmentGoal=None, architectureGoal=None, backend=None, person=None, limit=10):
    """ Provides an array of providers that will map the given environment and
    architecture to a native backend.
    
    The last item in the array will be a
    native backend instance (if goals aren't given, otherwise it will be an
    object that yields that goal environment.) Every other item will, in turn,
    be an object that provides one step of an environment.

    For instance, if we want to run a "dos" object that runs on "x86", we will
    invoke this command with environment and architecture given accordingly.
    The native environment might be "linux" on "ARM", so we must find a set
    of objects that will give us that environment. This method will return the
    hypothetical array of objects that will get us there. The first object
    might be DOSBox, which provides the "dos"/"x86" environment but runs on
    "ubuntu:14.04"/"x86-64". So the next object in the array might be an
    ubuntu distribution providing "ubuntu:14.04"/"x86-64". So we will need
    something to run that distribution, therefore the next object might be
    VirtualBox, providing the "*"/"x86-64" environment. Let's assume we can
    run VirtualBox natively. In this case, our search is complete.

    If this returns None, then the path cannot be made.
    """

    if limit <= 0:
      return None

    ret = []

    from occam.objects.records.object import ObjectRecord

    if backend:
      # Get backend reference
      provides = self.handlerCall(backend, 'provides')
      environmentGoal = provides[0][0]
      architectureGoal = provides[0][1]

    BackendManager.Log.noisy("Looking up provider path from %s %s to %s %s" % (environment, architecture, environmentGoal, architectureGoal))

    providers = list(self.providersFor(environment, architecture))

    # Look for backends that can handle this object
    for k, v in self.handlers.items():
      if v['class'].canProvide(environment, architecture):
        handler = self.handlerFor(k)
        if backend is None or handler.name() == backend:
          providers.append(handler)

    # Look for secondary objects that can handle this object
    for provider in providers:
      if not isinstance(provider, ObjectRecord):
        BackendManager.Log.noisy("Trying backend %s" % (provider.name()))
        currentEnvironment  = environment
        currentArchitecture = architecture
      else:
        BackendManager.Log.noisy("Trying object %s to reach %s/%s" % (provider.name, environmentGoal, architectureGoal))
        currentEnvironment  = provider.environment
        currentArchitecture = provider.architecture

      # Look for successful path endpoints:
      if environmentGoal is None and architectureGoal is None:
        # We are looking for absolutely ANY path to a backend (not an Object)
        if not isinstance(provider, ObjectRecord):
          BackendManager.Log.noisy("Path successful: Found a backend matching our goal.")
          ret.append(provider)
          return ret
      elif (environmentGoal is None or currentEnvironment == environmentGoal) and (architectureGoal is None or currentArchitecture == architectureGoal):
        # We found an object that meets our goal
        BackendManager.Log.noisy("Found %s @ %s" % (provider.name, provider.revision))
        provider = self.objects.retrieve(id=provider.id, revision=provider.revision)
        ret.append(provider)
        return ret
      elif not isinstance(provider, ObjectRecord) and backend:
        # We hit a backend, is it the one we requested?
        if provider.name() == backend:
          # Cool. We found it.
          ret.append(provider)
          return ret

      if not isinstance(provider, ObjectRecord):
        # We hit a backend, but this is not our goal
        BackendManager.Log.noisy("Path not successful: Hit a backend, but not our goal.")
        continue

      # Get a path from this point to a backend or goal
      result = self.providerPath(currentEnvironment, currentArchitecture, environmentGoal, architectureGoal, backend=backend, limit = limit - 1, person = person)

      # If we can't get a path from this point to a goal, then this point is not in our path
      if result is None:
        BackendManager.Log.noisy("Path not successful: Can't find a path from %s/%s to %s/%s" % (environment, architecture, environmentGoal, architectureGoal))
        continue

      # Otherwise, append the path and succeed
      provider = self.objects.retrieve(id=provider.id, revision=provider.revision, person=person)

      # TODO: handle this failure better
      if provider:
        ret.append(provider)
        ret.extend(result)
        return ret

      # Otherwise we keep trying...

    # Cannot find a route
    return None

  def store(self, uuid, revision, objectInfo):
    """ Records the given object as a possible backend provider.

    Looks at the object given and, if it is a provider, will add this object to
    the Environment Map. The Environment Map consists of records that when given
    an environment/architecture pair yields all possible environment/architectures
    that the given object can run upon.

    If the object is an emulator for ARM64, for instance, and the emulator runs
    on ubuntu or x86-64, then this function will figure out what things run
    ubuntu on x86-64... what do those things run upon... and exhaust all
    possibilities. It will record the map between this Provider record and
    each possible environment/architecture pair.

    Now, whenever we have an ARM64 application, we can quickly know exactly what
    possible routes we have to emulator or virtualize the application.

    In the end: it normalizes the pathfinding algorithm.
    """

    targetEnvironment  = objectInfo.get('environment')
    targetArchitecture = objectInfo.get('architecture')

    backends = self.providing(environment=targetEnvironment, architecture=targetArchitecture)
    ret = self.datastore.updateEnvironmentMap(uuid, revision, objectInfo, backends = backends)

    return ret

  def resolveServiceFor(self, backend, serviceInfo):
    """ Determines if the given backend could possibly resolve the given service.

    This, ultimately, does not guarantee that the service is available. This is
    often reflected by the Target instead. For instance, a service provided by
    'singularity' might require support on the actual node that invokes the job.
    This is reflected by the Target information provided when selecting where to
    run the given job.

    This function, on the other hand, can be used by the Manifest Manager to
    determine if such a service could possibly be provided in that manner. If
    this returns False, then regardless of the configuration of any target
    selecting this backend, the backend can not be used in conjunction with the
    requested service.

    Arguments:
      backend (str): The name of the backend.
      serviceInfo (dict): The metadata describing the requested service.

    Returns:
      bool: Returns True if the backend could provide the service.
    """

    ret = False
    try:
      ret = self.handlerCall(backend, 'resolveService', serviceInfo)
    except:
      ret = False

    return ret

  def backendCanSupport(self, backend, environment, architecture):
    """ Determines if a given backend can provide the given environment/architecture pair.

    Returns True when the given backend can support the given environment/architecture pair.
    Returns False otherwise.

    Uses the Environment Map to make this decision.

    Arguments:
      backend (str): The backend name.
      environment (str): The tag representing the environment.
      architecture (str): The tag representing the architecture.

    Returns:
      bool: Returns True when the backend supports the environment/architecture pair.
    """

    return self.datastore.backendCanSupport(backend, environment, architecture)

  def canSupport(self, environment, architecture, environmentGoal=None, architectureGoal=None):
    """ Determines if there is a route between given environment/architecture pairs.

    Returns True when the given environment/architecture pair has a possible
    route to a native backend or the given goal.

    When the `environmentGoal` and `architectureGoal` are both `None`, then it
    must find a native backend to target.

    Arguments:
      environment (str): The tag representing the source environment.
      architecture (str): The tag representing the source architecture.
      environmentGoal (str): The tag representing the targeted environment.
      architectureGoal (str): The tag representing the targeted architecture.
    """

    return self.datastore.canSupport(environment, architecture, environmentGoal, architectureGoal)

  def providing(self, environment, architecture):
    """ Yields names of all backends that can manage the given environment/architecture pair.
    
    Returns an array of strings.

    Returns an empty array when the given environment and architecture cannot
    run directly on an available backend.

    Note that `backendsFor` is the general-purpose case mapping any environment
    and architecture pair that has a possible route to a backend. This function
    is for determining that route at its base case.

    This is different from `backendsFor` in that this `providing` function only
    yields backend names that run the given pair directly. That is, the
    'singularity' backend runs 'linux'/'x86-64' directly. Therefore, `providing`
    would return 'singularity' when given 'linux' and 'x86-64' but not when
    given 'dos' and 'x86'. However, if there is an emulator available, then
    `backendsFor` might return 'singularity' in that case.

    Arguments:
      environment (str): The tag representing the source environment.
      architecture (str): The tag representing the source architecture.

    Returns:
      list: An array of strings representing backends that can directly run the given environment/architecture pair.
    """

    # Go through the available backends and find one that provides the
    # requested environment/architecture combination

    ret = []

    for k, v in self.handlers.items():
      if v['class'].canProvide(environment, architecture):
        ret.append(k)

    return ret

# Error Classes

class BackendError(OccamError):
  """ Base class for all backend errors.
  """

class BackendPluginError(BackendError):
  """ Base class for all backend plugin errors.
  """

class BackendPluginMissingError(BackendPluginError):
  """ Thrown when a plugin does not exist.
  """

  def __init__(self, backendName):
    super().__init__(f"Backend provider {backendName} not known")

class BackendPluginInterfaceUnknownError(BackendPluginError):
  """ Thrown when a plugin has an interface implementation we are not aware of.
  """

  def __init__(self, backendName, funcName):
    super().__init__(f"Backend provider {backendName} found but is not compatible due to the presence of unknown interface function {funcName}.")

class BackendPluginInitializationError(BackendPluginError):
  """ Thrown when a plugin reported a problem when initializing.
  """

  def __init__(self, backendName):
    super().__init__(f"Backend provider {backendName} found but could not be initialized")

class BackendPluginUnimplementedError(BackendPluginError):
  """ Thrown when a method within a plugin is not implemented.
  """

  def __init__(self, backendName, method):
    super().__init__(f"Backend provider {backendName} does not implement {method}")

# Plugin Registry

def backend(name, priority=0):
  """ This decorator will register the given class as a backend.
  """

  def register_backend(cls):
    BackendManager.register(name, cls)
    cls = loggable("BackendManager")(cls)

    def init(self, subConfig, occamPath):
      self.configuration = subConfig
      self.occamPath = occamPath

    cls.__init__ = init
    return cls

  return register_backend

def interface(func):
  """ This decorator assigns the given function as part of the plugin.

  Only functions tagged with this decorator can be used as part of a plugin
  implementation.
  """

  # Attach it to the plugin
  def _pluginCall(self, *args, **kwargs):
    return func(self, *args, **kwargs)

  _pluginCall.registered = True
  return _pluginCall
