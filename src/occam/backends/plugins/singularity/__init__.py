# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.backends.manager import backend, interface

import sys
import codecs
import select
import subprocess
import os
import glob
import time
import json
import shutil
import base64

@backend('singularity')
class Singularity:
  """ Implements the logic to build/run objects as Singularity processes.
  """

  @interface
  def initialize(self):
    pass

  @staticmethod
  def provides():
    """ Returns a list of all possible environments and architecture pairs that
    can be created and used by this backend.
    """

    return [
      ['singularity', 'x86-64']
    ]

  @interface
  def services(self):
    """ Returns a list of all possible services that this backend can resolve.
    """

    return [
        {
          "name": "network",
          "type": "service",
          "summary": "Allow a generic network to be resolved."
        },
        {
          "name": "nvidia-driver",
          "type": "service",
          "summary": "Use the system's native NVIDIA driver, if possible."
        }
    ]

  @interface
  def resolveService(self, serviceInfo):
    """ Resolves the given service, if possible.
    """

    if serviceInfo.get('name') == 'nvidia-driver':
      return {
        "summary": "The system's native NVIDIA driver will be mounted within the container, if available."
      }

    if serviceInfo.get('name') == 'network':
      return {
        "summary": "A generic network will be provided within the container."
      }

    return False

  @staticmethod
  def name():
    return "singularity"

  @staticmethod
  def canProvide(environment, architecture):
    """ Returns True if this backend can run the given environment/architecture.
    """

    return [environment, architecture] in Singularity.provides()

  def mergeVariables(self, a, b):
    if 'LD_LIBRARY_PATH' in a and 'LD_LIBRARY_PATH' in b:
      a['LD_LIBRARY_PATH'] = "%s:%s" % (a['LD_LIBRARY_PATH'], b['LD_LIBRARY_PATH'])
      del b['LD_LIBRARY_PATH']
    a.update(b)
    return a

  # TODO: purgeBuiltImage
  def purge(self, obj):
    """
    Deletes any local cached copy of the built object.
    """

    # TODO: this D: D: D:
    return False

  @interface
  def detect(self):
    """ Returns True when singularity is available.
    """

    # Just check that singularity is in PATH and runs.
    command = ['singularity']

    # Singularity, by itself, returns 1, for some reason.
    return self.executeSingularityCommand(command, stdout = subprocess.DEVNULL, stderr = subprocess.DEVNULL) == 1

  @staticmethod
  def removeRootPath(hostPath, paths):
      """ Removes the given host path from the task local path.
      """

      root = None
      if paths:
        root = paths['root']

      localPath = None
      if root:
        localPath = os.path.join(root, hostPath[1:])

      if localPath and os.path.lexists(localPath):
        if not os.path.islink(localPath) and os.path.isdir(localPath):
          shutil.rmtree(localPath)
        else:
          os.remove(localPath)

  @staticmethod
  def symlinkRootPath(hostPath, destinationPath, paths):
      root = None
      if paths:
        root = paths['root']

      localPath = None
      if root:
        localPath = os.path.join(root, hostPath[1:])

      # Delete existing file
      if localPath and os.path.lexists(localPath):
        if not os.path.islink(localPath) and os.path.isdir(localPath):
          shutil.rmtree(localPath)
        else:
          os.remove(localPath)

      # Symlink
      os.makedirs(os.path.dirname(localPath), exist_ok=True)
      os.symlink(destinationPath, localPath)

  @staticmethod
  def mountCommandForSoftLink(hostPath, access='ro', mounted=None, paths=None, force=False):
    """
    Returns the list of command tokens (['-B', 'path:path:ro', ...]) to use to
    generate the singularity command to mount in the given file represented by path
    and all files it links to.
    """

    mounted = mounted or {}

    command = []

    root = None
    if paths:
      root = paths['root']

    if os.path.exists(hostPath):
      localPath = None
      if root:
        localPath = os.path.join(root, hostPath[1:])

      if not hostPath in mounted:
        mountPath = os.path.join("/occam/system", base64.urlsafe_b64encode(hostPath.encode('utf-8')).decode('utf-8').replace("=", ""))
        if os.path.isfile(hostPath):
          command = ['-B', '%s:%s:%s' % (hostPath, mountPath, access)]
          mounted[hostPath] = True

        # Now, create the symlink to the hostPath within the container so they match
        if localPath:
          # Ensure that directories up to the file or directory we are mounting
          # exist in the VM file system
          os.makedirs(os.path.dirname(localPath), exist_ok=True)

          # If force is True, delete the existing VM file system
          if os.path.lexists(localPath) and force:
            if os.path.isdir(localPath):
              shutil.rmtree(localPath)
            else:
              os.remove(localPath)

           # If it doesn't already exist, symlink it (if a file) or mount it
           # (if it is a directory)
          if not os.path.lexists(localPath):
            if not os.path.isfile(hostPath):
              os.makedirs(localPath, exist_ok=True)
              command = ['-B', '%s:%s:%s' % (hostPath, hostPath, access)]

              # Determine actual path if host file is a symlink
              while os.path.islink(hostPath):
                hostPath = os.path.realpath(hostPath)
                os.makedirs(localPath, exist_ok=True)
                command += ['-B', '%s:%s:%s' % (hostPath, hostPath, access)]
            else:
              os.symlink(mountPath, localPath)

      if os.path.islink(hostPath):
        curPath = os.path.dirname(hostPath)
        nextPath = os.readlink(hostPath)
        nextPath = os.path.join(curPath, nextPath)
        command = Singularity.mountCommandForSoftLink(nextPath, access, mounted, paths = paths, force = force)

    return command

  def mountNVIDIADriver(self, mounted=None, paths=None):
    """
    Returns the list of command tokens (['-B', 'path:path:ro', ...]) to use to
    generate the singularity command to mount in the NVIDIA driver if it exists on
    the system.
    """

    # TODO: use ld.so.conf
    # TODO: self.system.libraryPaths() + [...]
    SEARCH_LIST=["/usr/lib/xorg/modules", "/usr/lib/xorg/modules/drivers", "/usr/lib/xorg/modules/extensions", "/usr/lib/x86_64-linux-gnu/vdpau"]
    FILE_LIST=["libnvidia*", 
               "libnvcu*",
               #"libwfb*",
               "nvidia_drv*",
               "libglx*"]

    mounted = mounted or {}

    command = []

    for pattern in FILE_LIST:
      for root in SEARCH_LIST:
        for path in glob.iglob(os.path.join(root, pattern)):
          command += Singularity.mountCommandForSoftLink(path, "ro", mounted, paths = paths)

    return command

  @interface
  def run(self, task, paths, network, stdout, stdin=None):
    """ Runs the task using a singularity backend.

    Fails if the singularity command or container init fails. Note that this is
    different from the task failing. To determine if the task failed inspect
    the run report. See pullRunReport() in the jobs manager.
    """

    #self.prepare(task, paths)
    #return 0

    providedOut = stdout
    if stdout is None:
      import sys
      if hasattr(sys.stdout, 'buffer'):
        stdout = sys.stdout.buffer
      else:
        stdout = sys.stdout

    # First, ensure that the base container is built
    baseContainer = task['running'][0]['objects'][0]
    baseContainerId = baseContainer.get('id')

    command = ['singularity', '--silent', 'exec']

    # Run as the current user
    #command = command + ['--user', '%s:%s' % (str(os.getuid()), str(os.getgid()))]

    # Singularity Environment
    environment = task.get('running')[0]['objects'][0]

    # Get rid of all env-vars
    command.append('-e')

    # Do not mount from system root
    command.append('-c')
    command.append('-C')

    # Generate a new pid namespace
    command.append('-p')

    # Mount task volume
    command.append('-B')
    command.append('%s:/home/occam:rw' % paths['home'])
    command.append('-B')
    command.append('%s:/home/occam/task:rw' % paths['task'])

    # Create and bind a tmp directory
    # (We cannot mount a tmpfs ourselves, here... singularity mounts one that's
    #  much too small. TODO: update when singularity can handle mount options.)
    # In the future, we would mount the tmpfs (after the root volume binds
    # below), but for now, we just create a 'tmp' directory in the root.
    #command.append('-B')
    #command.append('/tmp:/tmp:rw')
    # Create the directory in the root task
    tmpPath = os.path.join(paths.get('root'), 'tmp')
    if not os.path.exists(tmpPath):
      os.mkdir(tmpPath)

    # Add root volumes
    rootPath = paths.get('root')
    if rootPath:
      for subPath in os.listdir(rootPath):
        command.append('-B')
        command.append('%s:%s:rw' % (os.path.join(rootPath, subPath), os.path.join("/", subPath)))

    # Add each volume
    for k, pathInfo in paths.items():
      if isinstance(pathInfo, dict):
        command.append('-B')
        command.append('%s:%s:ro' % (pathInfo.get('path'), pathInfo.get('mount')))

        if 'localPath' in pathInfo:
          command.append('-B')
          command.append('%s:%s:rw' % (pathInfo.get('localPath'), pathInfo.get('localMount')))

        if 'buildPath' in pathInfo:
          command.append('-B')
          command.append('%s:%s:rw' % (pathInfo.get('buildPath'), pathInfo.get('buildMount')))
          
    # Mount some devices
    command.append('-B')
    command.append('/dev/full:/dev/full:rw')

    # Determine if we need other services
    network = None
    for service in baseContainer.get('requires', []):
      if service.get('name') == 'nvidia-driver':
        # Add the NVIDIA drivers, if we can see them on the native system
        command.extend(self.mountNVIDIADriver(paths = paths))
        command.append("--nv")
      elif service.get('name') == 'network':
        # Add a basic network device to the container
        network = True

    # Add network options (ports, etc)
    #for port in network.get('ports', []):
    #  command.append("-p")
    #  command.append("%s:%s" % (port['port'], port['bind']))

    if network is None:
      command.append("--net")
      command.append("--network")
      command.append("none")
      command.append("--hostname")
      command.append("occam")
    else:
      # Copy in a resolv.conf and hostname
      if rootPath:
        resolvPath = os.path.join(rootPath, "etc", "resolv.conf")
        if not os.path.lexists(resolvPath) and os.path.exists("/etc/resolv.conf"):
          shutil.copyfile("/etc/resolv.conf", resolvPath)

      if rootPath:
        hostsPath = os.path.join(rootPath, "etc", "hosts")
        if not os.path.lexists(hostsPath) and os.path.exists("/etc/hosts"):
          shutil.copyfile("/etc/hosts", hostsPath)

          #with open(hostsPath, "w+") as f:
          #  f.write("127.0.0.1    localhost    occam-big\n")
          #  f.write("::1          localhost    occam-big\n")

        hostPath = os.path.join(rootPath, "etc", "host")
        if not os.path.lexists(hostPath) and os.path.exists("/etc/host"):
          shutil.copyfile("/etc/host", hostPath)

        hostnamePath = os.path.join(rootPath, "etc", "hostname")
        if not os.path.lexists(hostnamePath) and os.path.exists("/etc/hostname"):
          shutil.copyfile("/etc/hostname", hostnamePath)

        nssPath = os.path.join(rootPath, "etc", "nsswitch.conf")
        if not os.path.lexists(nssPath) and os.path.exists("/etc/nsswitch.conf"):
          shutil.copyfile("/etc/nsswitch.conf", nssPath)

      # Delete openssl.cnf, if it exists
      Singularity.removeRootPath("/etc/ssl", paths)

      # Ensure that we have system certificates
      certPaths = ["/etc/ca-certificates", "/etc/ca-certificates.conf", "/etc/pki",
                   "/usr/share/ca-certificates", "/etc/crypto-policies",
                   "/usr/share/crypto-policies"]

      # Add OPENSSLDIR to match the local openssl (if needed)
      parts = subprocess.check_output(["openssl","version","-d"]).decode('utf-8').split(' ')
      ssldir = None
      if parts[0] == "OPENSSLDIR:":
        ssldir = parts[1].strip()[1:-1]
        mountOpenSSLPath = True
        for certPath in certPaths:
          if ssldir.startswith(certPath):
            mountOpenSSLPath = False

        if mountOpenSSLPath:
          command += Singularity.mountCommandForSoftLink(ssldir, "ro", paths = paths, force = True)

        # Symlink /etc/ssl/openssl.cnf to this path by symlinking OPENSSLDIR to /etc/ssl
        if ssldir != "/etc/ssl":
          Singularity.symlinkRootPath("/etc/ssl", ssldir, paths)

      # Now mount in remaining paths
      for certPath in certPaths:
        command += Singularity.mountCommandForSoftLink(certPath, "ro", paths = paths, force = True)

    # Ensure a hosts file exists (/etc/hosts) if it doesn't already
    # and ensure it has a localhost pointing to 127.0.0.1
    if rootPath:
      hostsPath = os.path.join(rootPath, "etc", "hosts")
      if not os.path.lexists(hostsPath):
        with open(hostsPath, "w") as f:
          f.write("127.0.0.1    localhost    occam\n")
          f.write("::1          localhost    occam\n")

    # Ensure a machine-id file exists (/etc/machine-id) if it doesn't already
    # and ensure it is regular
    if rootPath:
      machineIDPath = os.path.join(rootPath, "etc", "machine-id")
      if not os.path.lexists(machineIDPath):
        with open(machineIDPath, "w") as f:
          f.write("123456789abcdef123456789abcdef12")

    # Ensure a os-release file exists (/etc/os-release) if it doesn't already
    # and ensure it is regular
    if rootPath:
      osReleasePath = os.path.join(rootPath, "etc", "os-release")
      if not os.path.lexists(osReleasePath):
        with open(osReleasePath, "w") as f:
          f.write('NAME="Occam Linux"\n'
              'PRETTY_NAME="Occam Linux"\n'
              'ID=occam\n'
              'BUILD_ID=rolling\n'
              'ANSI_COLOR="38;2;23;147;209"\n'
              'HOME_URL="https://occam.software/"\n'
              'DOCUMENTATION_URL="https://occam.software/"\n'
              'SUPPORT_URL="https://occam.software/"\n'
              'DEFAULT_HOSTNAME=occam\n'
              'BUG_REPORT_URL="https://gitlab.com/groups/occam-archive/-/issues"\n')

    # Add some security holes
    command.extend(self.capabilitiesFor(task))
    #command.extend(self.securityOptionsFor(task))

    # Determine the path of the singularity container image
    containerPath = environment.get('file')
    if containerPath is None:
      # We cannot run using an environment without a container image
      return False

    # Get the local path of the container file
    containerLocalPath = paths[environment.get('index')].get('path')
    containerPath = os.path.join(containerLocalPath, containerPath)

    command.append(containerPath)

    runCommand = environment.get('run', {}).get('command')

    if runCommand is None:
      # We cannot run this
      return False

    if not isinstance(runCommand, list):
      runCommand = [runCommand]

    command.extend(runCommand)

    pty = False
    if task.get('interactive', False):
      stdout = providedOut
      pty = True

    exitCode = self.executeSingularityCommand(
      command, cwd=paths['task'], interactive=True, stdout = stdout,
      stdin = stdin, pty = True, storePID = task
    )

    return exitCode

  def safeName(self, name):
    from uuid import uuid5, NAMESPACE_URL
    return str(uuid5(NAMESPACE_URL, "occam://singularity/%s" % (name)))

  def storePID(self, task, pid):
    name = "occam-%s" % (self.safeName(task.get('id')))

    os.makedirs(self.occamPath, exist_ok=True)
    pidPath = os.path.join(self.occamPath, name + ".json")

    info = {
      'pid': pid
    }

    try:
      with open(pidPath, "w+") as f:
        f.write(json.dumps(info))
    except:
      pass

  def clearPID(self, task):
    name = "occam-%s" % (self.safeName(task.get('id')))

    pidPath = os.path.join(self.occamPath, name + ".json")

    try:
      if os.path.exists(pidPath):
        os.remove(pidPath)
    except:
      pass

  def lookupPID(self, task):
    name = "occam-%s" % (self.safeName(task.get('id')))

    pidPath = os.path.join(self.occamPath, name + ".json")

    pid = None
    try:
      if os.path.exists(pidPath):
        with open(pidPath, "r") as f:
          pid = json.load(f).get('pid')
    except:
      pass

    return pid

  @interface
  def signal(self, task, signal):
    """ Gives the given signal to the given task, if it is running.
    """

    # The default is KILL
    if signal < 0:
      signal = 9

    # Look up the pid and pursue the process
    pid = self.lookupPID(task)
    if pid is not None:
      command = ["kill", '-' + str(signal), str(pid)]
      return self.executeSingularityCommand(command)

    return 0

  @interface
  def terminate(self, task):
    """ Gives the given signal to the given task, if it is running.
    """

    return self.signal(task, 9)

  def capabilitiesFor(self, obj):
    """ Returns the capability list needed to run the given object.
    """
    ret = []

    # Allows ptrace
    ret += ["--add-caps", "SYS_PTRACE"]

    return ret

  def executeSingularityCommand(self, command, cwd=None, stdout=None, stderr=None, stdin=None, wait=True, interactive=False, pty=False, storePID=None):
    Log.noisy("Invoking native command: %s %s" % ("(interactive)" if interactive else "", ' '.join(command)))

    # Detect if we are to use the vendored singularity
    vendorPath = os.path.realpath(os.path.join(os.path.dirname(os.path.realpath(__file__)), "..", "..", "..", "..", "vendor", "bin", "singularity"))
    if os.path.exists(vendorPath):
      command[0] = vendorPath

    parseToLog = False
    parseTo = None
    if stdout is not None and stdout != subprocess.PIPE and stdout != subprocess.DEVNULL:
      parseTo = stdout
      parseToLog = True
      stdout = subprocess.PIPE
      stderr = subprocess.PIPE

    if not interactive:
      if stdout is None:
        # Capture stdout and replay it on the log (as stderr)
        stdout = subprocess.PIPE
        parseToLog = True

      if stderr is None:
        # Capture stderr and replay it on the log
        stderr = subprocess.PIPE
        parseToLog = True

    host  = None
    guest = None
    old_tty = None

    def update_size(signum, frame):
      # Get the terminal size of the real terminal, set it on the pty
      try:
        buf = array.array('h', [0, 0, 0, 0])
        fcntl.ioctl(PTY.STDOUT_FILENO, termios.TIOCGWINSZ, buf, True)
        fcntl.ioctl(host, termios.TIOCSWINSZ, buf)

        # Pass along signal
        process.send_signal(signal.SIGWINCH)
      except:
        pass

    if pty:
      import array
      import termios
      import pty as PTY
      import tty
      import signal as UNIXSignal
      import fcntl

      # save original tty setting then set it to raw mode
      try:
        old_tty = termios.tcgetattr(sys.stdin)
        if not parseToLog:
          tty.setraw(sys.stdin.fileno())
      except:
        pass

      # open pseudo-terminal to interact with subprocess
      host, guest = PTY.openpty()

      stdout = guest
      stderr = guest
      stdin  = guest

      UNIXSignal.signal(UNIXSignal.SIGWINCH, update_size)

      update_size(UNIXSignal.SIGWINCH, None)

    #print("line 641!",command)
    #import pdb;pdb.set_trace()
    process = subprocess.Popen(command, cwd=cwd, stdin=stdin, stdout=stdout, stderr=stderr, start_new_session = pty)

    if storePID:
      self.storePID(storePID, process.pid)

    if pty and guest:
      os.close(guest)
    elif not interactive and process.stdin:
      process.stdin.close()

    utfDecoder = codecs.getreader('utf-8')

    totalRead = 0

    if wait:
      if parseToLog or pty:
        log = b""

        closed = []
        readers = []
        if pty:
          readers = [host]
          if not parseToLog:
            readers.append(sys.stdin)
          closed.append(sys.stdin)
        else:
          stdout = process.stdout
          stderr = process.stderr
          readers = [stdout, stderr]

        try:
          while True:
            readable, writable, exceptional = select.select(readers, [], readers)

            numRead = 0

            for reader in readable:
              read = None
              if reader is sys.stdin and host:
                d = os.read(sys.stdin.fileno(), 10240)
                os.write(host, d)
              elif host and reader is host:
                try:
                  read = os.read(host, 10240)
                except OSError as e:
                  pass

                if read:
                  if parseTo is not None:
                    parseTo.write(read)
                    if parseTo is sys.stdout:
                      parseTo.flush()
                  else:
                    os.write(sys.stdout.fileno(), read)

                if read is None or len(read) == 0:
                  closed.append(reader)
                  readers.remove(reader)
              else:
                read = reader.read(1)
                log += read
                if (read == b"\n"):
                  if parseTo is not None:
                    parseTo.write(log)
                    if parseTo is sys.stdout:
                      parseTo.flush()
                  else:
                    Log.external(log, 'singularity')
                  log = b""

                numRead += len(read)
                totalRead += len(read)

                if len(read) == 0:
                  closed.append(reader)
                  readers.remove(reader)

            if len(readable) == 0 and process.poll() is not None:
              break

            if numRead == 0 and len(closed) == 2:
              break
        except KeyboardInterrupt:
          return -1

        if len(log) > 0:
          if parseTo is not None:
            parseTo.write(log)
          else:
            Log.external(log, 'singularity')

      process.communicate()

      if pty and old_tty:
        # restore tty settings back
        try:
          termios.tcsetattr(sys.stdin, termios.TCSADRAIN, old_tty)
        except:
          pass

      if storePID:
        self.clearPID(storePID)

      return process.returncode
    else:
      return process

  def rootBasepath(self):
    """ Returns the root basepath for the singularity filesystem.
    """

    return {
      "separator": "/",
      "mount": "/occam/{{ id }}-{{ revision }}",
      "cwd": "{{ paths.localMount }}/local",
      "taskLocal": "/home/occam/task",
      "local": "/home/occam/task/objects/{{ index }}",
    }

  def prepare(self, task, paths):
    """ Creates any cached images for the given task.
    """

    # Gather the information about the intended singularity base environment
    environmentInfo = task['running'][0]['objects'][0]
    environmentIndex = environmentInfo['index']
    environmentPath = paths[environmentIndex]['path']

    # Specifically, get the real path of the SIF file for that base environment
    occamBaseImagePath = os.path.join(environmentPath, environmentInfo.get('file'))

    # Gather the path of the prepared filesystem
    basePath = paths['home']
    rootPath = paths['root']

    # Generate the singularity definition file
    definition = f"""
Bootstrap: localimage
From: {occamBaseImagePath}

%setup
  cp -r {rootPath}/* ${{SINGULARITY_ROOTFS}}/.
  chmod -R a+w ${{SINGULARITY_ROOTFS}}
    """

    # Create the singularity image using the prepared files and the definition
    definitionPath = os.path.realpath(os.path.join(basePath, "container.def"))
    containerPath = os.path.realpath(os.path.join(basePath, "container.sif"))
    with open(definitionPath, 'w+') as f:
      f.write(definition)

    # We need to invoke our own singularity binary so we can build without root
    command = ['/home/occam/occam/vendor/singularity', 'build', containerPath, definitionPath]
    code = subprocess.Popen(command).communicate()
