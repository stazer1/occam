# The Commands Component

This component handles all of the various Occam commands and registering new
commands.  This also provides the parameter parsing capabilities and is where
all generic parameters are defined.

Examples:
```
  To create a command, write the following in a new file and import it::

    # my_command.py:
    # invoked by: "occam my-component my-command <something_else>+ [-a foo] [-b]"
    from command_manager import command, option, argument

    @command('my-component', 'my-command')
    @argument("something_else", nargs="?")
    @option("-a", "--argument", action = "store",
                                dest   = "my_argument",
                                help   = "this is my argument. it is followed by a string.")
    @option("-b", "--boolean",  action = "store_true",
                                dest   = "my_boolean_argument",
                                help   = "this turns on something")
    class MyCommand:
      def do(self):
        # Pull out the argument value
        value = self.options.argument

        if self.options.my_boolean_argument:
          # do something
          pass

        for argument in self.options.something_else:
          print(argument)

```

And you can run this with: "occam my-component my-command --argument foo --boolean a b c"

This will print out "a" "b" and "c" since those are normal (positional) arguments.

Note that all arguments (such as help, dest, etc) to @option and @argument are optional.

To subclass a command, there are some interesting tricks to consider. When you subclass
you will inherit all arguments and options of the subclassed command. When you specify
an option with the same name, you will override the option. It cannot have a different
destination, short, or long name. You can turn off options by overriding them and setting
the 'nargs' argument to '0'.

However, when you do this for a positional argument (one specified by @argument) that is
required by the subclassed command, you will need to specify a function that will
satisfy the argument and provide a value. You will use the 'using' argument of @argument
to do so. A tad involved and confusing, but take a look at this example:
```
  # my_command.py
  from command_manager import command, option, argument

  @command('my-component', 'my-command')
  @argument("something")
  @option("-a", "--argument", action = "store",
                              dest   = "my_argument",
                              help   = "this is my argument. it is followed by a string.")
  @option("-b", "--boolean",  action = "store_true",
                              dest   = "my_boolean_argument",
                              help   = "this turns on something")
  class MyCommand:
    def do(self):
      # Pull out the argument value
      value = self.options.argument

      if self.options.my_boolean_argument:
        # do something
        pass

      for argument in self.options.something_else:
        print(argument)
```

And subclassing it, as such:

```
  # my_extra_command.py
  from my_command import MyCommand

  from command_manager import command, option, argument

  @command('my-component', 'my-extra-command')
  @argument("something", nargs="0", using='provideSomething')
  class MyExtraCommand(MyCommand):
    def provideSomething(self):
      return "something!!"
```

This `my-extra-command` will do exactly the same things as my-command, except instead of
expecting the argument to be provided, it is assumed to be "something!!" instead. Since
we do not specify a `do` function, the command works exactly the same as the base
command.
