# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log    import loggable
from occam.config import Config

import math, re
import platform
import os
import codecs
import subprocess

from occam.manager import manager, uses

from occam.databases.manager import DatabaseManager
from occam.network.manager   import NetworkManager
from occam.storage.manager   import StorageManager

@loggable
@manager("system")
@uses(DatabaseManager)
@uses(NetworkManager)
class SystemManager:
  """
  This OCCAM manager does lookups about the state of the system it runs on.
  """

  @staticmethod
  def popen(command, stdout=subprocess.DEVNULL, stdin=None, stderr=subprocess.DEVNULL, cwd=None, env=None):
    return subprocess.Popen(command, stdout=stdout, stdin=stdin, stderr=stderr, cwd=cwd, env=env)

  def libraryPaths(self):
    """
    Returns the paths that ldconfig and ld look for libraries.
    """

    locations = []

    p = SystemManager.popen(["ldconfig", "-v"], stdout=subprocess.PIPE)

    reader = codecs.getreader('utf-8')
    f = reader(p.stdout)
    for line in f:
      if not line.startswith("\t"):
        location = line.split(':')[0].strip()
        if location:
          locations.append(location)

    return locations

  def locateLibrary(self, name):
    """
    Returns the path of the given library. Returns None if the library could
    not be found.
    """

    location = None

    check = []

    if not name.endswith(".so") or not name.endswith(".a"):
      check = ["%s.so" % name, "%s.a" % name]
    else:
      check = [name]

    p = SystemManager.popen(["ldconfig", "-p"], stdout=subprocess.PIPE)

    reader = codecs.getreader('utf-8')
    f = reader(p.stdout)
    for line in f:
      for filename in check:
        if '=>' in line and line.startswith("\t%s" % (filename)):
          location = line.split('=>')[1].strip()
          return location

    p.wait()

    return location

  def localInfo(self):
    """ Returns the node info for our own local node.
    """

    import json

    # Open the node info

    nodeInfoPath = os.path.join(Config.root(), "occam.json")
    try:
      with open(nodeInfoPath, 'r+') as f:
        nodeInfo = json.load(f)
    except:
      nodeInfo = {}

    config = Config.load()
    components = config.get('system', {}).get('components', [])
    passwordPolicy = config.get('passwords', {}).get('policy', {})

    nodeInfo.update({
      "components": components,
      "passwordPolicy": passwordPolicy
    })

    # Also parse information stored in the system metadata
    system = self.retrieve()
    if system:
      nodeInfo['host'] = system.host
      nodeInfo['port'] = system.port

    nodeInfo['version'] = self.version()

    return nodeInfo

  def version(self):
    """ Returns the Occam version.
    """

    return "1.0"

  def machineInfo(self):
    """ Returns metadata that identifies aspects of the hardware of this system.
    """

    ret = {}

    # Reads system information
    ret['platform'] = {}
    ret['platform']['machine'] = platform.machine()
    ret['platform']['system']  = platform.system()
    ret['platform']['kernel']  = platform.release()
    ret['platform']['version'] = platform.version()

    # Reads processor information
    ret['processors'] = []

    # Parses the cpuinfo file
    with open("/proc/cpuinfo") as f:
      currentProcessor = {}
      for line in f:
        if len(line.strip()) == 0:
          if currentProcessor:
            ret['processors'].append(currentProcessor)
            currentProcessor = {}

        if ':' in line:
          key, value = map(lambda x: x.strip(), line.split(':', 1))
          if key == "processor":
            currentProcessor['id'] = value
          elif key == "flags":
            currentProcessor['flags'] = list(map(lambda x: x.strip(), value.split(" ")))
          elif value != "":
            currentProcessor[key] = value

    return ret

  def storeInfo(self, info):
    """ Stores node info for our local node.
    """

    import json

    nodeInfoPath = os.path.join(Config.root(), "occam.json")
    with open(nodeInfoPath, 'w+') as f:
      f.write(json.dumps(info))

  def path(self):
    return Config.root()

  def initialized(self):
    """Returns True when this OCCAM node is initialized.

    Returns:
        bool: True when Occam has been initialized. False otherwise.
    """

    return os.path.exists(self.path())

  def initialize(self):
    """Initializes a local Occam node, if necessary.

    Returns:
        bool: True when Occam has been initialized properly. False on error.
    """

    import importlib
    import shutil

    from occam.manager import Manager

    # Create an example policy.
    policyExampleSrcPath = os.path.join(os.path.dirname(__file__),
                                        '..',
                                        '..',
                                        '..',
                                        'terms.yml.sample')
    policyDir = os.path.realpath(Config.load()['paths'].get("policies"))
    if not os.path.exists(policyDir):
      SystemManager.Log.noisy(f"creating directory {policyDir}")
    os.makedirs(policyDir, exist_ok = True)

    policyExampleDestPath = os.path.join(policyDir, 'terms.yml.sample')
    shutil.copyfile(policyExampleSrcPath, policyExampleDestPath)

    # Create the database
    self.database.createDatabase()

    # Establish the global system record
    system = self.retrieve()
    if system is None:
      # Set up system record with default paths
      SystemManager.Log.write("Establishing occam system in %s" % (self.path()))

      from occam.system.records.system import SystemRecord

      system = SystemRecord()

      system.host = self.network.hostname()
      system.port = 9292

      session = self.database.session()
      self.database.update(session, system)
      self.database.commit(session)

    # Initialize components (look for an initialize function in the manager)

    # Get a list of active components from the configuration
    components = self.configuration.get('components', [])

    # Import all managers for every active component
    for component in components:
      # Import the component manager
      try:
        mod = importlib.import_module("%s.manager" % (component))
      except ImportError:
        continue

      # Import the write manager as well, if it exists
      try:
        mod = importlib.import_module("%s.write_manager" % (component))
      except ImportError:
        continue

    # We will traverse the manager graph to initialize each manager as they
    # depend on one another. That is, we will initialize managers that have
    # no dependencies first and follow through to the more complex managers.
    pending = Manager.managers
    done = []

    # Some components report information to the node block. This information is
    # propagated on discovery so that other nodes can discover service
    # information (IPFS ports, etc) that exist on each node. The component
    # returns that information from its `initialize` method instead of the
    # typical Boolean return value when that component wishes to do so.
    nodeInfo = {}

    # Traverse managers and load them based on their dependencies
    while pending:
      current = pending
      pending = []
      for manager in current:
        canProceed = True

        # If this component has an initialization method (and isn't ourselves)
        # then we ensure that all the managers it uses have already been
        # previously initialized. If so, we initialize the component.
        # Manager dependency cycles are not allowed.
        if hasattr(manager, 'initialize') and manager is not SystemManager:
          for k, requirement in manager._managers.items():
            if not requirement in done:
              canProceed = False

        if canProceed:
          # If requirements are satisfied and the initializion method exists,
          # perform component initialization.
          if hasattr(manager, 'initialize') and manager is not SystemManager:
            SystemManager.Log.write(f"Initializing {manager.__name__}")
            ret = manager().initialize()
            if ret is False or ret is None:
              SystemManager.Log.error(f'failed to initialize {manager.__name__}')
              return False
            elif isinstance(ret, dict):
              # The component wants to report an information block
              nodeInfo[manager._token] = ret

          # Make a note that this manager has been completed
          done.append(manager)
        else:
          # We still need to initialize this manager, so push it back on the
          # list and we will try again next loop.
          pending.append(manager)

    # Store the component information block
    self.storeInfo(nodeInfo)

    # We were successful in initializing all components
    return True

  def retrieve(self):
    """ Returns a SystemRecord representing the System configuration.
    """

    import sql

    session = self.database.session()

    systems = sql.Table('systems')

    query = systems.select()

    self.database.execute(session, query)

    from occam.system.records.system import SystemRecord

    record = self.database.fetch(session)

    if record is None:
      return None

    return SystemRecord(record)

  def computeStoreSize(self):
    """ Returns the total storage used by the local object storage.

    Returns a dictionary with an 'objectStore' key with an 'apparent' which is the sum of the reported bytes and 'physical' which is the filesystem bytes used.
    """

    # TODO: we could cache the sizes of particular directories and
    #       use timestamps to expire them

    ret = {}

    storagePath = Config.load().get('storage', {}).get('local', {}).get('path', os.path.join(Config.root(), "objects"))
    ret['objectStore'] = {}
    ret['objectStore']['path'] = storagePath

    # TODO: UNIX only. Fix up (maybe pick a reasonable block size?)
    statv = os.statvfs(storagePath)
    ret['objectStore']['blockSize'] = statv.f_frsize

    ret['objectStore']['apparent'] = 0
    ret['objectStore']['physical'] = 0

    ret['objectStore']['git'] = {}
    ret['objectStore']['git']['apparent'] = 0
    ret['objectStore']['git']['physical'] = 0

    ret['objectStore']['builds'] = {}
    ret['objectStore']['builds']['apparent'] = 0
    ret['objectStore']['builds']['physical'] = 0

    ret['objectStore']['cache'] = {}
    ret['objectStore']['cache']['apparent'] = 0
    ret['objectStore']['cache']['physical'] = 0

    ret['objectStore']['uncached'] = {}
    ret['objectStore']['uncached']['apparent'] = 0
    ret['objectStore']['uncached']['physical'] = 0

    ret['objectStore']['object.json'] = {}
    ret['objectStore']['object.json']['apparent'] = 0
    ret['objectStore']['object.json']['physical'] = 0

    ret['objectStore']['resource'] = {}
    ret['objectStore']['resource']['apparent'] = 0
    ret['objectStore']['resource']['physical'] = 0

    def computeSpaceForPath(filepath, storagePath, accumulator, buildCache = None):
      if buildCache is None:
        buildCache = {}

      # Ensure we don't follow symlinks
      stat = os.lstat(filepath)

      relpath = filepath[len(storagePath):]
      uuid = None
      subpath = None
      if len(relpath) > 43:
        uuid = relpath[7:43]
        subpath = relpath[44:].split('/')[0]

      fileSize   = stat.st_size
      storedSize = stat.st_blocks * 512
      accumulator['apparent'] += fileSize
      accumulator['physical'] += storedSize

      if subpath == "cache":
        accumulator['cache']['apparent'] += fileSize
        accumulator['cache']['physical'] += storedSize
      else:
        accumulator['uncached']['apparent'] += fileSize
        accumulator['uncached']['physical'] += storedSize

      if filepath.endswith("object.json"):
        accumulator['object.json']['apparent'] += fileSize
        accumulator['object.json']['physical'] += storedSize

      if subpath == "repository" and "/repository/.git" in filepath:
        accumulator['git']['apparent'] += fileSize
        accumulator['git']['physical'] += storedSize

      if subpath == "resource":
        accumulator['resource']['apparent'] += fileSize
        accumulator['resource']['physical'] += storedSize

      if subpath == "builds":
        accumulator['builds']['apparent'] += fileSize
        accumulator['builds']['physical'] += storedSize
        if uuid not in buildCache:
          buildCache[uuid] = {"apparent": 0, "physical": 0}
        else:
          buildCache[uuid]["apparent"] += fileSize
          buildCache[uuid]["physical"] += storedSize

        if 'largest' not in accumulator['builds'] or accumulator['builds']['largest']['apparent'] < buildCache[uuid]['apparent']:
          accumulator['builds']['largest'] = accumulator['builds'].get('largest', {})
          accumulator['builds']['largest']['id'] = uuid
          accumulator['builds']['largest']['apparent'] = buildCache[uuid]['apparent']
          accumulator['builds']['largest']['physical'] = buildCache[uuid]['physical']

    buildCache = {}
    for dirpath, dirnames, filenames in os.walk(storagePath):
      for f in filenames + dirnames:
        filepath = os.path.join(dirpath, f)

        computeSpaceForPath(filepath, storagePath, ret['objectStore'], buildCache=buildCache)

    computeSpaceForPath(storagePath, storagePath, ret['objectStore'])

    return ret

  def computeRunCacheSize(self):
    """ Returns information about the current storage utilization of the run cache.
    """

    ret = {}

    runsPath = os.path.join(Config.root(), "runs")
    ret['runCache'] = {}
    ret['runCache']['path'] = runsPath
    ret['runCache']['apparent'] = 0
    ret['runCache']['physical'] = 0

    cache = {}
    largest = None

    accumulator = ret['runCache']

    for dirpath, dirnames, filenames in os.walk(runsPath):
      for f in filenames + dirnames:
        filepath = os.path.join(dirpath, f)
        uuid = None
        if (len(filepath) > len(runsPath)):
          uuid = filepath[len(runsPath):][7:43]
          if uuid not in cache:
            cache[uuid] = {}
            cache[uuid]['apparent'] = 0
            cache[uuid]['physical'] = 0

        stat = os.lstat(filepath)

        fileSize   = stat.st_size
        storedSize = stat.st_blocks * 512
        accumulator['apparent'] += fileSize
        accumulator['physical'] += storedSize

        if uuid is not None:
          cache[uuid]['apparent'] += fileSize
          cache[uuid]['physical'] += storedSize
          if largest is None or cache[largest]['apparent'] < cache[uuid]['apparent']:
            largest = uuid

    if largest:
      ret['largest'] = {
        "id": largest
      }
      ret['largest'].update(cache[largest])

    return ret

  def policies(self):
    """ Get system policies.

    Gets all of the system policies to which the user must agree to abide by
    in order to use the system. The policies are stored in the 'policies'
    directory as specified by the system configuration.

    Example:
    {
      "eula": {
        "title": "Terms and Conditions",
        "text": "Some terms",
      }
    }

    Returns:
      dict: A dictionary of policies. Each policy is a dictionary with a
        "title" and "text" key.
    """
    import yaml

    policies = {}

    # Load all of the policies into the policy directory.
    policyDir = os.path.realpath(Config.load()['paths'].get("policies"))
    policyKeyToPath = {}
    for dirpath, dirnames, filenames in os.walk(policyDir):
      for f in filter (lambda fn: fn.endswith('.yml'), filenames):
        path = os.path.join(dirpath, f)

        policyKey = f[0:-4]

        # Policy filenames must fall within a certain pattern in order to work
        # on the front-end.
        import re
        filteredPolicyKey = re.sub('[^a-zA-Z]', '', policyKey)

        # Ensure no two scrubbed file names have the same resulting key.
        if filteredPolicyKey in policies.keys():
          Log.error(f"Policy located at '{path}' can't be used because it "
                    f"resolves to the same key as the policy located at "
                    f"'{policyKeyToPath[filteredPolicyKey]}'")
          continue

        # Load the policy dictionary.
        policyKeyToPath[filteredPolicyKey] = path
        try:
          with open(path, "r") as fh:
            policies[filteredPolicyKey] = yaml.safe_load(fh)
        except (yaml.YAMLError, OSError, IOError) as e:
          Log.error(f"Unable to open the policy specified at '{path}': {e}")
          continue

    return policies
