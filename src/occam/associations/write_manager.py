# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.log     import loggable
from occam.manager import manager, uses

from occam.associations.manager import AssociationManager

@loggable
@manager("associations.write", reader=AssociationManager)
class AssociationWriteManager:
  """ This OCCAM manager keeps track of associations between objects.
  """

  def create(self, object, associationType, major, minor=None, identity=None):
    """ This creates an association.
    """

    import datetime
    published = datetime.datetime.now()

    self.datastore.write.createAssociation(object, associationType = associationType,
                                                   major           = major,
                                                   minor           = minor,
                                                   identity        = identity,
                                                   published       = published)

  def destroy(self, object, associationType, major, minor=None, identity=None):
    """ This destroys an existing association.
    """

    self.datastore.write.removeAssociation(object, associationType = associationType,
                                                   major           = major,
                                                   minor           = minor,
                                                   identity        = identity)
