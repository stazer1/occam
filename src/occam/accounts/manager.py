# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Account records are authentication records. They are attached to a Person
# which is a general account of a particular Person/Organization in the
# federation. If a Person has an Account record, they can authenticate to
# the current node. A Person may have an Account on multiple clusters in
# the federation, enabling them to log in to several different nodes.

# When you authenticate to the system, you generate a token. In this case it
# is an HMAC-SHA256 generated using a random 64 byte key. When an Account is
# created (see AccountManager.addAuthentication) that key is generated along
# with an RSA key. (The RSA key is not used for node authentication)

# For terminal access, the login token is implied by the owner of that shell.
# The token is generated upon a "login" command and stored in the shell
# owner's OCCAM home under 'auth.json'. This file is read when any command is
# used and treated as the current Person. (see AccountManager.currentPerson)

# For other access (or to force a particular Person,) a Person UUID and token
# can be passed to any OCCAM command using the "--as" and "--token" (-A, -T)
# arguments. (See CommandManager.execute and CommandManager.parser) In these
# cases, AccountManager.currentPerson is called with those instead of the keys
# found in the auth.json file. This is used for the socket connections and
# authenticated clients. For example, an interactive web server.

# Passwords are used to authenticate, as well. They are used to generate the
# token. Passwords often have less entropy than tokens, so they are point of
# least resistance to break authentication. Stronger authentication might be
# implemented later, such as an external secure authenticator, or multi-factor
# authentication.

# Passwords are stored in the database as a salted hash using bcrypt (can be
# configured to add new algorithms later) and uses a configurable number of
# rounds. Although all passwords are assumed to use the same number of rounds.
# If rounds were to change, all passwords would have to be reset somehow.
# There is no plan on how to do this.

import os, datetime

from occam.person import Person
from occam.config import Config

from occam.manager import uses, manager

from occam.system.manager        import SystemManager
from occam.objects.write_manager import ObjectWriteManager
from occam.permissions.manager   import PermissionManager
from occam.keys.manager          import KeyManager
from occam.people.write_manager  import PersonWriteManager
from occam.messages.manager      import MessageManager

@uses(MessageManager)
@uses(SystemManager)
@uses(ObjectWriteManager)
@uses(PermissionManager)
@uses(KeyManager)
@uses(PersonWriteManager)
@manager("accounts")
class AccountManager:
  """ Manages Accounts and People within the node or federation.
  """

  def addPerson(self, name, identity, subtypes=[]):
    """ Create a new person for this identity.

    Arguments:
      name (str): The name for the Person.
      identity (str): The identity URI we are making the Person for.
      subtypes (list): Any subtypes, as strings, we want on the new object.

    Returns:
      Person: The new created Person object.
    """

    info = None
    if subtypes:
      info = {
        "name": name,
        "type": "person",
        "subtype": subtypes
      }

    person = self.objects.write.create(name, object_type = "person",
                                             info = info,
                                             identity = identity)

    self.objects.write.store(person, identity)

    # Create the Person record, to attach it to this account
    self.people.write.update(identity, person)

    # Make Person objects public accessible
    personInfo = self.objects.infoFor(person)
    self.permissions.update(id = person.id, canRead=True, canWrite=False, canClone=False)

    # Make Person object writable by that person
    self.permissions.update(id = person.id, identity=person.identity, canRead=True, canWrite=True, canClone=False)

    # Return the new Person object
    return person

  def addMember(self, group, member):
    """ Adds an existing Person as a member under another Person

    Arguments:
      group (Person): The Occam object representing the group to be amended.
      member (Person): The Occam object representing the person to be added.
      person (Person): The Person who is adding the member to the group.

    Returns:
      MembershipRecord: The record reflecting the membership. None if the membership could not be created.
    """
    if self.objects.objectPositionWithin(group, member) is None:
      # Add the item to the object itself
      self.objects.write.addObjectTo(group, subObject=member, create=False)

      # Create the Membership record for access control purposes
      row = self.datastore.insertMembership(group, member)
    else:
      row = self.datastore.retrieveMembershipFor(group, member)

    # Return the MembershipRecord
    return row

  def removeMember(self, group, member):
    """ Removes an existing member, given as a Person, of its base Person.
    """

    self.objects.write.removeObjectFrom(group, member)

    self.datastore.deleteMembership(group, member)

  def retrieveMembershipsFor(self, member):
    """ Retrieves the list of groups for the given member Object.
    """

    return self.datastore.retrieveMembershipObjectsFor(member)

  def retrieveMembershipsOf(self, group):
    """ Retrieves the list of members for the given group.
    """

    return self.datastore.retrieveMembershipObjectsOf(group)

  def retrievePerson(self, identity):
    """ Retrieves the Person object associated with an identity.

    Arguments:
      identity (str): The identity URI to look up.

    Returns:
      Object: The person object associated with this account (or None)
    """

    account_db = self.datastore.retrieveAccount(identity = identity)
    if account_db is None:
      return None

    person = self.objects.retrieve(id = account_db.person_id)
    if person:
      person.roles = self.rolesFor(account_db)
      person.subscriptions = self.subscriptionsFor(account_db)

    return person

  def retrieveEmail(self, identity):
    """ Retrieves the email address associated with an identity.

    Arguments:
      identity (str): The identity URI to look up.

    Returns:
      String: The email address as string (or None)
    """

    account_db = self.datastore.retrieveAccount(identity = identity)
    if account_db is None:
      return None

    email_address = account_db.email
    if email_address is None:
      return None

    return email_address

  def retrieveAllAccounts(self):
    """ Retrieves all accounts in system.

    Returns:
      list: List of account records.
    """

    return self.datastore.retrieveAllAccounts()

  def validateEmail(self, account_db, token):
    """ Marks the account's email as validated if the token matches.

    Changes `email_verified` to True in associated account if given token matches
    the record.

    Arguments:
      account_db (AccountRecord): The associated account record.
      token (str): The verification token to attempt to match.

    Returns:
      bool: True if the token is verified and False otherwise.
    """

    token_expired = datetime.datetime.now() >= account_db.email_token_expiration
    if account_db.email_token != token or token_expired:
      return False

    self.datastore.verifyEmail(account_db)
    return True

  def isEmailAddressValid(self, email):
    """ Checks whether given email is valid.

    Arguments:
      email (str): The proposed email address.

    Returns:
      bool: True if email conforms to expected format, False otherwise.
    """
    import re

    # This is a broad and inclusive check. See
    # https://stackoverflow.com/questions/201323/how-to-validate-an-email-address-using-a-regular-expression
    # for REC 5322 compliant regex
    return bool(re.fullmatch(r"[^@]+@[^@]+\.[^@]+", email))

  def addEmail(self, account_db, email):
    """ Updates the email address for the given account.

    Arguments:
      account_db (AccountRecord): The account to update.
      email (str): The new email address.
    """

    token = self.generateEmailToken()
    expiration = datetime.datetime.now() + datetime.timedelta(minutes=15)
    self.datastore.updateEmail(account_db, email, expiration, token)
    self.messages.sendAccountConfirmation(account_db, "smtp")

  def setEmailVisibility(self, account_db, isPublic):
    """ Changes email visibility for the given account.

    Arguments:
      account_db (AccountRecord): The account to update.
      isPublic (bool): The value email_public will take.
    """

    self.datastore.setEmailVisibility(account_db, isPublic)

  def isResetTokenValid(self, account_db, token):
    """ Checks if token, as given, is still valid against the recorded token.

    This checks both that the token matches and that the token has not expired.

    Arguments:
      account_db (AccountRecord): The account record to assess.
      token (str): The reset token to match.

    Returns:
      bool: True if the token matches and is not expired and False otherwise.
    """

    correct_token = account_db.reset_token == token
    token_expired = datetime.datetime.now() >= account_db.reset_token_expiration
    return correct_token and not token_expired

  def primeAccountForPasswordReset(self, account):
    """ Gets freshly generated reset token, and sets new expiration time for
    the token.

    If there is an active reset token, we keep it until it is used or the token
    expires.

    Arguments:
      account (AccountRecord): The account being reset.

    Returns:
      string: The reset token to be used by the user to reset their account.
    """

    # See if there is an active reset token. We don't want anything to
    # invalidate a token too early (such as an automatic suggestion to change
    # your password when too many failures are detected).
    resetToken = account.reset_token
    expiration = account.reset_token_expiration
    now = datetime.datetime.now()

    # If it is time for a new token, create one and store it. Otherwise we will
    # reuse the existing token.
    #
    # We can't just keep extending the expiration time, because that would
    # allow an attacker to force the token to stay the same while they
    # attempted to brute force the URL.
    if expiration is None or resetToken is None or expiration <= now:
      # Generate a password token
      resetToken = self.generateResetToken()

      # 15 minutes until the password reset token is no longer valid
      expiration = datetime.datetime.now() + datetime.timedelta(minutes = 15)

      # Record the token
      self.datastore.setPasswordResetToken(account, expiration, resetToken)

    return resetToken

  def forgotPasswordReset(self, account_db):
    """ Acquires a reset token and sends the password reset email.
    """

    self.primeAccountForPasswordReset(account_db)

    # Send a notification of the reset (email, etc)
    self.messages.sendPasswordReset(account_db)

  def updatePerson(self, identity, person):
    """ Updates the Person associated with the given Account.

    Arguments:
      identity (str): The identity URI associated with the account.
      person (Person): The new Person to associate with the account.
    """

    self.datastore.updateAccount(identity, personId = person.id)
    self.people.write.update(identity, person)

  def retrieveAccount(self, username=None, email=None, identity=None):
    """ Retrieves the Account matching the given criteria.

    Arguments:
      username (str): A username.
      email (str): An email.
      identity (str): The identity URI for the actor.

    Returns:
      AccountRecord: The valid record of the Account or None if not known.
    """

    return self.datastore.retrieveAccount(username = username,
                                          email = email,
                                          identity = identity)

  def loginFailuresSinceSuccess(self, username, ipAddress, timeWindow=None):
    """ Retrieves either the number of login failures since the last 
    successful login from the same IP, or the number of login failures since
    the specified time.

    Password change counts as a success.

    Arguments:
      username (str): The username of the account for which a login is being
        attempted.
      ipAddress (str): The IP address used to attempt the login.
      timeWindow (datetime): Time from which we want to see the number of login
        failures.

    Returns:
      int: The number of failed logins that count towards locking out an
        account.
    """
    if timeWindow:
      eventTypes = ['password_change']
    else:
      eventTypes = ['password_change', 'login_success']

    # Figure out when the last password reset was if any.
    resets = self.datastore.retrieveAccountEvents(username = username,
                                                  ipAddress = ipAddress,
                                                  eventTypes = eventTypes,
                                                  latest = True)

    # We are only interested in the failures since time requested, or since the
    # last password reset if that happens to have occurred more recently.
    if timeWindow:
      if resets:
        fromTime = max(timeWindow, resets[0].event_time)
      else:
        fromTime = timeWindow

    else:
      fromTime = None
      if resets:
        fromTime = resets[0].event_time

    failures = self.datastore.retrieveAccountEvents(username = username,
                                                    ipAddress = ipAddress,
                                                    eventTypes = ['login_failure'],
                                                    fromTime = fromTime)

    return len(failures)

  def sendExcessiveLoginFailMessage(self, account, ipAddress, failureCount):
    """ Sends a message to a user alerting them about unsuccessful login attempts.

    It is the responsibility of the caller to ensure the account exists.

    Arguments:
      account (AccountRecord): The account with excessive login failures.
      ipAddress (str): The IP address used to access account.
      failureCount (int): How many failed logins have occurred since the last
        successful login.
    """
    import html
    if not account.email:
      return

    config = Config().load()
    domain = config.get("daemon", {}).get("externalHost", None)

    resetToken = self.primeAccountForPasswordReset(account)

    resetUrl = f"people/{account.identity_key_id}/reset-password/{resetToken}"

    # This could be passed in by anyone with console access. Escape it for good measure.
    ipAddress = html.escape(ipAddress)

    message = (f"There have been {failureCount} unsuccessful login attempts to "
               f"your account originating from the IP address "
               f"'{ipAddress}'.<br><br>"
               f"If this is not activity you recognize, please consider "
               f"changing your password or contact the system administrator. "
               f"You may click the link below to reset your password<br>"
               f"https://{domain}/{resetUrl}")

    self.messages.sendMessage("Occam Failed Login Attempts", message, account)

  def recordAccountEvent(self, username, ipAddress, eventType, eventTime):
    """ Records an event occurred for the given account.

    Arguments:
      username (str): The username of the account involved.
      ipAddress (str): The IP address where the event originated.
      eventType (str): The event type being logged.
      eventTime (datetime): When the event occurred.

    Returns:
      LoginRecord: The valid login information for the account.
    """
    return self.datastore.createAccountEvent(username = username,
                                             ipAddress = ipAddress,
                                             eventType = eventType,
                                             eventTime = eventTime)

  def authTokenPath(self):
    """ Returns the path of the local account's authentication token.

    This token is used to authenticate when someone logs in using the
    accounts 'login' command in their local terminal.

    Returns:
      str: The path of the local account's `auth.json` file.
    """

    return os.path.join(Config.root(), "auth.json")

  def currentAuthentication(self):
    """ Retrieves the current authentication metadata.

    Returns:
      dict: A set of metadata including the authentication token.
    """

    auth_token_path = self.authTokenPath()

    if os.path.exists(auth_token_path):
      import json

      try:
        with open(auth_token_path, 'r') as f:
          return json.load(f)
      except:
        pass

    return {}

  def removeAuthentication(self):
    """ Removes the local account's authentication token.

    This removes a generated token that authenticate's the current owner of the
    terminal session. This token was likely generated by a prior call to the
    account component's 'login' command.

    This function is likely called in response to the associated 'logout'
    command.
    """

    auth_token_path = self.authTokenPath()

    if os.path.exists(auth_token_path):
      os.remove(auth_token_path)

  def generateToken(self, account_db, obj = None, access = "readOnly"):
    """ Generates a JSON Web Token (JWT) authenticating the given account.

    This can also generate a token that authorizes actions to an object for
    other actors on the system. For instance, a token can be generated to give
    some people 'readOnly' or 'anonymous' access to an object that is otherwise
    private.

    The token is generated and then encoded using the account secret. Only this
    particular server (or one sharing the secret) can decode and accept the
    token.

    Arguments:
      account_db (AccountRecord): The account record to encode.
      obj (Object): The object to authorize, if any.
      access (str): The type of access to authorize for that object.

    Returns:
      str: The encoded string.
    """

    import jwt

    token = {
      'identity': account_db.identity_key_id
    }

    if obj:
      token[access] = obj.id

    secret = account_db.secret

    encoded = jwt.encode(token, secret, algorithm="HS256")

    return encoded

  def generateEmailToken(self):
    """ Generates an email validation token.

    Returns:
      str: A random string that authenticates a password reset.
    """

    # Just generate the same type of token as a reset
    return self.generateResetToken()

  def generateResetToken(self):
    """ Generates a password reset token.

    Returns:
      str: A random string that authenticates a password reset.
    """

    # See https://docs.python.org/3/library/secrets.html for best practices
    import secrets

    reset_token = secrets.token_urlsafe(32)
    return reset_token

  def generateAuthentication(self, account_db_object):
    auth_token_path = self.authTokenPath()

    token = self.generateToken(account_db_object)

    auth_token = {
      'account': account_db_object.username,
      'token':   token
    }

    import json

    with open(auth_token_path, 'w+') as f:
      f.write(json.dumps(auth_token))

    return auth_token

  def decodeToken(self, token):
    """ Returns the decoded token information.

    Arguments:
      token (str): The encoded token.

    Returns:
      dict: The decoded token data.
    """

    import jwt

    # We must first decode the token to determine the identity. Verification
    # would normally happen with the account secret, but we don't know the
    # account yet.
    decodedToken = {}
    try:
      unverified = jwt.decode(token.encode('utf-8'),
                              options = {"verify_signature": False},
                              algorithms=['HS256'])
    except:
      unverified = {}

    # Get the identity who's account secret we must check.
    identity = unverified.get('identity')
    if identity is not None:
      account_db = self.retrieveAccount(identity = identity)

      if account_db is not None:
        # Authenticate the original token against the account secret for the
        # identity the token held. If the token was a forgery, this will fail.
        # Otherwise we will be able to return the decoded token.
        try:
          decodedToken = jwt.decode(token.encode('utf-8'),
                                    account_db.secret,
                                    algorithms=['HS256'])
        except (jwt.exceptions.InvalidSignatureError, jwt.exceptions.DecodeError) as e:
          pass

    return decodedToken

  def currentPerson(self, token=None):
    """ Retrieves the Person associated with the current account or token.

    The current Person is either retrieved by the given authentication token
    or it is inferred by the local account of the terminal session via the
    `auth.json` file.

    Will return None if the account could not be authenticated, or the account
    is not known, or no account is logged in locally.

    Arguments:
      token (str): The authentication token to decode, or None.

    Returns:
      Person: The person object associated with the account, or None.
    """

    import jwt

    account_db = None

    if token is None:
      auth_token = self.currentAuthentication()

      if 'account' in auth_token:
        token = auth_token.get('token')

    if token is None:
      return None

    identity = self.decodeToken(token).get('identity')
    if identity is None:
      return None

    return self.retrievePerson(identity)

  def currentPersonId(self):
    """ Returns the object id of the Person associated with the current Account.

    Returns:
      str: The Person object id or None, if not known or no current actor.
    """

    person = self.currentPerson()

    if person:
      return person.id

    return None

  def authenticate(self, username=None, email=None, password=None):
    """ Authenticates an account on the system via username or email and password.

    Only one of either username or email may be specified to perform a lookup
    of the account. The function will return False if no account could be found.

    The password is then matched against the account's records and will only
    return True when successfully hashed.

    Arguments:
      username (str): The username of the account.
      email (str): The email of the account.
      password (str): The password to use.

    Returns:
      bool: True, if successful, and False otherwise.
    """

    # Use only one of: username/email
    if username and email:
      raise ValueError("Username and email cannot both be given to authenticate")

    if password is None or isinstance(password, int) or isinstance(password, float):
      raise TypeError("Password must not be None, int, or float.")

    # Retrieve the AccountRecord for the account matching either the username or email
    account = self.retrieveAccount(username=username, email=email)
    if account is None:
      # If no such account is found, we did not authenticate
      return False

    import bcrypt  # For password hashing

    return bcrypt.hashpw(password.encode('utf-8'), account.hashed_password.encode('utf-8')) == account.hashed_password.encode('utf-8')

  def rolesFor(self, account_db):
    """ Returns the roles for the given account.

    Arguments:
      account_db (AccountRecord): The account record containing the role list.

    Returns:
      list: A set of strings consisting of roles attached to the account.
    """

    # Gather existing roles (they are ; delimited)
    try:
      roles = (account_db.roles or ";;")[1:-1].split(';')
    except TypeError:
      raise TypeError

    # Ensure an empty list is properly handled
    if len(roles) == 1 and roles[0] == "":
      roles = []

    return roles

  def addRoles(self, account_db, newRoles=[]):
    """ Adds the given roles to the given AccountRecord
    """

    roles = self.rolesFor(account_db)

    # Append given roles
    roles.extend(newRoles)

    return self.commitRoles(account_db, roles)

  def removeRoles(self, account_db, rmRoles=[]):
    """ Remove account roles specified in rmRoles.
    """

    from occam.accounts.records.account import AccountRecord

    if not isinstance(account_db,AccountRecord):
      raise Exception("The argument should be an AccountRecord instance")

    roles = self.rolesFor(account_db)

    # Get the difference in the role set
    roles = list(set(roles) - set(rmRoles))

    return self.commitRoles(account_db, roles)

  def commitRoles(self, account_db, roles=[]):
    # Remove duplicates
    roles = list(set(roles))

    # Remove empty roles
    roles = list(filter(None, roles))

    # Update the roles list
    self.datastore.updateAccount(account_db.identity_key_id, roles = roles)

    # Return the roles list
    return roles

  def subscriptionsFor(self, account_db):
    """ Returns the email event subscriptions for the given account.

    Arguments:
      account_db (AccountRecord): The account to query.

    Returns:
      list: A set of strings consisting of the active event subscriptions.
    """

    # Parse the field
    subscriptions = (account_db.subscriptions or ";;")[1:-1].split(';')

    # Remove duplicates
    subscriptions = list(set(subscriptions))

    # Remove empty subscriptions
    subscriptions = list(filter(None, subscriptions))

    # Return the subscriptions list
    return subscriptions

  def addSubscriptions(self, account_db, newSubscriptions=[]):
    """ Adds the given subscriptions to the given AccountRecord

    Arguments:
      account_db (AccountRecord): The account to update.
      newSubscriptions (list): A set of strings to add to the account.

    Returns:
      list: A set of strings consisting of the active event subscriptions.
    """

    # Get the current subscriptions
    subscriptions = self.subscriptionsFor(account_db)

    # Append given subscriptions
    subscriptions.extend(newSubscriptions)

    # Commit and return the list
    return self.commitSubscriptions(account_db, subscriptions)

  def removeSubscriptions(self, account_db, removeSubscriptions=[]):
    """ Removes subscriptions found in the provided list.

    Arguments:
      account_db (AccountRecord): The account to update.
      newSubscriptions (list): A set of strings to remove from the account.

    Returns:
      list: A set of strings consisting of the active event subscriptions.
    """

    # Get the current subscriptions
    subscriptions = self.subscriptionsFor(account_db)

    # Get the difference in the subscription set
    subscriptions = list(set(subscriptions) - set(removeSubscriptions))

    # Commit and return the list
    return self.commitSubscriptions(account_db, subscriptions)

  def commitSubscriptions(self, account_db, subscriptions=[]):
    """ Updates the subscriptions active for the given account.

    This overrides all subscriptions of the given account and updates the list
    to be equal to the given list.

    Arguments:
      account_db (AccountRecord): The account to update.
      subscriptions (list): A set of strings to set for the account.

    Returns:
      list: A set of strings consisting of the active event subscriptions.
    """

    # Remove duplicates
    subscriptions = list(set(subscriptions))

    # Remove empty subscriptions
    subscriptions = list(filter(None, subscriptions))

    # Update the subscriptions list
    self.datastore.updateAccount(account_db.identity_key_id, subscriptions = subscriptions)

    # Return the subscriptions list
    return subscriptions

  def addAuthentication(self, person_id, username, password, identity, roles=[], email=None):
    # Get system admin record
    system = self.system.retrieve()

    # Accounts are active by default, unless moderation says otherwise
    active = True
    if system.moderate_accounts:
      # We are moderating accounts
      active = False

    # First account defaults to administrator role
    accountCount = self.datastore.countAccounts()
    if accountCount == 0:
      if not 'administrator' in roles:
        roles.append('administrator')

    # Add the roles designated by the admin table
    default_roles = ((system.default_roles or ";;")[1:-1] or "").split(';')
    for default_role in default_roles:
      if not default_role in roles:
        roles.append(default_role)

    # Gather a unique set of roles
    roles = list(set(roles))

    # Set subscriptions. Default to no subscriptions
    subscriptions = ['']

    # Create password auth
    hashed_password = self.hashPassword(password)

    secret = self.keys.randomHex(64)

    # NOTE: don't specify the email here, so we can distinguish DataNotUnique
    # errors when adding email.
    account = self.datastore.createAccount(username = username,
                                           person_id = person_id,
                                           hashed_password = hashed_password,
                                           identity = identity,
                                           secret = secret,
                                           roles = roles,
                                           subscriptions = subscriptions,
                                           active = active)

    # Add email address if provided and create email token.
    # Send verification msg after account added
    if email:
      self.addEmail(account, email)

  def hashPassword(self, password):
    """ Generates a password hash for the given string.

    Arguments:
      password (str): The password to encode.

    Returns:
      str: The hashed password string.
    """

    if password is None or isinstance(password, int) or isinstance(password, float):
      raise TypeError("Password must not be None, int, or float.")

    rounds = 12
    config_info = self.configuration
    rounds = config_info.get('passwords', {}).get('rounds', rounds)

    import bcrypt   # For password hashing
    return bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt(rounds, prefix=b"2a")).decode('utf-8')

  def updatePassword(self, account_db, password, ipAddress):
    """ Updates the password for the given account.

    Arguments:
      account_db (AccountRecord): The account record to update.
      password (str): The new password to use.
    """

    hashed_password = self.hashPassword(password)
    self.datastore.updatePassword(account_db, hashed_password)
    self.recordAccountEvent(username = account_db.username,
                            ipAddress = ipAddress,
                            eventType = "password_change",
                            eventTime = datetime.datetime.now())

  def failedPasswordComponents(self, password, username):
    """ Checks whether password meets policy standards.

    Arguments:
      password (str): The proposed password.
      username (str): The account username.

    Returns:
      list: Parts of policy that failed. Empty if requirements are met.
    """

    # TODO: Could make a check against a password dictionary a configurable option.
    from password_strength import PasswordPolicy
    from Levenshtein import distance

    config = Config().load()
    requirements = config.get("passwords", {}).get("policy")

    failedComponents = []
    if requirements is not None:
      policy = PasswordPolicy.from_names(**requirements)

      # policy.test returns empty list if password complies
      failedComponents = policy.test(password)

    failedDistance = []
    if distance(username, password) <= 1:
      failedDistance = ['Similarity to username']

    return failedComponents.extend(failedDistance)

  def isUsernameValid(self, username):
    """ Checks whether username meets standards.

    Arguments:
      username (str): The proposed username.

    Returns:
      bool: True if username complies with policy, False otherwise.
    """
    import re

    # The first character must be alphanumeric. There must be two characters or
    # more. Second character onward may include '-' or '_' characters.
    return bool(re.fullmatch(r"[a-zA-Z0-9][a-zA-Z0-9_-]+", username, flags = re.ASCII))

  def authorsFor(self, id):
    """ Returns a PersonRecord list for all authors of the given object.

    Arguments:
      id (str): The object ID.

    Returns:
      list: A set of PersonRecord elements for every author of the object.
    """

    return self.datastore.retrieveAuthors(id)

  def collaboratorsFor(self, id):
    """ Returns a PersonRecord list for all collaborators of the given object.

    Arguments:
      id (str): The object ID.

    Returns:
      list: A set of PersonRecord elements for every collaborator of the object.
    """

    return self.datastore.retrieveAuthors(id, kind="collaboratorships")

  def removeAuthorship(self, id, identity):
    """ Removes the given authorship.

    Arguments:
      id (str): The object id.
      identity (str): The identity of the author to remove from the object.
    """

    self.datastore.removeAuthor(id, identity)

  def removeCollaboratorship(self, id, identity):
    """ Removes the given collaboratorship.

    Arguments:
      id (str): The object id.
      identity (str): The identity of the collaborator to remove from the object.
    """

    self.datastore.removeAuthor(id, identity, kind = "collaboratorships")

  def addAuthorship(self, id, identity):
    """ Adds the given identity as an author of the given Object.

    Arguments:
      id (str): The object id.
      identity (str): The identity of the author to add to the object.
    """

    self.datastore.addAuthor(id, identity)

  def addCollaboratorship(self, id, identity):
    """ Adds the given identity as a collaborator of the given Object.

    Arguments:
      id (str): The object id.
      identity (str): The identity of the collaborator to add to the object.
    """

    self.datastore.addAuthor(id, identity, kind = "collaboratorships")

  def isAccountLocked(self, username, ipAddress):
    """ Determine if the account is locked for the given IP.

    Arguments:
      username (string): The username of the account in question.
      ipAddress (string): The reported IP of the person attempting to log in.
      pwConfig (dict): The dictionary of password related configuration
        settings from which to pull lockout rules.

    Returns:
      bool: Whether the number of login failures is sufficient to temporarily
        lock the account from the given IP.
    """

    pwConfig = Config().load().get("passwords", {})
    timeWindowMinutes = pwConfig.get("lockoutTimeWindowMinutes")
    lockoutThreshold = pwConfig.get("lockoutThreshold")

    if timeWindowMinutes is None or lockoutThreshold is None:
      return False

    timeDelta = datetime.timedelta(minutes = timeWindowMinutes)
    fromTime = datetime.datetime.now() - timeDelta
    numLoginFailures = self.loginFailuresSinceSuccess(username = username,
                                                      ipAddress = ipAddress,
                                                      timeWindow = fromTime)

    return numLoginFailures >= lockoutThreshold
