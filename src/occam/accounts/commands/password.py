# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.manager import uses

from occam.accounts.manager import AccountManager
from occam.objects.manager  import ObjectManager

from occam.commands.manager import command, option, argument

@command('accounts', 'password',
  category      = 'Account Management',
  documentation = "Resets the password for the given account.")
@argument("account_identifier")
@option("-p", "--password",     dest    = "password",
                                action  = "store",
                                type    = str,
                                help    = "the current password (if not an administrator)")
@option("-t", "--identifier-type", dest    = "identifier_type",
                                   action  = "store",
                                   default = "username",
                                   type    = str,
                                   help    = "type of identifier used to reset the password. May be one of 'accountIdentity', 'username', or 'email'")
@option("-n", "--new-password", dest    = "new_password",
                                action  = "store",
                                type    = str,
                                help    = "the new password")
@option("-r", "--reset-token", type     = str,
                               action   = "store",
                               help     = "The password reset token for resetting the password.")
# TODO: We are trusting that the back-end user is not going to consistently lie
# about this. This is mainly for protecting against bad actors on the
# front-end. Once we have encryption from the front-end to the back-end we can
# have them trade secrets to verify that the IP address attestation is legit.
# Otherwise we should treat the address as being localhost.
@option("-i", "--address",       dest   = "ip_address",
                                 action = "store",
                                 help   = "when given, the IP address of the client device")
@uses(ObjectManager)
@uses(AccountManager)
class PasswordCommand:
  def do(self):

    Log.header("Updating password for account %s" % self.options.account_identifier)

    account = None
    if self.options.identifier_type == "accountIdentity":
      account = self.accounts.retrieveAccount(identity = self.options.account_identifier)
    elif self.options.identifier_type == "username":
      account = self.accounts.retrieveAccount(username = self.options.account_identifier)
    elif self.options.identifier_type == "email":
      account = self.accounts.retrieveAccount(email = self.options.account_identifier)

    if account is None:
      Log.error("account identifier or password is invalid")
      return -1

    name = account.username
    password = self.options.password

    if self.options.reset_token is not None:
      validToken = self.accounts.isResetTokenValid(account,
                                                   self.options.reset_token)
      if not validToken:
        Log.error("The reset token is invalid, please request another and try "
                  "again.")
        return -1
    elif password is None:
      # Only administrators can perform this action
      if self.person is None:
        Log.error("Must be logged in.")
        return -1

      if 'administrator' not in self.person.roles:
        Log.error("Only an administrator can update the password for an account.")
        return -1
    elif not self.accounts.authenticate(username = name, password = password):
      Log.error("account identifier or password is invalid")
      return -1
      
    password = self.options.new_password
    if password is None:
      # getpass obscures passwords in the terminal
      import getpass
      password = getpass.getpass('\npassword for %s: ' % (name))
      confirm  = getpass.getpass('confirm password: ')

      if password != confirm:
        Log.error("Passwords do not match.")
        return -1

    # If the password does not match our requirements, reject it.
    failedComponents = self.accounts.failedPasswordComponents(password, name)
    if failedComponents:
      Log.error("""The password either doesn't conform to the password policy
                   or is too similar to other account information.""")

      Log.warning(f"Please address the following to comply with the password policy."
                  f"{failedComponents}.")
      return -1

    ip = self.options.ip_address
    if ip is None:
      ip = "localhost"
    self.accounts.updatePassword(account, password, ipAddress = ip)

    Log.done("Successfully updated %s" % (name))
    return 0
