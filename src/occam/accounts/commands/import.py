# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json, base64

from occam.log      import Log
from occam.datetime import Datetime

from occam.manager import uses

from occam.accounts.manager   import AccountManager
from occam.keys.write_manager import KeyWriteManager
from occam.objects.manager    import ObjectManager

from occam.databases.manager import DataNotUniqueError

from occam.commands.manager import command, option, argument

@command('accounts', 'import',
  category      = 'Account Management',
  documentation = "Exports the current authorized account.")
@argument('filename')
@argument('username')
@option("-p", "--password", dest    = "password",
                            action  = "store",
                            help    = "the password to use for the account (it will ask you, otherwise)")
@uses(AccountManager)
@uses(ObjectManager)
@uses(KeyWriteManager)
class AccountsImportCommand:
  """ This command will create an account matching the information provided.

  This includes the private and public keys comprising this new account.
  """

  def do(self):
    """ Performs the 'accounts import' command.
    """

    name = self.options.username

    account = self.accounts.retrieveAccount(name)
    if account is not None:
      Log.error("Username already taken.")
      return -1

    password = self.options.password
    if password is None:
      # getpass obscures passwords in the terminal
      import getpass
      password = getpass.getpass('\npassword for %s: ' % (name))
      confirm  = getpass.getpass('confirm password: ')

      if password != confirm:
        Log.error("Passwords do not match.")
        return -1

    info = {}
    with open(self.options.filename, 'rb') as f:
      info = json.load(f)

    account = info.get('account')

    person = info.get('person')

    keys = info.get('keys')

    identity = info.get('identity')

    # Get general metadata
    uri = identity.get('public').get('id')
    keyType = identity.get('public').get('type')
    published = identity.get('public').get('published')
    published = Datetime.from8601(published)

    # Get public key
    publicKey = identity.get('public').get('key')

    # Discover this identity
    self.keys.write.discover(uri, publicKey, keyType, published)

    # Get private key
    privateKey = identity.get('private').get('key')

    # Import all verify and signing keys
    for key in keys:
      verifyKeyId = key.get('public').get('id')
      verifyKeyType = key.get('public').get('type')
      verifyKey = key.get('public').get('key')

      signingKey = key.get('private').get('key')

      signature = key.get('public').get('signature')
      signatureType = signature.get('type')
      signatureDigest = signature.get('digest')
      signaturePublished = key.get('public').get('published')
      signaturePublished = Datetime.from8601(signaturePublished)
      signature = base64.b64decode(signature.get('data'))

      revokes = key.get('public').get('revokes')

      self.keys.write.discoverKey(uri, verifyKeyId, verifyKey, verifyKeyType, signature, signatureType, signatureDigest, signaturePublished, revokes, publicKey, keyType)
      try:
        self.keys.write.importSigningKey(signingKey, verifyKeyType, verifyKeyId)
      except DataNotUniqueError:
        pass

    # Import private key
    currentVerifyId = keys[0].get('public').get('id')
    try:
      self.keys.write.importIdentity(uri, type = keyType,
                                          privateKey = privateKey,
                                          verifyKeyId = currentVerifyId,
                                          published = published)
    except DataNotUniqueError:
      pass

    # Create an account attached to this identity
    obj = self.accounts.addPerson(name = person.get('name'),
                                  identity = uri,
                                  subtypes = person.get('subtype', []))
    objectInfo = self.objects.infoFor(obj)
    personId = obj.id

    # And an authentication
    self.accounts.addAuthentication(personId, name, password, uri, [], account.get('email'))

    return 0
