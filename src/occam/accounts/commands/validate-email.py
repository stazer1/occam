# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.accounts.manager import AccountManager
from occam.commands.manager import command, option, argument

from occam.manager import uses

@command('accounts', 'validate-email',
         category      = 'Account Management',
         documentation = 'Validates account email')
@argument("identity",       type = str,
                            help = "The account identity")
@argument("validate_token", type = str,
                            help = "The account validation token")
@uses(AccountManager)
class ValidateEmailCommand:
  """ Validates account email. Email validation is checked as condition for
      other account actions.
  """
  def do(self):

    account = self.accounts.retrieveAccount(identity=self.options.identity)

    if account is None:
      Log.error("The identity given might be incorrect, please try again.")
      return -1
    
    else:
      if self.accounts.validateEmail(account, self.options.validate_token):
        Log.done(f"Email {account.email} for account {account.person_id} is confirmed.")
        return 0

      else:
        Log.error("We were unable to validate the address, please try again.")
        return -1