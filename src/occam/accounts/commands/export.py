# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json, base64

from occam.log import Log

from occam.manager import uses

from occam.accounts.manager   import AccountManager
from occam.keys.write_manager import KeyWriteManager
from occam.objects.manager    import ObjectManager

from occam.commands.manager import command, option, argument

@command('accounts', 'export',
  category      = 'Account Management',
  documentation = "Exports the current authorized account.")
@uses(AccountManager)
@uses(ObjectManager)
@uses(KeyWriteManager)
class AccountsExportCommand:
  """ This command will output a structure consisting of all account metadata.

  This includes the private and public keys comprising the account.

  For security purposes, this command must be authenticated and will only
  produce a structure for the current authenticated account.
  """

  def do(self):
    """ Performs the 'accounts export' command.
    """

    # Fail if not authenticated
    if self.person is None:
      Log.error("Must be authenticated.")
      return 1

    # Our goal is to populate a structure with all keys and metadata for an
    # account on the system.
    ret = {}

    # Get the account details
    account = self.accounts.retrieveAccount(identity = self.person.identity)
    ret['account'] = {
      'username': account.username,
      'email': account.email
    }

    # Get the person object metadata
    personInfo = self.objects.infoFor(self.person)
    ret['person'] = {
      'id': self.person.id,
      'uid': self.person.uid,
      'info': personInfo
    }

    if len(personInfo.get('images', [])) > 0:
      data = self.objects.retrieveFileFrom(self.person, personInfo['images'][0]).read()
      ret['person']['avatar'] = base64.b64encode(data).decode('utf-8')

    # Retrieve the public and private keys for this actor
    public = self.keys.retrieve(self.person.identity)
    private = self.keys.write.identityKeyFor(self.person.identity)

    # Export the account private and public keys
    ret['identity'] = {
      'public': {
        'key': public.key,
        'type': public.key_type or "RSA",
        'id': public.uri,
        'published': public.published.isoformat()
      },
      'private': {
        'key': private.key,
        'type': private.key_type,
        'id': private.uri,
        'published': private.published.isoformat()
      }
    }

    # Export the account verifying and signing keys
    ret['keys'] = []

    keys = self.keys.verifyingKeysFor(self.person.identity)
    for key in keys:
      signingKey = self.keys.write.signingKeyFor(self.person.identity, key.id)

      ret['keys'].append({
        'public': {
          'key': key.key,
          'type': key.key_type or "RSA",
          'id': key.id,
          'uri': key.uri,
          'published': key.published.isoformat(),
          'signature': {
            'data': base64.b64encode(key.signature).decode('utf-8'),
            'type': key.signature_type or "PKCS1_v1_5",
            'digest': key.signature_digest or "SHA512"
          }
        },
        'private': {
          'key': signingKey.key,
          'type': signingKey.key_type or "RSA",
        }
      })

      # Also tag whether or not this key is revoking another
      if key.revokes:
        ret['keys'][-1]['public']['revokes'] = key.revokes

    # Dump the export structure
    Log.output(json.dumps(ret))

    # End successfully
    return 0
