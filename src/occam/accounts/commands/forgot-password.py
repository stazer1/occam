# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.accounts.manager import AccountManager
from occam.commands.manager import command, option, argument
from occam.messages.manager import MessageManager

from occam.manager import uses

@command('accounts', 'forgot-password',
         category      = 'Account Management',
         documentation = 'Sends email with password reset link to given account')
@argument("username_or_email", type = str,
                   help = "The username or email address of the account.")
@uses(MessageManager)
@uses(AccountManager)
class ForgotPasswordCommand:
  """ Sends email with password reset link to email associated with given
  account.
  """
  def do(self):

    username = None
    email = None
    usingEmail = self.accounts.isEmailAddressValid(self.options.username_or_email)
    if usingEmail:
      email = self.options.username_or_email
    else:
      username = self.options.username_or_email

    account = self.accounts.retrieveAccount(username = username, email = email)

    if account is not None:
      self.accounts.forgotPasswordReset(account)

    Log.done(f"If an email address was registered to the account specified "
             f"an email will be sent with instructions on how to reset your "
             f"password.")

    return 0
