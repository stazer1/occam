# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.log     import loggable
from occam.manager import manager, uses

from occam.semver  import Semver

@loggable
@manager("versions")
class VersionManager:
  """ 
  """

  def retrieve(self, obj):
    """ Will return a list of known versions for the given object.
    """

    # TODO: ask the federation to resolve when a tag is not known.
    #       There is definitely room for DHT innovation for version ranges.
    return self.datastore.retrieveVersions(obj.id, obj.uid)

  def query(self, obj):
    """ This function will return a version string if the specified object has one.

    It will target the particular revision the object is current representing.

    It it has multiple, it will pick one arbitrarily.

    Returns:
      str: The version string for this revision or None if there isn't one.
    """

    rows = self.datastore.retrieveVersions(obj.id, obj.uid, obj.revision)

    if not rows:
      return None

    return rows[0].tag

  def resolve(self, obj, version, subversion=None, versions=None, penalties=None):
    """ This function will return the revision of the given version string.
    
    The version string can be a parsable identifier or a full semver string. It
    works the same as npm's semver.

    Returns None when a version within the requested version range cannot be
    found.
    """

    if version is None:
      return None, None

    # Look up versions

    if '@' in version:
      version, extra = version.split('@', 1)
      if subversion is None:
        subversion = extra

    tags = self.retrieve(obj)

    if versions is None:
      versions = [tag.tag for tag in tags]

    if subversion is not None:
      versions = list(filter(lambda x: x.endswith("@" + subversion), versions))

    # Remove the subversions from the tags
    versions = list(map(lambda x: x.split('@')[0], versions))

    currentVersion = Semver.resolveVersion(version, versions)
    chosenVersion = currentVersion

    # Go and pick the best (most-likely) revision
    revision = None
    if currentVersion:
      if subversion:
        currentVersion = currentVersion + "@" + subversion

      for item in tags:
        compareVersion = item.tag
        if subversion is None:
          # Ignore the subversion... so we pick any
          compareVersion = compareVersion.split('@', 1)[0]
        if compareVersion == currentVersion:
          revision = item.revision

    if revision and penalties and obj.id in penalties and revision in penalties[obj.id]:
      revision = None

    if revision is None and chosenVersion in versions:
      # Negate this version and try again
      versions.remove(chosenVersion)
      return self.resolve(obj, version, subversion = subversion,
                                        versions = versions,
                                        penalties = penalties)

    return revision, currentVersion

class VersionError(Exception):
  """ Base class for all versioning errors.
  """
