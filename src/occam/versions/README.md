# The Versions Component

This component manages version tags applied to archived objects.

## Motivation

In the archive, software and data held within objects are typically 'versioned'
in the sense they have revisions and snapshots. These hold the views of such
objects at particular points in time and track the changes made over time. Yet,
this is not the prevailing sense of 'version' that exists in the world.

In traditional software parlance, a version is a specific snapshot that is given
some semantic name that makes that snapshot uniquely identifiable. Windows 10
has the '10' version tag that differentiates it from previous versions of
Windows. Yet, we get software updates all the time. In fact, if I investigate my
system further, I see that my Windows machine is on version `10.0.18362.836` and
release `1903`.

Versioning can get complicated, but what matters here are the major version,
which is `10`, and the minor versions that follow. These generally increment as
new updates are released. Although these numbers, on the surface, are somewhat
arcane, we can follow them through simple relationships. When the major version
changes, things that use this software may break. When minor versions change,
things might be added or fixed, so our milage may vary. It is often safe and
encouraged to update software as its minor version changes (Windows does it
behind the scenes automatically, these days) but it often requires some effort
(or money) to upgrade to a new major version.

These major and minor versions work together with these common sense rules and
greatly help the system keep even archived software up and running in a
practical way. These rules, so far, are just a small part of how version tags
are made and used to convey this information. These logical relationships
among tags are often called "Semantic Versioning" and Occam employs a particular
style to convey that logic in its version specification, which will follow.

## Semantic Versioning

Occam makes use of a semantic versioning standard that is based on one the `npm`
project uses which is specified at [`semver.org`](https://semver.org/). The
basic idea is this (directly quoted from that page):

Given a version number MAJOR.MINOR.PATCH, increment the:

* MAJOR version when you make incompatible API changes,
* MINOR version when you add functionality in a backwards compatible manner, and
* PATCH version when you make backwards compatible bug fixes.

By following these simple rules, a piece of software that depends on another
may request a range of versions and even account for those versions that have
not been created yet.

The syntax that follows is used to describe those ranges of valid versions of
those dependencies. This syntax is strongly based on the versioning syntax `npm`
uses which is described within the
[npmjs.com](https://docs.npmjs.com/about-semantic-versioning)
documentation.

### Syntax

Versions are derived from tags that separate the different specifiers (major,
minor, patch, and revision) using the `.` delimiter. When a version is specified
without any other special symbols, to be described later, it is always assumed
by the system to contain a wildcard immediately following the specified tag.
For instance, `1.0` is considered the same as `1.0.x` where the `x` can stand
for any patch version. Similarly, a `1` by itself stands for `1.x` which is
itself shorthand for `1.x.x`, and so on. Wildcards are either `x`, `X`, or `*`
and all such symbols act the same. They allow matching against any tag that at
least matches the preceding parts of the tag, as seen in the previous examples
and the examples to follow throughout.

To literally specify only one particular tag, you would use the `=` specifier
before the tag. That is `=1.5.4` specifies the exact tag `1.5.4` and would not
match other tags such as `1.5.4.r1` or even `1.5.4.0`. The `!=` specifier is
similar and asserts that that tag is never matched. For instance, `!=1.5.5`
would prevent that particular tag from ever being used. This is generally most
useful with logical conjunctions described later to further limit more broad
ranges.

Speaking of ranges, other specifiers exist to help describe ranges of version
tags and are generally
self-explanatory. The `>`, `>=`, `<`, and `<=` specifiers, which provided before
a version tag, describe versions that are within the range understood by those
mathematical symbols. For instance, `>=1.4` would match `1.4` by virtue of the
`=` yet also match all tags with a minor version that is larger or any tag with
a major version that is larger than the given tag. Therefore, this would match
`1.4.0` and `2.0` as well. Without the `=`, for instance `>1.4`, would restrict
this range to be exclusive and only match the successive tags and not match the
exact tag `1.4`.

For the purposes of this ordering, a lack of a field is
considered less than a provided field, and, therefore, `1.4.0` would be seen as
"greater" than `1.4` which has no provided patch field. That is, the empty field
is not equal to `0`. Also, an alphanumeric string, such as `beta` or `release1`
or `p1` are all considered "less" than a strictly numeric field. Generally
alphanumeric tags are release candidates or testing tags in practice, so this
ordering allows for those tags to not interfere with normal releases. They also
allow for tags that relate to non-normative metadata changes from interfering
with normal version resolution, although this should perhaps be discouraged.
The system will then choose the "largest" matching version tag for the object.

It may not be desirable for `>=1.x` to be used as a version request if, by the
logic of semantic versioning, a `2.0` release is likely to break compatibility.
You could, for instance, provide a specifier that reads `>=1.4, <2` which
would suggest a version matches if and only if it is both greater than or equal
to `1.4` and exclusively less than `2`. However, these cases are common enough
that there is a specific specifier. The `^` (caret) specifier indicates that a
version matches if it is greater than or equal for any tag up to, but not
including, the next major version. For instance, `^5.4.0` matches `5.4.0.1`,
`5.5`, but not `6.0` and so on. If you want to restrict to the minor versions,
the `~` (tilde) specifier will suffice. `~5.4.0` again matches `5.4.0.1` but
not `5.5` or beyond.

To provide a more flexible method of specifying ranges, you can use the `...`
(triple period) between two version specifications. In this case, the tag
`1.3.4 ... 1.4.8` would match any version inclusive to that range. It would
be shorthand for `>=1.3.4, <=1.4.8` using the `,` as an "and" conjunction,
to be described later. One caveat here is that ranged tags cannot contain
any wildcards (`x`, `X`, or `*`) as they make it ambiguous when the range
begins or ends.

To chain multiple rules, you can separate rules by a `,` (comma) to denote an
logical "and" relationship. That is, both rules must be satisfied together.
We already saw that the `^1.4` shorthand means `>=1.4, <2`, yet we can use
our logical "and" to build complex ranges when necessary. For instance, perhaps
we know that we can use any release after a particular version, yet one release
in particular has a bug that interferes. We can then specify a version such as
`~1.4.5, !=1.4.7` which would match versions `1.4.5` and `1.4.6` but skip over
a problematic `1.4.7` before allowing `1.4.8`, presumably where our bug was
fixed.

To more flexibly chain rules, we can also use the `||` (double pipe) operator
to specify a logical "or" relationship. As opposed to an "and", these say that
we can match using any of the rules. A version specified as `1.x || 3.x` would
match any tag that either has a `1` as its major version or a `3` instead.
This is not necessarily an exclusive relationship. You could specify a tag such
as `^1.x || 1.5...2.3` even though parts of the listed range overlap the implied
range of the first. That is, the "or" operator here is not the same as it is
used in English, but rather a bit more flexible.

The `||` (or) operator has a weaker precedence than the `,` (and) operator.
That means when used together, the rules apply the "and" relationship before
the "or" relationship. So, a tag that contains both, such as `a, b || c` would
be thought of as "either 'a' AND 'b' ... OR 'c'". For example, the version given
as `^1.x,!=1.3.2 || ^2.x` would match any tag that has a `1` as a major version
except the tag `1.3.2` and it would match any tag that has a `2` as the major
version. Therefore, some care should be taken when using both operators to
ensure that one's intention is met.

### Soft Versions

To support systems which have the concept of a "soft version", the following
syntax is added. Tags may start with an `*` to denote that they are a soft
version. These versions are ignored in the presense of a non-soft version
that occurs at any other point. You can only use them with an exact tag,
much like `=1.3.4` but with the idea that this rule is ignored when another
rule exists.

For example, if object A has a dependency, B, and requests the version `*1.3`,
it will resolve to version `1.3`. However, if object A has a dependency, C,
which also itself has the same dependency B but requires version `=1.4`, then
the system will resolve both object A and C's request for B using version `1.4`
by ignoring that first rule.

## Subversions

A secondary version tag is also available as a suffix to a normal version tag.
This is useful for situations where a single version of some software or data
has different build contexts. For instance, a Python library might be version
`1.2.3`, yet it would have different dependencies when being used with Python
`3.6` or `3.7`. Therefore, that software object, in our system, has a version
tag of `1.2.3@3.7` if it is prepared for Python `3.7`.

When a subversion is specified, it filters all tags to those that contain the
suffix matching that subversion. There is no wildcard matching for subversions.
The version tags are then, and otherwise, matched by comparing the resulting
tag after removing the suffix. That is, `1.2.3@z` is larger than `1.2.1@a` but
both match the version specifier `1.2.x`, yet only one matches `1.2.x@a`.

The subversion can be specified using the `subversion` field. For a dependency,
it might look like this:

```
"dependencies": [
  {
    "id": "QmRt6cEP2MsyxaAvyzHCH9FFwCn3NAtLRzbJdW4tVPBkSV",
    "type": "python-library",
    "name": "six",
    "version": "~1.2.3",
    "subversion: "3.7"
  }
]
```

This would then match any versions of the form `1.2.x` that are equal to or
greater than `1.2.3` but containing the subversion suffix `@3.7`. Here,
`1.2.3@3.7` matches but `1.2.3@3.6` would not.

## Creation and Signing

When a version is attached to an object, it is associated with a specific
revision. In this case, the revision refers to the commit hash for a snapshot
of the git repository backing the object. The version tag, then, can be used as
an alias for that revision.

An object tag, overall, can specify the version after the `:` specifier. For
instance, the tag:

`QmaPgCeGMbd5xtcbYwYTjY6BUECvHujjQPenhbH2GWektY:3.0`

specifies the `grep` application version `3.0`, which is an alias currently for
the revision `5dt8S66MNbuMkmWHBSWTcRqYkb1t1p`. You could also explictly specify
this with the tag:

`QmaPgCeGMbd5xtcbYwYTjY6BUECvHujjQPenhbH2GWektY@5dt8S66MNbuMkmWHBSWTcRqYkb1t1p`

Therefore, these two object tags are, once the version is established, the same.

To create a version, the command `occam versions new` is used. You specify the
object tag, which should refer to the revision or it will use the current
latest revision. You also follow that with the requested version tag. For
instance, creating that `3.0` tag for grep looks like the following:

```
occam versions new QmaPgCeGMbd5xtcbYwYTjY6BUECvHujjQPenhbH2GWektY@5dt8S66MNbuMkmWHBSWTcRqYkb1t1p 3.0
```

This will create a new version record associating that revision of that object
with the given tag. Furthermore, since the version tags are distributed along
with object data as objects are pulled to other servers, the versions need to be
validated. The object data itself is signed and verifiable as it is copied from
one machine to the next. Each object has an "identity" that owns it which can
digitally "sign" a document that labels a revision with a version tag. The same
mechanism, therefore, that verifies that an object as it is received belongs to
the actor that created it can, then, also be used to verify the version tags
specified by that owning actor.

When an object is retrieved from a server, it already distributes the public key
belonging to the owner. It then transmits a list of known version tags along
with signatures that are incredibly difficult to forge which will prove that
relationship. The destination server will reject any tags that cannot be
verified using these signatures.
