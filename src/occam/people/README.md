# The People Component

This component handles retrieval of logical actors (people) in the system.

This is distinct from the accounts component, which is concerned with login and
logout of users. People are objects that represent external actors that have
relationships to artifact objects in Occam. People may be authors or
collaborators, whereas accounts correspond directly to someone who has login
credentials to the system.
