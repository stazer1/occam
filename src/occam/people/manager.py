# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.log     import loggable
from occam.manager import manager, uses

from occam.objects.manager import ObjectManager

@loggable
@manager("people")
@uses(ObjectManager)
class PersonManager:
  """ This OCCAM manager holds information about the relationship about Actors.

  Specifically, this can query relationships between identity and person Objects.
  """

  def retrieveAll(self, uri = None, person = None):
    """ Retrieves a list of all known people for a particular identity or universally.

    Args:
      uri (str) The identity URI to lookup. (Default: None, look for all.)
      person (Object) The person object doing the lookup.

    Returns:
      list A list of resolved Object references to the appropriate "person" objects.
    """

    # Acquire a set of PersonRecords
    rows = self.datastore.retrievePersonAll(uri = uri)

    # Turn them into Objects
    ret = [self.objects.retrieve(row.id, person = person) for row in rows]

    ret = list(filter(lambda x: x is not None, ret))

    return ret

  def retrieve(self, uri, person = None):
    """ Retrieves information about a particular license that matches the given key.

    Args:
      uri (str): The identity URI representing the queried actor.
      person (Object) The person object doing the lookup.
      
    Returns:
      Object: The "person" object or None when the person cannot be found.
    """

    # Acquire a PersonRecord
    row = self.datastore.retrievePerson(uri = uri)

    if row is None:
      return None

    # Turn them into Objects
    return self.objects.retrieve(row.id, person = person)
