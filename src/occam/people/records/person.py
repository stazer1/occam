# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.databases.manager import table, index

@table("people")
class PersonRecord:
  """ Stores extra metadata pertaining to Person objects.
  """

  schema = {

    # Primary Key

    # This id is shared with Object

    "id": {
      "type": "string",
      "length": 256,
    },

    # The identity attached to this Person.
    "identity_uri": {
      "type": "string",
      "length": 256,
      "primary": True
    },

    # Attributes

    "username": {
      "type": "string",
      "length": 128
    },

    "email": {
      "type": "string",
      "length": 128
    }
  }

@index(PersonRecord, "identities")
class PersonIdentityIndex:
  ids = ['identity_uri']
