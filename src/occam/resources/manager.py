# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import json

from occam.config import Config
from occam.log    import loggable
from occam.object import Object
from occam.error  import OccamError

from occam.manager import manager, uses

from occam.storage.manager import StorageManager

@loggable
@uses(StorageManager)
@manager("resources")
class ResourceManager:
  """ This OCCAM manager handles resource installation.
  """

  handlers = {}
  instantiated = {}

  def __init__(self):
    """ Initialize the resource manager.
    """

    import occam.resources.plugins.file
    import occam.resources.plugins.tar
    import occam.resources.plugins.git
    import occam.resources.plugins.mercurial
    import occam.resources.plugins.svn
    import occam.resources.plugins.zip
    import occam.resources.plugins.docker
    import occam.resources.plugins.iso

  @staticmethod
  def register(resourceType, handlerClass):
    """ Adds a new resource type.
    """

    ResourceManager.handlers[resourceType] = handlerClass

  def handlerFor(self, resourceTypes):
    """ Returns an instance of a handler for the given type.
    """

    # Default type
    resourceType = 'file'

    # Ensure resourceTypes is a list of types
    if not isinstance(resourceTypes, list):
      resourceTypes = [resourceTypes]
    
    # Check for available resource handlers
    for testType in resourceTypes:
      if testType in self.handlers:
        resourceType = testType

    if not resourceType in self.handlers:
      raise ResourcePluginMissingError(resourceType)

    if not resourceType in ResourceManager.instantiated:
      try:
        instance = self.handlers[resourceType]()
      except:
        instance = None

      # Set the instance
      if instance:
        ResourceManager.instantiated[resourceType] = instance

    if ResourceManager.instantiated.get(resourceType) is None:
      # The driver could not be initialized
      raise ResourcePluginInitializationError(resourceType)

    return ResourceManager.instantiated.get(resourceType)

  def handlerCall(self, resourceTypes, funcName, handler=None, *args, **kwargs):
    """ Calls the plugin implementation.
    """

    if handler is None:
      handler = self.handlerFor(resourceTypes)

    if not hasattr(handler, funcName) or \
       not callable(getattr(handler, funcName)) or \
       not hasattr(getattr(handler, funcName), 'registered') or \
       not getattr(handler, funcName).registered:
      raise ResourcePluginUnimplementedError(resourceTypes[0], funcName)

    return getattr(handler, funcName)(*args, **kwargs)

  def uidTokenFor(self, type, name, source):
    """ Returns the reference UID token for the Resource Object.
    """

    # type, name, source

    return ("type:%s\nname:%s\nsource:%s" % (type, name, source)).encode('utf-8')

  def uidFor(self, type, name, source):
    """ Returns the reference UID for the Resource Object.
    """

    # Base58(SHA256(type, name, source))
    token = self.uidTokenFor(type, name, source)

    # Create a URI by hashing the public key with a multihash
    from occam.storage.plugins.ipfs_vendor.multihash import multihash
    hashedBytes = multihash.encode(token, multihash.SHA2_256)

    from occam.storage.plugins.ipfs_vendor.base58 import b58encode
    uri = b58encode(bytes(hashedBytes))
    
    return uri

  def idTokenFor(self, type, name, source, identity):
    """ Returns the ID token for the Resource Object.
    """

    # uid, identity, type, name, source

    uid = self.uidFor(type, name, source)
    return ("uid:%s\nidentity:%s\ntype:%s\nname:%s\nsource:%s" % (uid, identity, type, name, source)).encode('utf-8')

  def idFor(self, type, name, source, identity):
    """ Returns the ID for the Resource Object.
    """

    # Base58(SHA256(uid, identity, type, name, source))

    token = self.idTokenFor(type, name, source, identity)

    # Create a URI by hashing the public key with a multihash
    from occam.storage.plugins.ipfs_vendor.multihash import multihash
    hashedBytes = multihash.encode(token, multihash.SHA2_256)

    from occam.storage.plugins.ipfs_vendor.base58 import b58encode
    uri = b58encode(bytes(hashedBytes))
    
    return uri

  def infoFor(self, id=None, uid=None):
    """ Returns the information for the given resource.
    """
    if uid is None and id is None:
      raise ValueError("uid or id must be given.")

    row = self.retrieveResource(id = id, uid = uid)

    if row:
      return {
        "id": row.id,
        "uid": row.uid,
        "identity": row.identity_uri,
        "revision": row.revision,
        "name": row.name,
        "source": row.source,
        "type": "resource",
        "subtype": [row.resource_type]
      }
    else:
      return None

  def pathFor(self, uid, revision, resourceType=None, handler=None):
    """ Returns the path the object of the given uid would be stored.
    """

    if handler is None:
      if resourceType is None:
        obj = self.datastore.retrieveResource(uid=uid)

        if not obj is None:
          resourceType = obj.resource_type

      if resourceType is None:
        return None

      handler = self.handlerFor(resourceType)

    storagePath = self.storage.resourcePathFor(uid, revision)
    return self.handlerCall([resourceType], "pathFor", handler = handler,
                                                       uid = uid,
                                                       path = storagePath,
                                                       revision = revision)

  def fileListFor(self, uid, revision, resourceType=None, handler=None):
    """ Retrieves a file listing for the given resource.
    """

    if resourceType is None:
      obj = self.datastore.retrieveResource(uid=uid)

      if not obj is None:
        resourceType = obj.resource_type

    if resourceType is None:
      return None

    if handler is None:
      handler = self.handlerFor(resourceType)

  def urlFor(self, uid, revision, resourceType=None, handler=None, protocol=None):
    """ Determines the URL for the given resource data.

    This URL can be used as an 'alternative source' or mirror of the data and
    can be used when pulling the resource from a remote node.
    """

    if resourceType is None:
      obj = self.datastore.retrieveResource(uid=uid)

      if not obj is None:
        resourceType = obj.resource_type

    if resourceType is None:
      return None

    if handler is None:
      handler = self.handlerFor(resourceType)

    return self.handlerCall([resourceType], "urlFor", handler = handler,
                                                      uid = uid,
                                                      revision = revision,
                                                      protocol = protocol)

  def install(self, resourceInfo, path):
    """ Installs the given resource to the given path.
    """

    if resourceInfo is None:
      return

    if resourceInfo.get('mount') == "ignore":
      # Ignoring instantiation of this resource
      return

    # Retrieve known resource info
    knownResourceInfo = self.infoFor(resourceInfo.get('id'), resourceInfo.get('uid'))

    # Get resource Type
    resourceType = resourceInfo.get('subtype', ['file'])
    handler = self.handlerFor(resourceType)
    basePath = path

    # Default destination:
    resourceInfo['to'] = resourceInfo.get('to', 'package')

    # Do not allow a 'to' path with a relative ..
    if ".." in resourceInfo['to']:
      ResourceManager.Log.error("Resource's destination path contains a '..' which is not allowed.")
      return False

    path = os.path.join(basePath, resourceInfo['to'])

    uid = resourceInfo.get('uid')
    identity = knownResourceInfo.get('identity')
    revision = resourceInfo.get('revision')

    resourcePath = None

    if not revision is None and not uid is None:
      storagePath = self.storage.resourcePathFor(uid, revision)

      exists = self.handlerCall([resourceType], "exists", handler = handler,
                                                          uid = uid,
                                                          path = storagePath,
                                                          revision = revision)
      if exists:
        resourcePath = self.handlerCall([resourceType], "pathFor", handler = handler,
                                                                   uid = uid,
                                                                   path = storagePath,
                                                                   revision = revision)
        ResourceManager.Log.noisy("Installing resource from %s" % (resourcePath))

    if not resourcePath is None:
      ret = self.handlerCall([resourceType], "install", handler = handler,
                                                        uid = uid,
                                                        revision = revision,
                                                        path = resourcePath,
                                                        resourceInfo = resourceInfo,
                                                        identity = identity,
                                                        destination = os.path.realpath(path))

      if ret:
        self.handlerCall([resourceType], "actions", handler = handler,
                                                    uid = uid,
                                                    revision = revision,
                                                    path = resourcePath,
                                                    resourceInfo = resourceInfo,
                                                    identity = identity,
                                                    destination = os.path.realpath(basePath))

      return ret

    raise ResourceNotFoundError(resourceInfo, "Resource not found")

  def installAll(self, resources, path):
    """ Returns an array of Objects for pulled objects.
    
    This array, like pullAll, corresponds directly to the list of
    resources to install in the given Object's `install` section. This method
    will install the resources to the given path.

    Arguments:
      resources (list): A set of resource metadata for each resource to install.
      path (str): The base path in which to install the resource.

    Returns:
      list: The set of resources installed.
    """

    for resource in resources:
      # Right now, we only install objects marked as resources
      if resource.get('type') != 'resource':
        continue

      # Install the resource at the given path
      self.install(resource, path)

      # Recursively install
      if 'install' in resource:
        self.installAll(resource, path)

    return resources

  def resourcesFor(self, objectInfo, build=False):
    """ Returns the list of resources for the given object metadata and phase.

    Arguments:
      objectInfo (ObjectInfo): The object metadata to interpret.
      build (bool): Whether or not we are using the build phase.

    Returns:
      list: A set of resource metadata describing the resources.
    """

    # Get the resources from the `install` field
    resources = objectInfo.get('install', [])

    # Depending on the phase, also add resources found within that section
    if build and 'build' in objectInfo:
      resources.extend(objectInfo['build'].get('install', []))
    else:
      resources.extend(objectInfo.get('run', {}).get('install', []))

    # Return the list of resource metadata
    return resources

  def cloneAll(self, obj):
    """ Returns an array of new resource infos for pulled objects.
    
    This array corresponds
    directly to the list of resources to install in the given Object's 'install'
    section. It will clone the resource when possible and update that section to
    point to the cloned version. Otherwise, it will leave the resource info
    alone.
    """

    ret = []
    
    objectInfo = obj.objectInfo()

    resources = objectInfo.get('install', [])

    if not isinstance(resources, list):
      resources = [resources]

    for resourceInfo in resources:
      new_info = self.clone(resourceInfo)
      ret.append(new_info)

    return ret

  def clone(self, resourceInfo):
    """
    Will clone the given resource, if necessary. If not necessary, will just
    return the resource as is. Generally, this is useful for cloning objects
    that have a git repository attached. When you clone that object, you mean
    to modify the code, so you must also fork the repositories.
    """

    install_type = resourceInfo.get('type', 'object')
    uid          = resourceInfo.get('id')
    revision     = resourceInfo.get('revision')
    source       = resourceInfo.get('source')
    name         = resourceInfo.get('name')
    to           = resourceInfo.get('to', 'package')

    handler = self.handlerFor(install_type)
    clonable = self.handlerCall([resourceType], "clonable", handler = handler,
                                                            uid = uid,
                                                            revision = revision,
                                                            name = name,
                                                            source = source,
                                                            to = to)
    if clonable:
      newResourceInfo = self.handlerCall([resourceType], "clone", handler = handler,
                                                                  uid = uid,
                                                                  revision = revision,
                                                                  name = name,
                                                                  source = source,
                                                                  to = to)
      newInfo = resourceInfo.copy()

      # Overwrite identifying tags in the new resource info structure
      for tag in ['id', 'revision']:
        newInfo[tag] = newResourceInfo.get(tag, newInfo.get(tag))

      return newInfo
    else:
      return resourceInfo

  def retrieveFile(self, id, uid, revision, resourceType, start=0, length=None):
    """ Pulls out a stream for retrieving the resource data for this object.
    """

    handler = self.handlerFor(resourceType)
    path = self.pathFor(uid, revision, handler=handler)
    return self.handlerCall([resourceType], "retrieve", handler = handler,
                                                        uid = uid,
                                                        revision = revision,
                                                        path = path,
                                                        start = start,
                                                        length = length)

  def retrieveFileStat(self, id, uid, revision, resourceType):
    """ Pulls out the file status for the resource object.
    """

    handler = self.handlerFor(resourceType)
    path = self.pathFor(uid, revision, handler=handler)
    ret = self.handlerCall([resourceType], "stat", handler = handler,
                                                   uid = uid,
                                                   revision = revision,
                                                   path = path)

    if not isinstance(resourceType, list):
      resourceType = [resourceType]

    traversable = self.handlerCall([resourceType], "traversable", handler = handler,
                                                                  uid = uid,
                                                                  revision = revision,
                                                                  path = path)

    ret["from"] = {
      "type": "resource",
      "subtype": resourceType,
      "traversable": traversable,
      "id": id,
      "uid": uid,
      "revision": revision,
      "path": path
    }

    return ret

  def retrieveRelativeDirectory(self, id, uid, revision, resourceType, actions, installPath, path):
    """ Returns a directory listing for a resource relative to an object.

    Args:
      id (str): The identifier for the resource.
      uid (str): The base identifier for the resource.
      revision (str): The hash for the resource.
      resourceType (str): The type of resource.
      installPath (str): The path the resource is to be installed within the object.
      path (str): The path within the object to retrieve.

    Returns:
      list: A list of directory information (including item lists) for the given path within an object.
    """

    data = {"items": [], "directory": {}}

    ret = []

    destinationPath = os.path.normpath(os.path.join("/", os.path.normpath(os.path.dirname(installPath))))
    installationPath = os.path.normpath(os.path.join("/", os.path.normpath(installPath)))

    resourceInfo = self.infoFor(uid = uid, id = id)
    resourceInfo['revision'] = revision;
    resourceInfo['to'] = installPath

    if actions.get('postUnpack') != "delete":
      if destinationPath == path:
        item = self.retrieveFileStat(id, uid, revision, resourceType)

        if item:
          item["name"] = os.path.basename(installPath)
          data['directory'][item['name']] = item
          data['items'].append(item)
          ret.append((resourceInfo, data, None,))
      elif destinationPath.startswith(path):
        # The directory exists, at least.
        subPath = destinationPath[len(path):]
        if subPath[0] == "/":
          subPath = subPath[1:]
        subDirectory = subPath.split('/', 1)[0]
        item = {}
        item["name"] = subDirectory
        item["type"] = "tree"
        data['directory'][item['name']] = item
        data['items'].append(item)
        ret.append((resourceInfo, data, None,))

    data = {"items": [], "directory": {}}

    if actions.get('postUnpack') != "delete":
      if path.startswith(installationPath):
        item = self.retrieveFileStat(id, uid, revision, resourceType)

        # When the resource is a 'tree' type (like a git repository) or
        # it is 'traversable' (a file that contains files), we can
        # go /into/ that resource when the path specifies it

        # This works when we want to list the object such as
        # <ID>@<REVISION>/<RESOURCE NAME>/file.txt
        if item and (item["type"] == "tree" or item.get("from", {}).get("traversable")):
          # List the directory within the resource
          subPath = path[len(installationPath):]
          subData = self.retrieveDirectoryFrom(id, uid, revision, resourceType, subPath)
          data['items'].extend(subData['items'])
          data['directory'].update(subData['directory'])
          ret.append((resourceInfo, data, subPath,))

    data = {"items": [], "directory": {}}

    unpackPath = actions.get('unpack')
    if unpackPath:
      unpackPath = os.path.normpath(os.path.join("/", unpackPath))

    comparePath = path
    if not path.startswith == "/":
      comparePath = "/" + path
    if unpackPath and os.path.commonpath([unpackPath,comparePath]) == unpackPath:
      internalPath = path[len(unpackPath)-1:]

      try:
        items = self.retrieveDirectoryFrom(id, uid, revision, resourceType, internalPath)
      except:
        items = {}

      # Incorporate the resource provenance to each entry:
      for item in items.get('items', []):
        if 'from' in items:
          item['from'] = items['from']

      data['items'].extend(items.get('items', []))
      data['directory'].update(items.get('directory', {}))

      ret.append((resourceInfo, data, internalPath,))

    return ret

  def retrieveDirectoryFrom(self, id, uid, revision, resourceType, subpath):
    """ Pulls out a stream for retrieving the given file for this object.
    """

    handler = self.handlerFor(resourceType)
    path = self.pathFor(uid, revision, handler = handler)
    data = self.handlerCall([resourceType], "retrieveDirectory", handler = handler,
                                                                 uid = uid,
                                                                 revision = revision,
                                                                 path = path,
                                                                 subpath = subpath)
    data = data or {}

    data['items'] = data.get('items', [])

    if not isinstance(data.get('items'), list):
      data['items'] = []

    if 'directory' not in data:
      data['directory'] = {}

    directory = data['directory']
    for item in data.get('items', []):
      if item['name'] not in directory:
        data['directory'][item['name']] = item

      if not 'mime' in item:
        item['mime'] = self.storage.mimeTypeFor(item['name'])

    data['from'] = {
      "type": "resource",
      "subtype": [resourceType],
      "id": id,
      "uid": uid,
      "revision": revision,
      "path": subpath,
    }

    return data

  def retrieveResource(self, resourceType=None, id=None, uid=None, revision=None, source=None):
    return self.datastore.retrieveResource(resourceType=resourceType, id=id, uid=uid, revision=revision, source=source)

  def retrieveFileStatFrom(self, id, uid, revision, resourceType, subpath):
    """ Pulls out the file status for the given path for this object.
    """

    handler = self.handlerFor(resourceType)
    path = self.pathFor(uid, revision, handler=handler)
    return self.handlerCall([resourceType], "retrieveFileStat", handler = handler,
                                                                uid = uid,
                                                                revision = revision,
                                                                path = path,
                                                                subpath = subpath)

  def retrieveFileFrom(self, id, uid, revision, resourceType, subpath, start=0, length=None):
    """ Pulls out a stream for retrieving the given file for this object.
    """
    handler = self.handlerFor(resourceType)
    path = self.pathFor(uid, revision, handler=handler)
    return self.handlerCall([resourceType], "retrieveFile", handler = handler,
                                                            uid = uid,
                                                            revision = revision,
                                                            path = path,
                                                            subpath = subpath,
                                                            start = start,
                                                            length = length)

  def retrieveHistory(self, id, uid, revision, resourceType):
    """ Returns the known history of this resource and any related resources.
    """

    handler = self.handlerFor(resourceType)
    path = self.pathFor(uid, revision, handler=handler)
    ret = self.handlerCall([resourceType], "retrieveHistory", handler = handler,
                                                              uid = uid,
                                                              revision = revision,
                                                              path = path)

    if ret:
      return ret

    resources = self.datastore.retrieveResources(resourceType = resourceType,
                                                 id           = id,
                                                 uid          = uid)

    return list(map(lambda row:
      {
        "revision": row.revision,
        "name": row.name,
        "source": row.source,
        "type": "resource",
        "subtype": [row.resource_type]
      },
      resources))

class ResourceError(OccamError):
  """ Base class for all resource errors.
  """

class ResourcePluginError(ResourceError):
  """ Base class for all resource plugin errors.
  """

class ResourcePluginMissingError(ResourcePluginError):
  """ Thrown when a plugin does not exist.
  """

  def __init__(self, name):
    super().__init__(f"Resource handler for type {name} not known")

class ResourcePluginInterfaceUnknownError(ResourcePluginError):
  """ Thrown when a plugin has an interface implementation we are not aware of.
  """

  def __init__(self, name, funcName):
    super().__init__(f"Resource handler for type {name} found but is not compatible due to the presence of unknown interface function {funcName}.")

class ResourcePluginInitializationError(ResourcePluginError):
  """ Thrown when a plugin reported a problem when initializing.
  """

  def __init__(self, name):
    super().__init__(f"Resource handler for type {name} found but could not be initialized")

class ResourcePluginUnimplementedError(ResourcePluginError):
  """ Thrown when a method within a plugin is not implemented.
  """

  def __init__(self, name, method):
    super().__init__(f"Resource handler for type {name} does not implement {method}")

class ResourceNotFoundError(ResourceError):
  """ This error is thrown when a resource is requested, but not found.
  """

  def __init__(self, resourceInfo, message):
    self.resourceInfo = resourceInfo
    self.message = message

  def __str__(self):
    return self.message + ": " + self.resourceInfo.get('name')

def resource(name):
  """
  This decorator will register the given class as a resource.
  """

  def register_resource(cls):
    ResourceManager.register(name, cls)
    return cls

  return register_resource

def interface(func):
  """ This decorator assigns the given function as part of the plugin.

  Only functions tagged with this decorator can be used as part of a plugin
  implementation.
  """

  # Attach it to the plugin
  def _pluginCall(self, *args, **kwargs):
    return func(self, *args, **kwargs)

  _pluginCall.registered = True
  return _pluginCall
