# The Configurations Component

This component handles configurations including modification and validation.

A configuration is an object that has a data file (JSON) and a schema (JSON)
that when used together defines a hierarchical key/value data structure that
can be validated against rules.

This manager supports updating these configuration objects allowing only valid
changes according to the validations.

Configurations can contain values that involve ranges of possible settings.
These configurations are meant to produce permutations of possible options
which result in multiple executions of objects.

These ranges have much flexibility. They are, in the end, lists of values for
a particular key. But, here are some examples and what they imply:

- "0...5"     - A list of values from 0 and including 5 [0, 1, 2, 3, 4, 5]
- "[0...5]"   - A list of values that are not permuted. Any other similar list will always pair values with this list.
- "1...8:x*2" - A list of values from 1 to 8 stepped by powers of 2 [1, 2, 4, 8]
- "0...8:x*2" - This is just [0], When a value repeats, it stops the iteration.

Pairs of configuration fields can be managed together. The first method is
through non-permuting lists. These are time-locked together such that each
pair of items is always contained together.

"[0...5]" and "[1...6]" produce pairs (0, 1), (1, 2), etc which mean there are
still only 6 executions of an object (not 36, if the options were permuted)

The next method is through variables. These allow people to construct ranges
based on the values currently being used by another key. There are some rules
that have to be abided by variables. For instance, you cannot use variables
on both sides of a non-permuted range from a non-permuted variable. This is
a confusing rule, but consider:

"i = [0...2]" and "[i...i+1]"

The non-permuted range must remain the same size, but this is impossible.
Variable "i" clearly has 3 different values at time 0, 1, and 2, but
the given range is ambiguous. However, the variable used to create a permuted
range is perfectly fine:

"i = [0...2]" and "i...i+1"

Means when i is 0, the second field permutes values 0 and 1. When i is 1, that
second field permutes through 1 and 2, and finally when i is 2, the permuted
key is both 2 and 3. Which means 6 executions. You can do other interesting
dynamic ranges such as:

"i = [0...2]" and "j = 0...i"

Which has i=0, j=0; i=1, j=0; i=1, j=1; i=2, j=0; i=2, j=1; i=2, j=2. Which
is a geometric expansion of j via i. This results in the sum of i number of
executions. Technically, "i" being iterative here doesn't matter.
