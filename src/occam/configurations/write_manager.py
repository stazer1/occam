# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log     import loggable
from occam.manager import manager, uses

import os

from occam.objects.manager        import ObjectManager
from occam.configurations.manager import ConfigurationManager
from occam.databases.manager      import DataNotUniqueError

@loggable
@manager("configurations.write", reader=ConfigurationManager)
@uses(ObjectManager)
@uses(ConfigurationManager)
class ConfigurationWriteManager:
  """ This manages the writing of information about used keys in configurations.
  """

  def store(self, output, generator, index, values, person = None):
    """ Adds a record of the mapping of configuration values to a given output.

    Arguments:
      output (Object): The object that was generated via a configuration.
      generator (Object): The object that described the configuration.
      index (int): The input index of the considered configuration.
      values (dict): The set of values to remember.
    """

    # Generate the configuration mapping
    configuration = self.configurations.keyStringFor(values)

    # Generate the hash for these values
    hash = self.configurations.hashFor(values)

    # Add the record
    self.datastore.write.createConfigurationMapping(id = output.id,
                                                    revision = output.revision,
                                                    baseId = generator.id,
                                                    baseRevision = generator.revision,
                                                    inputIndex = int(index),
                                                    configuration = configuration,
                                                    hash = hash)
