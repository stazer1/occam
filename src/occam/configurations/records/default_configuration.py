# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.databases.manager import table, index

@table("default_configurations")
class DefaultConfigurationRecord():
  """ Relates a local link to a configuration default.
  """

  schema = {

    # Primary Key (The object being configured.)

    "id": {
      "foreign": {
        "table": "objects",
        "key":   "id"
      }
    },

    # Foreign Key to Configuration Object

    "target_object_id": {
      "foreign": {
        "table": "objects",
        "key":   "id"
      }
    },

    # Attributes

    # The identity associated with the configuration
    "identity": {
      "type": "string",
      "length": 128
    },

    # The index of the wire serving the configuration input
    "index": {
      "type": "integer"
    },

    # The local link identifier
    "link": {
      "type": "string",
      "length": 128
    },

    # Constraints

    "relationship": {
      "constraint": {
        "type": "unique",
        "primary": True,
        "columns": ["id", "identity", "index", "target_object_id"],
        "conflict": "replace"
      }
    }
  }

@index(DefaultConfigurationRecord, "target_object_ids")
class DefaultConfigurationTargetObjectIdIndex:
  ids = ['target_object_id']

@index(DefaultConfigurationRecord, "indexes")
class DefaultConfigurationIndexIndex:
  ids = ['index']

@index(DefaultConfigurationRecord, "identities")
class DefaultConfigurationIdentityIndex:
  ids = ['identity']
