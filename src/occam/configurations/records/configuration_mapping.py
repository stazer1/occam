# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.databases.manager import table

@table('configuration_mappings')
class ConfigurationMappingRecord:
  schema = {
    # Fields

    # This holds the key/value pairs delimited by ';'
    # The keys will be encoded in base64
    "configuration": {
      "type": "text",
      "default": ";;"
    },

    # This holds the identifier of the output object
    "id": {
      "type": "string",
      "length": 256
    },

    # The revision of the output object
    "revision": {
      "type": "string",
      "length": 128
    },

    # A hash of the set values (configuration field)
    "hash": {
      "type": "string",
      "length": 128
    },

    # This holds the identifier of the base object
    # This is the object that generated the output (and contains the
    # configuration schema)
    "base_id": {
      "type": "string",
      "length": 256
    },

    # The revision of this base object
    "base_revision": {
      "type": "string",
      "length": 128
    },

    # The index of the input that is being configured
    "input_index": {
      "type": "integer"
    },

    # Constraint and Primary Key
    # (Only allow one configuration mapping per unique set of object ids and
    #  wire index)
    "relationship": {
      "constraint": {
        "type": "unique",
        "columns": ["id", "revision", "base_id", "base_revision", "input_index"],
        "conflict": "replace"
      }
    }
  }
