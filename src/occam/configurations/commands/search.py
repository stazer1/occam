# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

from occam.commands.manager import command, option, argument

from occam.object     import Object
from occam.log        import Log
from occam.key_parser import KeyParser

from occam.manager import uses

from occam.objects.manager        import ObjectManager
from occam.configurations.manager import ConfigurationManager

@command('configurations', 'search',
  category      = 'Configuration Management',
  documentation = "Searches for previously used values or generated output objects.")
@argument("object",   type  = "object",
                      help  = "the configuration or root object to query")
@argument("index",    type  = int,
                      default = -1,
                      nargs = "?",
                      help  = "the index of the input for the configuration to query")
@option("-k", "--key", action = "append",
                       dest   = "keys",
                       help   = "when specified, returns known values for the given key")
@option("-v", "--value", action = "append",
                         dest   = "values",
                         nargs  = 2,
                         help   = "when specified, returns outputs generated with the given value")
@option("-r", "--relationship", action = "append",
                                help = "treat the provided object as a target of the given relationship")
@uses(ObjectManager)
@uses(ConfigurationManager)
class ConfigurationSearchCommand:
  def do(self):
    # Get the object to query
    obj = self.objects.resolve(self.options.object, person = self.person)

    if obj is None:
      Log.error("cannot find object with id %s" % (self.options.object.id))
      return -1

    # We will return a JSON document of the form:
    # {
    #   values: { "base_id@base_revison": { "index": { key: [ ... ], key: [ ... ], ... }, ... }, ... },
    #   schemata: { "base_id@base_revison": { "index": { key: info, key: info, ... }, ... }, ... },
    #   outputs: [ { "object": { normalized object info }, "configuration": { "base_id@base_revision": { "index": { key: value, key: value, ... }, ... }, ... }, ... } ]
    # }

    # If no key is given, we find all possible keys
    ret = {
      "values": {},
      "outputs": []
    }

    ret["values"] = self.configurations.lookupValues(obj, relationship = self.options.relationship,
                                                          index = self.options.index,
                                                          keys = self.options.keys,
                                                          values = self.options.values)

    # We find outputs that match ALL provided key/value pairs
    ret["outputs"] = []
    rows = self.configurations.lookupOutput(obj, relationship = self.options.relationship,
                                                 index = self.options.index,
                                                 values = self.options.values)

    lookup = {}
    schemaLookup = {}

    for row in rows:
      token = row.object.id + "@" + row.object.revision
      if token in lookup:
        info = lookup[token]
      else:
        info = {
          "object": {
            "id": row.object.id,
            "uid": row.object.uid,
            "revision": row.object.revision,
            "identity": row.object.identity_uri,
            "owner": {
              "id": row.object.owner_id or row.object.id
            },
            "name": row.object.name,
            "type": row.object.object_type,
            "subtype": list(filter(None, row.object.subtype[1:-1].split(';'))),
            "tags": list(filter(None, row.object.tags[1:-1].split(';'))),
            "description": row.object.description,
            "environment": row.object.environment,
            "architecture": row.object.architecture,
            "organization": row.object.organization,
          },
          "configurations": {}
        }
        lookup[token] = info
        ret["outputs"].append(info)

      # Skip unconfigured output
      if str(row.input_index) == "-1":
        continue

      baseToken = row.base_id + "@" + row.base_revision
      fullToken = baseToken + "@" + str(row.input_index)

      if fullToken not in schemaLookup:
        # Get the object to query for the schema
        base = self.objects.retrieve(row.base_id, revision = row.base_revision,
                                                  person = self.person)

        if base:
          # Retrieve the schema
          schema = self.configurations.schemaFor(base, row.input_index)

          # Filter by the items in the values listing
          values = ret["values"][baseToken][str(row.input_index)]
          schema = self.configurations.filterSchema(schema, values)

          # Record it in the schema lookup and schema field
          ret["schemata"] = ret.get("schemata", {})
          ret["schemata"][baseToken] = ret["schemata"].get(baseToken, {})
          ret["schemata"][baseToken][str(row.input_index)] = schema
          schemaLookup[fullToken] = schema
        else:
          # Do not attempt that again
          schemaLookup[fullToken] = True

      info["configurations"][baseToken] = info["configurations"].get(baseToken, {})
      info["configurations"][baseToken][str(row.input_index)] = {
        "hash": row.hash,
        "configuration": row.decoded
      }

    Log.output(json.dumps(ret))
    return 0
