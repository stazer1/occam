# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sql
import sql.aggregate

from occam.log import loggable
from occam.databases.manager import uses, datastore

from occam.objects.database import ObjectDatabase
from occam.links.database import LinkDatabase

@loggable
@uses(ObjectDatabase)
@uses(LinkDatabase)
@datastore("configurations")
class ConfigurationDatabase:
  """ Manages the database interactions for configuration management.
  """

  def queryDefaultConfigurations(self, id=None,
                                       identity=None,
                                       index=0,
                                       key=None):
    """ Returns a instantiated query object for default configuration records.

    Args:
      id (str): The identifier for the object being configured.
      identity (str): The identity URI for the person accessing the default.
      index (int): The index of the wire for the configuration to use.
      key (str): The key for the query to return.

    Returns:
      sql.Query: The query to be executed.
    """

    defaultConfigurations = sql.Table("default_configurations")
    if key:
      query = defaultConfigurationss.select(defaultConfigurations.__getattr__(key))
    else:
      query = defaultConfigurations.select()

    query.where = sql.Literal(True)

    if id:
      query.where = query.where & (defaultConfigurations.id == id)

    if identity:
      query.where = query.where & (defaultConfigurations.identity == identity)

    if index:
      query.where = query.where & (defaultConfigurations.index == index)

    return query

  def retrieveDefaultConfiguration(self, id=None,
                                         identity=None,
                                         index=0):
    """ Retrieves default configuration records that match given criteria.

    Args:
      id (str): The identifier for the object being configured.
      identity (str): The identity URI for the person accessing the default.
      index (int): The index of the wire for the configuration to use.

    Returns:
      DefaultConfigurationRecord: The record reflecting the default configuration or None.
    """

    # Create a session
    from occam.configurations.records.default_configuration import DefaultConfigurationRecord
    session = self.database.session()

    # Retrieve the query
    query = self.queryDefaultConfigurations(id = id,
                                            identity = identity,
                                            index = index)

    # Execute the query
    self.database.execute(session, query)

    # Retrieve data
    row = self.database.fetch(session)

    # Return the result
    if row is not None:
      row = DefaultConfigurationRecord(row)

    return row

  def createDefaultConfiguration(self, id=None,
                                       targetObjectId=None,
                                       identity=None,
                                       index=None,
                                       link=None):
    """ Creates a default configuration record to track the staged data.
    """

    from occam.configurations.records.default_configuration import DefaultConfigurationRecord

    # Create a session
    session = self.database.session()

    # Create an empty record
    defaultConfiguration = DefaultConfigurationRecord()

    # Start filling out the record
    defaultConfiguration.id = id
    defaultConfiguration.target_object_id = targetObjectId
    defaultConfiguration.identity = identity
    defaultConfiguration.index = index
    defaultConfiguration.link = link

    # Submit the record
    self.database.update(session, defaultConfiguration)
    self.database.commit(session)

    return defaultConfiguration

  def queryConfigurationValues(self, baseId, baseRevision, relationship, index, keys=None, values=None, key=None):
    configurationMappings = sql.Table("configuration_mappings")

    if key:
      query = configurationMappings.select(identities.__getattr__(key))
    else:
      query = configurationMappings.select(configurationMappings.id,
                                           configurationMappings.revision,
                                           configurationMappings.configuration,
                                           configurationMappings.hash,
                                           configurationMappings.base_id,
                                           configurationMappings.base_revision,
                                           configurationMappings.input_index)
    
    if relationship:
      # Get the subquery for the base object via the LinkDatabase
      links = self.links.queryLinks(targetObjectId = baseId,
                                    targetObjectRevision = baseRevision,
                                    relationship = relationship,
                                    key = 'source_object_id')
      query.where = (configurationMappings.id.in_(links))

      configurationMappings2 = sql.Table("configuration_mappings")
      subquery = configurationMappings2.select(configurationMappings2.id)
      subquery.where = (configurationMappings2.id.in_(links))

      # TODO:
      # We also want any objects that do not have configurations to be supplied
      # as well. This requires wrapping our query in a join with the links as
      # well.
      # just AND with objects not in 'query' yet found in 'links' and supply the
      # ConfigurationMappingRecord, but with a hash of ';;' and
      # configuration = ';;'.
      objects = sql.Table('objects')
      unionQuery = objects.select(objects.id,
                                  objects.revision,
                                  sql.As(sql.Literal(';;'), "configuration"),
                                  sql.As(sql.Literal('a9032d0faccd7977d530484b5368c4dbc6c7c8964941a94a97a269c079c9f2ea'), "hash"),
                                  sql.As(objects.id, "base_id"),
                                  sql.As(objects.revision, "base_revision"),
                                  sql.As(sql.Literal(-1), "input_index"))
      unionQuery.where = objects.id.in_(links) & (sql.operators.Not(objects.id.in_(subquery)))
      query = query | unionQuery
    else:
      # Otherwise, the provided object *is* the output object
      query.where = (configurationMappings.base_id == baseId) & \
                    (configurationMappings.base_revision == baseRevision)

    # If index is -1, then we ignore it and return all configuration mappings
    if index >= 0:
      query.where = query.where & (configurationMappings.input_index == index)

    # We can filter by keys that were set
    if keys:
      keyQuery = sql.Literal(False)
      for key in keys:
        keyQuery = keyQuery | (configurationMappings.configuration.like("%;" + key + "=%"))
      query.where = query.where & keyQuery

    # We can filter by configurations that have particular set values
    # TODO: allow looking for 'missing' values which indicate the use of the default value
    if values:
      valueQuery = sql.Literal(False)
      for value in values:
        valueQuery = valueQuery | (configurationMappings.configuration.like("%;" + value + ";%"))
      query.where = query.where & valueQuery

    return query

  def retrieveConfigurationValues(self, baseId, baseRevision, relationship, index, keys=None, values=None):
    from occam.configurations.records.configuration_mapping import ConfigurationMappingRecord

    import json
    import base64

    session = self.database.session()

    query = self.queryConfigurationValues(baseId = baseId,
                                          baseRevision = baseRevision,
                                          relationship = relationship,
                                          index = index,
                                          keys = keys,
                                          values = values)

    # Execute the query
    self.database.execute(session, query)

    # Return a set of values for each key we see
    ret = {}

    # TODO: also include the default value for rows without the key

    # Retrieve data
    rows = self.database.many(session, size=100)
    if rows:
      for row in rows:
        row = ConfigurationMappingRecord(row)
        
        # Skip unconfigured output
        if str(row.input_index) == "-1":
          continue

        baseToken = row.base_id + "@" + row.base_revision
        ret[baseToken] = ret.get(baseToken, {})
        ret[baseToken][str(row.input_index)] = ret[baseToken].get(str(row.input_index), {})
        block = ret[baseToken][str(row.input_index)]

        # Decode the values
        for key in (keys or []):
          try:
            index = row.configuration.index(f";{key}=")
            index = index + 1 + len(key) + 1
            end = row.configuration.index(';', index)

            # Get the value from that entry
            value = row.configuration[index:end]

            # Decode key/value
            key = key + "=="
            key = base64.b64decode(key.encode('utf-8')).decode('utf-8')
            value = value + "=="
            value = json.loads(base64.b64decode(value.encode('utf-8')).decode('utf-8'))

            # Remember it
            block[key] = block.get(key, set())
            block[key].add(value)
          except:
            block[key] = block.get(key, set())
            block[key].add(None)

        # Get all keys by splitting by ';' and decoding
        if keys is None or len(keys) == 0:
          for item in row.configuration[1:-1].split(';'):
            # Skip any that are blank (default configuration)
            if item == "":
              continue

            key, value = item.split('=')

            # Decode key/value
            key = key + "=="
            key = base64.b64decode(key.encode('utf-8')).decode('utf-8')
            value = value + "=="
            value = json.loads(base64.b64decode(value.encode('utf-8')).decode('utf-8'))

            # Remember it
            block[key] = block.get(key, set())
            block[key].add(value)

    # Turn sets into lists so they can be serialized
    for baseToken in ret.keys():
      for inputIndex in ret[baseToken].keys():
        for k in ret[baseToken][inputIndex].keys():
          ret[baseToken][inputIndex][k] = list(ret[baseToken][inputIndex][k])

    # Return a dictionary of keys to lists of used values
    return ret

  def retrieveConfigurationOutputs(self, baseId, baseRevision, relationship, index, values=None):
    # Join the id/revision with the object records and pull out normalized
    # forms of each output object from the repository knowledge.
    from occam.configurations.records.configuration_mapping import ConfigurationMappingRecord

    import json
    import base64

    session = self.database.session()

    query = self.queryConfigurationValues(baseId = baseId,
                                          baseRevision = baseRevision,
                                          relationship = relationship,
                                          index = index,
                                          values = values)

    # Join this query with the objects table
    objects = sql.Table('objects')
    join = query.join(objects)
    join.condition = (query.id == join.right.id)
    query = join.select(objects.id,
                        objects.revision,
                        objects.name,
                        objects.object_type,
                        objects.description,
                        objects.environment,
                        objects.architecture,
                        objects.subtype,
                        objects.uid,
                        objects.parent_id,
                        objects.owner_id,
                        objects.identity_uri,
                        query.configuration,
                        query.hash,
                        query.base_id,
                        query.base_revision,
                        query.input_index)

    # Execute the query
    self.database.execute(session, query)

    # Return a set of values for each key we see
    ret = {}

    # TODO: also include the default value for rows without the key

    # Retrieve data
    rows = self.database.many(session, size=100)
    if rows:
      from occam.objects.records.object import ObjectRecord

      def mergeRows(row):
        ret = ConfigurationMappingRecord(row)
        ret.object = ObjectRecord(row)

        ret.decoded = {}

        for item in ret.configuration[1:-1].split(';'):
          # Skip any that are blank (default configuration)
          if item == "":
            continue

          key, value = item.split('=')

          # Decode key/value
          key = key + "=="
          key = base64.b64decode(key.encode('utf-8')).decode('utf-8')
          value = value + "=="
          value = json.loads(base64.b64decode(value.encode('utf-8')).decode('utf-8'))

          ret.decoded[key] = value

        return ret

      rows = [mergeRows(row) for row in rows]

    return rows
