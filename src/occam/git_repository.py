# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os     # Path functions
import codecs # String decoding
import sys    # Exception information
import json   # For parsing recipes
import time   # For timeout

from occam.log import loggable

from datetime import datetime

# TODO: respond to error codes and return False on errors

@loggable
class GitRepository:
  """ This class represents a local git repository.

  This support class will handle retrieving OCCAM metadata from objects given a
  revision and a object path that points to the git repository representing that
  object.

  When the GitRepository is created using a network path, the repository is
  automatically cloned, as needed, to a temporary path. This path is then
  deleted when the class is destructed.
  """

  @staticmethod
  def popen(command, stdout=None, stdin=None, stderr=None, cwd=None, env=None):
    """ Handles opening a subprocess to invoke a git command.

    This wraps the existing subprocess.Popen() constructor. By default, it
    throws out stdout and will capture stderr.

    The `stdout`, `stdin`, and `stderr` arguments can be set to the expected
    values of the `subprocess.Popen` constructor. The `stdout` argument has a
    default value of `subprocess.DEVNULL`, and the `stderr` argument has a
    default value of `subprocess.PIPE` in order to capture common errors. The
    defaults are set when `None` is provided as a value.

    Arguments:
      command (list): A list of strings comprising the command to invoke.
      stdout (int): The subprocess standard output.
      stdin (int): The subprocess standard input.
      stderr (int): The subprocess standard error.
      cwd (path): The path in which the command will be invoked.
      env (dict): The environment variables to set during the invocation.

    Returns:
      subprocess.Popen: The process class.
    """

    import subprocess
    if stdout is None:
      stdout = subprocess.DEVNULL

    if stderr is None:
      # Capture stderr by default to catch lock errors
      stderr = subprocess.PIPE

    return subprocess.Popen(command, stdout=stdout, stdin=stdin, stderr=stderr, cwd=cwd, env=env)

  @staticmethod
  def wait(path, timeout=5):
    """ Waits until the lock in the given git repository local path is released.

    Arguments:
      path (str): The path to the git repository which contains the `.git` subpath.
      timeout (float): The number of seconds to wait, maximum.
    """

    elapsed = 0
    while os.path.exists(os.path.join(path, '.git', 'index.lock')) and elapsed < timeout:
      time.sleep(0.5)
      elapsed += 0.5

  @staticmethod
  def interpretLockFailure(process):
    """ Determines whether or not the git process failed due to a lock.

    The process must have had stderr preserved.

    Arguments:
      process (subprocess.Popen): The executed process.

    Returns:
      bool: True if there was a failure due to a lock error.
    """

    # Interpret if the repository is locked
    err = process.stderr.read().decode('utf-8')
    return ".git/index.lock': File exists" in err

  @staticmethod
  def create(path, alternates=None, objectFormat=None):
    """ Creates a git repository for a new artifact.

    The `alternates` parameter can be None to signify that there is no alternate
    repository. The alternates repository is one which satisfies any git object
    that is queried but not found in this repository. The is useful in the
    creation of object pools.

    The `objectFormat` parameter can allow for specifying the type of revision
    hash system in place. For instance "sha256" will, on supporting systems and
    versions of git, create a repository compatible with sha256 commit hashes.

    Arguments:
      path (str): The path in which to initialize a git repository.
      alternates (str): The path that serves as the alternates repository.
      objectFormat (str): The object format to store the repository commits.
    """

    GitRepository.Log.noisy("creating git repository in %s" % (path))

    import subprocess
    command = ['git', 'init']

    # Add the --object-format argument, if provided
    if objectFormat is not None:
      command.append("--object-format")
      command.append(objectFormat)

    # Perform the git init command
    p = GitRepository.popen(command, stdout=subprocess.PIPE, cwd=path)
    p.wait()

    # Remove silly 'master' branch. Masters are a terrible, oppressive idea.
    # They do not exist, and should not exist, within a distributed system.
    # Give it a random identifier >:]
    from uuid import uuid4
    import subprocess
    p = GitRepository.popen(['git', 'checkout', '-b', str(uuid4())], stdout=subprocess.PIPE, cwd=path)
    p.wait()

    # Delete the master branch
    p = GitRepository.popen(['git', 'branch', '-d', 'master'], stdout=subprocess.PIPE, cwd=path)
    p.wait()

    # Turn off garbage collection
    p = GitRepository.popen(['git', 'config', '--local', 'gc.auto', '0'], stdout=subprocess.PIPE, cwd=path)
    p.wait()

    # Add alternates (if requested)
    if alternates:
      with open(os.path.join(path, ".git", "objects", "info", "alternates"), "w+") as f:
        f.write(alternates)
        f.write("/.git/objects")

    return GitRepository(path)

  @staticmethod
  def exists(path):
    """ Determines whether or not the given path contains a git repository.

    It makes no determination of the actual health of the repository found at
    the given path. It may be true that this function determines a git
    repository exists at `path`, but the repository cannot be understood.

    Arguments:
      path (str): The path in which a git repository might exist.

    Returns:
      bool: True when the path contains a git repository.
    """

    return os.path.exists(os.path.join(path, '.git'))

  @staticmethod
  def isAt(url, cert=None):
    """ Determines if the given URL is an accessible git repository.

    Arguments:
      url (str): The URL to query.
      cert (str): The path of an SSL certificate to use.

    Returns:
      bool: True when the given `url` is determined to be a git repository.
    """

    # Look for git repo
    result = -1

    # Determine the environment
    env = dict(os.environ)
    if not cert is None:
      env = dict(os.environ, GIT_SSL_CAINFO=cert)

    env['GIT_TERMINAL_PROMPT'] = "0"

    try:
      result = GitRepository.popen(['git', 'ls-remote', url], env=env).wait()
    except:
      GitRepository.Log.error("cannot open resource: %s" % (sys.exc_info()[0]))

    return result == 0

  @staticmethod
  def remote(url, branch="HEAD", cert=None):
    """ Returns the revision of the given branch of the given URL.
    """

    import subprocess

    # Determine the environment
    env = None
    if not cert is None:
      env = dict(os.environ, GIT_SSL_CAINFO=cert)

    command = ['git', 'ls-remote', url, branch]
    p = GitRepository.popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=env)
    stdout, stderr = p.communicate()

    return stdout.split(b'\t')[0].decode('utf-8')

  @staticmethod
  def version():
    """ Returns the git version as an integer.

    It will return MMMmmmppp (where M is the major version, m is minor (with
    leading zeroes), and p is the patch level (with leading zeroes)). It
    returns this as a single integer.

    Returns:
      int: The version encoded as a single integer in the form `MMMmmmppp`.
    """

    # Cache result
    if not hasattr(GitRepository, '_version'):
      # Run git --version to retrieve the version string.
      import subprocess
      p = GitRepository.popen(['git', '--version'], stdout=subprocess.PIPE)

      # Parse out the version sequence
      # Pulls out the first line of stdout, strips the whitespace
      # And then splits on the '.' to form an array of three parts
      version = p.stdout.read().split(b'\n')[0].strip().decode('utf-8')
      parts = version.split(' ')[-1].split('.')

      # Pad parts array out until it has 4 items
      while len(parts) < 4:
        parts.append(0)

      # Encode the version as a number
      ret = 0
      for part in parts:
        ret = ret * 1000 + int(part)

      GitRepository._version = ret

    return GitRepository._version

  def __init__(self, path, revision=None, cert=None, externalToken=None):
    """ Opens `path` as a git repository.

    If `path` is a network path, the repository will be cloned to a temporary
    path. That path will be accessible via the `path` property of the
    constructed instance.

    If such a temporary path is created, it will be subsequently destroyed when
    the object is destructed.

    Arguments:
      path (str): The local file path or network URL for the git repository.
      revision (str): The revision to initially checkout as a multihash.
      cert (str): The path to the SSL certificate to use when cloning.
      externalToken (str): The Occam token to also pass along when cloning.
    """

    if not revision:
      revision = "HEAD"

    revision = GitRepository.hexFromMultihash(revision)

    self.revision = revision
    self.path = None
    self.tmp = True

    env = None
    if not cert is None:
      env = dict(os.environ, GIT_SSL_CAINFO=cert)

    import re
    import tempfile     # For temporary directories
    import subprocess

    # Check for a scheme in the path to see if this is a remote pull or not.
    if not re.match(r'^[a-z]+://', path):
      # Open a local git repository (by referencing it directly)
      self.tmp  = False
      self.path = path
      return

    # Open a remote git repository (by cloning locally)
    tmpdir = os.path.realpath(tempfile.mkdtemp(prefix="occam-"))
    self.path = tmpdir

    command = ['git']

    # If a token is needed to access the repo, we need to provide it via a
    # header, as git will not accept paths with queries in them.
    if externalToken:
      command.extend(['-c', f"http.extraHeader=X_OCCAM_TOKEN:{externalToken}"])

    # Invoke the command to clone
    command.extend(['clone', path, tmpdir])

    p = GitRepository.popen(
      command, stdout=subprocess.DEVNULL, stderr=subprocess.PIPE,
      cwd=tmpdir, env=env
    )
    stdout, stderr = p.communicate()

    if p.returncode != 0:
      err = stderr.decode('utf8')

      # Try to only dump out the fatal portion of stderr if possible.
      match = re.search("fatal:.*$", err)
      if match:
        err = match.group(0)
      raise RuntimeError(f"Failed to git clone '{path}': {err}.")

    # TODO: check for revision errors, throw exception

  def __del__(self):
    """ The destructor that deletes any temporary paths.
    """

    try:
      if self.tmp:
        import shutil
        shutil.rmtree(self.path)
    except Exception as e:
      GitRepository.Log.error(f"Failed to delete {self.path}: {e}")

  @staticmethod
  def hasRevision(path, revision="HEAD"):
    """ Determines if the given revision exists within the given repository.

    Arguments:
      path (str): The path to a git repository.
      revision (str): The revision to query.

    Returns:
      bool: True when the given revision exists within the given repository.
    """

    revision = GitRepository.hexFromMultihash(revision)

    import subprocess
    # cat-file -e (suppresses output and just returns error code)
    GitRepository.Log.noisy(f"Checking if {path} has revision {revision}")
    p = GitRepository.popen(['git', 'cat-file', '-e', revision], cwd=path)

    errorCode = p.wait()
    return errorCode == 0

  @staticmethod
  def fullRevision(path, revision="HEAD"):
    """ Determines the full revision hash for the given revision and repository.

    Arguments:
      path (str): The path to a git repository.
      revision (str): The revision to query.

    Returns:
      str: The full revision string or False if there was an error.
    """

    revision = GitRepository.hexFromMultihash(revision)

    import subprocess
    p = GitRepository.popen(['git', 'rev-parse', revision], stdout=subprocess.PIPE, cwd=path)

    ret = p.stdout.read().decode('utf-8').strip()

    errorCode = p.wait()
    if errorCode != 0:
      return False

    # Encode as a multihash
    return GitRepository.hexToMultihash(ret)

  @staticmethod
  def hexToMultihash(revision):
    """ Returns the multihash for a normal SHA-1 revision.

    Arguments:
      revision (str): A normal hexadecimal SHA-1 revision string.

    Returns:
      str: A standard multihash of the same SHA-1 string.
    """

    # Create a URI by hashing the public key with a multihash
    from occam.storage.plugins.ipfs_vendor.multihash import multihash
    revisionBytes = bytearray.fromhex(revision)
    if len(revisionBytes) == 20:
      hashedBytes = bytearray([multihash.SHA1, len(revisionBytes)])
      hashedBytes.extend(revisionBytes)
    else:
      hashedBytes = bytearray([multihash.SHA2_256, len(revisionBytes)])
      hashedBytes.extend(revisionBytes)

    from occam.storage.plugins.ipfs_vendor.base58 import b58encode
    ret = b58encode(bytes(hashedBytes))

    return ret

  @staticmethod
  def hexFromMultihash(multihash):
    """ Returns the normal expected hex for a multihash.

    Arguments:
      multihash (str): The multihash of a revision.

    Returns:
      str: The native git hexadecimal revision string.
    """

    # Detect a multihash, otherwise do nothing
    if len(multihash) != 30 and len(multihash) != 46:
      return multihash

    from occam.storage.plugins.ipfs_vendor.base58 import b58decode
    hashedBytes = b58decode(multihash)

    from occam.storage.plugins.ipfs_vendor.multihash import multihash as _multihash
    hashedBytes = _multihash.decode(hashedBytes)

    import binascii

    ret = binascii.hexlify(bytearray(hashedBytes)).decode('utf-8')

    return ret

  def retrieveJSON(self, filepath):
    """ Retrieves the JSON data for the given file.

    It uses the current revision and path of the repository and uses the `show`
    subcommand of git to retrieve the data.

    Arguments:
      filepath (str): The path to a file within the repository.

    Raises:
      IOError: When the file requested is not found.

    Returns:
      dict: The JSON data or {} if the JSON data is invalid.
    """

    from collections import OrderedDict

    import subprocess
    p = GitRepository.popen(['git', 'show', "%s:./%s" % (self.revision, filepath)], stdout=subprocess.PIPE, cwd=self.path)
    reader = codecs.getreader('utf-8')

    try:
      object_info = json.load(reader(p.stdout), object_pairs_hook=OrderedDict)
    except:
      object_info = {}
    code = p.wait()
    if code != 0:
      raise IOError("File Not Found")

    return object_info

  def retrieveSize(self):
    """ Returns the size of the repository.

    The resulting structure will contain a 'size' field containing the number of
    bytes taken up by the tracked data for the object.

    A 'size-pack' field contains a more complete total of the space taken up by
    cached and temporary "pack" files used by git behind the scenes that
    nonetheless take up storage space.

    The 'count' field contains the number of "loose" objects within the
    repository structure.

    Returns:
      dict: A dict containing at least a 'size' property in bytes.
    """

    import subprocess
    p = GitRepository.popen(['git', 'count-objects', '-v'], stdout=subprocess.PIPE, cwd=self.path)

    ret = {}
    alternateSize = {}
    for line in p.stdout:
      line = line.decode('utf-8').rstrip("\r\n")

      if line.startswith("size:"):
        try:
          ret["size"] = int(line[5:].strip()) * 1024
        except:
          ret["size"] = 0
      elif line.startswith("alternate:"):
        # Recursively gather the size of the alternate
        alternatePath = line[10:].strip()
        alternateSize = GitRepository(alternatePath).retrieveSize()
      elif line.startswith("size-pack:"):
        try:
          ret["size-pack"] = int(line[10:].strip()) * 1024
        except:
          ret["size-pack"] = 0

    code = p.wait()

    if code != 0:
      raise IOError("File Not Found")

    ret['size-pack'] = ret.get('size-pack', 0) + alternateSize.get('size-pack', 0)
    ret['size'] = ret.get('size', 0) + alternateSize.get('size', 0)

    return ret

  def retrieveFile(self, filepath, start=0, length=None, staged=False):
    """ Retrieves a data stream for the given file.

    When `staged` is True, this will retrieve the file data within the current
    uncommitted index.

    Arguments:
      filepath (str): The path to a file within the repository.
      start (int): The offset to the first byte of the file to retrieve.
      length (int): The number of bytes to retrieve (None for all.)
      staged (bool): Whether or not to retrieve the indexed version.

    Raises:
      IOError: When the file requested is not found.

    Returns:
      io.BytesIO: A data stream representing the file data.
    """

    if staged:
      # XXX: Sanitize path?
      if filepath.startswith("/"):
        filepath = filepath[1:]
      path = os.path.join(self.path, filepath)
      ret = open(path, "rb")
      ret.seek(start)
      return ret
    else:
      import subprocess
      p = GitRepository.popen(['git', 'show', self.revision + ":./" + filepath], stdout=subprocess.PIPE, cwd=self.path)

      ret = None
      p.stdout.read(start)
      ret = p.stdout

      #code = p.wait()
      #if code != 0 and code != -9:
      #  raise IOError("File Not Found")

    return ret

  def retrieveFileStat(self, filepath, staged = False):
    """ Returns the file status of the given file within the git repository.

    When `staged` is True, this will retrieve the file data within the current
    uncommitted index.

    The file metadata returned has several fields. The `name` field is the name
    of the file. The `stat` contains the OS-level mode information. The `size`
    field is the size of the file in bytes. The `type` field is either `"blob"`
    for a normal data file or `"tree"` for a directory. The `hash` field is the
    hash of the file, if part of a git commit.

    Arguments:
      filepath (str): The path to a file within the repository.
      staged (bool): Whether or not to retrieve the indexed version.

    Raises:
      IOError: When the file requested is not found.

    Returns:
      dict: The file metadata.
    """

    if filepath.startswith("/") and filepath != "/":
      filepath = filepath[1:]

    GitRepository.Log.noisy("getting file stat for '%s' at %s" % (filepath, self.path))

    if staged:
      # XXX: Sanitize path?
      path = os.path.join(self.path, filepath)

      info = {}
      stat = os.stat(path)
      info['name'] = os.path.basename(path)
      info['mode'] = stat.st_mode
      info['size'] = os.path.getsize(path)
      info['type'] = 'blob'
      info['date'] = datetime.fromtimestamp(stat.st_mtime).isoformat() + "Z"
      if os.path.isdir(path):
        info['type'] = 'tree'
      return info

    if filepath == "/":
      filepath = ""
      return {
        'name': ".",
        'type': "tree",
        'size': None,
        'mode': 0o040000,
        'hash': self.revision
      }

    import subprocess
    command = ['git', 'ls-tree', self.revision, "-l", "--", filepath]

    p = GitRepository.popen(command, stdout=subprocess.PIPE, cwd=self.path)

    info = None
    for line in p.stdout:
      info = {}
      line = line.decode('utf-8').rstrip("\r\n")
      prime_components = line.split('\t')
      components = [x.strip() for x in prime_components[0].split()]
      info['mode'] = int(components[0], 8)
      info['type'] = components[1]
      info['hash'] = components[2]
      if components[3] == "-":
        info['size'] = None
      else:
        try:
          info['size'] = int(components[3])
        except:
          info['size'] = 0

      info['name'] = prime_components[1]

    code = p.wait()

    if code != 0:
      raise IOError("File Not Found")

    return info

  def retrieveDirectory(self, filepath, staged = False):
    """ Returns the directory listing of the given filepath.

    When `staged` is True, this will retrieve the directory metadata for files
    within the uncommitted index.

    Returns a dictionary containing the information within the directory::

      {
        "items": [{
          "name": "foo.txt",   # name of the file
          "size": 1384,        # file size in bytes
        }, ... ],
      }

    Arguments:
      filepath (str): The path to a directory within the repository.
      staged (bool): Whether or not to retrieve the indexed version.

    Raises:
      IOError: When the directory requested is not found.

    Returns:
      dict: The directory metadata.
    """

    if filepath.startswith("/") and filepath != "/":
      filepath = filepath[1:]

    ret = {}
    ret['directory'] = {}
    ret['items'] = []

    GitRepository.Log.noisy("reading directory at revision " + self.revision)

    import subprocess
    p = GitRepository.popen(['git', 'ls-tree', self.revision + ":./" + filepath, "-l"], stdout=subprocess.PIPE, cwd=self.path)

    for line in p.stdout:
      info = {}
      line = line.decode('utf-8').rstrip("\r\n")
      prime_components = line.split('\t')
      info['name'] = prime_components[1]
      components = [x.strip() for x in prime_components[0].split()]
      info['stat'] = components[0]
      info['type'] = components[1]
      info['hash'] = components[2]
      if components[3] == "-":
        info['size'] = None
      else:
        try:
          info['size'] = int(components[3])
        except:
          info['size'] = 0

      ret['directory'][info['name']] = info
      ret['items'].append(info)

    code = p.wait()

    if code != 0 and not staged:
      raise IOError("File Not Found")

    if staged:
      # We also need the not-yet-committed new files
      # We will just do a traditional view of the files, and add all of them!
      path = self.path
      if filepath != "/":
        path = os.path.join(path, filepath)

      try:
        files = os.listdir(path)
      except:
        files = []

      for subPath in files:
        if subPath == ".git":
          continue
        fullPath = os.path.join(path, subPath)

        info = {}
        info['name'] = subPath
        info['stat'] = os.stat(fullPath).st_mode
        info['size'] = os.path.getsize(fullPath)
        info['type'] = 'blob'
        if os.path.isdir(fullPath):
          info['type'] = 'tree'
        ret['directory'][info['name']] = info
        ret['items'].append(info)

    return ret

  def history(self, path = None, before = None, after = None, limit=10):
    """ Returns a list of log entries from the given revision.

    Arguments:
      path (str): The filter path to a file or directory within the repository.
      before (str): A iso8601 date string to filter the history.
      after (str): A iso8601 date string to filter the history.
      limit (int): The maximum number of results to return.

    Raises:
      IOError: When the specified path is not found.

    Returns:
      list: A set of commit metadata that match the criteria provided.
    """

    ret = []

    dateType = "iso8601-strict"

    if GitRepository.version() < 2000000000:
      # Git 1.x does not have the good iso8601...
      dateType = "iso8601"

    import subprocess
    args = ['git', 'log', '--date=%s' % (dateType), '-%s' % (str(limit)), self.revision]

    if before:
      args.append("--before")
      args.append(before)

    if after:
      args.append("--after")
      args.append(after)

    p = GitRepository.popen(args, stdout=subprocess.PIPE, cwd=self.path)

    commit = None
    readingCommit = False

    for line in p.stdout:
      info = {}
      line = line.decode('utf-8').rstrip("\r\n")

      # The initial commit message placeholder is four spaces
      # since that is the indentation 'git' yields that we
      # will eventually remove later.
      commit = commit or {"message":  "    ",
                          "author":   "",
                          "date":     "",
                          "revision": ""}

      if readingCommit:
        if line == "":
          readingCommit = False
          ret.append(commit)
          commit = None
        else:
          commit["message"] = commit.get("message", "    ") + line.lstrip("\t ") + "\n"
      else:
        if line.startswith("commit "):
          commit["revision"] = GitRepository.hexToMultihash(line[7:].strip())
        elif line.startswith("Author: "):
          commit["author"] = line[8:].strip()
        elif line.startswith("Date: "):
          commit["date"] = line[6:].strip()

          if dateType == "iso8601":
            # We need to make this date conform
            import re
            commit["date"] = commit["date"].replace(" ", "T", 1).replace(" ", "")
            commit["date"] = re.sub(r"([+-]\d\d)(\d\d)$", "\\1:\\2", commit["date"])

        if line == "":
          readingCommit = True

    if commit:
      commit["message"] = commit.get('message', '    ')[4:]
      ret.append(commit)

    code = p.wait()

    if code != 0:
      raise IOError("File Not Found")

    return ret

  def commit(self, message, filepath='-a', retries=20):
    """ Commits the current indexed changes to the repository.

    Arguments:
      message (str): The commit message to apply to the log.
      filepath (str): The file to commit (or '-a' for all files in index.)
      retries (int): The number of times to retry on lock error.

    Returns:
      str: The new revision or False on error.
    """

    GitRepository.Log.noisy("commiting '%s' to %s" % (message, self.path))

    # Wait until the repository is unlocked
    GitRepository.wait(self.path)

    # Call 'git commit {filepath}'
    import subprocess
    p = GitRepository.popen(['git', 'commit', filepath, '--author="occam <occam@occam.cs.pitt.edu>"', '--file=-'], stdout=subprocess.PIPE, stdin=subprocess.PIPE, cwd=self.path, env={"GIT_COMMITTER_NAME": "occam", "GIT_COMMITTER_EMAIL": "occam@occam.cs.pitt.edu"})
    p.stdin.write(message.encode('utf-8'))
    p.stdin.close()
    errorCode = p.wait()

    # Interpret if the repository is locked somehow, and retry if so
    if errorCode != 0:
      # Interpret error code
      out = p.stdout.read().decode('utf-8')
      if "nothing to commit" in out:
        # This is fine... it is empty
        return self.head()

      if GitRepository.interpretLockFailure(p) and retries > 0:
        # Retry
        return self.commit(message, filepath, retries - 1)

      # Hard fail
      return False

    return self.head()

  def delete(self, filepath, staged=False, retries=20):
    """ Removes the given file from the repository.

    When `staged` is True, this will delete the file locally within the current
    uncommitted index.

    Arguments:
      filepath (str): The path to the doomed file.
      staged (bool): Whether or not to delete the staged file.
      retries (int): The number of times to retry on lock error.

    Returns:
      bool: True when the command completes successfully.
    """

    # Wait until the repository is unlocked
    GitRepository.wait(self.path)

    if staged:
      # XXX: Sanitize path?
      if filepath.startswith("/"):
        filepath = filepath[1:]
      path = os.path.join(self.path, filepath)
      os.remove(path)
      return True

    # Call 'git rm {filepath}'
    p = GitRepository.popen(['git', 'rm', filepath], cwd=self.path)
    errorCode = p.wait()

    # Interpret if the repository is locked somehow, and retry if so
    if errorCode != 0:
      if GitRepository.interpretLockFailure(p) and retries > 0:
        # Retry
        return self.delete(filepath, retries - 1)
      return False

    return True

  def move(self, old, new, retries=20):
    """ Moves or renames the given file within the repository.

    Arguments:
      old (str): The path to the file within the repository.
      new (str): The new path for the file within the repository.
      retries (int): The number of times to retry on lock error.

    Returns:
      bool: True when the command completes successfully.
    """

    # Wait until the repository is unlocked
    GitRepository.wait(self.path)

    # TODO: handle errors
    # Call 'git mv {old} {new}'
    p = GitRepository.popen(['git', 'mv', old, new], cwd=self.path)
    errorCode = p.wait()

    # Interpret if the repository is locked somehow, and retry if so
    if errorCode != 0:
      if GitRepository.interpretLockFailure(p) and retries > 0:
        # Retry
        return self.move(old, new, retries - 1)
      return False

    return True

  def add(self, filename='*', retries=20):
    """ Adds the given file path within the repository to the index.

    Arguments:
      filename (str): The path to the file within the repository.
      retries (int): The number of times to retry on lock error.

    Returns:
      bool: True when the command completes successfully.
    """

    # Wait until the repository is unlocked
    GitRepository.wait(self.path)

    # Call 'git add {filename}'
    import subprocess
    p = GitRepository.popen(['git', 'add', filename], cwd=self.path)
    errorCode = p.wait()

    # Interpret if the repository is locked somehow, and retry if so
    if errorCode != 0:
      if GitRepository.interpretLockFailure(p) and retries > 0:
        # Retry
        return self.add(filename, retries - 1)
      return False

    return True

  def initialCommit(self, revision="HEAD"):
    """ Returns the first commit from the given reference.

    Arguments:
      revision (str): The revision from which to start the query.

    Returns:
      str: The initial commit in the native git revision format.
    """

    revision = GitRepository.hexFromMultihash(revision)

    import subprocess
    p = GitRepository.popen(['git', 'rev-list', "--max-parents=0", revision], stdout=subprocess.PIPE, cwd=self.path)

    ret = p.stdout.read().decode('utf-8').strip()

    errorCode = p.wait()
    if errorCode != 0:
      return False

    return ret

  def head(self):
    """ Returns the current full HEAD revision.

    Returns:
      str: The current HEAD revision as a multihash.
    """

    return GitRepository.fullRevision(self.path)

  def parent(self, revision="HEAD"):
    """ Returns the parent revision of the given revision.

    Arguments:
      revision (str): The revision string to query.

    Returns:
      str: The full revision string of the parent commit as a multihash.
    """

    revision = GitRepository.hexFromMultihash(revision)

    return GitRepository.fullRevision(self.path, f"{revision}^")

  def clone(self, to, shared=False):
    """ Clones the repository to the given path.

    When `shared` is True, the new cloned repository will "share" objects via
    the alternates mechanism. That is, the new repository will be empty and will
    follow-up and unknown object queries by looking in the path of the original
    repository (which is, in this case, provided here by `to`.)

    Arguments:
      to (str): The path in which to initialize the cloned repository.
      shared (bool): When True, establish a shared clone.

    Returns:
      GitRepository: The new repository.
    """

    opts = []
    if shared:
      opts.append("-s")

    import subprocess
    p = GitRepository.popen(['git', 'clone', self.path, to, *opts], cwd=self.path, stdin=subprocess.PIPE)
    p.communicate("")

    return GitRepository(to)

  def submoduleRevisionFor(self, path):
    """ Returns the revision tag for the submodule at the given path.

    Returns:
      str: The revision intended for the given submodule path.
    """

    import subprocess
    p = GitRepository.popen(['git', 'submodule', 'status', path], stdout=subprocess.PIPE, cwd=self.path)
    ret = p.stdout.read().decode('utf-8').strip().split(' ')[0]
    if ret.startswith('+') or ret.startswith('-'):
      ret = ret[1:]

    p.wait()

    return ret

  def submoduleInit(self):
    """ Instantiates the submodule metadata for this repository.

    Returns:
      bool: True when the command completes successfully.
    """

    p = GitRepository.popen(['git', 'submodule', 'init'], cwd=self.path)
    p.wait()

    return True

  def submoduleUpdate(self):
    """ Updates the submodules within this git repository.

    Assumes the submodules were initialized via submoduleInit().

    Returns:
      bool: True when the command completes successfully.
    """

    p = GitRepository.popen(['git', 'submodule', 'update'], cwd=self.path)
    p.wait()

    return True

  def addRemote(self, name, path):
    # TODO: handle errors
    p = GitRepository.popen(['git', 'remote', 'add', name, path], cwd=self.path)
    p.wait()

    return True

  def rmRemote(self, name):
    """ Removes the given remote.

    Arguments:
      name (str): The name of the remote to remove.

    Returns:
      bool: True when the command completes successfully.
    """

    # TODO: handle errors
    p = GitRepository.popen(['git', 'remote', 'rm', name], cwd=self.path)
    p.wait()

    return True

  def fetch(self, name, tags=False, progressCallback=None):
    """ Performs a fetch of this repository of the given remote.

    Args:
      name (str): The name of the remote to fetch from.
      tags (bool): When True, this fetches all tags.
      progressCallback (function): When provided, this will be called with a
                                   dict containing the 'percent', which is a
                                   number out of 100, and a 'reason' which is
                                   a string containing the current step.

    Returns:
      bool: True when the command completes successfully.
    """

    import subprocess

    # TODO: handle errors
    command = ['git', 'fetch', name]
    if tags:
      command.append("--tags")

    stderr = None
    if progressCallback:
      command.append("--progress")
      stderr = subprocess.PIPE

    p = GitRepository.popen(command, cwd=self.path, stderr = stderr, stdin=subprocess.PIPE)

    reasons = [
        (b"Counting objects", 0.1,),
        (b"Compressing objects", 0.1,),
        (b"Receiving objects", 0.4,),
        (b"Resolving deltas", 0.4,),
    ]

    # Pull progress out of the stderr
    if progressCallback:
      buffer = b""
      while p.poll() is None:
        buffer = buffer + p.stderr.read(20)
        lines = buffer.split(b"\r")
        buffer = lines.pop()

        for line in lines:
          start = 0
          for reason in reasons:
            if reason[0] in line:
              # Find the part after the message
              line = line.split(reason[0], 1)[1]
              # Remove the ":" as well with the slice and get things up to the %
              line = line[1:].split(b",")[0].split(b"%")[0].decode('utf-8').strip()
              # Parse the percent
              percent = (int(line) * reason[1]) + start
              # Call the callback
              progressCallback({
                "reason": reason[0].decode('utf-8'),
                "percent": percent
              })
              break

            start += 100 * reason[1]

      progressCallback({
        "reason": reasons[-1][0].decode('utf-8'),
        "percent": 100
      })

    p.communicate("")

    return True

  def deleteRef(self, ref):
    """ Deletes a reference.

    Arguments:
      ref (str): The name of the reference to remove.

    Returns:
      bool: True when the command completes successfully.
    """

    p = GitRepository.popen(['git', 'update-ref', '-d', '--', ref], cwd=self.path)
    p.wait()

    return True

  def updateRef(self, ref, revision):
    """ Updates the reference.

    Arguments:
      ref (str): The name of the reference to update.
      revision (str): The multihash of the revision to associate with `ref`.

    Returns:
      bool: True when the command completes successfully.
    """

    revision = GitRepository.hexFromMultihash(revision)

    p = GitRepository.popen(['git', 'update-ref', '--', ref, revision], cwd=self.path)
    p.wait()

    return True

  def hasBranch(self, name):
    """ Returns True if the given branch exists.
    """

    p = GitRepository.popen(['git', 'show-branch', '--', name], cwd=self.path)

    errorCode = p.wait()
    return errorCode == 0

  def fetchRemoteBranches(self, name, retries=20):
    """ Performs a 'git fetch' on a branch.

    Arguments:
      name (str): The name of the branch to query.
      retries (int): The number of times to retry on lock error.

    Returns:
      bool: True when the command completes successfully.
    """

    # TODO: handle errors
    p = GitRepository.popen(['git', 'branch', '--track', name], cwd=self.path)
    p.wait()

    p = GitRepository.popen(['git', 'fetch', name], cwd=self.path)
    p.wait()

    return True

  def fetchPath(self, path, retries=20):
    """ Performs a 'git fetch' command on a path.

    Arguments:
      path (str): The path or URL to fetch from.
      retries (int): The number of times to retry on lock error.

    Returns:
      bool: True when the command completes successfully.
    """

    # Wait until the repository is unlocked
    GitRepository.wait(self.path)

    # TODO: handle errors
    p = GitRepository.popen(['git', 'fetch', '--', path], cwd=self.path)
    errorCode = p.wait()

    # Interpret if the repository is locked somehow, and retry if so
    if errorCode != 0:
      if GitRepository.interpretLockFailure(p) and retries > 0:
        # Retry
        return self.fetchPath(path, retries - 1)

    return True

  def branch(self):
    """ Returns the name of the current branch.

    Returns:
      str: The name of the current branch.
    """

    # TODO: handle errors
    import subprocess
    p = GitRepository.popen(['git', 'rev-parse', '--abbrev-ref', 'HEAD'], stdout=subprocess.PIPE, cwd=self.path)

    ret = p.stdout.read().decode('utf-8').strip()
    p.wait()

    return ret

  def addBranch(self, name, retries=20):
    """ Performs a 'git branch' command.

    Arguments:
      name (str): The name of the branch to create.
      retries (int): The number of times to retry on lock error.

    Returns:
      bool: True when the command completes successfully.
    """

    # Wait until the repository is unlocked
    GitRepository.wait(self.path)

    # TODO: handle errors
    p = GitRepository.popen(['git', 'branch', name], cwd=self.path)
    errorCode = p.wait()

    # Interpret if the repository is locked somehow, and retry if so
    if errorCode != 0:
      if GitRepository.interpretLockFailure(p) and retries > 0:
        # Retry
        return self.addBranch(name, retries - 1)

    return True

  def switchBranch(self, name, retries=20):
    """ Performs a 'git checkout' command.

    Arguments:
      name (str): The name of the branch to check out.
      retries (int): The number of times to retry on lock error.

    Returns:
      bool: True when the command completes successfully.
    """

    # Wait until the repository is unlocked
    GitRepository.wait(self.path)

    # TODO: handle errors
    p = GitRepository.popen(['git', 'checkout', name], cwd=self.path)
    errorCode = p.wait()

    # Interpret if the repository is locked somehow, and retry if so
    if errorCode != 0:
      if GitRepository.interpretLockFailure(p) and retries > 0:
        # Retry
        return self.switchBranch(name, retries - 1)

    return True

  def checkoutBranch(self, branchName, oldBranchName=None, remoteName=None, retries=20):
    """ Checks out the given branch.

    Will create a new branch when the `branchName` is not already known.

    When `remoteName` is provided, that remote is used to determine which branch
    the new branch is copied from. When `oldBranchName` is given, that is the
    name of that remote branch. When `oldBranchName` remains `None`, the name is
    presumed to be the same as given by `branchName`.

    Arguments:
      branchName (str): The name of the new branch.
      oldBranchName (str): The name of branch to checkout from.
      remoteName (str): The name of the remote to checkout from.
      retries (int): The number of times to retry on lock error.

    Returns:
      bool: True when the command completes successfully.
    """

    # TODO: handle errors
    if oldBranchName is None:
      oldBranchName = branchName

    # Wait until the repository is unlocked
    GitRepository.wait(self.path)

    if remoteName is None:
      p = GitRepository.popen(['git', 'checkout', '-b', branchName], cwd=self.path)
    else:
      p = GitRepository.popen(['git', 'checkout', '-b', branchName, '%s/%s' % (remoteName, oldBranchName)], cwd=self.path)
    errorCode = p.wait()

    # Interpret if the repository is locked somehow, and retry if so
    if errorCode != 0:
      if GitRepository.interpretLockFailure(p) and retries > 0:
        # Retry
        return self.checkoutBranch(branchName, oldBranchName, remoteName, retries - 1)

    return True

  def reset(self, revision, hard=True, retries=20):
    """ Resets the repository to the given revision.

    Arguments:
      revision (str): The revision to make the new HEAD.
      hard (bool): When True, also purges changes in the index.
      retries (int): The number of times to retry on lock error.

    Returns:
      bool: True when the command completed successfully.
    """

    revision = GitRepository.hexFromMultihash(revision)

    # Wait until the repository is unlocked
    GitRepository.wait(self.path)

    if hard:
      p = GitRepository.popen(['git', 'reset', "--hard", revision], cwd=self.path)
    else:
      p = GitRepository.popen(['git', 'reset', revision], cwd=self.path)
    errorCode = p.wait()

    # Interpret if the repository is locked somehow, and retry if so
    if errorCode != 0:
      if GitRepository.interpretLockFailure(p) and retries > 0:
        # Retry
        return self.reset(revision, hard, retries - 1)

    return True

  def stash(self):
    """ Performs a `git stash` command.

    Returns:
      bool: True when the command completed successfully.
    """

    p = GitRepository.popen(['git', 'stash'], cwd=self.path)
    p.wait()

    return True

  def stashPop(self):
    """ Performs a `git stash pop` command.

    Returns:
      bool: True when the command completed successfully.
    """

    p = GitRepository.popen(['git', 'stash', 'pop'], cwd=self.path)
    p.wait()

    return True

  def config(self):
    """ Returns a ConfigParser representing the git config if it exists.

    Returns:
      configparser.ConfigParser: The parsed configuration parameters.
    """

    import configparser # For parsing the git config

    configPath = os.path.join(self.path, ".git", "config")
    config = configparser.ConfigParser()
    if os.path.exists(configPath):
      config.read(configPath)

    return config

  def configUpdate(self, config):
    """ Writes the given ConfigParser back out to the repository.

    Arguments:
      config (configparser.ConfigParser): The configuration parameters.
    """

    configPath = os.path.join(self.path, ".git", "config")
    if os.path.exists(configPath):
      with open(configPath, 'w') as configFile:
        config.write(configFile)

  def gitmodules(self, revision="HEAD"):
    """ Returns a ConfigParser representing the .gitmodules files.

    Returns:
      configparser.ConfigParser: The parsed gitmodules metadata.
    """

    import configparser # For parsing the git config

    # TODO: this could be in any path, I believe

    f = self.retrieveFile(".gitmodules")
    gitmodules = configparser.ConfigParser()

    if f:
      gitmodules.read(f)

    return gitmodules

  def staged(self):
    """ Returns a list of staged files.

    Returns:
      list: A set of strings of file paths of changed uncommitted files.
    """

    ret = []

    import subprocess
    args = ['git', 'diff', '--name-only', '--cached']

    p = GitRepository.popen(args, stdout=subprocess.PIPE, cwd=self.path)

    for line in p.stdout:
      ret.append(line.strip())

    p.wait()
    return ret
