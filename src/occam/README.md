# Occam System Components

Occam's functionality is modular, with features being broken into a number of components.

Occam components are comprised of the following elements:

## manager.py

The `manager.py` file contains the core functionality of the component. Functions defined here are expected to be used by other managers to integrate into the system. For example, the Job Manager relies on the Object Manager for finding object information. An example manager is shown below.

```
$ cat foo/manager.py

import datetime
import json

from occam.log import loggable

from occam.accounts.manager      import AccountManager
from occam.backends.manager      import BackendManager
from occam.databases.manager     import DatabaseManager
from occam.network.manager       import NetworkManager

# This allows for calls to SomeManager.Log.error() and other loggers in the
# event something goes wrong.
@loggable
# These @uses decorators allow the manager being defined to reference the
# managers named like 'self.accounts.somefunc()'.
@uses(AccountManager)
@uses(BackendManager)
@uses(DatabaseManager)
@uses(NetworkManager)
# This manager decorator decorates the class with manager capabilities. The
# name passed to the decorator gives a short name the manager. When this class
# is referenced by a @uses decorator the short name "something" would be used
# to call functions i.e. self.some.somefunc().
@manager("something")
class SomeManager:
  def somefunc(self):
    pass
```

## commands

Advanced users as well as the web front-end communicate with the Occam daemon through commands. The commands are broken down by component.

Command modules should have the same name as the command they will be run as.

```
$ cat foo/commands/foo-me.py

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.foo.manager import FooManager

# Commands are setup via decorators like @command, @argument, and @option. Like
# managers they use the @uses decorator to gain access to managers. Usually
# commands will only @uses the manager they fall under.
@command('foo', 'foo-me',
  category      = 'Foo Management',
  documentation = "Some documentation")
@argument("foo_id", type=str, help = "The identifier for the foo.")
@option("-f", "--flag-arg", action = "store",
                            dest = "flag_arg",
                            help = ("Some argument known to the "
                                   "command as self.options.flag_arg"))
@uses(FooManager)
class FooMeCommand:
  # The do() method is where the command is entered once called from the
  # command line.
  def do(self):
    """ Performs the 'foo foo-me' command, which does foo.
    """

    # Code goes here.
```

## database.py

Database functionality is defined in `database.py`. This is where query logic and record fetching logic should go.

## plugins

If a component uses external tools or can make use of a number of configurable algorithms to accomplish a task, it gets a `plugins/` directory. In this directory are subpackages named after the plugin. For example:

```
$ ls foo/plugins
__init__.py
bar/
baz/

$ cat foo/plugins/bar/__init__.py

from occam.config import Config
from occam.log import loggable
from occam.manager import uses

from occam.foo.manager import fooer

from occam.log import loggable

from occam.accounts.manager import AccountManager


# Plugins may also write to log.
@loggable
# Should minimally use other managers.
@uses(AccountManager)
# Plugins use decorators defined in the component's manager to declare themselves.
@fooer("bar")
class BarFooer:

  # In this example we are going to pass a configuration loaded from config.yml
  # to configure this plugin.
  def __init__(self, configuration):
    self._configuration = configuration

  def someFoo(self):
    # Some plugin functionality goes here.
    pass

$ cat foo/manager.py

# ... import stuff

@manager("foo)"
class FooManager:

  # This holds onto all of the plugin drivers we have already loaded.
  drivers = {}

  # Our decorator will use this method to register drivers on load.
  @staticmethod
  def register(adapter, driverClass):
    """ Adds a new fooer driver to the possible drivers.
    """
    FooManager.drivers[adapter] = driverClass

  # This function will be called by other methods in the manager to access the
  # plugin driver logic.
  def driverFor(self, fooer):
    """ Returns an instance of a driver for the given fooer backend.

    Arguments:
      fooer (String): The name of the fooer.
    """

    if not fooer in FooManager.drivers:
      # Attempt to find the driver plugin
      import importlib

      try:
        importlib.import_module("occam.foo.plugins.%s" % (fooer))
      except ImportError as e:
        # On error, we return None
        FooManager.Log.warning(f"Failed to import fooer plugin for {fooer}: {e}")
        return None

      if not fooer in FooManager.drivers:
        # If the import does not register the driver correctly, we also error out.
        return None

    return FooManager.drivers[fooer](self.configurationFor(fooer))

  def doAFoo(self, fooerName="bar"):
    driver = self.driverFor(fooerName)
    if driver:
      driver.someFoo()

def fooer(name: str):
  """ This decorator will register a fooer of foos.
  """

  def _fooer(cls):
    FooManager.register(name, cls)
    return cls

  return _fooer
```

## records

Database schemas are defined by files in the `records/` directory of a component.

```
$ cat foo/records/foo.py

from occam.databases.manager import table

# This decorator is how tables are declared. During system initialization and
# migration, these records are used to establish the database schema.
@table('foos')
class FooRecord:
  schema = {
    "id": {
      "type": "integer",
      # The integer for this field will auto increment when created.
      "serial": True,
      # This record will be treated as a primary key.
      "primary": True
    },

    "status": {
      "type":    "string",
      "length":  8,
      "values":  ["foo", "bar", "bax", "baz"],
      "default": "foo"
    },

    "some_time_column": {
      "type": "datetime"
    },


    "job_id": {
      # Use this technique to specify foreign keys.
      "foreign": {
        "table": "jobs",
        "key": "id"
      }
    },
  }
```

## Component List

* [accounts](src/occam/accounts/README.md)
* [analysis](src/occam/analysis/README.md)
* [associations](src/occam/associations/README.md)
* [backends](src/occam/backends/README.md)
* [builds](src/occam/builds/README.md)
* [caches](src/occam/caches/README.md)
* [citations](src/occam/citations/README.md)
* [commands](src/occam/commands/README.md)
* [configurations](src/occam/configurations/README.md)
* [daemon](src/occam/daemon/README.md)
* [databases](src/occam/databases/README.md)
* [discover](src/occam/discover/README.md)
* [git](src/occam/git/README.md)
* [ingest](src/occam/ingest/README.md)
* [jobs](src/occam/jobs/README.md)
* [keys](src/occam/keys/README.md)
* [licenses](src/occam/licenses/README.md)
* [links](src/occam/links/README.md)
* [manifests](src/occam/manifests/README.md)
* [messages](src/occam/messages/README.md)
* [network](src/occam/network/README.md)
* [nodes](src/occam/nodes/README.md)
* [objects](src/occam/objects/README.md)
* [people](src/occam/people/README.md)
* [permissions](src/occam/permissions/README.md)
* [resources](src/occam/resources/README.md)
* [services](src/occam/services/README.md)
* [storage](src/occam/storage/README.md)
* [system](src/occam/system/README.md)
* [vendor](src/occam/vendor/README.md)
* [versions](src/occam/versions/README.md)
* [workflows](src/occam/workflows/README.md)
