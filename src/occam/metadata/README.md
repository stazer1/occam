# The Metadata Component

This Occam manager handles retrieval of metadata entries for artifacts within
the repository. Through plugins, several types of metadata standards can be used
within the system. Clients can expose metadata through appropriate protocols and
call upon this manager for the data representation.

## Commands

### `list`

This will list the possible metadata standards known to this system.

If given an object, this will produce a metadata listing for the resource
which gives high-level details about the metadata that can be queried.

For instance `occam metadata list` will produce something like:

```
{
  "plugins": {
    "oaipmh": {
      "urls": [
        "root": "/",
        "accept": "application/oai-pmh+xml",
        "rel": "oai-pmh"
      },
      {
        "root": "/oai-pmh",
        "accept": ["application/oai-pmh+xml", "application/xml"],
        "rel": "oai-pmh"
      ]
    }
  }
}
```

Here, the content for each plugin comes from the plugin's `metadata` function
nestled within its implementation. This is only announced if the plugin is
specified in the Occam configuration and is detected as available via the
plugin's `detect` function within its implementation.

### `view`

This will view the metadata record for the given resource in the given format.

The response is given in stages. The first line is the content type of the
resulting record as a MIME type. What follows this line is the response body,
which can be streamed directly from there.

### `http`

This will respond to a metadata HTTP request, if the metadata standard supports
such a query. It allows clients to quickly provide the metadata protocol without
building support for it directly in each client.

The required arguments are the `method` and the `path` which would comprise of
the HTTP method ("GET", "POST", etc) and the relative URL path for the resource
requested.

Then, a set of GET parameters or POST form data (query parameters) can be given
via `-q` and the POST data is passed via stdin. Any request HTTP header can be
passed via `-h` as two strings in the form `key` followed by `value`.

The response is given in stages. The first line is the HTTP status code. The
second line is a JSON-encoded set of strings representing the HTTP headers for
the response. What follows the second line is the response body, which can be
streamed directly from there.

## Plugins

### OAI-PMH

The **Open Archives Initiative Protocol for Metadata Harvesting** is a protocol
designed around interoperability among data repository systems. The protocol
designates two different systems. **Data Providers** are repositories that
provided the metadata via OAI-PHM. This allows **Service Providers** the
ability to query, harvest, mirror, and download (etc) that data. The protocol
is structured around six "verbs" that are invoked via HTTP.

#### Related Links

* [OAI-PHM 2.0 Specification](http://www.openarchives.org/OAI/openarchivesprotocol.html)
