# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log    import Log
from occam.object import Object
from occam.semver import Semver

from occam.commands.manager import command, option, argument

from occam.objects.manager  import ObjectManager
from occam.metadata.manager import MetadataManager

from occam.manager import uses

import json

@command('metadata', 'schema',
  category      = 'Metadata Management',
  documentation = "Views metadata schema.")
@argument("format", type=str)
@uses(MetadataManager)
class MetadataSchemaCommand:
  def do(self):
    Log.header("Retrieving metadata schema")

    response = self.metadata.schema(self.options.format)

    Log.pipe(json.dumps(response))
    return 0
