# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.log     import loggable
from occam.manager import manager, uses
from occam.error   import OccamError

@loggable
@manager("metadata")
class MetadataManager:
  """ This component organizes different metadata generators.
  """

  handlers = {}
  instantiated = {}

  def __init__(self):
    import importlib

    # Look at configuration at import the plugins specified
    pluginList = self.configuration.get('plugins', [])
    for pluginName in pluginList:
      importlib.import_module(pluginName)

  @staticmethod
  def register(metadataName, handlerClass):
    """ Adds a new metadata provider plugin.

    Arguments:
      metadataName (str): The short name of the metadata standard.
      handlerClass (object): The handler that implements the metadata interface.
    """

    MetadataManager.handlers[metadataName] = {'class': handlerClass}

  def handlerFor(self, metadataName):
    """ Returns an instance of a handler for the given name.

    Arguments:
      metadataName (str): The short name of the metadata standard.
    """

    if not metadataName in MetadataManager.handlers:
      raise MetadataPluginMissingError(metadataName)

    # Instantiate a metadata provider if we haven't seen it before
    if not metadataName in MetadataManager.instantiated:
      # Pull the configuration (from the 'metadata' subpath and keyed by the name)
      subConfig = self.configurationFor(metadataName)

      # Create a driver instance
      instance = MetadataManager.handlers[metadataName]['class'](subConfig)

      # If there is a problem detecting the backend, cancel
      # This will set the value in the instantiations to None
      try:
        if not instance.detect():
          instance = None
      except:
        instance = None

      # Set the instance
      MetadataManager.instantiated[metadataName] = instance

    if MetadataManager.instantiated.get(metadataName) is None:
      # The driver could not be initialized
      raise MetadataPluginInitializationError(metadataName)

    return MetadataManager.instantiated[metadataName]

  def handlerCall(self, metadataName, name, *args, **kwargs):
    handler = self.handlerFor(metadataName)

    if not hasattr(handler, name) or \
       not callable(getattr(handler, name)) or \
       not hasattr(getattr(handler, name), 'registered') or \
       not getattr(handler, name).registered:
      raise MetadataPluginUnimplementedError(metadataName, name)

    return getattr(handler, name)(*args, **kwargs)

  def configurationFor(self, metadataName):
    """ Returns the configuration for the given metadata plugin.

    This is found within the occam configuration (config.yml) under the
    "metadata" section under the given metadata plugin name.
    """

    config = self.configuration
    subConfig = config.get(metadataName, {})

    return subConfig

  def list(self):
    """ Reports the available metadata providers.
    """

    ret = {}

    for metadataName in MetadataManager.handlers.keys():
      try:
        handler = self.handlerFor(metadataName)
        ret[metadataName] = handler.info()
      except MetadataPluginInitializationError as e:
        pass

    return ret

  def metadata(self, metadataName, object, objectInfo, person):
    return self.handlerCall(metadataName, "metadata", self, object, objectInfo, person)

  def view(self, metadataName, object, objectInfo, person):
    return self.handlerCall(metadataName, "view", self, object, objectInfo, person)

  def schema(self, metadataName):
    return self.handlerCall(metadataName, "schema")

  def http(self, metadataName, path, method, query, headers, data, person):
    """ Responds to the given HTTP request.

    Arguments:
      metadataName (str): The short name representing the metadata plugin.
      path (str): The URL relative path.
      method (str): The type of HTTP request ("GET", "POST", etc)
      query (dict): The set of query parameters from a GET request or form data from a POST request.
      headers (dict): The set of strings comprising the HTTP headers for the request.
      data (bytes): The decoded request body from a POST request. None if no data was sent.
    """

    return self.handlerCall(metadataName, "http", self, path, method, query, headers, data, person)

# Error Classes

class MetadataError(OccamError):
  """ Base class for all metadata errors.
  """

class MetadataPluginError(MetadataError):
  """ Base class for all metadata plugin errors.
  """

class MetadataPluginMissingError(MetadataPluginError):
  """ Thrown when a plugin does not exist.
  """

  def __init__(self, metadataName):
    super().__init__(f"Metadata provider {metadataName} not known")

class MetadataPluginInterfaceUnknownError(MetadataPluginError):
  """ Thrown when a plugin has an interface implementation we are not aware of.
  """

  def __init__(self, metadataName, funcName):
    super().__init__(f"Metadata provider {metadataName} found but is not compatible due to the presence of unknown interface function {funcName}.")

class MetadataPluginInitializationError(MetadataPluginError):
  """ Thrown when a plugin reported a problem when initializing.
  """

  def __init__(self, metadataName):
    super().__init__(f"Metadata provider {metadataName} found but could not be initialized")

class MetadataPluginUnimplementedError(MetadataPluginError):
  """ Thrown when a method within a plugin is not implemented.
  """

  def __init__(self, metadataName, method):
    super().__init__(f"Metadata provider {metadataName} does not implement {method}")

# Plugin Registry

def metadata(name):
  """ This decorator will register the given class as a metadata provider.
  """

  def register_provider(cls):
    MetadataManager.register(name, cls)
    cls = loggable("MetadataManager")(cls)

    def init(self, subConfig):
      self.configuration = subConfig

    cls.__init__ = init

    # Determine if the interfaces provided are known
    # (Not having a complete set is not an error until one is needed since
    #  backward compatibility is desired.)
    for k, v in cls.__dict__.items():
      if hasattr(v, "registered"):
        if k not in ["view", "http", "detect", "metadata", "distill", "schema", "info"]:
          raise MetadataPluginInterfaceUnknownError(name, k)

    return cls

  return register_provider

def interface(func):
  """ This decorator assigns the given function as part of the plugin.

  Only functions tagged with this decorator can be used as part of a plugin
  implementation.
  """

  # Attach it to the plugin
  def _pluginCall(self, *args, **kwargs):
    return func(self, *args, **kwargs)

  _pluginCall.registered = True
  return _pluginCall
