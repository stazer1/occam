# OAI-PMH Occam Metadata Plugin

This plugin implements the
[OAI-PMH protocol](http://www.openarchives.org/OAI/openarchivesprotocol.html)
and several related metadata standards typical in such deployments.

This metadata standard is simply an HTTP protocol, so it emulates a record
retrieval to satisfy the `view` command just to be complete.

## Identifiers

OAI-PMH requires identifiers in the form of `oai:<repo>:<id>` where `<repo>` is
a tag that specifies the repository in some way and `<id>` is the identifier of
an artifact within the repository.

For Occam, we use the `occam` repository tag and the normal object identifiers
in the `<id>` field. This leads an object with the identifier
`QmfY1XACrB8cew7UALD7KEnPyp8dNznfEA5BPP5xFJztg6` forming an OAI tag of
`oai:occam:QmfY1XACrB8cew7UALD7KEnPyp8dNznfEA5BPP5xFJztg6`. OAI-PMH verbs only
handle such identifiers. An identifier without this scheme will cause an
`idDoesNotExist` error response.

This identifier schema is itself reported as a result of the `Identify` verb
response.

## Examples

The following are some of the user stories as implemented via occam metadata
commands.

### Querying the repository identity via `Identify`

This will retrieve the `<Identify>` record:

```
$ occam metadata http oai-pmh get /oai-pmh -q verb Identify
```

Resulting in something similar to the following response, which is based on the
server configuration of the Occam instance.

```
  200
  {"Content-Type": "text/xml"}
  <?xml version='1.0' encoding='UTF-8'?>
<OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd"><responseDate>2022-05-05T17:54:57Z</responseDate><request verb="Identify">http://localhost/oai-pmh</request><Identify><repositoryName>Occam</repositoryName><baseURL>http://localhost/oai-pmh</baseURL><protocolVersion>2.0</protocolVersion><deletedRecord>transient</deletedRecord><granularity>YYYY-MM-DDThh:mm:ssZ</granularity><compression>deflate</compression><description><oai-identifier xmlns="http://www.openarchives.org/OAI/2.0/oai-identifier" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/oai-identifier http://www.openarchives.org/OAI/2.0/oai-identifier.xsd"><scheme>oai</scheme><repositoryIdentifier>occam</repositoryIdentifier><delimiter>:</delimiter><sampleIdentifier>oai:occam:QmaPgCeGMbd5xtcbYwYTjY6BUECvHujjQPenhbH2GWektY</sampleIdentifier></oai-identifier></description></Identify></OAI-PMH>
```

### Querying metadata formats via `ListMetadataFormats`

The following will respond with the `<ListMetadataFormats>` listing of all
known formats available for any particular object.

```
$ occam metadata http oai-pmh get /oai-pmh -q verb ListMetadataFormats
```

The response is as follows:

```
  200
  {"Content-Type": "text/xml"}
  <?xml version='1.0' encoding='UTF-8'?>
<OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd"><responseDate>2022-05-05T18:13:04Z</responseDate><request verb="ListMetadataFormats">http://localhost/oai-pmh</request><ListMetadataFormats><metadataFormat><metadataPrefix>oai_dc</metadataPrefix><schema>http://www.openarchives.org/OAI/2.0/oai_dc.xsd</schema><metadataNamespace>http://www.openarchives.org/OAI/2.0/oai_dc/</metadataNamespace></metadataFormat></ListMetadataFormats></OAI-PMH>
```

And then one can specify an object via `identifier`:

```
$ occam metadata http oai-pmh get /oai-pmh -q verb ListMetadataFormats -q identifier oai:occam:QmfY1XACrB8cew7UALD7KEnPyp8dNznfEA5BPP5xFJztg6
```

The response is as follows (or the appropriate `cannotDisseminateFormat` error):

```
  200
  {"Content-Type": "text/xml"}
  <?xml version='1.0' encoding='UTF-8'?>
<OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd"><responseDate>2022-05-05T18:15:20Z</responseDate><request verb="ListMetadataFormats" identifier="oai:occam:QmfY1XACrB8cew7UALD7KEnPyp8dNznfEA5BPP5xFJztg6">http://localhost/oai-pmh</request><ListMetadataFormats><metadataFormat><metadataPrefix>oai_dc</metadataPrefix><schema>http://www.openarchives.org/OAI/2.0/oai_dc.xsd</schema><metadataNamespace>http://www.openarchives.org/OAI/2.0/oai_dc/</metadataNamespace></metadataFormat></ListMetadataFormats></OAI-PMH>
```

### Performing a `GetRecord` HTTP request

This will retrieve the `<GetRecord>` XML response for the given identifier with
the `oai_dc` Dublin Core metadata record attached:

```
$ occam metadata http oai-pmh get /oai-pmh -q verb GetRecord -q identifier oai:occam:QmfY1XACrB8cew7UALD7KEnPyp8dNznfEA5BPP5xFJztg6 -q metadataPrefix oai_dc
```

Results in:

```
  200
  {"Content-Type": "text/xml"}
  <?xml version='1.0' encoding='UTF-8'?>
<OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd"><responseDate>2022-05-05T17:48:04Z</responseDate><request>http://localhost/oai-pmh</request><GetRecord> ... </GetRecord></OAI-PMH>
```

### Invoking the basic `view`

This will also retrieve the `<GetRecord>` XML response for the given identifier
with the `oai_dc` Dublin Core metadata record attached since the `view` command
emulates such a request. It will result in the same XML output of the `http`
command without the HTTP response headers. It will report the content type as
"text/xml":

```
$ occam metadata view oai-pmh QmfY1XACrB8cew7UALD7KEnPyp8dNznfEA5BPP5xFJztg6
```

Results in:

```
  text/xml
  <?xml version='1.0' encoding='UTF-8'?>
<OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd"><responseDate>2022-05-05T17:48:04Z</responseDate><request>http://localhost/oai-pmh</request><GetRecord> ... </GetRecord></OAI-PMH>
```
