# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.metadata.manager import metadata, interface

from occam.objects.manager  import ObjectManager
from occam.versions.manager import VersionManager

from occam.manager import uses

import datetime

@metadata('oai-pmh')
@uses(ObjectManager)
@uses(VersionManager)
class OAIPMHMetadataPlugin:
  """ Provides the OAI-PMH metadata view for objects in the system.
  """

  @interface
  def detect(self):
    """ Returns True when this plugin is available.
    """

    # Heck yeah, we are!
    return True

  @interface
  def info(self):
    """ Returns metadata about the metadata plugin.
    """

    return {
      "short": "OAI-PMH",
      "name": "Open Archives Initiative Protocol for Metadata Harvesting",
      "summary": "The Open Archives Initiative Protocol for Metadata Harvesting provides an application-independent interoperability framework based on metadata harvesting.",
      "urls": [
        {
          "root": "/",
          "accept": ["application/oai-pmh+xml"],
          "rel": "oai-pmh"
        },
        {
          "root": "/oai-pmh",
          "accept": ["application/oai-pmh+xml", "application/xml", "text/xml"],
          "rel": "oai-pmh"
        }
      ]
    }

  @interface
  def http(self, manager, path, method, query, headers, data, person):
    """ Responds to the given HTTP request.

    Arguments:
      manager (MetadataManager): The instantiated metadata manager.
      path (str): The URL relative path.
      method (str): The type of HTTP request ("GET", "POST", etc)
      query (dict): The set of query parameters from a GET request or form data from a POST request.
      headers (dict): The set of strings comprising the HTTP headers for the request.
      data (bytes): The decoded request body from a POST request. None if no data was sent.
      person (Person): The actor requesting the entry, if any.

    Returns:
      list: A tuple consisting of [status (number), headers (dict), and body (str)].
    """

    # By default, we respond with an empty 200
    status = 200
    newHeaders = {}
    body = ""

    # But it will always contain XML at some point
    newHeaders["Content-Type"] = "text/xml"

    # Get the OAI-PMH verb to perform
    verb = query.get('verb')

    # Start building the XML response
    from xml.etree import ElementTree
    root = self.createRoot(path = path,
                           query = query,
                           headers = headers);

    # Perform the verb

    if verb == "GetRecord":
      self.handleGetRecord(manager, root, query, person)
    elif verb == "ListRecords":
      self.handleListRecords(root, query, person)
    elif verb == "ListIdentifiers":
      self.handleListIdentifiers(root, query, person)
    elif verb == "Identify":
      self.handleIdentify(root, query, person)
    elif verb == "ListMetadataFormats":
      self.handleListMetadataFormats(root, query, person)
    elif verb == "ListSets":
      self.handleListSets(root, query, person)
    else: # Verb unknown; Report error
      error = ElementTree.SubElement(root, "error", code = "badVerb")
      error.text = "Illegal OAI verb"

    body = ElementTree.tostring(root, encoding = 'UTF-8',
                                      xml_declaration = True)

    # Return status, headers, and body
    return [status, newHeaders, body]

  @interface
  def metadata(self, manager, object, objectInfo, person):
    pass

  @interface
  def view(self, manager, object, objectInfo, person):
    """ Generates the metadata document for the given object.

    For OAI-PMH, this returns the result of a GetRecord query for the given
    object with the object id as `identifier` and `oai_dc` as the
    `metadataPrefix`.

    This is because the Dublin Core metadata standard is always available.
    Therefore, the `view` action gives at least a general overview of any
    particular object. The real OAI-PMH functionality is exposed via the `http`
    interfaces through a compatible web client.

    Arguments:
      manager (MetadataManager): The instantiated metadata manager.
      object (Object): The artifact in question.
      objectInfo (ObjectInfo): The artifacts main Occam metadata.
      person (Person): The actor making the request.

    Returns:
      str: The XML document serving as a response to the metadata request.
    """

    from xml.etree import ElementTree

    root = self.createRoot()
    self.handleGetRecord(manager, root, {
      "identifier": f"oai:occam:{object.id}",
      "metadataPrefix": "oai_dc"
    }, person)

    return [
      "text/xml",
      ElementTree.tostring(root, encoding = 'UTF-8',
                                 xml_declaration = True)
    ]

  def createRoot(self, path = None, query = None, headers = None):
    """ Creates the XML document root for an OAI-PMH response.

    Returns:
      xml.etree.ElementTree.Element: The element serving as the document root.
    """

    if path is None:
      path = "/oai-pmh"

    if query is None:
      query = {}

    if headers is None:
      headers = {}

    from xml.etree import ElementTree

    namespaces = {
        "xmlns": "http://www.openarchives.org/OAI/2.0/",
        "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
        "xsi:schemaLocation": "http://www.openarchives.org/OAI/2.0/ http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd",
    }

    # Create the root of the response
    root = ElementTree.Element("OAI-PMH", **namespaces)

    # Add the response date
    responseDate = ElementTree.SubElement(root, "responseDate")
    responseDate.text = datetime.datetime.utcnow().replace(microsecond=0).isoformat() + "Z"

    # Add the request
    request = ElementTree.SubElement(root, "request", **query)
    request.text = headers.get("SCHEME", "http") + "://" + headers.get("HOST", "localhost") + path

    return root

  def handleGetRecord(self, manager, root, query, person):
    """ Modifies the given document with a response to a GetRecord verb.

    Arguments:
      root (xml.etree.ElementTree.SubElement): The base element to which this appends.
      query (dict): The query parameters to the request.
      person (Person): The actor making the request, if any.

    Returns:
      bool: Returns True upon creating a result and False upon creating <error>.
    """

    from xml.etree import ElementTree

    if not query.get('identifier'):
      # The identifier is required
      error = ElementTree.SubElement(root, "error", code = "badArgument")
      error.text = f"identifier is a required argument"
      return False

    # Get the object, if we need it here
    # We can look for 'metadata' entries in the object itself that matches
    # a metadata standard.
    id = query['identifier']
    if id.startswith("oai:occam:"):
      id = id[10:]
    else:
      # Invalid identifier, currently; Report an error
      error = ElementTree.SubElement(root, "error", code = "idDoesNotExist")
      error.text = f"{id} has an invalid structure and is not a valid identifier"
      return False

    object = self.objects.retrieve(id, person = person)

    if object is None:
      # Report an error instead
      error = ElementTree.SubElement(root, "error", code = "idDoesNotExist")
      error.text = f"{id} has the structure of a valid LOC identifier, but it maps to no known item"
      return False

    # Get the prefix requested
    prefix = query.get('metadataPrefix')
    if not prefix:
      # This is an error. This field is required.
      error = ElementTree.SubElement(root, "error", code = "badArgument")
      error.text = f"metadataPrefix is a required argument"
      return False

    # Check that the requested metadata is possible
    objectInfo = self.objects.infoFor(object)
    valid = self.metadataInfoFor(manager, prefix, object, objectInfo, person)
    if not valid:
      error = ElementTree.SubElement(root, "error", code = "cannotDisseminateFormat")
      error.text = f"the {prefix} format cannot be disseminated for {id}"
      return False

    base = ElementTree.SubElement(root, "GetRecord")
    record = ElementTree.SubElement(base, "record")

    # Craft the record <header>
    header = self.headerFor(record, object)

    # Then the <metadata> according to the format requested
    metadata = self.metadataFor(manager, record, prefix, object, objectInfo, person)

    # And finally the <about> section according to the format requested
    about = ElementTree.SubElement(record, "about")

    return True

  def handleListRecords(self, manager, root, query, person):
    """ Modifies the given document with a response to a ListRecords verb.

    Arguments:
      root (xml.etree.ElementTree.SubElement): The base element to which this appends.
      query (dict): The query parameters to the request.
      person (Person): The actor making the request, if any.

    Returns:
      bool: Returns True upon creating a result and False upon creating <error>.
    """

    from xml.etree import ElementTree

    # This is a set of <record> elements.
    # If 'set' is specified, we will look everything up by tag.
    # At any rate, `from` and `until` specify utc iso8601 dates and filter the
    # object listing.

    # Get the prefix requested
    prefix = query.get('metadataPrefix')
    if not prefix:
      # This is an error. This field is required.
      error = ElementTree.SubElement(root, "error", code = "badArgument")
      error.text = f"metadataPrefix is a required argument"
      return False

    # It is an error if we do not understand the metadata prefix
    if prefix not in self.standards():
      error = ElementTree.SubElement(root, "error", code = "cannotDisseminateFormat")
      error.text = f"{prefix} is unknown to this repository"
      return False

    objects = []

    # Optionally, we can filter by tag ('set' in OAI-PMH terms)
    tag = query.get('set')

    # Optionally, a `resumptionToken` value allows a list to be continued
    cursor = query.get('resumptionToken')

    # TODO: Get the list of objects (or a subset)
    objects = []

    if not objects:
      # It is an error if the filtering results in an empty list
      error = ElementTree.SubElement(root, "error", code = "noRecordsMatch")
      error.text = f"the filters result in an empty list"
      return False

    # Now we output the set...
    # (can be empty, it seems, if all objects in result cannot disseminate the
    #  requested prefix)
    base = ElementTree.SubElement(root, "ListRecords")

    # For every object, we will append its header if it has the metadata
    # format requested.
    for object in objects:
      objectInfo = self.objects.infoFor(object)

      # Can this object respond to this metadata prefix?
      valid = self.metadataInfoFor(maanger, prefix, object, objectInfo, person)

      # If it can, it will add the header
      if valid:
        # Add <record> to the <ListRecords> element
        record = ElementTree.SubElement(base, "record")
        # Add <header> to the <record> element
        self.headerFor(record, object)
        # Add <metadata> to the <record> element
        self.metadataFor(manager, record, prefix, object, objectInfo, person)
        # Add <about> to the <record> element
        self.aboutFor(record, prefix, object)

    return True

  def handleListIdentifiers(self, root, query, person):
    """ Modifies the given document with a response to a ListIdentifiers verb.

    Arguments:
      root (xml.etree.ElementTree.SubElement): The base element to which this appends.
      query (dict): The query parameters to the request.
      person (Person): The actor making the request, if any.

    Returns:
      bool: Returns True upon creating a result and False upon creating <error>.
    """

    from xml.etree import ElementTree

    # This is like ListRecords that it returns <record> elements, but with
    # only <header> sections

    # Get the prefix requested
    prefix = query.get('metadataPrefix')
    if not prefix:
      # This is an error. This field is required.
      error = ElementTree.SubElement(root, "error", code = "badArgument")
      error.text = f"metadataPrefix is a required argument"
      return False

    # It is an error if we do not understand the metadata prefix
    if prefix not in self.standards():
      error = ElementTree.SubElement(root, "error", code = "cannotDisseminateFormat")
      error.text = f"{prefix} is unknown to this repository"
      return False

    objects = []

    # Optionally, we can filter by tag ('set' in OAI-PMH terms)
    tag = query.get('set')

    # Optionally, a `resumptionToken` value allows a list to be continued
    cursor = query.get('resumptionToken')

    # TODO: Get the list of objects (or a subset)
    objects = []

    if not objects:
      # It is an error if the filtering results in an empty list
      error = ElementTree.SubElement(root, "error", code = "noRecordsMatch")
      error.text = f"the filters result in an empty list"
      return False

    # Now we output the set...
    # (can be empty, it seems, if all objects in result cannot disseminate the
    #  requested prefix)
    base = ElementTree.SubElement(root, "ListIdentifiers")

    # For every object, we will append its header if it has the metadata
    # format requested.
    for object in objects:
      # Can this object respond to this metadata prefix?
      valid = self.metadataInfoFor(prefix, object)

      # If it can, it will add the header
      if valid:
        # Add <header> to the <ListIdentifiers> element
        self.headerFor(base, object)

    return True

  def handleIdentify(self, root, query, person):
    """ Modifies the given document with a response to a Identify verb.

    Arguments:
      root (xml.etree.ElementTree.SubElement): The base element to which this appends.
      query (dict): The query parameters to the request.
      person (Person): The actor making the request, if any.

    Returns:
      bool: Returns True upon creating a result and False upon creating <error>.
    """

    from xml.etree import ElementTree

    # We need the `<request>` from the root to get the URL
    request = list(root.iter('request'))[0]

    # Just announces the repository
    # TODO: get these from the SystemManager
    base = ElementTree.SubElement(root, "Identify")
    name = ElementTree.SubElement(base, "repositoryName")
    name.text = "Occam"

    baseURL = ElementTree.SubElement(base, "baseURL")
    baseURL.text = request.text

    protocolVersion = ElementTree.SubElement(base, "protocolVersion")
    protocolVersion.text = "2.0"

    # Marks this as maybe having deleted records but not strictly
    deletedRecord = ElementTree.SubElement(base, "deletedRecord")
    deletedRecord.text = "transient"

    granularity = ElementTree.SubElement(base, "granularity")
    granularity.text = "YYYY-MM-DDThh:mm:ssZ"

    compression = ElementTree.SubElement(base, "compression")
    compression.text = "deflate"

    # What follows is a set of <description> containers for different
    # repository schemata

    # We will announce our identifier schema
    # <oai-identifier>
    description = ElementTree.SubElement(base, "description")
    oaiNamespaces = {
        "xmlns": "http://www.openarchives.org/OAI/2.0/oai-identifier",
        "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
        "xsi:schemaLocation": "http://www.openarchives.org/OAI/2.0/oai-identifier http://www.openarchives.org/OAI/2.0/oai-identifier.xsd"
    }
    oaiIdentifier = ElementTree.SubElement(description, "oai-identifier", **oaiNamespaces)
    scheme = ElementTree.SubElement(oaiIdentifier, "scheme")
    scheme.text = "oai"
    repositoryIdentifier = ElementTree.SubElement(oaiIdentifier, "repositoryIdentifier")
    repositoryIdentifier.text = "occam"
    delimiter = ElementTree.SubElement(oaiIdentifier, "delimiter")
    delimiter.text = ":"
    sampleIdentifier = ElementTree.SubElement(oaiIdentifier, "sampleIdentifier")
    sampleIdentifier.text = "oai:occam:QmaPgCeGMbd5xtcbYwYTjY6BUECvHujjQPenhbH2GWektY"

    # TODO: We can use a <friends> description block to point to neighborly
    # nodes in the federation!

  def handleListMetadataFormats(self, root, query, person):
    """ Modifies the given document with a response to a ListMetadataFormats verb.

    Arguments:
      root (xml.etree.ElementTree.SubElement): The base element to which this appends.
      query (dict): The query parameters to the request.
      person (Person): The actor making the request, if any.

    Returns:
      bool: Returns True upon creating a result and False upon creating <error>.
    """

    from xml.etree import ElementTree

    # An object is not required
    object = None

    # Reports the possible metadata formats for the system or the given object
    # if one was requested via the identifier
    if query.get('identifier'):
      # Get the object, if we need it here
      # We can look for 'metadata' entries in the object itself that matches
      # a metadata standard.
      id = query['identifier']
      if id.startswith("oai:occam:"):
        id = id[10:]
      else:
        # Invalid identifier, currently; Report an error
        error = ElementTree.SubElement(root, "error", code = "idDoesNotExist")
        error.text = f"{id} has an invalid structure and is not a valid identifier"
        return False

      object = self.objects.retrieve(id, person = person)

      if object is None:
        # Report an error instead
        error = ElementTree.SubElement(root, "error", code = "idDoesNotExist")
        error.text = f"{id} has the structure of a valid LOC identifier, but it maps to no known item"
        return False

    # This responds with a set of <metadataFormat> elements
    base = ElementTree.SubElement(root, "ListMetadataFormats")
    
    # Get the prefixes available for this object
    prefixes = self.prefixesFor(object)

    for prefix, standard in self.standards().items():
      # If this metadata standard is selected, add it to the document
      # (ignoring the data)
      if prefix in prefixes:
        record = ElementTree.SubElement(base, "metadataFormat")

        for k, v in standard.items():
          recordItem = ElementTree.SubElement(record, k)
          recordItem.text = v

    return True

  def handleListSets(self, root, query, person):
    """ Modifies the given document with a response to a ListSets verb.

    Arguments:
      root (xml.etree.ElementTree.SubElement): The base element to which this appends.
      query (dict): The query parameters to the request.
      person (Person): The actor making the request, if any.

    Returns:
      bool: Returns True upon creating a result and False upon creating <error>.
    """

    from xml.etree import ElementTree

    base = ElementTree.SubElement(root, "ListSets")

    # Lists the known tags in the system
    tags = self.objects.searchTags()

    # In Occam, sets are their full names and have no shorthand
    # Therefore, setSpec and setName are the same.

    # We may in the future support prefixed tags that point to tags within
    # metadata standards using a `{prefix}:{tag}` standard.
    for tag in tags:
      setElement = ElementTree.SubElement(base, "set")
      setSpec = ElementTree.SubElement(setElement, "setSpec")
      setSpec.text = tag

      # TODO: If the setSpec is prefixed, we can ask the metadata standard
      #       to describe the set and add a <setDescription> record as well
      #       as getting a formal <setName> that differs from <setSpec>

      # TODO: It is an open question, with regard to the above TODO, if such
      #       a tag results in a different <setSpec>, but keeping the prefix
      #       avoids a set name collision and disambiguates downstream when
      #       objects are harvested. OAI-PMH does not tag any "set" with a
      #       metadata prefix, which seems like an oversight for this reason.
      setName = ElementTree.SubElement(setElement, "setName")
      setName.text = tag

    return True

  def standards(self):
    """ Reports the known metadata standards that can be reported via OAI-PMH.

    The resulting dictionary has keys related to the `metadataPrefix` requested
    by any of the OAI-PMH verbs. The values here are themselves dictionaries
    containing the metadata information generally reported via the
    `ListMetadataFormats` verb. This includes values for `metadataPrefix`,
    `schema`, and the `metadataNamespace`.

    This function lists *all* known standards. It is up to the individual verbs
    to filter and use the standards either requested by the action or available
    to the requested object.

    Returns:
      dict: A dictionary keyed by a metadata prefix containing metadata info.
    """

    return {
      "oai_dc": {
        "metadataPrefix": "oai_dc",
        "schema": "http://www.openarchives.org/OAI/2.0/oai_dc.xsd",
        "metadataNamespace": "http://www.openarchives.org/OAI/2.0/oai_dc/"
      }
    }

  def prefixesFor(self, object):
    """ Returns the prefixes available for this object.
    """

    # Get the known metadata standards we can return via GetRecord
    standards = self.standards()

    # The repository-wide prefixes to start
    prefixes = ["oai_dc"]

    # Check the object for other standards if it has any embedded metadata
    if object:
      info = self.objects.infoFor(object) or {}

      for k, v in info.get('metadata', {}).items():
        if k in standards and k not in prefixes:
          prefixes.append(k)

    return prefixes

  def dublinCoreFor(self, manager, object, objectInfo, person):
    """ Returns the Dublic Core (oai_dc) metadata for the given object.

    Arguments:
      object (Object): The resolved object to generate metadata for.

    Returns:
      dict: The namespaces to use on each root element of the following dict.
      dict: The appropriate metadata.
    """

    # Negotiate with the Dublin Core plugin for the metadata
    core = manager.metadata("dublin-core", object, objectInfo, person)

    # Prepare output with XML namespaces
    dc = {}
    for k, v in core.items():
      dc[f"dc:{k[0:1].lower()}{k[1:]}"] = v

    # Gather XML namespaces (dc -> oai_dc)
    ns = {
      "xmlns:oai_dc": "http://www.openarchives.org/OAI/2.0/oai_dc/",
      "xmlns:dc": "http://purl.org/dc/elements/1.1/",
      "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
      "xsi:schemaLocation": "http://www.openarchives.org/OAI/2.0/oai_dc/ http://www.openarchives.org/OAI/2.0/oai_dc.xsd"
    }

    # It gets crammed into a <oai_dc:dc/> block
    ret = {
      "oai_dc:dc": dc
    }

    return ns, ret

  def metadataInfoFor(self, manager, prefix, object, objectInfo, person):
    """ Returns the metadata information for the given object and standard.

    Returns:
      dict: The metadata information or None if the object has none.
    """

    # Assume we cannot generate metadata for this object
    ret = None
    
    # Either the metadata is attached to the object itself
    # Or we negotiate the metadata standard with the normal metadata
    if prefix == "oai_dc":
      ret = self.dublinCoreFor(manager, object, objectInfo, person)

    return ret

  def headerFor(self, record, object):
    """ Crafts the <header> element for the given object.

    It crafts this by constructing it as a child element of the given `record`
    base element. This `record`, and its overall document, will be modified as
    a side effect of this method.

    Arguments:
      record (xml.etree.ElementTree.SubElement): The base <record> element.
      object (Object): The resolved object to craft the header for.

    Returns:
      xml.etree.ElementTree.SubElement: The prepared <header> element.
    """

    from xml.etree import ElementTree

    # Adding the `status = 'deleted'` attribute is useful for tombstones
    header = ElementTree.SubElement(record, "header")
    identifier = ElementTree.SubElement(header, "identifier")
    identifier.text = f"oai:occam:{object.id}"
    datestamp = ElementTree.SubElement(header, "datestamp")
    datestamp.text = datetime.datetime.utcnow().replace(microsecond=0).isoformat() + "Z"

    # We can add one or more setSpec elements for the tags in the object
    info = self.objects.infoFor(object)
    for tag in info.get('tags', []):
      setSpec = ElementTree.SubElement(header, "setSpec")
      setSpec.text = tag

    return header

  def metadataFor(self, manager, record, prefix, object, objectInfo, person):
    """ Crafts the <metadata> element for the given object and standard.

    It crafts this by constructing it as a child element of the given `record`
    base element. This `record`, and its overall document, will be modified as
    a side effect of this method.

    Arguments:
      record (xml.etree.ElementTree.SubElement): The base <record> element.
      prefix (str): The metadata prefix to use.
      object (Object): The resolved object to craft the metadata for.

    Returns:
      xml.etree.ElementTree.SubElement: The prepared <metadata> element.
    """

    from xml.etree import ElementTree

    metadata = ElementTree.SubElement(record, "metadata")

    ns, info = self.metadataInfoFor(manager, prefix, object, objectInfo, person)

    # Add all root items
    for rootName, subitems in info.items():
      item = ElementTree.SubElement(metadata, rootName, **ns)

      def dictToXML(item, subitems):
        # And subitems
        for k, v in subitems.items():
          if not isinstance(v, list):
            v = [v]

          # Add subitems
          for entry in v:
            subitem = ElementTree.SubElement(item, k)
            if isinstance(entry, dict):
              dictToXML(subitem, entry)
            else:
              subitem.text = entry

      dictToXML(item, subitems)

    return metadata

  def aboutFor(self, record, prefix, object):
    """ Crafts the <about> element for the given object and standard.

    It crafts this by constructing it as a child element of the given `record`
    base element. This `record`, and its overall document, will be modified as
    a side effect of this method.

    Arguments:
      record (xml.etree.ElementTree.SubElement): The base <record> element.
      prefix (str): The metadata prefix to use.
      object (Object): The resolved object to craft the about section for.

    Returns:
      xml.etree.ElementTree.SubElement: The prepared <about> element.
    """

    from xml.etree import ElementTree

    about = ElementTree.SubElement(record, "about")

    return about
