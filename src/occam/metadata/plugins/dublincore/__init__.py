# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.metadata.manager import metadata, interface

from occam.objects.manager  import ObjectManager
from occam.versions.manager import VersionManager

from occam.manager import uses

import datetime

@metadata('dublin-core')
@uses(ObjectManager)
@uses(VersionManager)
class DublinCoreMetadataPlugin:
  """ Provides the Dublin Core metadata view for objects in the system.
  """

  @interface
  def detect(self):
    """ Returns True when this plugin is available.
    """

    # Heck yeah, we are!
    return True

  @interface
  def info(self):
    """ Returns metadata about the metadata plugin.
    """

    return {
      "short": "Dublin Core",
      "name": "Dublin Core",
      "summary": ""
    }

  @interface
  def http(self, manager, path, method, query, headers, data, person):
    pass

  @interface
  def view(self, manager, object, objectInfo, person):
    """ Generates the metadata document for the given object.

    Arguments:
      manager (MetadataManager): The instantiated metadata manager.
      object (Object): The artifact in question.
      objectInfo (ObjectInfo): The artifacts main Occam metadata.
      person (Person): The actor making the request.

    Returns:
      str: The XML document serving as a response to the metadata request.
    """

    metadata = self.metadata(manager, object, objectInfo, person)

    # Generates XML
    from xml.etree import ElementTree

    namespaces = {
        "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
        "xmlns:dc": "http://purl.org/dc/elements/1.1/"
    }

    # Create the root of the response
    root = ElementTree.Element("metadata", **namespaces)

    # Add all fields
    def dictToXML(root, metadata):
      for k, v in metadata.items():
        if not isinstance(v, list):
          v = [v]

        # Add subitems
        for entry in v:
          subitem = ElementTree.SubElement(root, f"dc:{k[0:1].lower()}{k[1:]}")
          if isinstance(entry, dict):
            dictToXML(subitem, entry)
          else:
            subitem.text = entry

    dictToXML(root, metadata)

    return [
      "text/xml",
      ElementTree.tostring(root, encoding = 'UTF-8',
                                 xml_declaration = True)
    ]

  def appendRequires(self, dc, object, objectInfo, person):
    dependencies = objectInfo.get('dependencies', [])
    dependencies += objectInfo.get('run', {}).get('dependencies', [])
    dependencies += objectInfo.get('build', {}).get('dependencies', [])

    for dependency in dependencies:
      dc['requires'] = dc.get('requires', [])
      dc['requires'].append(f"occam://{dependency.get('id')}")

    return dc

  def appendRelated(self, dc, object, objectInfo, person):
    for related in objectInfo.get('related', []):
      if 'object' in related and 'id' in related['object']:
        dc['relation'] = dc.get('relation', [])
        if 'type' in related['object']:
          dc['relation'].append({
            'identifier': [f"occam://{related['object']['id']}"],
            'type': [related['object']['type']],
            'title': [related['object'].get('name', related['object'].get('summary', related.get('relation', 'unknown')))],
            'subject': related['relation']
          })

          if 'subtype' in related['object']:
            # Append subtype as a type
            subtypes = related['object']['subtype']

            if not isinstance(subtypes, list):
              subtypes = [subtypes]

            dc['relation'][-1]['type'].extend(subtypes)
        else:
          dc['relation'].append(f"occam://{related['object']['id']}")

    return dc
  
  def populateGeneric(self, dc, object, objectInfo, person):
    """ Returns a representation of the generic occam metadata.
    """

    # We do not even attempt to generate a "Subject" section.
    # We push 'organization' into 'Creator' instead of publisher because it
    # is less clear if it is appropriate... Occam has a specific 'publisher'
    # tag for explicit cases of a publishing entity whereas 'organization' is
    # meant as a creation entity.
    if objectInfo.get('publisher'):
      dc["publisher"] = [objectInfo['publisher']]

    if objectInfo.get('published'):
      dc["date"] = [objectInfo['published']]

    if objectInfo.get('language'):
      dc["language"] = [objectInfo['language']]

    if objectInfo.get('license'):
      licenses = objectInfo['license']
      if not isinstance(licenses, list):
        licenses = [licenses]

      dc["rights"] = []

      for license in licenses:
        if isinstance(license, dict):
          dc["rights"].append(license.get('name'))

          # Append a reference to the file indicated by the license tag
          if 'file' in license:
            dc["rights"].append(f"occam://{object.id}/{object.revision}/{license.get('file')}")

          # Or the whole URL, if specified
          if 'url' in license:
            dc["rights"].append(license['url'])
        else:
          dc["rights"].append(license)

    if objectInfo.get('name'):
      dc["title"] = [objectInfo['name']]

    if objectInfo.get('type'):
      dc["type"] = [objectInfo['type']]

    if objectInfo.get('summary'):
      dc["description"] = [objectInfo['summary']]

    if isinstance(objectInfo.get('authors'), list):
      for author in objectInfo.get('authors'):
        dc["creator"] = dc.get('creator') or []
        dc["creator"].append(author)

    if objectInfo.get('organization'):
      dc["creator"] = dc.get('creator') or []
      dc["creator"].append(objectInfo['organization'])

    if isinstance(objectInfo.get('collaborators'), list):
      for collaborator in objectInfo.get('collaborators'):
        dc["contributor"] = dc.get('contributor') or []
        dc["contributor"].append(collaborator)

    return dc

  @interface
  def metadata(self, manager, object, objectInfo, person):
    """ Returns the appropriate metadata for the given object.

    Arguments:
      manager (MetadataManager): The instantiated metadata manager.
    """

    dc = {}

    if isinstance(objectInfo.get('metadata', {}).get("dublin-core"), dict):
      # Reform the keys into appropriate dc forms (first letter lowercase)

      # The schema will generally use uppercase letters as is used in the
      # dublin core specification even if they become lowercase in XML
      # representations such as the type this plugin generates.
      for k, v in objectInfo['metadata']["dublin-core"].items():
        if k[0] == '_':
          # This is a directive
          if k == "_addIdentifier" and v:
            if 'identifier' not in dc:
              dc['identifier'] = []

            dc["identifier"].append(f"occam://{object.id}")
          elif k == "_addDependencies" and v:
            self.appendRequires(dc, object, objectInfo, person)
          elif k == "_addRelations" and v:
            self.appendRelated(dc, object, objectInfo, person)
          elif k == "_addGeneric" and v:
            self.populateGeneric(dc, object, objectInfo, person)
          else:
            # We ignore any directive we don't understand
            pass
        else:
          dc[f"{k[0:1].lower()}{k[1:]}"] = v
    else:
      # Pull out fields from the object itself
      self.populateGeneric(dc, object, objectInfo, person)

      dc["identifier"] = [f"occam://{object.id}"]

      self.appendRequires(dc, object, objectInfo, person)
      self.appendRelated(dc, object, objectInfo, person)

    return dc

  @interface
  def distill(self, object, objectInfo, person):
    """ Returns a normalized version of the metadata.
    """

    return {}

  @interface
  def schema(self):
    """ Generates the schema used to edit such metadata.

    Returns:
      dict: The structure of the metadata as an Occam configuration schema.
    """

    # The descriptions here are largely written by Diane Hillmann and found in
    # official capacity at:
    # https://www.dublincore.org/specifications/dublin-core/usageguide/elements/
    return {
      "_addGeneric": {
        "label": "Automatically Add Metadata",
        "type": "boolean",
        "default": True,
        "description": "When set, this will append any normal Occam metadata within the artifact as the appropriate entries. Any added manually will replace the automatically assigned values."
      },
      "_addIdentifier": {
        "label": "Automatically Add Identifier",
        "type": "boolean",
        "default": True,
        "description": "When set, this will append the Occam identifier as a Dublin Core identifier."
      },
      "_addDependencies": {
        "label": "Automatically Add Dependencies",
        "type": "boolean",
        "default": True,
        "description": "When set, this will append any Occam dependencies within the artifact as 'Requires' entries."
      },
      "_addRelations": {
        "label": "Automatically Add Related",
        "type": "boolean",
        "default": True,
        "description": "When set, this will append any Occam related objects within the artifact as 'Relation' entries."
      },
      "Title": {
        "description": "The name given to the resource. Typically, a Title will be a name by which the resource is formally known.\n\n**Guidelines for creation of content**: If in doubt about what constitutes the title, repeat the Title element and include the variants in second and subsequent Title iterations. If the item is in HTML, view the source document and make sure that the title identified in the title header (if any) is also included as a Title.\n\n**Examples**:\n\n> Title=\"A Pilot's Guide to Aircraft Insurance\"\n> Title=\"The Sound of Music\"\n> Title=\"Green on Greens\"\n> Title=\"AOPA's Tips on Buying Used Aircraft\"",
        "type": "array",
        "element": "string"
      },
      "Subject": {
        "description": "The topic of the content of the resource. Typically, a Subject will be expressed as keywords or key phrases or classification codes that describe the topic of the resource. Recommended best practice is to select a value from a controlled vocabulary or formal classification scheme.\n\n**Guidelines for creation of content**: Select subject keywords from the Title or Description information, or from within a text resource. If the subject of the item is a person or an organization, use the same form of the name as you would if the person or organization were a Creator or Contributor.\n\nIn general, choose the most significant and unique words for keywords, avoiding those too general to describe a particular item. Subject might include classification data if it is available (for example, Library of Congress Classification Numbers or Dewey Decimal numbers) or controlled vocabularies (such as Medical Subject Headings or Art and Architecture Thesaurus descriptors) as well as keywords.\n\nWhen including terms from multiple vocabularies, use separate element iterations. If multiple vocabulary terms or keywords are used, either separate terms with semi-colons or use separate iterations of the Subject element.\n\n**Examples**:\n\n> Subject=\"Aircraft leasing and renting\"\n> Subject=\"Dogs\"\n> Subject=\"Olympic skiing\"\n> Subject=\"Street, Picabo\"",
        "type": "array",
        "element": "string"
      },
      "Description": {
        "description": "An account of the content of the resource. Description may include but is not limited to: an abstract, table of contents, reference to a graphical representation of content or a free-text account of the content.\n\n**Guidelines for creation of content**: Since the Description field is a potentially rich source of indexable terms, care should be taken to provide this element when possible. Best practice recommendation for this element is to use full sentences, as description is often used to present information to users to assist in their selection of appropriate resources from a set of search results.\n\nDescriptive information can be copied or automatically extracted from the item if there is no abstract or other structured description available. Although the source of the description may be a web page or other structured text with presentation tags, it is generally not good practice to include HTML or other structural tags within the Description element. Applications vary considerably in their ability to interpret such tags, and their inclusion may negatively affect the interoperability of the metadata.\n\n**Examples**:\n\n> Description=\"Illustrated guide to airport markings and lighting signals, with particular reference to SMGCS (Surface Movement Guidance and Control System) for airports with low visibility conditions.\"\n> Description=\"Teachers Domain is a multimedia library for K-12 science educators, developed by WGBH through funding from the National Science Foundation as part of its National Science Digital Library initiative. The site offers a wealth of classroom-ready instructional resources, as well as online professional development materials and a set of tools which allows teachers to manage, annotate, and share the materials they use in classroom teaching.\"",
        "type": "array",
        "element": "string"
      },
      "Type": {
        "description": "The nature or genre of the content of the resource. Type includes terms describing general categories, functions, genres, or aggregation levels for content. Recommended best practice is to select a value from a controlled vocabulary (for example, the DCMIType vocabulary ). To describe the physical or digital manifestation of the resource, use the FORMAT element.\n\n**Guidelines for content creation**: If the resource is composed of multiple mixed types then multiple or repeated Type elements should be used to describe the main components.\n\nBecause different communities or domains are expected to use a variety of type vocabularies, best practice to ensure interoperability is to include at least one general type term from the DCMIType vocabulary in addition to the domain specific type term(s), in separate Type element iterations.\n\n**Examples**:\n\n> Type=\"Image\"\n> Type=\"Sound\"\n> Type=\"Text\"\n> Type=\"simulation\"\n\n**Note**: The first three values are taken from the DCMI Type Vocabulary, and follow the capitalization conventions for that vocabulary. The last value is a term from an unspecified source.\n\nThe item described is an Electronic art exhibition catalog:\n\n> Type=\"Image\"\n> Type=\"Text\"\n> Type=\"Exhibition catalog\"\n\n**Note**: The first two values are taken from the DCMI Type Vocabulary, and follow the capitalization conventions for that vocabulary. The last value is a term from an unspecified source.\n\nThe item described is a Multimedia educational program with interactive assignments:\n\n> Type=\"Image\"\n> Type=\"Text\"\n> Type=\"Software\"\n> Type=\"InteractiveResource\"\n\n**Note**: All values in this example are taken from the DCMI Type Vocabulary, and follow the capitalization conventions for that vocabulary.",
        "type": "array",
        "element": "string"
      },
      "Source": {
        "description": "A Reference to a resource from which the present resource is derived. The present resource may be derived from the Source resource in whole or part. Recommended best practice is to reference the resource by means of a string or number conforming to a formal identification system.\n\n**Guidelines for content creation**: In general, include in this area information about a resource that is related intellectually to the described resource but does not fit easily into a Relation element.\n\n**Examples**:\n\n> Source=\"RC607.A26W574 1996\" [where \"RC607.A26W574 1996\" is the call number of the print version of the resource, from which the present version was scanned]\n> Source=\"Image from page 54 of the 1922 edition of Romeo and Juliet\"",
        "type": "array",
        "element": "string"
      },
      "Relation": {
        "description": "A reference to a related resource. Recommended best practice is to reference the resource by means of a string or number conforming to a formal identification system.\n\n**Guidelines for content creation**: Relationships may be expressed reciprocally (if the resources on both ends of the relationship are being described) or in one direction only, even when there is a refinement available to allow reciprocity. If text strings are used instead of identifying numbers, the reference should be appropriately specific. For instance, a formal bibliographic citation might be used to point users to a particular resource.",
        "type": "array",
        "element": "string"
      },
      "IsPartOf": {
        "description": "This property describes the relationship between the described resource and another resource of which the described resource is a physical or logical part (e.g. a painting as part of a collection, an article as part of a journal, etc.). The described resource is like a \"child\" in a hierarchical or \"parent/child\" relationship. For the reciprocal statement use HasPart.",
        "type": "array",
        "element": "string"
      },
      "HasPart": {
        "description": "This property describes the relationship between the desribed resource and another resource which is a physical or logical part of the described resource (e.g. the described resource is a collection of paintings, or a journal with different articles, etc.). The described resource is like the \"parent\" in a hierarchical or \"parent/child\" relationship. For the reciprocal statement use IsPartOf.",
        "type": "array",
        "element": "string"
      },
      "IsVersionOf": {
        "description": "This property describes the relationship between the described resource and another resource, that is a former version, edition or adaptation of the described resource (e.g. the described resource is the revision of a book, or another recording of a song, etc.). Another version implies changes in the content of a resource. For resources with different formats use isFormatOf. For the reciprocal statement use hasVersion.",
        "type": "array",
        "element": "string"
      },
      "HasVersion": {
        "description": "This property describes the relationship between the desribed property and another property, that is a later version, edition or adaptation of the described resource (e.g. the described resource is the older version of a revised book, or of a song, etc.). Another version implies changes in the content of a resource. For resources with different formats use hasFormat. For the reciprocal statement use IsVersionOf.",
        "type": "array",
        "element": "string"
      },
      "IsFormatOf": {
        "description": "This property describes the relationship between the described resource and another resource, that is a former version of the described resource with the same intellectual content but presented in another format (e.g. the described resource is the microfilm version of a printed book, or the pdf version of a doc document). For intellectual changes between resources use isVersonOf. For the reciprocal statement use HasFormat.",
        "type": "array",
        "element": "string"
      },
      "HasFormat": {
        "description": "This property describes the relationship between the described resource and another resource, that is a later version of the described resource with the same intellectual content but presented in another format (e.g. the desribed resource is a printed book that is also availabel as a microfilm, or a doc document that is also available as pdf). For intellectual changes between resources use hasVersion. For the reciprocal statement use IsFormatOf.",
        "type": "array",
        "element": "string"
      },
      "Replaces": {
        "description": "This property describes the relationship between the described resource and another resource, that has been supplanted, displaced or superseeded by the described resource. It is used for the valid version in chain of versions (e.g. the described resource is the the last draft of a contract, or the current version of guidelines). For the reciprocal statement use IsReplacedBy.",
        "type": "array",
        "element": "string"
      },
      "IsReplacedBy": {
        "description": "This property describes the relationship between the described resource and another resource, that supplants, displaces or superseedes the described resource. It is used, when in chain of versions only one version is valid (e.g. the described resource is one of the former drafts of a contract, or a former version of guidelines). For the reciprocal statement use replaces.",
        "type": "array",
        "element": "string"
      },
      "Requires": {
        "description": "This property describes the relationship between the described resource and another resource supporting the function, delivery or coherence of the content of the described resource (e.g. the described resource is an application that can be used only with a particular software, or hardware). For the reciprocal statement use IsRequiredBy.",
        "type": "array",
        "element": "string"
      },
      "IsRequiredBy": {
        "description": "The described resource is necesssary for the function, delivery or coherence of the content of the resource the property references to (e.g. the described resource is a software or hardware necesssary to use a particular application). For the reciprocal statement use Requires.",
        "type": "array",
        "element": "string"
      },
      "References": {
        "description": "This property describes the relationship between the described resource and another resource that is cited, referenced, or otherwise pointed to by the described resource (e.g. the described resource is an article citing a book, or an interview pointing to a play). For the reciprocal statement use IsReferencedBy.",
        "type": "array",
        "element": "string"
      },
      "IsReferencedBy": {
        "description": "This property describes the relationship between a resource and another resource that points to the described resource by citation, acknowledgement, etc (e.g. the described resource is a book cited in an article, or a play pointed to in an interview, etc.). For the reciprocal statement use references.",
        "type": "array",
        "element": "string"
      },
      "ConformsTo": {
        "description": "This property describes the relationship between a resource and an established standard, to which the described resource conforms (e.g. a metadata record that conforms to the RDA standard, or a pipe that conforms to ISO 3183, etc.)",
        "type": "array",
        "element": "string"
      },
      "Coverage": {
        "description": " The extent or scope of the content of the resource. Coverage will typically include spatial location (a place name or geographic co-ordinates), temporal period (a period label, date, or date range) or jurisdiction (such as a named administrative entity). Recommended best practice is to select a value from a controlled vocabulary (for example, the Thesaurus of Geographic Names [Getty Thesaurus of Geographic Names, http://www.getty.edu/research/tools/vocabulary/tgn/]). Where appropriate, named places or time periods should be used in preference to numeric identifiers such as sets of co-ordinates or date ranges.\n\n**Guidelines for content creation**: Whether this element is used for spatial or temporal information, care should be taken to provide consistent information that can be interpreted by human users, particularly in order to provide interoperability in situations where sophisticated geographic or time-specific searching is not supported. For most simple applications, place names or coverage dates might be most useful. For more complex applications, consideration should be given to using an encoding scheme that supports appropriate specification of information, such as DCMI Period, DCMI Box or DCMI Point.\n\n**Examples**:\n\n> Coverage=\"1995-1996\"\n> Coverage=\"Boston, MA\"\n> Coverage=\"17th century\"\n> Coverage=\"Upstate New York\"",
        "type": "array",
        "element": "string"
      },
      "Creator": {
        "description": "An entity primarily responsible for making the content of the resource. Examples of a Creator include a person, an organization, or a service. Typically the name of the Creator should be used to indicate the entity.\n\n**Guidelines for creation of content**: Creators should be listed separately, preferably in the same order that they appear in the publication. Personal names should be listed surname or family name first, followed by forename or given name. When in doubt, give the name as it appears, and do not invert.\n\nIn the case of organizations where there is clearly a hierarchy present, list the parts of the hierarchy from largest to smallest, separated by full stops and a space. If it is not clear whether there is a hierarchy present, or unclear which is the larger or smaller portion of the body, give the name as it appears in the item.\n\nIf the Creator and Publisher are the same, do not repeat the name in the Publisher area. If the nature of the responsibility is ambiguous, the recommended practice is to use Publisher for organizations, and Creator for individuals. In cases of lesser or ambiguous responsibility, other than creation, use Contributor.\n\n**Examples**:\n\n> Creator=\"Shakespeare, William\"\n> Creator=\"Wen Lee\"\n> Creator=\"Hubble Telescope\"\n> Creator=\"Internal Revenue Service. Customer Complaints Unit\"",
        "type": "array",
        "element": "string"
      },
      "Publisher": {
        "description": "The entity responsible for making the resource available. Examples of a Publisher include a person, an organization, or a service. Typically, the name of a Publisher should be used to indicate the entity.\n\n**Guidelines for content creation**: The intent of specifying this field is to identify the entity that provides access to the resource. If the Creator and Publisher are the same, do not repeat the name in the Publisher area. If the nature of the responsibility is ambiguous, the recommended practice is to use Publisher for organizations, and Creator for individuals. In cases of ambiguous responsibility, use Contributor.",
        "type": "array",
        "element": "string"
      },
      "Contributor": {
        "description": "An entity responsible for making contributions to the content of the resource. Examples of a Contributor include a person, an organization or a service. Typically, the name of a Contributor should be used to indicate the entity.\n\n**Guideline for content creation**: The same general guidelines for using names of persons or organizations as Creators apply here. Contributor is the most general of the elements used for \"agents\" responsible for the resource, so should be used when primary responsibility is unknown or irrelevant.",
        "type": "array",
        "element": "string"
      },
      "Rights": {
        "description": "Information about rights held in and over the resource. Typically a Rights element will contain a rights management statement for the resource, or reference a service providing such information. Rights information often encompasses Intellectual Property Rights (IPR), Copyright, and various Property Rights. If the rights element is absent, no assumptions can be made about the status of these and other rights with respect to the resource.\n\n**Guidelines for content creation**: The Rights element may be used for either a textual statement or a URL pointing to a rights statement, or a combination, when a brief statement and a more lengthy one are available.\n\n**Examples**:\n\n> Rights=\"Access limited to members\"\n> Rights=\"http://cs-tr.cs.cornell.edu/Dienst/Repository/2.0/Terms\"",
        "type": "array",
        "element": "string"
      },
      "Date": {
        "description": "A date associated with an event in the life cycle of the resource. Typically, Date will be associated with the creation or availability of the resource. Recommended best practice for encoding the date value is defined in a profile of ISO 8601 [Date and Time Formats, W3C Note, http://www.w3.org/TR/NOTE- datetime] and follows the YYYY-MM-DD format.\n\n**Guidelines for content creation**: If the full date is unknown, month and year (YYYY-MM) or just year (YYYY) may be used. Many other schemes are possible, but if used, they may not be easily interpreted by users or software.\n\n**Examples:**\n\n> Date=\"1998-02-16\"\n> Date=\"1998-02\"\n> Date=\"1998\"",
        "type": "array",
        "element": "string"
      },
      "Format": {
        "description": "The physical or digital manifestation of the resource. Typically, Format may include the media-type or dimensions of the resource. Examples of dimensions include size and duration. Format may be used to determine the software, hardware or other equipment needed to display or operate the resource.\n\nRecommended best practice is to select a value from a controlled vocabulary (for example, the list of Internet Media Types [http://www.iana.org/ assignments/media-types/] defining computer media formats).\n\n**Guidelines for content creation**: In addition to the specific physical or electronic media format, information concerning the size of a resource may be included in the content of the Format element if available. In resource discovery size, extent or medium of the resource might be used as a criterion to select resources of interest, since a user may need to evaluate whether they can make use of the resource within the infrastructure available to them.\n\nWhen more than one category of format information is included in a single record, they should go in separate iterations of the element.\n\n**Examples**:\n\n> Title=\"Dublin Core™ icon\"\n> Identifier=\"http://purl.org/metadata/dublin_core/images/dc2.gif\"\n> Type=\"Image\">\n Format=\"image/gif\"\n> Format=\"4 kB\"\n> Subject=\"Saturn\"\n> Type=\"Image\"\n> Format=\"image/gif 6\"\n> Format=\"40 x 512 pixels\"\n> Identifier=\"http://www.not.iac.es/newwww/photos/images/satnot.gif\"\n> Title=\"The Bronco Buster\"\n> Creator=\"Frederic Remington\"\n> Type=\"Physical object\"\n> Format=\"bronze\"\n> Format=\"22 in.\"",
        "type": "array",
        "element": "string"
      },
      "Identifier": {
        "description": "An unambiguous reference to the resource within a given context. Recommended best practice is to identify the resource by means of a string or number conforming to a formal identification system. Examples of formal identification systems include the Uniform Resource Identifier (URI) (including the Uniform Resource Locator (URL), the Digital Object Identifier (DOI) and the International Standard Book Number (ISBN).\n\n**Guidelines for content creation**: This element can also be used for local identifiers (e.g. ID numbers or call numbers) assigned by the Creator of the resource to apply to a particular item. It should not be used for identification of the metadata record itself.\n\n**Examples**:\n\n> Identifier=\"http://purl.oclc.org/metadata/dublin_core/\"\n> Identifier=\"ISBN:0385424728\"\n> Identifier=\"H-A-X 5690B\" [publisher number]",
        "type": "array",
        "element": "string"
      },
      "Language": {
        "description": "A language of the intellectual content of the resource. Recommended best practice for the values of the Language element is defined by RFC 3066 [RFC 3066, http://www.ietf.org/rfc/ rfc3066.txt] which, in conjunction with ISO 639 [ISO 639, http://www.oasis- open.org/cover/iso639a.html]), defines two- and three-letter primary language tags with optional subtags. Examples include \"en\" or \"eng\" for English, \"akk\" for Akkadian, and \"en-GB\" for English used in the United Kingdom.\n\n**Guidelines for content creation**: Either a coded value or text string can be represented here. If the content is in more than one language, the element may be repeated.\n\n**Examples**:\n\n> Language=\"en\"\n> Language=\"fr\"\n> Language=\"Primarily English, with some abstracts also in French.\"\n> Language=\"en-US\"",
        "type": "array",
        "element": "string"
      },
      "Audience": {
        "description": "A class of entity for whom the resource is intended or useful. A class of entity may be determined by the creator or the publisher or by a third party.\n\n**Guidelines for content creation**: Audience terms are best utilized in the context of formal or informal controlled vocabularies. None are presently recommended or registered by DCMI, but several communities of interest are engaged in setting up audience vocabularies. In the absence of recommended controlled vocabularies, implementors are encouraged to develop local lists of values, and to use them consistently.\n\n**Examples**:\n\n> Audience=\"elementary school students\"\n> Audience=\"ESL teachers\"\n> Audience=\"deaf adults\"",
        "type": "array",
        "element": "string"
      }
    }
