# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.metadata.manager import metadata, interface

from occam.objects.manager  import ObjectManager
from occam.versions.manager import VersionManager
from occam.licenses.manager import LicenseManager
from occam.network.manager import NetworkManager

from occam.manager import uses

import datetime
import json

@metadata('codemeta')
@uses(ObjectManager)
@uses(VersionManager)
@uses(LicenseManager)
@uses(NetworkManager)
class CodeMetaMetadataPlugin:
  """ Provides the CodeMeta metadata view for objects in the system.
  """

  @interface
  def detect(self):
    """ Returns True when this plugin is available.
    """

    # Heck yeah, we are!
    return True

  @interface
  def info(self):
    """ Returns metadata about the metadata plugin.
    """

    return {
      "short": "CodeMeta",
      "name": "CodeMeta",
      "summary": ""
    }

  @interface
  def http(self, manager, path, method, query, headers, data, person):
    pass

  @interface
  def view(self, manager, object, objectInfo, person):
    """ Generates the metadata document for the given object.

    Arguments:
      manager (MetadataManager): The instantiated metadata manager.
      object (Object): The artifact in question.
      objectInfo (ObjectInfo): The artifacts main Occam metadata.
      person (Person): The actor making the request.

    Returns:
      str: The XML document serving as a response to the metadata request.
    """

    metadata = self.metadata(manager, object, objectInfo, person)

    # We just output the appropriate JSON-LD form
    metadata["@context"] = "https://doi.org/10.5063/schema/codemeta-2.0"
    metadata["@type"] = "SoftwareSourceCode"

    return [
      "application/json",
      json.dumps(metadata)
    ]

  def appendRequires(self, codemeta, object, objectInfo, person):
    dependencies = objectInfo.get('dependencies', [])
    dependencies += objectInfo.get('run', {}).get('dependencies', [])
    dependencies += objectInfo.get('build', {}).get('dependencies', [])

    for dependency in dependencies:
      codemeta['softwareRequirements'] = codemeta.get('softwareRequirements', [])
      codemeta['softwareRequirements'].append({
        "identifier": f"occam://{dependency.get('id')}",
        "name": dependency.get('name')
      })

      if 'version' in dependency:
        codemeta['softwareRequirements'][-1]['version'] = dependency['version']
      elif 'revision' in dependency:
        codemeta['softwareRequirements'][-1]['version'] = dependency['revision']

      if 'type' in dependency:
        codemeta['softwareRequirements'][-1]['additionalType'] = [f"occam://types/{dependency['type']}"]

    return codemeta

  def appendRelated(self, dc, object, objectInfo, person):
    for related in objectInfo.get('related', []):
      if 'object' in related and 'id' in related['object']:
        dc['relation'] = dc.get('relation', [])
        if 'type' in related['object']:
          dc['relation'].append({
            'identifier': [f"occam://{related['object']['id']}"],
            'type': [related['object']['type']],
            'title': [related['object'].get('name', related['object'].get('summary', related.get('relation', 'unknown')))],
            'subject': related['relation']
          })

          if 'subtype' in related['object']:
            # Append subtype as a type
            subtypes = related['object']['subtype']

            if not isinstance(subtypes, list):
              subtypes = [subtypes]

            dc['relation'][-1]['type'].extend(subtypes)
        else:
          dc['relation'].append(f"occam://{related['object']['id']}")

    return dc

  def populateGeneric(self, codemeta, object, objectInfo, person):
    """ Returns a representation of the generic occam metadata.
    """

    if objectInfo.get('publisher'):
      codemeta["publisher"] = [objectInfo['publisher']]

    if objectInfo.get('published'):
      codemeta["date"] = [objectInfo['published']]

    if objectInfo.get('language'):
      codemeta["language"] = [objectInfo['language']]

    if objectInfo.get('license'):
      licenses = objectInfo['license']
      if not isinstance(licenses, list):
        licenses = [licenses]

      codemeta["license"] = []

      for license in licenses:
        if isinstance(license, str):
          info = self.licenses.retrieve(license)
        else:
          info = {}

        url = info.get('spdx', {}).get('url')
        name = info.get('name', license)

        if isinstance(license, dict):
          # We look up the license as a SPDX URL

          # Append a reference to the file indicated by the license tag
          # Or the whole URL, if specified
          if 'url' in license:
            codemeta["license"].append(license['url'])
          elif 'file' in license:
            codemeta["license"].append(f"occam://{object.id}/{object.revision}/{license.get('file')}")
        else:
          codemeta["license"].append(url or name)

    # Consolidate licenses if there is only 1
    if len(codemeta.get("license", [])) == 1:
      codemeta["license"] = codemeta["license"][0]

    #if objectInfo.get('type'):
    #  codemeta["type"] = [objectInfo['type']]

    def craftPerson(info):
      person = {
        '@type': 'Person'
      }

      if isinstance(info, str):
        person['name'] = info
      elif isinstance(info, dict):
        if 'identity' in info:
          person['@id'] = info['identity']
        if 'name' in info:
          person['name'] = info['name']

      return person

    # One-to-one keys
    for key in ["name", "publisher"]:
      if objectInfo.get(key):
        codemeta[key] = objectInfo[key]

    if objectInfo.get('tags') and isinstance(objectInfo["tags"], list):
      codemeta["keywords"] = objectInfo["tags"]

    if objectInfo.get('summary'):
      codemeta["description"] = objectInfo['summary']

    if isinstance(objectInfo.get('authors'), list):
      for author in objectInfo.get('authors'):
        codemeta["creator"] = codemeta.get('creator') or []
        person = craftPerson(author)
        codemeta["creator"].append(person)

    if isinstance(objectInfo.get('collaborators'), list):
      for collaborator in objectInfo.get('collaborators'):
        codemeta["contributor"] = codemeta.get('contributor') or []
        person = craftPerson(collaborator)
        codemeta["contributor"].append(person)

    for resource in objectInfo.get('build', {}).get('install', []) + objectInfo.get('install', []) + objectInfo.get('run', {}).get('install', []):
      subtype = resource.get('subtype')
      if isinstance(subtype, list):
        subtype = subtype[0]

      if subtype == 'git':
        # A code repository?
        if 'source' in resource and self.network.isURL(resource['source']):
          codemeta["codeRepository"] = resource['source']

    architecture = objectInfo.get('run', {}).get('architecture', objectInfo.get('architecture'))
    if architecture:
      codemeta["processorRequirements"] = architecture

    environment = objectInfo.get('run', {}).get('environment', objectInfo.get('environment'))
    if environment:
      codemeta["operatingSystem"] = [environment]

    if 'description' in objectInfo:
      if isinstance(objectInfo['description'], dict) and objectInfo['description']['file']:
        # Craft the URL to the object description
        codemeta["readme"] = f"occam://{object.id}/{object.revision}/{objectInfo['description']}"

      # We can also be clever, if we want, and see if there is a README in the repository
      # and then link that... but that is a TODO for later
      pass

    # TODO: Can we determine a programming language that is used perhaps?
    # We can search for 'language' types and add the names
    # We can search for 'compiler' types and try to determine their languages

    return codemeta

  @interface
  def metadata(self, manager, object, objectInfo, person):
    """ Returns the appropriate metadata for the given object.

    Arguments:
      manager (MetadataManager): The instantiated metadata manager.
    """

    codemeta = {}

    if isinstance(objectInfo.get('metadata', {}).get("codemeta"), dict):
      info = objectInfo['metadata']['codemeta']

      if info.get('_addIdentifier'):
        codemeta["identifier"] = f"occam://{object.id}"

      if info.get('_addDependencies'):
        self.appendRequires(codemeta, object, objectInfo, person)

      if info.get('_addRelations'):
        self.appendRelated(codemeta, object, objectInfo, person)

      if k == "_addGeneric" and v:
        self.populateGeneric(codemeta, object, objectInfo, person)

      for k, v in info:
        if k == "_description":
          k = "description"

        if not k.startswith("_"):
          codemeta[k] = v

    else:
      # Just populate a generic thing
      self.populateGeneric(codemeta, object, objectInfo, person)
      codemeta["identifier"] = f"occam://{object.id}"
      self.appendRequires(codemeta, object, objectInfo, person)
      self.appendRelated(codemeta, object, objectInfo, person)

    return codemeta

  @interface
  def distill(self, object, objectInfo, person):
    """ Returns a normalized version of the metadata.
    """

    return {}

  @interface
  def schema(self):
    """ Generates the schema used to edit such metadata.

    Returns:
      dict: The structure of the metadata as an Occam configuration schema.
    """

    address = {
      "streetAddress": {
        "type": "string",
        "label": "Street Address",
        "description": "The street address. For example, 1600 Amphitheatre Pkwy."
      },
      "addressLocality": {
        "type": "string",
        "label": "Locality",
        "description": "The locality in which the street address is, and which is in the region. For example, Mountain View."
      },
      "addressRegion": {
        "type": "string",
        "label": "Region",
        "description": "The region in which the locality is, and which is in the country. For example, California or another appropriate first-level Administrative division."
      },
      "postalCode": {
        "type": "string",
        "label": "Postal Code",
        "description": "The postal code. For example, 94043."
      },
      "addressCountry": {
        "type": "string",
        "label": "Country",
        "description": "The country. For example, USA. You can also provide the two-letter ISO 3166-1 alpha-2 country code."
      },
      "postOfficeBoxNumber": {
        "type": "string",
        "label": "Post Office Box Number",
        "description": "The post office box number for PO box addresses."
      }
    }

    person = {
      "identifier": {
        "type": "string",
        "label": "Identifier",
        "description": "URL identifier, ideally an ORCID ID for individuals, a FundRef ID for funders."
      },
      "name": {
        "type": "string",
        "label": "Name",
        "description": "The name of this entity or Person."
      },
      "givenName": {
        "type": "string",
        "label": "Given Name",
        "description": "Given name. In the U.S., the first name of a Person. This can be used along with familyName instead of the name property."
      },
      "familyName": {
        "type": "string",
        "label": "Family Name",
        "description": "Family name. In the U.S., the last name of an Person. This can be used along with givenName instead of the name property."
      },
      "email": {
        "type": "string",
        "label": "Email Address",
        "description": "Email address."
      },
      "affiliation": {
        "type": "string",
        "label": "Affiliation",
        "description": "An organization that this person is affiliated with. For example, a school/university."
      },
      "address": {
        "label": "Address",
        "description": "Physical address of the item.",
        **address
      }
    }

    return {
      "_addGeneric": {
        "label": "Automatically Add Metadata",
        "type": "boolean",
        "default": True,
        "description": "When set, this will append any normal Occam metadata within the artifact as the appropriate entries. Any added manually will replace the automatically assigned values."
      },
      "_addIdentifier": {
        "label": "Automatically Add Identifier",
        "type": "boolean",
        "default": True,
        "description": "When set, this will append the Occam identifier as the unique identifier."
      },
      "_addDependencies": {
        "label": "Automatically Add Dependencies",
        "type": "boolean",
        "default": True,
        "description": "When set, this will append any Occam dependencies within the artifact as 'softwareRequirements' entries."
      },
      "_addRelations": {
        "label": "Automatically Add Related",
        "type": "boolean",
        "default": True,
        "description": "When set, this will append any Occam related objects within the artifact as 'relatedLink' entries."
      },
      "identifier": {
        "label": "Unique Identifier",
        "type": "string",
        "description": "The unique identifier that uniquely identifies this artifact. (Using the automatic option above will add an Occam identifier automatically.)"
      },
      "name": {
        "type": "string",
        "label": "Name",
        "description": "The name of the item."
      },
      "url": {
        "type": "string",
        "label": "URL",
        "description": "URL of the item."
      },
      "_description": {
        "type": "string",
        "label": "Description",
        "description": "A description of the item."
      },
      "author": {
        "type": "array",
        "element": person,
        "label": "Author(s)",
        "description": "The author of this content or rating. Please note that author is special in that HTML 5 provides a special mechanism for indicating authorship via the rel tag. That is equivalent to this and may be used interchangeably."
      },
      "creator": {
        "type": "array",
        "element": person,
        "label": "Creator(s)",
        "description": "The creator/author of this CreativeWork. This is the same as the Author property for CreativeWork."
      },
      "contributor": {
        "type": "array",
        "element": person,
        "label": "Contributor(s)",
        "description": "A secondary contributor to the CreativeWork or Event."
      },
      "maintainer": {
        "type": "array",
        "element": person,
        "label": "Maintainer(s)",
        "description": "Individual(s) responsible for maintaining the software (usually includes an email contact address)."
      },
      "editor": {
        "type": "array",
        "element": person,
        "label": "Editor(s)",
        "description": "Specifies the Person who edited the CreativeWork."
      },
      "publisher": {
        "type": "array",
        "element": person,
        "label": "Publisher(s)",
        "description": "The publisher of the creative work."
      },
      "sponsor": {
        "type": "array",
        "element": person,
        "label": "Sponsor(s)",
        "description": "A person or organization that supports a thing through a pledge, promise, or financial contribution. e.g. a sponsor of a Medical Study or a corporate sponsor of an event."
      },
      "producer": {
        "type": "array",
        "element": person,
        "label": "Producer(s)",
        "description": "The person or organization who produced the work (e.g. music album, movie, tv/radio series etc.)."
      },
      "provider": {
        "label": "Provider(s)",
        "type": "array",
        "element": person,
        "description": "The service provider, service operator, or service performer; the goods producer. Another party (a seller) may offer those services or goods on behalf of the provider. A provider may also serve as the seller. Supersedes carrier."
      },
      "funder": {
        "type": "array",
        "element": person,
        "label": "Funder(s)",
        "description": "A person or organization that supports (sponsors) something through some kind of financial contribution."
      },
      "funding": {
        "type": "string",
        "label": "Funding Source",
        "description": "Funding source (e.g. specific grant)."
      },
      "codeRepository": {
        "type": "string",
        "label": "Code Repository",
        "description": "Link to the repository where the un-compiled, human readable code and related code is located (SVN, GitHub, CodePlex, institutional GitLab instance, etc.)."
      },
      "programmingLanguage": {
        "type": "string",
        "label": "Programming Language",
        "description": "The computer programming language."
      },
      "runtimePlatform": {
        "type": "string",
        "label": "Runtime Platform",
        "description": "Runtime platform or script interpreter dependencies (Example - Java v1, Python2.3, .Net Framework 3.0). Supersedes runtime."
      },
      "targetProduct": {
        "type": "string",
        "label": "Target Product",
        "description": "Target Operating System / Product to which the code applies. If applies to several versions, just the product name can be used."
      },
      "applicationCategory": {
        "type": "string",
        "label": "Application Category",
        "description": "Type of software application, e.g. ‘Game, Multimedia’."
      },
      "applicationSubCategory": {
        "type": "string",
        "label": "Application Sub-Category",
        "description": "Subcategory of the application, e.g. ‘Arcade Game’."
      },
      "downloadUrl": {
        "type": "string",
        "label": "Download URL",
        "description": "If the file can be downloaded, URL to download the binary."
      },
      "fileSize": {
        "type": "string",
        "label": "File Size",
        "description": "Size of the application / package (e.g. 18MB). In the absence of a unit (MB, KB etc.), KB will be assumed."
      },
      "installUrl": {
        "type": "string",
        "label": "Install URL",
        "description": "URL at which the app may be installed, if different from the URL of the item."
      },
      "memoryRequirements": {
        "type": "string",
        "label": "Memory Requirements",
        "description": "Minimum memory requirements."
      },
      "operatingSystem": {
        "type": "array",
        "element": "string",
        "label": "Operating System",
        "description": "Operating systems supported (Windows 7, OSX 10.6, Android 1.6)."
      },
      "permissions": {
        "type": "string",
        "label": "Permissions",
        "description": "Permission(s) required to run the app (for example, a mobile app may require full internet access or may run only on wifi)."
      },
      "processorRequirements": {
        "type": "string",
        "label": "Processor Requirements",
        "description": "Processor architecture required to run the application (e.g. IA64)."
      },
      "releaseNotes": {
        "type": "string",
        "label": "Release Notes",
        "description": "Description of what changed in this version."
      },
      "softwareHelp": {
        "type": "string",
        "label": "Software Help",
        "description": "Software application help."
      },
      "buildInstructions": {
        "type": "string",
        "label": "Build Instructions URL",
        "description": "A link to installation instructions/documentation."
      },
      "issueTracker": {
        "type": "string",
        "label": "Issue Tracker URL",
        "description": "A link to the software bug reporting or issue tracking system."
      },
      "contIntegration": {
        "type": "string",
        "label": "Continuous Integration URL",
        "description": "A link to the continuous integration service."
      },
      "developmentStatus": {
        "type": ["Unknown", "Concept", "WIP", "Suspended", "Abandoned", "Active", "Inactive", "Unsupported", "Moved"],
        "default": "Unknown",
        "label": "Development Status",
        "description": "Description of development status, e.g. Active, inactive, suspended, at time of preservation. See repostatus.org."
      },
      "embargoDate": {
        "type": "date",
        "label": "Embargo Date",
        "description": "Software may be embargoed from public access until a specified date (e.g. pending publication, 1 year from publication)."
      },
      "softwareRequirements": {
        "type": "array",
        "element": "string",
        "label": "Software Requirements",
        "description": "Required software dependencies."
      },
      "softwareSuggestions": {
        "type": "array",
        "element": "string",
        "label": "Software Optional Dependencies",
        "description": "Optional dependencies , e.g. for optional features, code development, etc."
      },
      "softwareVersion": {
        "type": "string",
        "label": "Software Version",
        "description": "Version of the software instance."
      },
      "storageRequirements": {
        "type": "string",
        "label": "Storage Requirements",
        "description": "Storage requirements (free space required)."
      },
      "supportingData": {
        "type": "string",
        "label": "Supporting Data",
        "description": "Supporting data for a SoftwareApplication."
      },
      #"referencePublication": {
      #  "description": "An academic publication related to the software."
      #},
      "readme": {
        "type": "string",
        "label": "Read-me URL",
        "description": "A link to the software Readme file."
      },
      "citation": {
        "type": "array",
        "element": "string",
        "label": "Citation",
        "description": "A citation or reference to another creative work, such as another publication, web page, scholarly article, etc."
      },
      "copyrightHolder": {
        "type": "string",
        "label": "Copyright Holder",
        "description": "The party holding the legal copyright to the CreativeWork."
      },
      "copyrightYear": {
        "type": "string",
        "label": "Copyright Year",
        "description": "The year during which the claimed copyright for the CreativeWork was first asserted."
      },
      "dateCreated": {
        "type": "datetime",
        "label": "Date Created",
        "description": "The date on which the CreativeWork was created or the item was added to a DataFeed."
      },
      "dateModified": {
        "type": "datetime",
        "label": "Date Modified",
        "description": "The date on which the CreativeWork was most recently modified or when the item’s entry was modified within a DataFeed."
      },
      "datePublished": {
        "type": "datetime",
        "label": "Date Published",
        "description": "Date of first broadcast/publication."
      },
      "encoding": {
        "type": "string",
        "label": "Encoding",
        "description": "A media object that encodes this CreativeWork. This property is a synonym for associatedMedia. Supersedes encodings."
      },
      "fileFormat": {
        "type": "string",
        "label": "File Format",
        "description": "Media type, typically MIME format (see IANA site) of the content e.g. application/zip of a SoftwareApplication binary. In cases where a CreativeWork has several media type representations, ‘encoding’ can be used to indicate each MediaObject alongside particular fileFormat information. Unregistered or niche file formats can be indicated instead via the most appropriate URL, e.g. defining Web page or a Wikipedia entry."
      },
      "keywords": {
        "type": "array",
        "element": "string",
        "label": "Keywords",
        "description": "Keywords or tags used to describe this content."
      },
      "license": {
        "type": "string",
        "label": "License",
        "description": "A license document that applies to this content, typically indicated by URL."
      },
      "version": {
        "type": "string",
        "label": "Version",
        "description": "The version of the CreativeWork embodied by a specified resource."
      },
      "isAccessibleForFree": {
        "type": "boolean",
        "label": "Is Accessible for Free",
        "description": "A flag to signal that the publication is accessible for free."
      },
      "isPartOf": {
        "type": "string",
        "label": "Is Part Of",
        "description": "Indicates a CreativeWork that this CreativeWork is (in some sense) part of. Reverse property `hasPart`."
      },
      "hasPart": {
        "type": "string",
        "label": "Has Part",
        "description": "Indicates a CreativeWork that is (in some sense) a part of this CreativeWork. Reverse property `isPartOf`."
      },
      "position": {
        "type": "string",
        "label": "Position",
        "description": "The position of an item in a series or sequence of items. (While schema.org considers this a property of CreativeWork, it is also the way to indicate ordering in any list (e.g. the Authors list). By default arrays are unordered in JSON-LD."
      },
      "sameAs": {
        "type": "string",
        "label": "Same As",
        "description": "URL of a reference Web page that unambiguously indicates the item’s identity. E.g. the URL of the item’s Wikipedia page, Wikidata entry, or official website."
      },
      "relatedLink": {
        "type": "array",
        "element": "string",
        "label": "Related Link",
        "description": "A link related to this object, e.g. related web pages."
      }
    }
