# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.metadata.manager import metadata, interface

from occam.objects.manager  import ObjectManager
from occam.versions.manager import VersionManager

from occam.manager import uses

@metadata('marcxml')
@uses(ObjectManager)
@uses(VersionManager)
class MARCXMLMetadataPlugin:
  """ Provides the MARCXML metadata view for objects in the system.
  """

  @interface
  def detect(self):
    """ Returns True when this plugin is available.
    """

    return True

  @interface
  def metadata(self):
    """ Returns metadata about the metadata plugin.
    """

    return {}

  @interface
  def http(self, path, method, query, headers, data, person):
    pass

  @interface
  def view(self, object, person):
    """ Generates the metadata document for the given object.
    """

    # TODO: move this into a generic place for both marcxml and marc plugins
    marc = self.marcFor(object)

    # Craft the XML document that represents the given object
    xml = "<>"

    return xml

  def marcFor(self, object):
    # Get the object info
    info = self.objects.infoFor(object)

    # If it has specific MARC metadata, use it
    marc = {}
    if info.get('metadata', {}).get('marc'):
      marc = info['metadata']['marc']
    else:
      # Otherwise, generate a decent default marc card
      marc = {}

    return marc
