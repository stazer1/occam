# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log
from occam.config import Config

from occam.manager import uses

from types import SimpleNamespace

from occam.objects.write_manager  import ObjectWriteManager
from occam.configurations.manager import ConfigurationManager
from occam.workflows.manager      import WorkflowManager
from occam.nodes.manager          import NodeManager
from occam.network.manager        import NetworkManager
from occam.discover.manager       import DiscoverManager
from occam.jobs.manager           import JobManager
from occam.databases.manager      import DatabaseManager

from urllib.parse import quote

@command('workflows', 'job-init',
  category      = 'Workflow Management',
  documentation = "Launches a workflow.")
@argument("job_id", type=str, help = "The identifier for the job.")
@argument("url", type=str, help = "The url of the workflow run metadata.")
@option("-r", "--remote-job", action = "store",
                              dest = "remote_job",
                              help = "The job identifier on the remote coordinator.")
@uses(NetworkManager)
@uses(NodeManager)
@uses(ObjectWriteManager)
@uses(DatabaseManager)
@uses(ConfigurationManager)
@uses(WorkflowManager)
@uses(DiscoverManager)
@uses(JobManager)
class JobInitCommand:
  def do(self):
    # Get the local job
    job = self.jobs.jobFromId(self.options.job_id)

    # Get the run metadata by parsing the url
    # occam://host:port/runs/id
    run, remoteJob = self.workflows.runAndJobForURL(self.options.url)

    if not run:
      Log.error("Cannot find the given run")
      return 1

    # If it is remote, send the workflow job-init command to that coordinator
    config = Config().load()
    host = self.network.hostname()
    thisHost = config.get("daemon", {}).get("externalHost", host)
    thisPort = config.get("daemon", {}).get("port", None)

    # Parse URL
    parts = self.network.parseURL(self.options.url)

    # If this is a URL for our own local node, just retrieve the run normally
    if parts.netloc != f"{thisHost}:{thisPort}" and parts.netloc != f"localhost:{thisPort}":
      remoteURL = f"occam://{parts.netloc}/workflows/job-init/{remoteJob.id}/{quote(self.options.url, safe='')}"
      self.network.get(remoteURL)
      return 0

    # Get the identity of the job actor
    person = self.jobs.personFor(job)

    # Get the workflow tag
    workflowTag = self.objects.parseObjectTag(run.workflow_tag)

    # Initialize the jobs in that run
    workflow = self.objects.resolve(workflowTag, person = person)

    # Get the coordinator URL
    host, port, scheme = self.nodes.parseURL(self.options.url)
    url = f"{scheme}://{host}:{port}"

    # We will need to discover the workflow if we do not have it
    if workflow is None:
      node = self.nodes.discover(url)
      obj = self.discover.discover(
        id                    = workflowTag.id,
        revision              = workflowTag.revision,
        version               = workflowTag.version,
        withBuild             = False,
        withBuildDependencies = False,
        #networkOptions = NetworkOptions(externalToken = self.options.external_token),
        person = person,
        nodes = [node])

    if workflow is None:
      Log.error("Cannot find the workflow.")
      return 1

    self.workflows.launch(workflow, run, person=person)
    return 0
