# The Workflows Component

This component handles workflows, configurations, and job generation.

## Workflows

Workflows are composed of object nodes. The object's inputs and outputs are
passed to one another in order of their appearance in the workflow. When a
workflow is run, the object nodes are traversed and jobs are launched to handle
execution of that portion of the workflow. Configurations may indicate that
multiple permutations of a run are required using different instantiations of
the configuration.

![the workflow task generation process starts with logical blocks](../../../docs/diagrams/workflow_diagram.svg)

The above shows a basic situation. There are two inputs, one is data of some
kind and the other is a configuration. A configuration is indeed somewhat
special, as we will describe, but otherwise is organized just as any kind of
object. These are both connected to an object that is suggested will compute
some result. The result flows to the next object, which is also suggested will
run. The result of this workflow is the output the final object produces.

### What is a Running Object

Some objects in the workflow "run" and others do not. The logic behind which are
which is basic. An object can have a `run` environment described. This is, at
its most basic, a "Runnable Object". However, there are circumstances where such
an object is not meant to run, even when in others it would.

If the object has its `file` parameter used when
attached to a workflow, it indicates that a *file* and not the object itself is
being used as input. The file is just being sourced from the object. This is
different from when the object has a `file` tag, inherently. This object might
still, in fact, run. (The `file` might indicate the binary ROM information, for
instance.) The `file` tag we are referring to that would override running an
object is when it appears in the workflow's description.

The other situation is when the object is using its "self" output pin in the
workflow. This is the `-1` output index, which is not a real output for the
object (which has indices that start at 0 and are described within the object's
`outputs` field in its Occam `object.json` metadata.) This overrides the idea
that the object would run and instead passes it as input to the indicated object
in the chain.

### Queueing

Workflows are queued for execution via the front-end or the `occam workflows
queue` command. When a workflow is queued, an init job is created to represent
the starting of the workflow. A run is then created and tied to that job.

The init job calls `workflows job-init` which will start the "run". The workflow
manager scans the workflow description for its running head nodes. These are the
first tasks that must run. It then queues jobs for those nodes within the same
scheduler and owned by the same actor as the current workflow init job. (See
`getInitialNodes` in the `WorkflowManager`.)

![the general high-level workflow process diagram](../../../docs/diagrams/workflow_flowchart.svg)

When any such work job finishes, the job will finalize by calling `workflows
job-done`. This command will look at the job's status and react to failure and,
upon success, will look at what work that job generated. If a workflow node
generates output, the node or nodes attached to that output might get a chance
to run. If such a downstream node has its inputs satisfied (it has at least one
set of data on all attached pins), it will then queue with this new input
corresponding to the brand new output generated by the prior job. If it does
not, nothing is queued. A subsequent job will trigger it, instead.

It is possible for such a new output to spawn multiple instances of the
downstream node. If that node has several inputs on another pin lined up, then
it would need to permutate those inputs with our new output. So, if it had three
pending inputs on pin A, our output being sent along pin B would queue three
jobs, one for each input on pin A and our output on pin B.

There is some complicated logic involved to know when such a job should be
generated. It is not necessarily proven that the workflow resolver will, in
situations where it might race generated output, queue jobs properly. It will,
however, queue them optimistically. So, it might accidentally race and generate
two runs of another node if each of two jobs that produce that node's two inputs
complete at the same time. Generally, this is not an issue for the correctness
of the resulting workflow.

All workflow coördination is done on the same node, which minimizes the issue.
Even when workflows are generated to operate on slurm clusters, the node that
performed the `job-init` is the node that receives all `job-done` calls as well.

### Cancelling

Workflows may be cancelled, which prevents additional jobs from being generated
for the workflow. The process to terminate would try to stop the initialization
job, if it still exists, followed by terminating each queued job belonging to
the run.
