# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.databases.manager import table

@table('runs')
class RunRecord:
  """ This table contains information about a run of a workflow.

  When a workflow is executed, it spawns a RunRecord which is the main keeper
  of metadata for a single run of a workflow.

  Within a single run, many jobs and tasks will be dispatched. The next jobs
  and tasks will spawn from the information gained when running previous jobs.
  The accounting necessary to keep track of this is spread out among
  RunOutputRecord and the job records (joined by RunJobRecord). This RunRecord
  is the single record that ties them all together.
  """

  schema = {
    # Primary Key

    "id": {
      "type": "integer",
      "serial": True,
      "primary": True
    },

    # Identity URI

    "identity": {
      "type": "string",
      "length": 48
    },

    # Experiment/Workflow Identifier

    "object_uid": {
      "type": "string",
      "length": 46
    },

    "object_tag": {
      "type": "string",
      "length": 256
    },

    "object_name": {
      "type": "string",
      "length": 128
    },

    # Workflow Identifier

    "workflow_uid": {
      "type": "string",
      "length": 46
    },

    "workflow_tag": {
      "type": "string",
      "length": 256
    },

    "workflow_name": {
      "type": "string",
      "length": 128
    },

    # Time/Date

    "queue_time": {
      "type": "datetime"
    },

    "elapsed_time": {
      "type": "datetime"
    },

    "finish_time": {
      "type": "datetime"
    },

    "failure_time": {
      "type": "datetime"
    },

    # Root job

    "job_id": {
      "foreign": {
        "table": "jobs",
        "key": "id"
      }
    },

    # Lock

    # When posting outputs, we lock the RunRecord
    # so that we don't race other node finalizes

    "lock": {
      "type": "integer"
    }
  }
