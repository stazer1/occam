from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.manager import uses

from occam.daemon.manager  import DaemonManager

@command('daemon', 'status',
  category      = 'Services',
  documentation = "Shows information about a running daemon.")
@option("-p", "--port", action = "store",
                        dest   = "port",
                        help   = "determines the port for the daemon to inspect")
@uses(DaemonManager)
class DaemonCommand:
  """ This command queries the status of daemons running under the current
  user.
  """

  def do(self):
    """ Perform the command.
    """

    import json

    daemons = self.daemon.status(port = self.options.port)
    Log.output(
      json.dumps(
        [
          {
            'port': daemon['port'],
            'pid': daemon['pid'],
            'status': daemon['status'],
          } for daemon in daemons
        ]
      )
    )
    return 0
