from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.manager import uses

from occam.daemon.manager  import DaemonManager

@command('daemon', 'run',
  category      = 'Services',
  documentation = "Starts a daemon in the foreground.")
@option("-p", "--port", action = "store",
                        dest   = "port",
                        help   = "determines the port on which to start the daemon")
@option("-s", "--host", action = "store",
                        dest   = "host",
                        help   = "determines the address on which to listen for daemon connections")
@uses(DaemonManager)
class DaemonCommand:
  """ This command starts a daemon on the given port.
  """

  def do(self):
    """ Perform the command.
    """

    self.daemon.run(host = self.options.host,
                    port = self.options.port,
                    synchronizeLog = True,
                    processingTag = self.options.processing_tag)
    return 0
