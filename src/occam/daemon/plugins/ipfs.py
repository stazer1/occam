# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.config import Config

from occam.manager import uses

from occam.daemon.manager import daemon

import subprocess

@daemon('ipfs')
class IPFS:

  def detect(self):
    """ Returns True when IPFS is available.
    """
    
    try:
      p = self.invokeIPFS([])
      code = p.wait()
    except:
      code = -1

    return code == 0

  def start(self):
    """ Start the daemon. """

    IPFS.Log.write("Starting IPFS Daemon")
    command = ["daemon"]

    self.daemon = self.invokeIPFS(command, ipfsPath=self.storePath())

    return self.daemon

  def stop(self):
    import signal
    try:
      self.daemon.send_signal(signal.SIGINT)
    except:
      pass

    self.daemon = None

  def storePath(self):
    """ Returns the path to the ipfs instance.
    """

    return self.configuration.get('path', os.path.join(Config.root(), "ipfs"))

  @staticmethod
  def path():
    """ Returns the path to the IPFS binary.
    """

    return (os.path.realpath("%s/../../../../../go-ipfs" % (os.path.realpath(__file__))))

  @staticmethod
  def popen(command, stdout=subprocess.DEVNULL, stdin=None, stderr=subprocess.DEVNULL, cwd=None, env=None):
    return subprocess.Popen(command, stdout=stdout, stdin=stdin, stderr=stderr, cwd=cwd, env=env)

  def invokeIPFS(self, args, stdout=subprocess.PIPE, stdin=None, stderr=subprocess.DEVNULL, ipfsPath=None, env=None):
    """ Internal method to invoke IPFS with the given arguments
    """

    if env is None:
      env = {}
    if not 'IPFS_PATH' in env and not ipfsPath is None:
      env['IPFS_PATH'] = ipfsPath

    env['PATH'] = os.getenv('PATH')

    command = ['%s/ipfs' % (IPFS.path())] + args
    return IPFS.popen(command, stdout=stdout, stdin=stdin, stderr=stderr, env=env)
