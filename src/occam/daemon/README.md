# The Occam Daemon

The Occam daemon can be run in the foreground or background to handle incoming commands from the frontend or other daemons in the case of a Slurm deployment.

When launched for a Slurm job, the daemon will be launched with the `-P` / `--processing-tag` flag, as will commands sent to the daemon. When a daemon receives a command it will check that the processing tags match. If so, the daemon will handle the command. Otherwise the command will be handed off to the appropriate daemon via Unix Domain Sockets. If a command comes in without a tag, it will be handled by whatever daemon receives the connection. If a processing tag is specified for a command and no daemon has a matching tag, the command will fail with a connection reset due to the receiving daemon hanging up after failing to pass the command to a daemon with the same tag.

It is possible that a command will have the misfortune of being received by a daemon that is in the process of shutting down. Due to how sockets work in Unix systems there is no way to avoid this. The effect is that the requester of the command will get a connection reset on the socket used to send the command. The command should be retried in this case.

## Using the Daemon
For deployments, the daemon should be launched as a Systemd service (or equivalent). See [the example service file](../../../docs/systemd/occam.service) for details.

You can accomplish this by:
  - copying the service file to /etc/systemd/system/: `sudo cp docs/systemd/occam.service /etc/systemd/system/`
  - enable the service: `sudo systemctl enable occam`
  - start the service: `sudo systemctl start occam`

## Testing the Daemon
For testing and debugging you will most likely want to run the daemon in the foreground. To run the daemon in the foreground run `occam daemon run`. If you want detailed information in the event of an error, you can prepend this command with `OCCAM_DEBUG=1`. This will give you a stack trace when exceptions make it to the top of the call stack.

To test that the daemon handles commands properly, launch the daemon and run `occam network post occam://localhost:32000/system/view`. You should receive system information as if you ran `occam system view`.

You can similarly test that the daemon is working correctly with processing tags. Launch two daemons using the `--processing-tag` flag. Ensure you use two distinct tags for the daemons. Watch the output of one. Run `occam network post occam://localhost:32000/system/view?P=YOURTAG`. Where YOURTAG is the tag of the daemon you are watching. Run this multiple times. You should always see the command get handled by the daemon you are watching. Daemons forwarding commands will print warning messages indicating the tag to which they are forwarding.

