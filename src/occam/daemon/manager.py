# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log     import loggable
from occam.manager import manager, uses

from occam.databases.manager import DatabaseManager
from occam.git.manager import GitManager

import select
import os

class socket_wrap():
  def __init__(self, socket):
    self.socket = socket
    self.buffer = self

  def write(self, data):
    self.socket.send(data)

  def flush(self):
    pass

  def read(self, amount):
    return self.socket.recv(amount)

  def fileno(self):
    return self.socket.fileno()

class WaitableEvent:
  """Provides an abstract object that can be used to resume select loops with
  indefinite waits from another thread or process. This mimics the standard
  threading.Event interface."""

  def __init__(self):
    self._read_fd, self._write_fd = os.pipe()

  def wait(self, timeout=None):
    rfds, wfds, efds = select.select([self._read_fd], [], [], timeout)
    return self._read_fd in rfds

  def isSet(self):
    return self.wait(0)

  def clear(self):
    if self.isSet():
      os.read(self._read_fd, 1)

  def set(self):
    if not self.isSet():
      os.write(self._write_fd, b'1')

  def fileno(self):
    """Return the FD number of the read side of the pipe, allows this object to
    be used with select.select()."""
    return self._read_fd

  def __del__(self):
    os.close(self._read_fd)
    os.close(self._write_fd)

def parseArgumentsList(options, argumentStr):
  """ Build out the option (keyword) argument list.

  Arguments:
    options(dict): A dict where the key is the argument (i.e. "--set")
      and the value is a list of items. If the item itself is a
      list, it is the multiple values.

      options = {
        "--set": [['x','y','z'], ['a','b']]
      }

      These will be rendered as ("--set x y z --set a b")

      When the value is explicitly true and not a string, then the option
      merely exists. For example:

      options = {
        "-j": true
      }

      Renders as ("-j").
  Returns:
    (String, list): A tuple consisting of a parsed argument list and a list
      form of the same arguments.
  """
  optionsArguments = []
  for key, arguments in options.items():
    if arguments is True:
      optionsArguments.append(key)
      continue

    if not isinstance(arguments, list):
      arguments = [arguments]

    for argumentList in arguments:
      optionsArguments.append(key)

      if not isinstance(argumentList, list):
        argumentList = [argumentList]

      optionsArguments.extend(argumentList)

  argumentsList = ""

  if optionsArguments:
    argumentsList = '"' + '" "'.join(map(lambda x: x.replace('"', '\\"'), optionsArguments)) + '"'

  if argumentStr:
    if argumentsList:
      argumentsList += " -- "

    argumentsList += " ".join(argumentStr)

  return argumentsList, optionsArguments

def retrieveStdin(conn, buffer, stdinLen):
  """ Get incoming data for use as stdin.

  Arguments:
    conn(Connection): Connection object from which we should read stdin.
    buffer(String): Byte string previously read from the connection. Some stdin
      may have been read already.
    stdinLen(int): Expected size of stdin.
  Returns:
    BytesIO: BytesIO object at the start of stdin if successful in retrieving
      stdin. None otherwise.
  """
  import io
  # The stdin key in the command dict contains the number of bytes to expect
  # for stdin.
  stdin = None
  # TODO: Wrap the network stream to zero-copy the data.
  DaemonManager.Log.write(f"Looking at stdin for {stdinLen} bytes")

  amountNeeded = stdinLen
  if len(buffer) >= stdinLen:
    # We already read everything we need for the stdin. Get the
    # stdin portion of the data in the buffer.
    stdin = io.BytesIO(buffer[0:stdinLen])
    amountNeeded = 0
  else:
    amountNeeded = amountNeeded - len(buffer)
    stdin = io.BytesIO(buffer)

  # Seek to the end of the read data so we can append in the right place.
  stdin.seek(0, os.SEEK_END)

  while amountNeeded > 0:
    # Read and append the remaining stdin needed for the command.
    DaemonManager.Log.write("Looking for remaining %s bytes" % (amountNeeded))
    data = conn.recv(amountNeeded)
    stdin.write(data)
    amountNeeded = stdinLen - stdin.tell()

  # Seek back to the start of the data.
  stdin.seek(0, os.SEEK_SET)

  return stdin

def sendResponse(conn, header, output):
  """ Send a response to the client.

  We send length, newline, content for both the header and output.

  Argument:
    conn (Connection): The connection we are sending the response through.
    header (dict): A header dictionary describing the result of the command.
    output (string): The actionable output of the command.
  """
  import json

  header = json.dumps(header).encode('utf8')
  conn.send((str(len(header)) + "\n").encode('utf8'))
  conn.send(header)
  conn.send((str(len(output)) + "\n").encode('utf8'))
  conn.send(output)

def readForLine(conn, buffer):
  """ Read the connection for a actionable line.

  This will be either a command in the form of a JSON dictionary, or a HTTP GET
  / POST in the event we are receiving a connection from Git.

  Arguments:
    conn (Connection): Connection from which we are reading.
    buffer (Str): Binary string data that has been read from the connection but
      not parsed.
  """

  import codecs

  chunkSize = 1024
  buffering = True
  line = ""

  while buffering:
    if b'\n' in buffer:
      # We have enough for at least one command. Split the buffer on the
      # newline, and move forward with the first command.
      (line, buffer) = buffer.split(b'\n', 1)
      try:
        line = codecs.decode(line, 'utf8').strip()
        break
      except:
        # The encoding was bad so we ignore this line.
        pass
    else:
      # We need more data from the connection before we can do anything.
      more = conn.recv(chunkSize)
      if not more:
        # Nothing else was sent to us over the connection.
        buffering = False
      else:
        buffer += more

  return line, buffer, buffering

def crossDomainSockPath(port, processingTag):
  """ Return the path to the socket for the daemon with the given settings.
  """
  from occam.config import Config

  return os.path.join(Config.root(), 'daemons', f'{port}-{processingTag}.sock')

# From https://docs.python.org/3/library/socket.html#socket.socket.sendmsg.
# NOTE: Once we drop support for Python versions below 3.9, we can use the
# socket module's send_fds.
def send_fds(sock, msg, fds):
  """ Send a list of file descriptors over the socket.
  """
  import array
  import socket

  # This line is what triggers the appropriate duplication of the file
  # descriptor between processes. See
  # https://man7.org/linux/man-pages/man7/unix.7.html
  return sock.sendmsg([msg], [(socket.SOL_SOCKET, socket.SCM_RIGHTS, array.array("i", fds))])

# From https://docs.python.org/3/library/socket.html#socket.socket.recvmsg.
# NOTE: Once we drop support for Python versions below 3.9, we can use the
# socket module's recv_fds.
def recv_fds(sock, msglen, maxfds):
  """ Receive from the socket a list of file descriptors.
  """
  import array
  import socket

  fds = array.array("i")
  msg, ancdata, flags, addr = sock.recvmsg(msglen, socket.CMSG_LEN(maxfds * fds.itemsize))
  for cmsg_level, cmsg_type, cmsg_data in ancdata:
    if cmsg_level == socket.SOL_SOCKET and cmsg_type == socket.SCM_RIGHTS:
      # Append data, ignoring any truncated integers at the end.
      fds.frombytes(cmsg_data[:len(cmsg_data) - (len(cmsg_data) % fds.itemsize)])
  return msg, list(fds)

def passToDaemon(targetTag, line, buffer, conn):
  """ Hand over the socket and processed input to the appropriate daemon.
  """

  import codecs
  import socket

  from occam.config import Config

  DaemonManager.Log.warning(f"Forwarding to daemon with tag {targetTag}")
  buffer = codecs.encode(line, 'utf8') + b'\n' + buffer

  # Connect to the Unix Socket of the daemon to which we will pass the command.
  uSock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
  port = conn.getsockname()[1]
  sockpath = crossDomainSockPath(port, targetTag)

  try:
    uSock.connect(sockpath)
  except FileNotFoundError as e:
    # Nothing is listening for this tag. Log the error and give up.
    DaemonManager.Log.error(f"Cannot open cross daemon socket {sockpath}.")
    conn.shutdown(socket.SHUT_RDWR)
    conn.close()
    return

  try:
    # Send the file descriptor for the socket connection that the other daemon
    # will be taking over. Note we have to send at least one byte.
    fd = send_fds(uSock, b"0", [conn.fileno()])

    # Send the data we read on the other daemon's behalf.
    uSock.send(codecs.encode(str(len(buffer)), 'utf8') + b'\n')
    uSock.send(buffer)

    # Wait for the other daemon to signal to us we are cleared to detach from
    # the connection. We will time out after a generous 10 seconds to avoid
    # hanging in the event the other daemon dies during the socket handoff.
    uSock.settimeout(10)
    msg = uSock.recv(1)

    # Our co-daemon is explicitly telling us it has failed to take over the
    # socket.
    if msg != b'0':
      DaemonManager.Log.error(f"Co-daemon failed to accept the connection.")
      conn.shutdown(socket.SHUT_RDWR)
  except:
    # Either through timeout or some unexpected error, we are detecting the
    # connection handoff is a failure.
    DaemonManager.Log.error(f"Failed to hand off connection to co-daemon.")
    conn.shutdown(socket.SHUT_RDWR)
    raise
  finally:
    uSock.shutdown(socket.SHUT_RDWR)
    uSock.close()

    conn.close()

def takeoverDaemonConnection(conn, synchronizeLog, path, processingTag):
  """ Take over the connection once held by another daemon.
  """

  import socket

  # Create the thread's log file
  logPath = os.path.join(path, f"daemon-{os.getpid()}.log")

  log = None
  if not synchronizeLog:
    log = open(logPath, 'wb+')
    DaemonManager.Log.threadInfo()['stderr'] = log

  # Receive the file descriptor for the connection.
  fd = recv_fds(conn, 1, maxfds = 1)[1][0]

  # Get the data that was parsed when the connection was initially opened.
  buffer = b''
  readDataSize, buffer, _ = readForLine(conn, buffer)
  readDataSize = int(readDataSize) - len(buffer)
  if readDataSize > 0:
    buffer += conn.recv(readDataSize)

  # Turn the file descriptor we got from the other daemon into a socket object.
  passedConn = socket.fromfd(fd, socket.AF_INET, socket.SOCK_STREAM)

  # Tell the other daemon everything is ok on our end.
  conn.send(b'0')

  # Hang up. Connections over the cross daemon socket are one shots.
  conn.shutdown(socket.SHUT_RDWR)
  conn.close()

  # Proceed to run the daemon process.
  try:
    daemonLoop(passedConn, processingTag, stderr = log, buffer = buffer)
  finally:
    passedConn.shutdown(socket.SHUT_RDWR)
    passedConn.close()

def daemonLoop(conn, processingTag = None, stderr = None, buffer = b''):
  """ Run the daemon loop for the given connection.

  Will continue serving until the client hangs up.
  """

  import codecs
  import http.client
  import io
  import json
  import sys
  import traceback

  # Whether or not this connection has handled a request successfully
  knownSuccessful = False

  forwarded = buffer != b''

  while True:
    # If we were forwarded a command, we can't wait for more input as we don't
    # know if more input is due to us yet. We must handle the first line before
    # we ask for more. We are guaranteed that at least one parsable line is in
    # the buffer.
    if not forwarded:
      readers, _, _ = select.select([conn], [], [])

      if len(readers) <= 0:
        continue
    else:
      forwarded = False

    line, buffer, buffering = readForLine(conn, buffer)

    # Pass HTTP requests to the Git component when they are the first request
    # on the connection.
    if (not knownSuccessful and (line.startswith("GET ") or
                                line.startswith("POST "))):
      # See if this request needs to be processed by a daemon that handles a
      # particular tag. If that is the case and we are not a processor of that
      # tag, we need to forward this command to another daemon that can handle
      # it.
      headers = http.client.parse_headers(io.BytesIO(buffer))
      tag = headers.get('X_OCCAM_PROCESSING_TAG')
      if tag is not None and processingTag is not None and tag != processingTag:
        passToDaemon(tag, line, buffer, conn)
        return

      GitManager().handleHTTPRequest(conn, line, buffer)
      # The buffer data we read was handled by Git - clear the buffer.
      buffer = b''
      continue

    if not buffer and buffering == False:
      # There was nothing left to handle on the connection, and the connection
      # was closed.
      DaemonManager.Log.write("Connection closed on the client's side.")
      return

    # Commands from clients are sent as JSON strings. Try to interpret the line
    # we parsed as a JSON string.
    try:
      line = json.loads(line)
    except:
      # What we read didn't constitute a JSON string. Skip this line.
      continue

    if not isinstance(line, dict):
      # The line correctly parsed as JSON, but wasn't a dictionary. We don't
      # know how to handle this case, so we ignore this line.
      continue

    # Parse the options sent with the command.
    options = line.get("options", {})
    argumentsList, optionsArguments = parseArgumentsList(options,
                                                         line["arguments"])

    # See if this command needs to be processed by a daemon that handles a
    # particular tag. If that is the case and we are not a processor of that
    # tag, we need to forward this command to another daemon that can handle
    # it.
    tag = options.get("-P") or options.get("--processing-tag")
    if tag is not None and processingTag is not None and tag != processingTag:
      passToDaemon(tag, json.dumps(line), buffer, conn)
      return

    # Log the command we received.
    promoted = "promoted " if line.get('promote') else ""
    DaemonManager.Log.write(f"Received {promoted}command '{line['component']} "
                            f"{line['command']} {argumentsList}'")

    stdin = None
    if "stdin" in line:
      stdin = retrieveStdin(conn, buffer, line["stdin"])

      # Ignore any extra data we were not expecting.
      buffer = b''

    # Build the command array from the dict.
    invocation = [line["component"]]

    if line["command"]:
      invocation.append(line["command"])

    if optionsArguments:
      # Put any non-option arguments behind a "--" so they are not interpreted
      # as options.
      if len(line["arguments"]) > 0:
        optionsArguments.append("--")
      invocation.extend(optionsArguments)

    invocation.extend(line["arguments"])

    stdout = sys.stdout.buffer
    header = {'status': 'ok'}

    # Promoted commands send an OK status and blank output right away (to have
    # a uniform response format). The output is later streamed over the socket.
    promote = line.get("promote", False)
    if promote:
      stdout = socket_wrap(conn)
      stdin  = socket_wrap(conn)

      sendResponse(conn, header, b"")

    from occam.commands.manager import CommandManager
    commands = CommandManager()

    # TODO: fix these error codes?
    error = True
    code = -1

    try:
      code = commands.execute(invocation, storeOutput = not promote,
                                          stdin = stdin,
                                          stdout = stdout,
                                          stderr = stderr,
                                          allowLocalPerson = False)
      error = code < 0
    except Exception as e:
      DaemonManager.Log.write(f"Error handling command ({line})")
      DaemonManager.Log.write(f"Error type: {sys.exc_info()[0]}")
      traceback.print_exc()
    except:
      DaemonManager.Log.write(f"Error handling command ({line})")
      DaemonManager.Log.write(f"Error type: {sys.exc_info()[0]}")

    # When we promote, the thread and socket are dedicated to a task
    # So, we will destroy the thread when the command finishes
    if promote:
      break

    # Send stdout
    output = DaemonManager.Log.storedOutput()
    output = output.read()

    errorMessage = DaemonManager.Log.errorMessage()
    if error:
      header = {
          'status': 'error',
          'message': errorMessage,
          'code':    code
      }

    sendResponse(conn, header, output)

    # We have successfully handled a request
    knownSuccessful = True

def handleDaemonConnection(conn, synchronizeLog, path, processingTag):
  """ Handle Occam commands for the given connection.

  Sandwiches the main daemon loop with setup and shutdown actions.
  """

  import socket

  # Create the thread's log file
  logPath = os.path.join(path, f"daemon-{os.getpid()}.log")

  # Disconnect from a forked database connection
  # (The database will automatically re-establish its connection)
  DatabaseManager().disconnect()

  log = None
  if not synchronizeLog:
    log = open(logPath, 'wb+')
    DaemonManager.Log.threadInfo()['stderr'] = log

  daemonLoop(conn, processingTag = processingTag, stderr=log)

  # Ensure connection is closed
  DaemonManager.Log.write("Closing connection")

  # The fd will be -1 when the connection is already closed.
  if conn.fileno() != -1:
    conn.shutdown(socket.SHUT_RDWR)
    conn.close()

  if log:
    DaemonManager.Log.write("Closing log")
    log.close()
    os.remove(logPath)

@loggable
@uses(GitManager)
@manager("daemon")
class DaemonManager:
  """ Handles an OCCAM daemon.

  The OCCAM daemon runs as a process and can accept commands from a socket.

  Generally, one daemon would run per server to accept and issue commands for
  that server. For instance, a web server can spawn an occam daemon on a known
  local port and issue occam commands to handle requests.

  The port has a configurable default, but can be overwritten by a command line
  argument to the "occam daemon" command.
  """

  subDaemons = {}

  def __init__(self):
    """ Initializes the daemon.
    """

    import occam.daemon.plugins.ipfs

    self.initializeInfoDirectory()

    self.stopFlag = False

  @staticmethod
  def register(serviceName, handlerClass):
    """ Adds a new sub-daemon.

    Arguments:
      serviceName (str): The name of the service to start.
      handlerClass (object): The handler that implements the daemon interface.
    """

    DaemonManager.subDaemons[serviceName] = {
      'class': handlerClass
    }

  def configurationFor(self, serviceName):
    """ Returns the configuration for the given sub-daemon.
    
    This configuration is found within the occam configuration (config.yml)
    under the "daemon" section under the given plugin name.
    """

    config = self.configuration
    subConfig = config.get(serviceName, {})

    return subConfig

  def defaultPort(self):
    """ Retrieves the default configured port.
    """

    # Get the port from configuration option daemon.port
    return self.configuration.get('port', 32000)

  def defaultHost(self):
    """ Retrieves the default configured hostname.
    """

    # Get the port from configuration option daemon.port
    return self.configuration.get('host', '127.0.0.1')

  def path(self):
    """ Returns the path to the daemon metadata.
    """

    from occam.config import Config
    import os

    basePath = Config.root()

    return os.path.join(basePath, "daemons")

  def initializeInfoDirectory(self):
    """ Initializes the directory for the metadata for running daemons.
    """

    import os

    path = self.path()

    if not os.path.exists(path):
      os.mkdir(path)

  def stopHandler(self, signum, frame):
    """ Handles SIGINT.
    """

    # If we already specified to stop, and got a second SIGINT, raise a
    # KeyboardInterrupt.
    if self.stopFlag:
      raise KeyboardInterrupt

    self.stopFlag = True

  def metadataFilePath(self, port, processingTag):
    """ Returns the path to the metadata file for the daemon.
    """
    return os.path.join(self.path(), f"{port}-{processingTag}.json")

  def writeDaemonMetadata(self, port, host, pid, processingTag):
    """ Write the daemon metadata file.
    """

    import json

    # Write the daemon metadata file
    metadata = {}
    metadata['port'] = port
    metadata['host'] = host
    metadata['pid']  = pid
    metadata['processingTag'] = processingTag

    # Get the command used to create this process.
    cmdCheck = None
    try:
      with open(os.path.join("/", "proc", str(pid), "cmdline")) as f:
        cmdCheck = f.read()
    except:
      pass

    metadata['cmd'] = cmdCheck
    path = self.path()
    metadataFilename = f"{port}-{processingTag}.json"
    metadataPath = self.metadataFilePath(port, processingTag)
    with open(metadataPath, 'w+') as f:
      json.dump(metadata, f)

    return metadataPath

  def createCommandSock(self, host, port, processingTag):
    """ Create a socket through which we will receive commands.
    """

    import socket

    commandSock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    commandSock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    if processingTag:
      # We will co-exist with other daemons potentially. Note that if another
      # daemon is on this port and isn't allowing reuse, then binding the
      # socket will fail.
      commandSock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)

    try:
      commandSock.bind((host, port))
    except socket.error as e:
      DaemonManager.Log.error(f"Could not bind the command socket: "
                              f"(host: {host}, port: {port}) "
                              f"Error {e.errno}: {e.strerror}")
      return None

    return commandSock

  def createCrossDaemonSock(self, port, processingTag):
    """ Create a socket through which other daemons can forward commands.

    We will receive commands from other daemons if the command has our
    processing tag.
    """

    import socket

    if processingTag is None:
      return None

    # Open a Unix Domain socket through which we can pass off commands we
    # accepted but which don't pertain to our processing tag.
    crossDaemonSock = socket.socket(socket.AF_UNIX)

    sockpath = crossDomainSockPath(port, processingTag)

    try:
      crossDaemonSock.bind(f"{sockpath}")
    except socket.error as e:
      DaemonManager.Log.error(f"Could not bind the cross daemon socket: "
                              f"({sockpath}) Error {e.errno}: "
                              f"{e.strerror}")
      return None

    return crossDaemonSock

  def handleConnect(self, commandSock, crossDaemonSock, processingTag, synchronizeLog):
    """ Handle incoming socket connections.
    """

    import signal
    import socket

    from multiprocessing import Process

    # Until we receive a stop signal via SIGINT, jump between handling command
    # and daemon connections.
    signal.signal(signal.SIGINT, self.stopHandler)
    while not self.stopFlag:
      try:
        # Note that the processes created here are spawned as daemon processes.
        # This means that if they outlive this parent process, they will be
        # immediately stopped - potentially without their resources being
        # cleaned up. This prevents the case where the main thread goes down,
        # but the port remains allocated because one of these threads is
        # blocked and unable to join.

        # Create a Process to handle the next incoming command connection.
        try:
          conn, addr = commandSock.accept()

          DaemonManager.Log.write("Connected with %s:%s" % (addr[0], str(addr[1])))

          p = Process(target=handleDaemonConnection,
                      args=(conn, synchronizeLog, self.path(), processingTag))
          p.daemon = True
          p.start()
        except socket.timeout:
          pass

        # Create a Process to handle the next incoming connection handing off a
        # command accepted on our behalf by another daemon.
        if processingTag:
          try:
            conn, addr = crossDaemonSock.accept()

            p = Process(target=takeoverDaemonConnection,
                        args=(conn, synchronizeLog, self.path(), processingTag))
            p.daemon = True
            p.start()
          except socket.timeout:
            pass
      except(KeyboardInterrupt):
        break

  def run(self, host = None, port = None, synchronizeLog = False,
                                          processingTag = None):
    """ Starts the server loop.
    """

    # Disconnect from the database while we process connections
    DatabaseManager().disconnect()

    # Run subdaemons first
    for name, info in DaemonManager.subDaemons.items():
      subConfig = self.configurationFor(name)
      subDaemon = info['class'](subConfig)
      if subDaemon.detect():
        subDaemon.start()
      else:
        DaemonManager.Log.noisy(f"Cannot detect {name} subdomain service; will not start.")

    import os
    import socket
    import time

    from multiprocessing import active_children

    from occam.network.manager import NetworkManager
    network = NetworkManager()

    if host is None:
      host = self.defaultHost()

    if port is None:
      port = self.defaultPort()

    try:
      port = int(port)
    except:
      DaemonManager.Log.error("Provided port is not a valid integer.")
      return -1

    if port > 65535 or port <= 0:
      DaemonManager.Log.error("Provided port is outside of the valid port "
                              "range (1 - 65535).")
      return -1

    # Get pid for this process
    pid = os.getpid()

    commandSock = self.createCommandSock(host, port, processingTag)
    if commandSock is None:
      return -1

    commandSock.settimeout(0.05)
    commandSock.listen(10)

    crossDaemonSock = self.createCrossDaemonSock(port, processingTag)

    if crossDaemonSock is not None:
      crossDaemonSock.settimeout(0.05)
      crossDaemonSock.listen(10)

    elif processingTag is not None:
      return -1

    metadataPath = self.writeDaemonMetadata(port, host, pid, processingTag)

    self.handleConnect(commandSock, crossDaemonSock, processingTag, synchronizeLog)

    maxWaitSeconds = 5
    DaemonManager.Log.write(f"Initiating shutdown. Waiting up to "
                            f"{maxWaitSeconds} seconds for command "
                            f"processes to stop. Send another interrupt "
                            f"to exit immediately.")

    # It may be the case that we have multiprocessing processes handling
    # connections. Some of these can finish very quickly (such as a simple
    # object status fetch). Others like jobs connect have no "normal" runtime.
    #
    # We try here to let those fast operations finish and exit. We might have
    # JUST received a connection before getting the shutdown signal. This wait
    # time gives that connection a chance to finish handoff to another daemon.
    for t in range(maxWaitSeconds):
      if not active_children():
        break
      time.sleep(1)

    DaemonManager.Log.write("Closing socket(s).")
    commandSock.shutdown(socket.SHUT_RDWR)
    commandSock.close()

    # If the cross daemon socket was created, we need to close it and delete
    # the residual link.
    if crossDaemonSock is not None:
      crossDaemonSock.shutdown(socket.SHUT_RDWR)
      crossDaemonSock.close()
      os.remove(crossDomainSockPath(port, processingTag))

    DaemonManager.Log.write("Socket(s) closed.")

    # Clean up the daemon metadata file
    os.remove(metadataPath)

  def start(self, host = None, port = None, processingTag = None):
    """ Starts the daemon.

    This will spawn a separate process to run as a daemon, if it isn't running
    on the given port already. If a daemon is already running on that port, or
    the port is not open for any other reason, the call will fail.

    Returns True upon success and False upon failure.
    """

    if port is None:
      port = self.defaultPort()

    DaemonManager.Log.write("Starting daemon on port %s" % (port))

    path = self.path()

    from occam.daemon.daemon import Daemon

    # Move ourselves to a background process
    server = Daemon(workingDir=path, logFile="daemon")
    server.create()

    # Run the server
    self.run(host = host, port = port, processingTag = processingTag)

  def status(self, port=None):
    """ Gives the status of all running daemons on the system.

    If you pass along a port, you can filter the list to just the daemon running
    on that port.

    For each daemon, it will report a dictionary object with keys for the port
    and the uptime if applicable.
    """

    import json
    import os
    import re
    import time

    # Walk the daemon path and read and then report the metadata

    path = self.path()
    statuses = []
    try:
      filenames = os.listdir(path)
    except OSError:
      DaemonManager.Log.error("Failed to list contents of %s" % (path))
      return statuses

    for filename in filenames:
      if filename.endswith(".json"):
        # Read json data and fill in fields only we know such as uptime and if
        # the process is still running.
        try:
          with open(os.path.join(path, filename)) as f:
            data = json.load(f)
            if 'pid' in data.keys() and 'port' in data.keys():
              # Filter by port if specified.
              if not port is None and data['port'] != port:
                continue

              # Determine if the process is still live, and if live it's runtime.
              try:
                # Figure out our best guess of the runtime. Ideally we would be
                # using creation time, but that is filesystem dependent.
                # Note that if we used os.stat()'s st_ctime here the behavior
                # would be divergent on Windows and non-Windows platforms.
                uptime = time.time() - os.path.getmtime(
                  os.path.join("/", "proc", str(data["pid"]))
                )
                data['status'] = {'state': 'up', 'uptime_seconds': uptime}
              except OSError:
                data['status'] = {'state': 'down'}

              statuses.append(data)
        except OSError:
          pass

    return statuses

  def delete_daemon_instance_file(self, metadataFilePath):
    try:
      os.remove(metadataFilePath)
    except FileNotFoundError:
      pass
    except OSError as e:
      DaemonManager.Log.write(
        "Failed to remove %s: %s." % (metadataFilePath, e)
      )
      return False

    return True

  def stop(self, port=None, processingTag=None, graceful_exit_timeout_s=30):
    """ Stops the daemon.

    If the daemon fails to terminate gracefully via SIGINT within
    graceful_exit_timeout_s seconds a SIGKILL will be sent.

    Returns True if the daemon was running and was successfully stopped.
    """

    import json
    import os
    import time

    if port is None:
      port = self.defaultPort()

    data = {}

    # Open the metadata file for this daemon.
    metadataFilePath = self.metadataFilePath(port, processingTag)
    try:
      with open(metadataFilePath) as f:
        data = json.load(f)
    except FileNotFoundError:
      pass
    except Exception as e:
      DaemonManager.Log.write(
        "Failed to read json file %s: %s" % (metadataFilePath, e)
      )
      return False

    pid = data.get('pid', None)
    cmd = data.get('cmd', None)

    cmd_check = None
    try:
      with open(os.path.join("/", "proc", str(pid), "cmdline")) as f:
        cmd_check = f.read()
    except FileNotFoundError:
      # The process is not running.
      self.delete_daemon_instance_file(metadataFilePath)

    if pid is None or cmd is None or cmd_check is None:
      DaemonManager.Log.error(
        "Cannot find a running daemon on port %s." % (str(port))
      )
      return False

    if cmd != cmd_check:
      DaemonManager.Log.error(
        "Cannot safely kill the daemon process. The process was launched with "
        "an unexpected command and may not be a daemon."
      )
      return False

    DaemonManager.Log.write("Stopping server running on port %s" % (str(port)))

    import _signal
    try:
      DaemonManager.Log.write("Attempting to gracefully stop the daemon...")
      # The daemon will stop accepting new connections and tell client threads
      # to start cleaning up when it receives a SIGINT. We do not send a
      # SIGTERM prior to SIGKILL because that will prevent the daemon from
      # performing graceful cleanup.
      os.kill(pid, _signal.SIGINT)

      # We want to give the daemon a chance to quit gracefully, but only
      # for a controlled amount of time.
      try:
        for attempt in range(0, graceful_exit_timeout_s):
          time.sleep(1)
          # Will throw ProcessLookupError if the process is no longer running.
          os.kill(pid, 0)

        DaemonManager.Log.write("Forcefully stopping the daemon...")

        # Ideally we would never have to do this. However, this fallback to
        # SIGKILL  means that after "occam daemon stop" the user won't have to
        # wait an unknown amount of time before "occam daemon start" will
        # successfully bind on the default port.
        #
        # I believe we can avoid the need for SIGKILL in the future by fixing
        # the daemon behavior when interacting with the frontend. For example,
        # as of 2020-04-14 when the front end is connected to the backend the
        # backend seemingly ignores SIGTERMs.
        #
        # See the discussion in the following threads:
        # https://unix.stackexchange.com/questions/8916/when-should-i-not-kill-9-a-process
        # https://stackoverflow.com/questions/690415/in-what-order-should-i-send-signals-to-gracefully-shutdown-processes
        os.kill(pid, _signal.SIGKILL)

        # Init does not have a hard deadline by which to kill a process. But in
        # practice this is a reasonable amount of time to wait before informing the user
        # the process is possibly a zombie.
        time.sleep(3)
        os.kill(pid, 0)
        DaemonManager.Log.write(
          "Server process %s running on port %s was potentially leaked while "
          "stopping the daemon." % (str(pid), str(port))
        )
      except ProcessLookupError:
        pass

      DaemonManager.Log.write("Server stopped.")

      return self.delete_daemon_instance_file(metadataFilePath)
    except ProcessLookupError:
      DaemonManager.Log.write("Server not actually running. Cleaning up.")
      self.delete_daemon_instance_file(metadataFilePath)

    return False

def daemon(name):
  """ This decorator will register a sub-daemon that should be started.
  """

  def register_daemon(cls):
    DaemonManager.register(name, cls)
    cls = loggable("DaemonManager")(cls)

    def init(self, subConfig):
      self.configuration = subConfig

    cls.__init__ = init

    return cls

  return register_daemon
