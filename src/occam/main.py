#!/usr/bin/env python3

# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys, os

def main(argv=None):
  # Remove the current path
  sys.path.remove(os.path.dirname(__file__))

  # Add the root source path
  occam_src_path = os.path.realpath(os.path.join(os.path.dirname(__file__), ".."))
  sys.path.append(occam_src_path)

  import i18n
  from occam.cli import CLI
  from occam.log import Log

  i18n.set('fallback', 'en')
  i18n.set('enable_memoization', True)

  localePath = os.path.realpath(os.path.join(os.path.dirname(__file__), "..", "..", "locales"))
  i18n.load_path.append(localePath)

  # Import all plugins
  plugin_path = os.path.join(os.path.dirname(__file__), "..", "..", "plugins")
  for path in os.listdir(plugin_path):
    fullpath = os.path.join(plugin_path, path, "src")
    if os.path.isdir(fullpath):
      sys.path.append(fullpath)
    localePath = os.path.join(plugin_path, path, "locales")
    if os.path.isdir(localePath):
      i18n.load_path.append(localePath)

  if argv is None:
    argv = sys.argv

  if len(argv) == 1:
    CLI().usage()
    return 0

  if len(argv) == 2:
    CLI().componentUsage(argv[1])
    return 0

  # The command is the first argument
  # For instance, "occam pull", argv[1] would be 'pull'
  return CLI().execute(argv[1:])

if __name__ == "__main__":
  try:
    exit(main())
  except KeyboardInterrupt as e:
    # 130 is 128+2 (fatal error signal 2)
    exit(130)
